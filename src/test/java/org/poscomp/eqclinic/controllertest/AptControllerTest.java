package org.poscomp.eqclinic.controllertest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.poscomp.eqclinic.controller.AptController;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


@ContextConfiguration(locations = {"classpath:spring-mvc.xml", "classpath:applicationContext.xml",
                "classpath:applicationContext-hibernate.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class AptControllerTest {

    private MockMvc mockMvc;

    @Mock
    private PatientService patientService;

    @Mock
    private DoctorService doctorService;

    @Mock
    private AppointmentService appointmentService;

    @Mock
    private ScenarioService scenarioService;

    @InjectMocks
    AptController aptController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(aptController).build();
    }


    @Test
    public void testSpDeleteAvailable_validJson() throws Exception {
        mockMvc.perform(post("/spDeleteAvailable").param("aptId", "10")).andExpect(status().isOk());
    }



}
