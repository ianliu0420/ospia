package org.poscomp.survey.controller;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Teacher;
import org.poscomp.eqclinic.domain.Video;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.processor.DownloadProcessor;
import org.poscomp.eqclinic.service.interfaces.AppointmentProgressService;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.TeacherService;
import org.poscomp.eqclinic.service.interfaces.VideoService;
import org.poscomp.eqclinic.util.StringTool;
import org.poscomp.eqclinic.ws.AllData;
import org.poscomp.eqclinic.ws.AllDataServiceLocator;
import org.poscomp.eqclinic.ws.Assessment;
import org.poscomp.eqclinic.ws.AssessmentList;
import org.poscomp.eqclinic.ws.AssessorDetails;
import org.poscomp.eqclinic.ws.Criterion;
import org.poscomp.eqclinic.ws.CriterionGrade;
import org.poscomp.eqclinic.ws.SubmitAssessmentResponse;
import org.poscomp.eqclinic.ws.TemplateField;
import org.poscomp.eqclinic.ws.Test;
import org.poscomp.survey.domain.Answer;
import org.poscomp.survey.domain.Question;
import org.poscomp.survey.domain.QuestionType;
import org.poscomp.survey.domain.Survey;
import org.poscomp.survey.service.AnswerService;
import org.poscomp.survey.service.QuestionService;
import org.poscomp.survey.service.QuestionTypeService;
import org.poscomp.survey.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class QuestionController {

    private static final Logger logger = Logger.getLogger(QuestionController.class);

    @Autowired
    private QuestionService questionService;

    @Autowired
    private QuestionTypeService questionTypeService;

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private AnswerService answerService;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private AppointmentProgressService appointmentProgressService;

    @Autowired
    private VideoService videoService;
    
    @Autowired
    private AppointmentService appointmentService;

    private static final String SURVEYPATH = "/survey";

    
    @RequestMapping(value = "submitFailSoca", method = { RequestMethod.GET })
    public void submitFailSoca(HttpServletRequest request, PrintWriter printWriter) {
    	String[] answers = {
      			 "How you felt the interview went for you at the time?",
      			 "The interview was very enjoyable for me, and hopefully, the SP as well. I felt that I had managed to explore the presenting complaint sufficiently and most importantly, the main concern surrounding the patient's complaint. I also felt that I had done a good job making the patient feel at ease and tried to match his personality to make him feel more comfortable.\n\nKeeping in mind the previous campus clinical SOCA, I think I held a fairly good pace of talking, not too fast this time round. Which is especially important because there were some audio/visual problems during the call.\n\nAlso, as Dr George Abouyanni had said in the previous hospital SOCA I did, I made sure to rephrase what the SP had said to me and repeat it back to him so that I know that I am understanding him correctly. Furthermore, while practising with peers, I was reminded to ask more open questions and give more time to patients to talk more and express themselves better. Hence, I was constantly reminding myself throughout the OSPIA that I had to give the patient ample time to express himself freely.\n\nIn Dr George Abouyanni's comments for my hospital SOCA, he told me that when patients say something and if I think it is relevant, I should jump onto it immediately. This is to ensure that the patient knows what I am referring to when asking about it and to prevent the patient from losing the willingness to explore the same topic, but at a later time.",
      			 "How this compares with the grade and comments entered by the assessor?",
      			 "Overall, I had hoped to be awarded more 'P+'s as I reckon I had fulfilled most of the criteria stated in the rubrics. In some areas, I felt that I had satisfied the criteria, but this was not reflected in the SP's thoughts. The SP thought that I could improve on clarifying/summarising at key areas, but I am very sure that I had done so, as stated above. I am unsure what the SP meant when he commented that my non-verbal behaviour was unsatisfactory, but I will remember to take note in the future. However, what I strongly disagree with is that I did not recognise the patient's needs as I had asked, in many different ways, what the concerns surrounding his presenting complaint is and how has it impacted his life. I repeated it back to him as well to ensure that there is a mutual understanding and he had agreed. Perhaps this was not felt enough by the SP. \n\nThis general comments were that he had enjoyed the session and had built a rapport with me, which I agreed with. As for his comments on how I had an approach matching him, it may not be appropriate for a quiet person. This is actually a very good point to raise as it had never occurred to me.  Naturally, I would match my approach and adjust my attitude when dealing with different patients to make them feel more comfortable. The fact that the SP had pointed it out, reiterated the importance of making a patient feel comfortable, even if it requires me to change my demeanour and behaviour.\n\nThe SP remarked that perhaps I should have passed on the case to a specialist. Indeed, I would have chosen to do that if I were a practising doctor. However, as I am a medical student who knows that this is purely for assessment purposes, I neglected to do that. But thinking retrospectively, just by saying that I will pass on the case to a more experienced doctor will definitely put the patient more at ease and be more confident of treatment outcome. Yet another good point raised by the SP.\n\nI mentioned in the pre-OSPIA interview that I would like to work on providing structure, but it still seems to be my weakness. I will continue to work on providing structure throughout more practises with my peers and in hospitals/campus sessions.",
      			 "What this means to you for how you will continue to develop your communication skills?",
      			 "This interview was very helpful as it pointed out many things that I had not thought of in detail previously. Ultimately, doctors have to put patients at ease and to instil confidence in them that they are in the right hands. Hence, I need to work on my reassurance with patients. I also definitely have to work on providing structure and enabling the patients to understand the reasons behind the questions I ask. I can do this by being more explaining more explicitly why I ask certain questions and to reiterate that the questions asked are to better understand the patient and his/her condition."
      	 	};

   	    String socaAnswer = "[{'qid':10,'qvalue':'P'},{'qid':1,'qvalue':'P'},{'qid':13,'qvalue':'An enthusiastic and friendly approach matching that of the simulated patient but perhaps more than a quiet person might like. Also cleverly took the opportunity to follow up on the topics which arose.  In view of the intractable nature of a persistant cough seemingly related to past medical treatment it would have been appropriate to suggest passing on to specialist.  I have not ticked the boxes as failures but more to point out where further emphasis would be appropirate. Definately built up a relationship with the patient and I enjoyed the compliments !  Best wishes with continuing studies.'},{'qid':2,'qvalue':'B'},{'qid':41,'qvalue':'1'},{'qid':4,'qvalue':'P'},{'qid':7,'qvalue':'P'},{'qid':5,'qvalue':'D'},{'qid':11,'qvalue':'D'},{'qid':8,'qvalue':'B'}]";
   	    String result = submitToEmedOSPIA(socaAnswer, "z5090512", "Claire Yinn", "Lim", answers);
   	    System.out.println(result);
      			 
          printWriter.write("submit success");
          printWriter.flush();
          printWriter.close();
    }
    
    
    
    @RequestMapping(value = SURVEYPATH + "/savequestion", method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
    @ResponseBody
    public ReturnValue saveQuestion(HttpServletRequest request, @RequestBody Question question) {
        ReturnValue rv = new ReturnValue();
        if (question != null) {
            questionService.save(question);
            rv.setCode(1);
        } else {
            rv.setCode(0);
        }
        return rv;
    }

    @RequestMapping(value = SURVEYPATH + "/addquestion", method = { RequestMethod.GET })
    public String addQuestion(HttpServletRequest request) {
        List<QuestionType> questionTypes = questionTypeService.findAll();
        request.setAttribute("questionTypes", questionTypes);
        request.setAttribute("redpage", "uhome_survey");
        return "survey/addquestion";
    }

    @RequestMapping(value = SURVEYPATH + "/editquestion/{questionId}", method = { RequestMethod.GET })
    public void editQuestion(HttpServletRequest request, PrintWriter printWriter, @PathVariable int questionId) {
        // here we use JSON library to convert the object to prevent the
        // deadlock
        Question question = questionService.findById(questionId);
        JsonConfig config = new JsonConfig();
        config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
        JSONObject jsonArray = JSONObject.fromObject(question, config);
        printWriter.write(jsonArray.toString());
        printWriter.flush();
        printWriter.close();
    }

    @RequestMapping(value = SURVEYPATH + "/editque/{questionId}", method = { RequestMethod.GET })
    public String editQue(HttpServletRequest request, PrintWriter printWriter, @PathVariable int questionId) {
        List<QuestionType> questionTypes = questionTypeService.findAll();
        request.setAttribute("questionTypes", questionTypes);
        request.setAttribute("questionId", questionId);
        request.setAttribute("redpage", "uhome_survey");
        return "survey/editquestion";
    }

    @RequestMapping(value = SURVEYPATH + "/listquestion", method = { RequestMethod.GET })
    public String listQuestion(HttpServletRequest request) {
        List<Question> questions = questionService.findAll(0, 0);
        request.setAttribute("questions", questions);
        request.setAttribute("redpage", "uhome_survey");
        return "survey/listquestion";
    }

    @RequestMapping(value = SURVEYPATH + "/deletequestion/{questionId}", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue deleteQuestion(HttpServletRequest request, @PathVariable int questionId) {

        Question question = questionService.findById(questionId);
        int relatedSurveyNo = question.getSurveys().size();

        ReturnValue rv = new ReturnValue();
        if (relatedSurveyNo != 0) {
            rv.setCode(0);
            rv.setContent("Some survey is using this question. Pelase edit these survey first.");
        } else {
            rv.setCode(1);
            questionService.delete(question);
        }
        return rv;
    }

    @RequestMapping(value = SURVEYPATH + "/addsurvey", method = { RequestMethod.GET })
    public String addSurvey(HttpServletRequest request) {
        List<Question> questions = questionService.findAll(0, 0);
        request.setAttribute("questions", questions);
        request.setAttribute("redpage", "uhome_survey");
        return "survey/addsurvey";
    }

    @RequestMapping(value = SURVEYPATH + "/savesurvey", method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
    @ResponseBody
    public ReturnValue saveSurvey(HttpServletRequest request, @RequestBody Survey survey) {
        ReturnValue rv = new ReturnValue();
        if (survey != null) {
            surveyService.save(survey, 1);
            rv.setCode(1);
        } else {
            rv.setCode(0);
        }
        return rv;
    }

    @RequestMapping(value = SURVEYPATH + "/editsurvey/{content}", method = { RequestMethod.GET })
    public String editSurvey(HttpServletRequest request, @PathVariable String content) {

        String[] contents = content.split("&");
        int surveyId = Integer.parseInt(contents[0]);

        Survey survey = surveyService.findById(surveyId);
        String orderOfQuestion = survey.getOrderOfQuestions();
        String[] idsStr = orderOfQuestion.split(",");
        List<Integer> quesitonIds = new ArrayList<Integer>();

        for (String idStr : idsStr) {
            if (!"".equals(idStr)) {
                quesitonIds.add(Integer.parseInt(idStr));
            }
        }
        List<Question> allQuestions = questionService.findAll(0, 0);
        List<Question> availableQuestions = new ArrayList<Question>();

        List<Question> myQuestions = new ArrayList<Question>();
        if (quesitonIds.size() > 0) {
            myQuestions = questionService.findByIds(quesitonIds);
        }
        request.setAttribute("myquestions", myQuestions);

        if (contents.length == 2) {
            String searchContent = contents[1];
            availableQuestions = questionService.findByContent(searchContent, orderOfQuestion);
            request.setAttribute("searchContent", searchContent);
        } else {
            for (Question question : allQuestions) {
                Integer questioId = question.getId();
                if (!quesitonIds.contains(questioId)) {
                    availableQuestions.add(question);
                }
            }
        }

        request.setAttribute("questions", availableQuestions);
        request.setAttribute("survey", survey);
        request.setAttribute("redpage", "uhome_survey");
        return "survey/editsurvey";

    }

    @RequestMapping(value = SURVEYPATH + "/addquestiontosurvey", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue addQuestionToSurvey(HttpServletRequest request) {

        String input = request.getParameter("input");
        String[] idsStr = input.split(";");
        int surveyId = Integer.parseInt(idsStr[0]);
        int questionId = Integer.parseInt(idsStr[1]);
        surveyService.addQuestion(surveyId, questionId);
        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        return rv;
    }

    @RequestMapping(value = SURVEYPATH + "/removequestiontosurvey", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue removeQuestionToSurvey(HttpServletRequest request) {

        String input = request.getParameter("input");
        String[] idsStr = input.split(";");
        int surveyId = Integer.parseInt(idsStr[0]);
        int questionId = Integer.parseInt(idsStr[1]);
        surveyService.removeQuestion(surveyId, questionId);
        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        return rv;
    }

    @RequestMapping(value = SURVEYPATH + "/listsurvey", method = { RequestMethod.GET })
    public String listSurvey(HttpServletRequest request) {
        List<Survey> surveys = surveyService.findAll(0, 0);
        request.setAttribute("surveys", surveys);
        request.setAttribute("redpage", "uhome_survey");
        return "survey/listsurvey";
    }

    @RequestMapping(value = SURVEYPATH + "/deletesurvey/{surveyId}", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue deleteSurvey(HttpServletRequest request, @PathVariable int surveyId) {

        Survey survey = surveyService.findById(surveyId);
        surveyService.delete(survey);
        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        return rv;
    }

    @RequestMapping(value = SURVEYPATH + "/answersurvey/{surveyId}", method = { RequestMethod.GET })
    public String answerSurvey(HttpServletRequest request, @PathVariable int surveyId) {
        request.setAttribute("surveyId", surveyId);
        request.setAttribute("ispreview", 0);
        request.setAttribute("redpage", "uhome_survey");
        return "survey/preview";
    }

    @RequestMapping(value = SURVEYPATH + "/preview/{surveyId}", method = { RequestMethod.GET })
    public String preview(HttpServletRequest request, @PathVariable int surveyId) {
        request.setAttribute("surveyId", surveyId);
        request.setAttribute("ispreview", 1);
        request.setAttribute("redpage", "uhome_survey");
        return "survey/preview";
    }

    @RequestMapping(value = SURVEYPATH + "/previewsurvey/{surveyId}", method = { RequestMethod.GET })
    public void previewSurvey(HttpServletRequest request, PrintWriter printWriter, @PathVariable int surveyId) {
        // here we use JSON library to convert the object to prevent the
        // deadlock
        Survey survey = surveyService.findById(surveyId);
        JsonConfig config = new JsonConfig();
        config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
        JSONObject jsonArray = JSONObject.fromObject(survey, config);
        printWriter.write(jsonArray.toString());
        printWriter.flush();
        printWriter.close();

    }

    @RequestMapping(value = SURVEYPATH + "/saveanswer", method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
    @ResponseBody
    public ReturnValue saveAnswer(HttpServletRequest request, @RequestBody Answer answer) {

        int surveyId = answer.getSurvey().getId();

        String step = "";
        if (surveyId == 1) {
            step = "patientSoca";
        } else if (surveyId == 2) {
            step = "patientSystemFeedback";
        } else if (surveyId == 12) {
            step = "patientAutonomy";
        } else if (surveyId == 4) {
            step = "doctorPerRefFirst";
        } else if (surveyId == 5) {
            step = "doctorEvaluatePatient";
        } else if (surveyId == 7) {
            step = "doctorSystemFeedback";
        } else if (surveyId == 6) {
            step = "doctorPerRefSecond";
        } else if (surveyId == 9) {
            step = "doctorSelfSoca";
        }else if (surveyId == 13) {
            step = "doctorAutonomy";
        }

        // check the user
        Doctor doctor = answer.getDoctor();
        Patient patient = answer.getPatient();
        Teacher teacher = answer.getTeacher();
        String sessionId = "";
        String userType = "";
        int userId = 0;
        if (doctor != null) {
            int doctorId = doctor.getDoctorId();
            doctor = doctorService.getDoctorByDoctorId(doctorId);
            answer.setDoctor(doctor);
            answer.setPatient(null);
            answer.setTeacher(null);
            sessionId = (String) request.getSession().getAttribute("doctorSessionIdCombined");
            userType = "doctor";
            userId = doctorId;

            if(surveyId == 6){
                Answer answerByPatient = answerService.loadAnswerBySessionIdUserId(sessionId, "patient", 1);
                String  receiptNum = answerByPatient.getReceiptNo();
                Patient submittedPatient = answerByPatient.getPatient();
                
                
                JSONArray answerArray;
                String[] answers = new String[6];
                try {
                    answerArray = new JSONArray(answer.getContent());
                    for (int j = 0; j < answerArray.length(); j++) {
                        org.json.JSONObject temp = (org.json.JSONObject) answerArray.get(j);
                        if (temp.getInt("qid") == 64) {
                            answers[0] = "How you felt the interview went for you at the time?";
                            answers[1] = temp.getString("qvalue");
                        }else if (temp.getInt("qid") == 65) {
                            answers[2] = "How this compares with the grade and comments entered by the assessor?";
                            answers[3] = temp.getString("qvalue");
                        }else if (temp.getInt("qid") == 66) {
                            answers[4] = "What this means to you for how you will continue to develop your communication skills?";
                            answers[5] = temp.getString("qvalue");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                
                Appointment appointment = appointmentService.getAppointmentBySessionId(sessionId);
                
                if(appointment.getSessionType()==2){
                	String receiptNo = submitToEmedOSPIA(answerByPatient.getContent(), doctor.getUserId(), submittedPatient.getFirstname(), submittedPatient.getLastname(), answers);
                	answer.setReceiptNo(receiptNo);
//                update(doctor.getUserId(), doctor.getPhase()+"", receiptNum,answers);
                }
                
                
            }
        } else if (patient != null) {
            int patientId = patient.getPatientid();
            patient = patientService.getPatientByPatientId(patientId);
            answer.setPatient(patient);
            answer.setDoctor(null);
            answer.setTeacher(null);
            sessionId = (String) request.getSession().getAttribute("patientSessionIdCombined");
            userType = "patient";
            userId = patientId;

            if (surveyId == 1) {
                
                Appointment appointment = appointmentService.getAppointmentBySessionId(sessionId);

                int doctorIdNumber = appointment.getDoctorId();
                int patientIdNumber = appointment.getPatientId();

                Doctor doctorForEmed = doctorService.getDoctorByDoctorId(doctorIdNumber);
                Patient patientForEmed = patientService.getPatientByPatientId(patientIdNumber);

                // check whether this interaction is a assessment
                int sessionType = appointment.getSessionType();
                
                if(sessionType==2){
                    
                    System.out.println("update receip number from SOCA");
                    
                    // submit the answer UNSW eMed
//                    String result = submitToEmed(answer.getContent(), doctorForEmed.getUserId(), patientForEmed.getFirstname(),
//                            patientForEmed.getLastname());
                    String result = null;
                    
                    if (result == null) {
                        Survey survey = surveyService.findById(surveyId);
                        answer.setSurvey(survey);
                        answer.setCreateAt(System.currentTimeMillis());
                        answerService.save(answer);
                        
                        if (!"".equals(step) && answer.getTeacher() == null) {
                            appointmentProgressService.updateAppointmentProgressState(sessionId, userType, userId, step);
                        }
                        
                        ReturnValue rv = new ReturnValue();
                        rv.setCode(1);
                        return rv;
                    }
//                    else{
//                        answer.setReceiptNo(result);
//                    }
                }
                
            }
            
        } else if (teacher != null) {
            int teacherId = teacher.getTeacherId();
            teacher = teacherService.getTeacherByTeacherId(teacherId);
            answer.setTeacher(teacher);
            answer.setPatient(null);
            answer.setDoctor(null);
            sessionId = (String) request.getSession().getAttribute("patientSessionIdCombined");

            if (surveyId == 1) {
                Appointment appointment = appointmentService.getAppointmentBySessionId(answer.getSessionId());
                int doctorIdNumber = appointment.getDoctorId();

                Doctor doctorForEmed = doctorService.getDoctorByDoctorId(doctorIdNumber);
                // submit the answer UNSW eMed
                String result = submitToEmed(answer.getContent(), doctorForEmed.getUserId(), teacher.getFirstname(), teacher.getLastname());

                if (result == null) {
                	Survey survey = surveyService.findById(surveyId);
                    answer.setSurvey(survey);
                    answer.setCreateAt(System.currentTimeMillis());
                    answerService.save(answer);
                	
                    ReturnValue rv = new ReturnValue();
                    rv.setCode(1);
                    return rv;
                }else{
                    answer.setReceiptNo(result);
                }
            }

        }

        if (!"".equals(step) && answer.getTeacher() == null) {
            int result = appointmentProgressService.updateAppointmentProgressState(sessionId, userType, userId, step);
        }
        Survey survey = surveyService.findById(surveyId);
        answer.setSurvey(survey);
        answer.setCreateAt(System.currentTimeMillis());
        answerService.save(answer);

        if (surveyId == 3) {
            Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
            apt.setFinishedPreSurvey(1);
            appointmentService.saveApp(apt);
        }

        if(surveyId == 11){
            // submit the final survey, update video table 
            String tempSessionId = answer.getSessionId();
            List<Video> videos = videoService.getVideoBySessionId(tempSessionId);
            if(videos!=null && videos.size()>0){
                Video lastVideo = videos.get(videos.size()-1);
                lastVideo.setReviewSurvey(1);
                videoService.saveVideo(lastVideo);
            }
        }
        
        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        return rv;
    }

    @RequestMapping(value = SURVEYPATH + "/loadanswer/{answerId}", method = { RequestMethod.GET })
    public void loadAnswer(HttpServletRequest request, PrintWriter printWriter, @PathVariable int answerId) {
        Answer answer = answerService.loadAnswerById(answerId);
        JsonConfig config = new JsonConfig();
        config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
        JSONObject jsonArray = JSONObject.fromObject(answer, config);
        printWriter.write(jsonArray.toString());
        printWriter.flush();
        printWriter.close();
    }
    
    public String submitToEmedOSPIA(String myAnswer, String studentId, String patientFirstName, String patientLastName, String[] reflectionAnswers ) {
        
        try {
        	
        	myAnswer = StringTool.filterString(myAnswer);
        	
            JSONArray answerArray = new JSONArray(myAnswer);

            Survey survey = surveyService.findById(1);
            Set<Question> questions = survey.getQuestions();

            String orderOfQuestion = survey.getOrderOfQuestions();

            String[] orders = orderOfQuestion.split(",");

            CriterionGrade[] criterionGradeList = new CriterionGrade[6];

            for (int i = 0; i < orders.length - 1; i = i + 2) {
                int quesionId = Integer.parseInt(orders[i]);
                int quesionId_next = Integer.parseInt(orders[i + 1]);

                org.json.JSONObject answer = null;
                org.json.JSONObject answer_next = null;

                for (int j = 0; j < answerArray.length(); j++) {
                    org.json.JSONObject temp = (org.json.JSONObject) answerArray.get(j);
                    if (temp.getInt("qid") == quesionId) {
                        answer = temp;
                    }
                }

                for (int j = 0; j < answerArray.length(); j++) {
                    org.json.JSONObject temp = (org.json.JSONObject) answerArray.get(j);
                    if (temp.getInt("qid") == quesionId_next) {
                        answer_next = temp;
                    }
                }

                CriterionGrade grade = new CriterionGrade();
                grade.setGrade(answer.getString("qvalue"));
                String reasons[] = { answer_next.getString("qvalue") };
                grade.setGradeReason(reasons);

                if (i / 2 == 4) {
                    criterionGradeList[i / 2] = grade;

                    String[] grades = answer.getString("qvalue").split(",");
                    String feedbackReasons = "";
                    if(grades.length>0){
                        feedbackReasons = "You need to focus on following skills: ";
                    }
                    
                    for (int j = 0; j < grades.length; j++) {

                        if (j==0) {
                            if ("1".equals(grades[j])) {
                                feedbackReasons = "Provide structure";
                            } else if ("2".equals(grades[j])) {
                                feedbackReasons = "Gather information";
                            } else if ("3".equals(grades[j])) {
                                feedbackReasons = "Build relationships & developing rapport";
                            } else if ("4".equals(grades[j])) {
                                feedbackReasons = "Ensure a shared understanding of patient's needs and perspective/impact of problem";
                            }
                        } else {
                            if ("1".equals(grades[j])) {
                                feedbackReasons += "; Provide structure";
                            } else if ("2".equals(grades[j])) {
                                feedbackReasons += "; Gather information";
                            } else if ("3".equals(grades[j])) {
                                feedbackReasons += "; Build relationships & developing rapport";
                            } else if ("4".equals(grades[j])) {
                                feedbackReasons += "; Ensure a shared understanding of patient's needs and perspective/impact of problem";
                            }
                        }

                    }
                    grade.setGrade(feedbackReasons);

                } else {
                    criterionGradeList[i / 2] = grade;
                }

            }

            Locale locale = Locale.US; 
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", locale);
            String currentDate = sdf.format(new Date());
            System.out.println(currentDate);
            for (int j = 0; j < criterionGradeList.length-1; j++) {
                CriterionGrade grade = criterionGradeList[j];
                String reasons = grade.getGradeReason()[0];
                
                String[] newReason = null;
                if(j==4){
                    newReason = grade.getGradeReason();
                }else{
                    newReason = parseReason(j, reasons);
                }
                grade.setGradeReason(newReason);
                grade.setCriterionNum((j + 1) + "");
                grade.setDateCreated(currentDate);
                grade.setDateModified(currentDate);
                TemplateField tf = new TemplateField();
                if(j==0){
                	tf.setValue("Provide structure");
                }else if(j==1){
                	tf.setValue("Gather information");
                }else if(j==2){
                	tf.setValue("Build relationships & developing rapport");
                }else if(j==3){
                	tf.setValue("Ensure a shared understanding of patient's needs and perspective/impact of problem");
                }else if(j==4){
                	tf.setValue("Overall feedback");
                }
                grade.setTitle(tf);
            }
            
            CriterionGrade grade_reflection = new CriterionGrade();
            grade_reflection.setGrade(" ");
            grade_reflection.setGradeReason(reflectionAnswers);
            criterionGradeList[5] = grade_reflection;
            criterionGradeList[5].setCriterionNum("6");
            TemplateField tf = new TemplateField();
            tf.setValue("Reflection answer");
            grade_reflection.setTitle(tf);
            
            AllDataServiceLocator serviceLocator = new AllDataServiceLocator();

            try {
                AllData service = serviceLocator.getDomino();
                // to use Basic HTTP Authentication:
                ((Stub) service)._setProperty(Call.USERNAME_PROPERTY, "WebServiceOSPIA");
                ((Stub) service)._setProperty(Call.PASSWORD_PROPERTY, "WebServiceOSPIA");

                Assessment assessment = new Assessment();
                // set the student Id
                assessment.setStudentId(studentId);

                // set assessor Id (SP name, or tutor name)
                assessment.setAssessorId("");
                AssessorDetails assessorDetails = new AssessorDetails();

                TemplateField assessorName = new TemplateField();
                assessorName.setLabel(patientFirstName);
                assessorName.setValue(patientLastName);
                assessorDetails.setNameOfAssessor(assessorName);
                assessment.setAssessorDetails(assessorDetails);
                // set the status
                assessment.setStatus("Completed");

                // set the receipt number
                assessment.setReceiptNumber(null);
                // OSPIA, SOCA
                assessment.setAssessmentType("OSPIA");
                
                // set submission date
                System.out.println(currentDate);
                assessment.setSubmissionDate(currentDate);

                // set date modified
                assessment.setDateStatusModified(currentDate);

                assessment.setCriterionList(createCrition());
                assessment.setGradedCriterionList(criterionGradeList);

                assessment.setAssessmentTitle("Patient-student communication assessment");

                SubmitAssessmentResponse response = service.submitAssessment(assessment);

                System.out.println(response.getMessage());
                System.out.println(response.getMissingCount());
                System.out.println(response.getReceiptNumber());
                
                logger.info(response.getMessage());
                logger.info(response.getMissingCount());
                logger.info(response.getReceiptNumber());

                return response.getReceiptNumber();

            } catch (Exception e) {
            	
            	e.printStackTrace();
                logger.error(e.getMessage(), e);
            	
            	try{
            		AllData service = serviceLocator.getDomino();
                    // to use Basic HTTP Authentication:
                    ((Stub) service)._setProperty(Call.USERNAME_PROPERTY, "WebServiceOSPIA");
                    ((Stub) service)._setProperty(Call.PASSWORD_PROPERTY, "WebServiceOSPIA");

                    Assessment assessment = new Assessment();
                    // set the student Id
                    assessment.setStudentId(studentId);

                    // set assessor Id (SP name, or tutor name)
                    assessment.setAssessorId("");
                    AssessorDetails assessorDetails = new AssessorDetails();

                    TemplateField assessorName = new TemplateField();
                    assessorName.setLabel(patientFirstName);
                    assessorName.setValue(patientLastName);
                    assessorDetails.setNameOfAssessor(assessorName);
                    assessment.setAssessorDetails(assessorDetails);
                    // set the status
                    assessment.setStatus("Completed");

                    // set the receipt number
                    assessment.setReceiptNumber(null);
                    // OSPIA, SOCA
                    assessment.setAssessmentType("OSPIA");
                    
                    // set submission date
                    System.out.println(currentDate);
                    assessment.setSubmissionDate(currentDate);

                    // set date modified
                    assessment.setDateStatusModified(currentDate);

                    assessment.setCriterionList(createCrition());
                    assessment.setGradedCriterionList(criterionGradeList);

                    assessment.setAssessmentTitle("Patient-student communication assessment");

                    SubmitAssessmentResponse response = service.submitAssessment(assessment);

                    System.out.println(response.getMessage());
                    System.out.println(response.getMissingCount());
                    System.out.println(response.getReceiptNumber());
                    
                    logger.info(response.getMessage());
                    logger.info(response.getMissingCount());
                    logger.info(response.getReceiptNumber());

                    return response.getReceiptNumber();
            	} catch (Exception e1){
            		e1.printStackTrace();
                    return "";
            	}
            }

        } catch (JSONException e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
            return "";
        }
    }
    
//     @RequestMapping(value = "/testws", method = {RequestMethod.GET})
//     public String testws(HttpServletRequest request) {
    // SOCAType: 1: OSPIA 2: SOCA
    public String submitToEmed(String myAnswer, String studentId, String patientFirstName, String patientLastName ) {
        
        try {
            JSONArray answerArray = new JSONArray(myAnswer);

            Survey survey = surveyService.findById(1);
            Set<Question> questions = survey.getQuestions();

            String orderOfQuestion = survey.getOrderOfQuestions();

            String[] orders = orderOfQuestion.split(",");

            CriterionGrade[] criterionGradeList = new CriterionGrade[6];

            for (int i = 0; i < orders.length - 1; i = i + 2) {
                int quesionId = Integer.parseInt(orders[i]);
                int quesionId_next = Integer.parseInt(orders[i + 1]);

                org.json.JSONObject answer = null;
                org.json.JSONObject answer_next = null;

                for (int j = 0; j < answerArray.length(); j++) {
                    org.json.JSONObject temp = (org.json.JSONObject) answerArray.get(j);
                    if (temp.getInt("qid") == quesionId) {
                        answer = temp;
                    }
                }

                for (int j = 0; j < answerArray.length(); j++) {
                    org.json.JSONObject temp = (org.json.JSONObject) answerArray.get(j);
                    if (temp.getInt("qid") == quesionId_next) {
                        answer_next = temp;
                    }
                }

                CriterionGrade grade = new CriterionGrade();
                grade.setGrade(answer.getString("qvalue"));
                String reasons[] = { answer_next.getString("qvalue") };
                grade.setGradeReason(reasons);

                if (i / 2 == 4) {
                    criterionGradeList[i / 2] = grade;

                    String[] grades = answer.getString("qvalue").split(",");
                    String feedbackReasons = "";
                    if(grades.length>0){
                        feedbackReasons = "You need to focus on following skills: ";
                    }
                    
                    for (int j = 0; j < grades.length; j++) {

                        if (j==0) {
                            if ("1".equals(grades[j])) {
                                feedbackReasons = "Provide structure";
                            } else if ("2".equals(grades[j])) {
                                feedbackReasons = "Gather information";
                            } else if ("3".equals(grades[j])) {
                                feedbackReasons = "Build relationships & developing rapport";
                            } else if ("4".equals(grades[j])) {
                                feedbackReasons = "Ensure a shared understanding of patient's needs and perspective/impact of problem";
                            }
                        } else {
                            if ("1".equals(grades[j])) {
                                feedbackReasons += "; Provide structure";
                            } else if ("2".equals(grades[j])) {
                                feedbackReasons += "; Gather information";
                            } else if ("3".equals(grades[j])) {
                                feedbackReasons += "; Build relationships & developing rapport";
                            } else if ("4".equals(grades[j])) {
                                feedbackReasons += "; Ensure a shared understanding of patient's needs and perspective/impact of problem";
                            }
                        }

                    }
                    grade.setGrade(feedbackReasons);

                } else {
                    criterionGradeList[i / 2] = grade;
                }

            }

            Locale locale = Locale.US; 
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", locale);
            String currentDate = sdf.format(new Date());
            System.out.println(currentDate);
            for (int j = 0; j < criterionGradeList.length-1; j++) {
                CriterionGrade grade = criterionGradeList[j];
                String reasons = grade.getGradeReason()[0];
                
                String[] newReason = null;
                if(j==4){
                    newReason = grade.getGradeReason();
                }else{
                    newReason = parseReason(j, reasons);
                }
                grade.setGradeReason(newReason);
                grade.setCriterionNum((j + 1) + "");
                grade.setDateCreated(currentDate);
                grade.setDateModified(currentDate);
                TemplateField tf = new TemplateField();
                if(j==0){
                	tf.setValue("Provide structure");
                }else if(j==1){
                	tf.setValue("Gather information");
                }else if(j==2){
                	tf.setValue("Build relationships & developing rapport");
                }else if(j==3){
                	tf.setValue("Ensure a shared understanding of patient's needs and perspective/impact of problem");
                }else if(j==4){
                	tf.setValue("Overall feedback");
                }
                grade.setTitle(tf);
            }
            
            CriterionGrade grade_reflection = new CriterionGrade();
            grade_reflection.setGrade("No Reflection");
            String reflection[]  = {};
            grade_reflection.setGradeReason(reflection);
            criterionGradeList[5] = grade_reflection;
            criterionGradeList[5].setCriterionNum("6");
            TemplateField tf = new TemplateField();
            tf.setValue("Reflection answer");
            grade_reflection.setTitle(tf);
            
            
            AllDataServiceLocator serviceLocator = new AllDataServiceLocator();

            try {
                AllData service = serviceLocator.getDomino();
                // to use Basic HTTP Authentication:
                ((Stub) service)._setProperty(Call.USERNAME_PROPERTY, "WebServiceOSPIA");
                ((Stub) service)._setProperty(Call.PASSWORD_PROPERTY, "WebServiceOSPIA");

                Assessment assessment = new Assessment();
                // set the student Id
                assessment.setStudentId(studentId);

                // set assessor Id (SP name, or tutor name)
                assessment.setAssessorId("");
                AssessorDetails assessorDetails = new AssessorDetails();

                TemplateField assessorName = new TemplateField();
                assessorName.setLabel(patientFirstName);
                assessorName.setValue(patientLastName);
                assessorDetails.setNameOfAssessor(assessorName);
                assessment.setAssessorDetails(assessorDetails);
                // set the status
                assessment.setStatus("Completed");

                // set the receipt number
                assessment.setReceiptNumber(null);
                // OSPIA, SOCA
                assessment.setAssessmentType("SOCA");
                
                // set submission date
                System.out.println(currentDate);
                assessment.setSubmissionDate(currentDate);

                // set date modified
                assessment.setDateStatusModified(currentDate);

                assessment.setCriterionList(createCrition());
                assessment.setGradedCriterionList(criterionGradeList);

                assessment.setAssessmentTitle("Patient-student communication assessment");

                SubmitAssessmentResponse response = service.submitAssessment(assessment);

                System.out.println(response.getMessage());
                System.out.println(response.getMissingCount());
                System.out.println(response.getReceiptNumber());
                
                logger.info(response.getMessage());
                logger.info(response.getMissingCount());
                logger.info(response.getReceiptNumber());

                return response.getReceiptNumber();

            } catch (Exception e) {
               
            	e.printStackTrace();
                logger.error(e.getMessage(), e);
            	
                try{
                	AllData service = serviceLocator.getDomino();
                    // to use Basic HTTP Authentication:
                    ((Stub) service)._setProperty(Call.USERNAME_PROPERTY, "WebServiceOSPIA");
                    ((Stub) service)._setProperty(Call.PASSWORD_PROPERTY, "WebServiceOSPIA");

                    Assessment assessment = new Assessment();
                    // set the student Id
                    assessment.setStudentId(studentId);

                    // set assessor Id (SP name, or tutor name)
                    assessment.setAssessorId("");
                    AssessorDetails assessorDetails = new AssessorDetails();

                    TemplateField assessorName = new TemplateField();
                    assessorName.setLabel(patientFirstName);
                    assessorName.setValue(patientLastName);
                    assessorDetails.setNameOfAssessor(assessorName);
                    assessment.setAssessorDetails(assessorDetails);
                    // set the status
                    assessment.setStatus("Completed");

                    // set the receipt number
                    assessment.setReceiptNumber(null);
                    // OSPIA, SOCA
                    assessment.setAssessmentType("SOCA");
                    
                    // set submission date
                    System.out.println(currentDate);
                    assessment.setSubmissionDate(currentDate);

                    // set date modified
                    assessment.setDateStatusModified(currentDate);

                    assessment.setCriterionList(createCrition());
                    assessment.setGradedCriterionList(criterionGradeList);

                    assessment.setAssessmentTitle("Patient-student communication assessment");

                    SubmitAssessmentResponse response = service.submitAssessment(assessment);

                    System.out.println(response.getMessage());
                    System.out.println(response.getMissingCount());
                    System.out.println(response.getReceiptNumber());
                    
                    logger.info(response.getMessage());
                    logger.info(response.getMissingCount());
                    logger.info(response.getReceiptNumber());

                    return response.getReceiptNumber();
                }catch(Exception e1){
                	 e1.printStackTrace();
                	 return "";
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }

        return "";

    }

    public String[] parseReason(int questionId, String oReasons) {
        ArrayList<String> reasons = new ArrayList<String>();
        ArrayList<String> reasons_result = new ArrayList<String>();
        if (questionId == 0) {
            reasons.add("A. Initiates the session appropriately with introductions, defining of the purpose and agenda");
            reasons.add("B. Clarifies and summarises at key points during the interview");
            reasons.add("C. Uses transitions and signposting");
            reasons.add("D. Manages time effectively");
            reasons.add("E. Closes the session appropriately with a plan and/or summary");
        } else if (questionId == 1) {
            reasons.add("A. Encourages the patient to tell their story in their own words");
            reasons.add("B. Explores the patient's problems and perspectives (beliefs, worries, feelings, goals)");
            reasons.add("C. Uses open questions initially, listens attentively, and then synthesizes closed questions as appropriate");
            reasons.add("D. Facilitates patient's responses using encouragement, pause/silence, repetition, paraphrasing, interpretation - with limited interruptions");
            reasons.add("E. Avoids using jargon and requests clarification and further information where needed");
        } else if (questionId == 2) {
            reasons.add("A. Picks up and acknowledges patient's non-verbal behaviour (e.g. body language, speech, facial expressions, affect)");
            reasons.add("B. Demonstrates respectful, encouraging and non-controlling non-verbal behavior (eye contact, facial expressions, posture, position, movement) and vocal rate, volume and tone");
            reasons.add("C. Acknowledges patient′s perspective and efforts to cope and is non-judgemental");
            reasons.add("D. Handles uncomfortable topics sensitively");
            reasons.add("E. Involves the patient, and shares own thinking as appropriate - ideas, thought processes, dilemmas");
        } else if (questionId == 3) {
            reasons.add("A. Explores impacts, concerns and expectations");
            reasons.add("B. Relates subsequent questioning and explanations to previously elicited ideas, concerns or expectations");
            reasons.add("C. Checks student's interpretation of information with the patient - clarifying and asking for any corrections or questions");
            reasons.add("D. Recognises and prioritises patient′s needs during interview");
        }

        if (oReasons == null || "".equals(oReasons)) {
            return null;
        }

        String[] str = oReasons.split(",");
        for (int i = 0; i < str.length; i++) {
            for (int j = 0; j < reasons.size(); j++) {
                if (reasons.get(j).startsWith(str[i])) {
                    reasons_result.add(reasons.get(j));
                }
            }
        }

        String[] result = new String[reasons_result.size()];

        for (int i = 0; i < reasons_result.size(); i++) {
            result[i] = reasons_result.get(i);
        }

        return result;

    }

    public Criterion[] createCrition() {

        CriterionGrade grade_f = new CriterionGrade();
        grade_f.setGrade("F");
        CriterionGrade grade_pm = new CriterionGrade();
        grade_f.setGrade("P-");
        CriterionGrade grade_p = new CriterionGrade();
        grade_f.setGrade("P");
        CriterionGrade grade_pp = new CriterionGrade();
        grade_f.setGrade("P+");
        CriterionGrade[] totalGradeList = { grade_f, grade_pm, grade_p, grade_pp };

        CriterionGrade focus_1 = new CriterionGrade();
        focus_1.setGrade("Provide structure");
        CriterionGrade focus_2 = new CriterionGrade();
        focus_2.setGrade("Gather information");
        CriterionGrade focus_3 = new CriterionGrade();
        focus_3.setGrade("Build relationships & developing rapport");
        CriterionGrade focus_4 = new CriterionGrade();
        focus_4.setGrade("Ensure a shared understanding of patient's needs and perspective/impact of problem");
        CriterionGrade[] overallGrades = { focus_1, focus_2, focus_3, focus_4 };

        
        String question1= "How you felt the interview went for you at the time?";
        String question2= "How this compares with the grade and comments entered by the assessor?";
        String question3= "What this means to you for how you will continue to develop your communication skills?";
        
        CriterionGrade ref_1 = new CriterionGrade();
        ref_1.setGrade(question1);
        CriterionGrade ref_2 = new CriterionGrade();
        ref_2.setGrade(question2);
        CriterionGrade ref_3 = new CriterionGrade();
        ref_3.setGrade(question3);
        CriterionGrade[] reflectionGrades = { ref_1, ref_2, ref_3};
        
        Criterion cris_1 = new Criterion();
        cris_1.setCriterionNum("1");
        TemplateField title_1 = new TemplateField();
        title_1.setLabel("Provide structure");
        title_1.setValue("Provide structure");
        cris_1.setTitle(title_1);
        cris_1.setDescription(title_1);
        cris_1.setCriterionGradeList(totalGradeList);

        Criterion cris_2 = new Criterion();
        cris_2.setCriterionNum("2");
        TemplateField title_2 = new TemplateField();
        title_2.setLabel("Gather information");
        title_2.setValue("Gather information");
        cris_2.setTitle(title_2);
        cris_2.setDescription(title_2);
        cris_2.setCriterionGradeList(totalGradeList);

        Criterion cris_3 = new Criterion();
        cris_3.setCriterionNum("3");
        TemplateField title_3 = new TemplateField();
        title_3.setLabel("Build relationships & developing rapport");
        title_3.setValue("Build relationships & developing rapport");
        cris_3.setTitle(title_3);
        cris_3.setDescription(title_3);
        cris_3.setCriterionGradeList(totalGradeList);

        Criterion cris_4 = new Criterion();
        cris_4.setCriterionNum("4");
        TemplateField title_4 = new TemplateField();
        title_4.setLabel("Ensure a shared understanding of patient's needs and perspective/impact of problem");
        title_4.setValue("Ensure a shared understanding of patient's needs and perspective/impact of problem");
        cris_4.setTitle(title_4);
        cris_4.setDescription(title_4);
        cris_4.setCriterionGradeList(totalGradeList);

        Criterion cris_5 = new Criterion();
        cris_5.setCriterionNum("5");
        TemplateField title_5 = new TemplateField();
        title_5.setLabel("Overall feedback");
        title_5.setValue("Overall feedback");
        cris_5.setTitle(title_5);
        cris_5.setDescription(title_5);
        cris_5.setCriterionGradeList(overallGrades);

        Criterion cris_6 = new Criterion();
        cris_6.setCriterionNum("6");
        TemplateField title_6 = new TemplateField();
        title_6.setLabel("Reflection answer");
        title_6.setValue("Reflection answer");
        cris_6.setTitle(title_6);
        cris_6.setDescription(title_6);
        cris_6.setCriterionGradeList(reflectionGrades);
        
        Criterion[] cris = { cris_1, cris_2, cris_3, cris_4, cris_5, cris_6 };
        return cris;
    }

    @RequestMapping(value = SURVEYPATH + "/saveanswerf2f", method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
    @ResponseBody
    public ReturnValue saveAnswerF2f(HttpServletRequest request, @RequestBody Answer answer) {
        
        int surveyId = answer.getSurvey().getId();

        // check the user
        Teacher teacher = answer.getTeacher();
        if(teacher != null) {
            
            int teacherId = teacher.getTeacherId();
            teacher = teacherService.getTeacherByTeacherId(teacherId);
            answer.setTeacher(teacher);
            answer.setPatient(null);
            answer.setDoctor(null);

            if (surveyId == 1) {
            	
                // submit the answer UNSW eMed
            	String result = submitToEmed(answer.getContent(), answer.getSessionId(), teacher.getFirstname(), teacher.getLastname());
                
                if (result == null) {
                    ReturnValue rv = new ReturnValue();
                    rv.setCode(0);
                    return rv;
                }else{
                    answer.setReceiptNo(result);
                }
            }

        }
        
        Survey survey = surveyService.findById(surveyId);
        answer.setSurvey(survey);
        answer.setCreateAt(System.currentTimeMillis());
        answerService.save(answer);

        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        return rv;
    }
    
    
    
    
    
 public void update(String studentId, String phase, String receiptNum, String[] answers){
        
        AllDataServiceLocator serviceLocator = new AllDataServiceLocator();

        try {
            AllData service = serviceLocator.getDomino();
            ((Stub) service)._setProperty(Call.USERNAME_PROPERTY, "WebServiceOSPIA");
            ((Stub) service)._setProperty(Call.PASSWORD_PROPERTY, "WebServiceOSPIA");
            
            AssessmentList assessmentList = service.getAssessments(studentId,phase);
            Assessment[] assessments = assessmentList.getAssessmentList();
            
            for (int i = 0; i < assessments.length; i++) {
                String receiptNumber = assessments[i].getReceiptNumber();
                if(receiptNum.equals(receiptNumber)){
                    String newReceiptNum = addReflectionToSOCA(assessments[i], answers);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    
    public String addReflectionToSOCA(Assessment assessment, String[] answers){
        
        AllDataServiceLocator serviceLocator = new AllDataServiceLocator();
        
        AllData service;
        try {
            service = serviceLocator.getDomino();
            
            ((Stub) service)._setProperty(Call.USERNAME_PROPERTY, "WebServiceOSPIA");
            ((Stub) service)._setProperty(Call.PASSWORD_PROPERTY, "WebServiceOSPIA");
            
            CriterionGrade[] criterionGradeList = assessment.getGradedCriterionList();
            CriterionGrade reflectGrade = criterionGradeList[5];
            // reflectGrade.setGrade("See below");
            reflectGrade.setGrade(" ");
            reflectGrade.setGradeReason(answers);
            criterionGradeList[5] = reflectGrade;
            
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
            String currentDate = sdf.format(new Date());
            assessment.setDateStatusModified(currentDate);

            assessment.setCriterionList(createCrition());
            assessment.setGradedCriterionList(criterionGradeList);
            assessment.setAssessmentType("SOCA");
            
            SubmitAssessmentResponse response = service.submitAssessment(assessment);
            System.out.println(response.getMessage());
            System.out.println(response.getMissingCount());
            System.out.println(response.getReceiptNumber());
            logger.info(response.getMessage());
            logger.info(response.getMissingCount());
            logger.info(response.getReceiptNumber());

            return response.getReceiptNumber();
            
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }
               
        return null;
    }
    
    
}
