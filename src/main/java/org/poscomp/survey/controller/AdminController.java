package org.poscomp.survey.controller;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.processor.DownloadProcessor;
import org.poscomp.eqclinic.service.interfaces.AppointmentProgressService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.TeacherService;
import org.poscomp.eqclinic.util.StringTool;
import org.poscomp.survey.domain.Admin;
import org.poscomp.survey.domain.Answer;
import org.poscomp.survey.domain.Question;
import org.poscomp.survey.domain.QuestionType;
import org.poscomp.survey.domain.Survey;
import org.poscomp.survey.service.AdminService;
import org.poscomp.survey.service.AnswerService;
import org.poscomp.survey.service.QuestionService;
import org.poscomp.survey.service.QuestionTypeService;
import org.poscomp.survey.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class AdminController {

    private static final Logger logger = Logger.getLogger(AdminController.class);

    @Autowired
    private AdminService adminService;
    
    private static final String SURVEYPATH = "/survey";
    
    @RequestMapping(value = SURVEYPATH, method = { RequestMethod.GET})
    public String adminHome(HttpServletRequest request) {
        request.setAttribute("redpage", "uhome_admin");
        return "survey/admin_home";
    }
    
    @RequestMapping(value = SURVEYPATH + "/ulogin_admin", method = { RequestMethod.GET})
    public String patientLogin(HttpServletRequest request) {
        return "survey/admin_login";
    }

    @RequestMapping(value = SURVEYPATH + "/uhome_admin", method = { RequestMethod.GET})
    public String redirect_uhome_admin(HttpServletRequest request) {
        request.setAttribute("redpage", "uhome_admin");
        return "survey/admin_home";
    }
    
    @RequestMapping(value = SURVEYPATH + "/logoff", method = { RequestMethod.GET })
    public String tutorlogoff(HttpServletRequest request) {
        String redirectUrl = SURVEYPATH + "/ulogin_admin";
        return "redirect:" + redirectUrl;

    }
    
}
