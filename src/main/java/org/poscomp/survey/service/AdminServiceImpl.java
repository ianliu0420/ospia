package org.poscomp.survey.service;


import javax.persistence.NoResultException;

import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.survey.dao.AdminDao;
import org.poscomp.survey.domain.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("adminService")
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminDao adminDao;

    @Override
    public void save(Admin admin) {

        if (isExistByUsername(admin.getUsername()) && isExistByEmail(admin.getEmail())) {
            admin.setUsername(null);
            admin.setPassword(null);
            admin.setEmail(null);
        } else {
            adminDao.save(admin);
        }
    }

    @Override
    public boolean isExistByUsername(String username) {
        return findByUsername(username) != null;
    }

    @Override
    public boolean isExistByEmail(String email) {
        return findByEmail(email) != null;
    }

    public Admin findByUsername(String username) {
        Admin admin = null;
        try {
            admin = adminDao.findByName(username);
        } catch (NoResultException e) {
        }
        return admin;
    }

    public Admin findByEmail(String email) {
        Admin admin = null;
        try {
            admin = adminDao.findByEmail(email);
        } catch (NoResultException e) {
        }
        return admin;
    }

    @Override
    public Admin findByUsernameAndPassword(String username, String password) {
        Admin admin = null;
        try {
            admin = adminDao.findByNameAndPassword(username, password);
        } catch (NoResultException e) {
        }
        return admin;
    }

    @Override
    public Admin findById(int id) {
        return adminDao.findById(id);
    }
    
    @Override
    public Admin adminLogin(String username, String password) {
        Admin admin = adminDao.findByName(username);
        if (admin != null && password.equals(admin.getPassword())) {
            return admin;
        }
        return null;
    }
}
