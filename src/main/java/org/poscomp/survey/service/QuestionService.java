package org.poscomp.survey.service;

import java.util.List;

import org.poscomp.survey.domain.Question;

public interface QuestionService {
    public void save(Question question);

    public Question findById(int id);

    public void delete(Question question);

    public void deleteById(int id);
    
    public List<Question> findAll(int pageNo, int pageSize);
    
    public List<Question> findByIds(List<Integer> ids);
    
    public List<Question> findByContent(String content, String ids);
}
