package org.poscomp.survey.dao;

import java.util.ArrayList;
import java.util.List;

import org.poscomp.survey.domain.QuestionOption;

public interface QuestionOptionDao {

    public int saveQuestionOption(QuestionOption questionOption);

    public List<Integer> saveQustionOptions(List<QuestionOption> options);
    
    public QuestionOption findById(int id);

    public List<QuestionOption> findByIds(ArrayList<Integer> ids);

}
