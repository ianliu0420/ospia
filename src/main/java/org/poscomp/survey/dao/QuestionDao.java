package org.poscomp.survey.dao;


import java.util.List;

import org.poscomp.survey.domain.Question;

public interface QuestionDao {
    public void save(Question question);

    public void delete(Question question);

    public Question findById(int id);

    public List<Question> findByContent(String content, String ids);
    
    public List<Question> listBySurveyId(int surveyId);
    
    public List<Question> findAll(int pageNo, int pageSize);
}
