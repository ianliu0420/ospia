package org.poscomp.survey.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.domain.ReflectionLog;
import org.poscomp.eqclinic.domain.Scenario;
import org.springframework.stereotype.Repository;

@Repository(value = "reflectionLogDao")
public class ReflectionLogDaoImpl implements ReflectionLogDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(ReflectionLog reflectionLog) {
        sessionFactory.getCurrentSession().saveOrUpdate(reflectionLog);
    }

    @Override
    public ReflectionLog getById(int reflectionLogId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from ReflectionLog as r where r.logId=:logId";
        Query query = session.createQuery(hql);
        query.setInteger("logId", reflectionLogId);
        ReflectionLog result = (ReflectionLog) query.uniqueResult();
        return result;
    }

}
