package org.poscomp.survey.dao;

import org.poscomp.eqclinic.domain.ReflectionLog;

public interface ReflectionLogDao {

    public void save(ReflectionLog reflectionLog);

    public ReflectionLog getById(int reflectionLogId);
    
}
