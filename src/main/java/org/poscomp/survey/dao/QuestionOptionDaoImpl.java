package org.poscomp.survey.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.survey.domain.QuestionOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository(value = "questionOptionDao")
public class QuestionOptionDaoImpl implements QuestionOptionDao {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int saveQuestionOption(QuestionOption questionOption) {
        sessionFactory.getCurrentSession().save(questionOption);
        return questionOption.getId();
    }


    @Override
    public List<Integer> saveQustionOptions(List<QuestionOption> options) {
        List<Integer> optionIds = new ArrayList<Integer>();

        for (QuestionOption questionOption : options) {

            int id = saveQuestionOption(questionOption);
            optionIds.add(id);

        }

        return optionIds;
    }


    @Override
    public QuestionOption findById(int id) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from QuestionOption as qo where qo.id = :id ";
        Query query = session.createQuery(hql);
        query.setInteger("id", id);
        QuestionOption result = (QuestionOption) query.uniqueResult();
        return result;
    }

    @Override
    public List<QuestionOption> findByIds(ArrayList<Integer> ids) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from QuestionOption as qo where 1=1 and (  ";

        for (int i = 0; i < ids.size(); i++) {
            if (i == 0) {
                hql += " qo.id=" + ids.get(i);
            } else {
                hql += " or qo.id=" + ids.get(i);
            }
        }
        hql += ")";

        Query query = session.createQuery(hql);
        List<QuestionOption> result = (List<QuestionOption>) query.list();
        return result;
    }

}
