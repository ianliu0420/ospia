package org.poscomp.survey.dao;


import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.survey.domain.Admin;
import org.poscomp.survey.domain.Question;
import org.springframework.stereotype.Repository;

@Repository("adminDao")
public class AdminDaoImpl implements AdminDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Admin admin) {
        sessionFactory.getCurrentSession().saveOrUpdate(admin);
    }

    @Override
    public Admin findByName(String name) {
        String hql = "from Admin as a where a.username=:name";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setString("name", name);
        Admin result = (Admin) query.uniqueResult();
        return result;
    }

    @Override
    public Admin findByEmail(String email) {
        String hql = "from Admin as a where a.email=:email";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setString("email", email);
        Admin result = (Admin) query.uniqueResult();
        return result;
    }

    @Override
    public Admin findByNameAndPassword(String name, String password) {

        String hql = "from Admin as a where a.username=:name and a.password=password";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setString("name", name);
        query.setString("password", password);
        Admin result = (Admin) query.uniqueResult();
        return result;
    }

    @Override
    public Admin findById(int id) {
        String hql = "from Admin as a where a.id=:id";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setInteger("id", id);
        Admin result = (Admin) query.uniqueResult();
        return result;
    }
}
