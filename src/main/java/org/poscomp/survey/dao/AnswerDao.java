package org.poscomp.survey.dao;

import java.util.List;

import org.poscomp.survey.domain.Answer;

public interface AnswerDao {

    public void save(Answer answer);
    
    public Answer loadAnswerById(int id);

    /*
     * userType: doctor | patient
     */
    public List<Answer> loadAnswerBySessionId(String sessionId);
    
    public List<Answer> loadAnswer();
    
}
