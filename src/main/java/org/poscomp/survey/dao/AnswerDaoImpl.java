package org.poscomp.survey.dao;


import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.VideoDaoImpl;
import org.poscomp.eqclinic.dao.interfaces.DoctorDao;
import org.poscomp.survey.domain.Admin;
import org.poscomp.survey.domain.Answer;
import org.poscomp.survey.domain.Question;
import org.springframework.stereotype.Repository;

@Repository("answerDao")
public class AnswerDaoImpl implements AnswerDao {

    private static final Logger logger = Logger.getLogger(AnswerDaoImpl.class);
    
    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Answer answer) {
        sessionFactory.getCurrentSession().saveOrUpdate(answer);
        logger.info("you have insert a survey answer");
    }

    @Override
    public Answer loadAnswerById(int id) {
        String hql = "from Answer as a where a.id=:id";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setInteger("id", id);
        Answer result = (Answer) query.uniqueResult();
        return result;
    }

    @Override
    public List<Answer> loadAnswerBySessionId(String sessionId){
        
        String hql = "from Answer as a where a.sessionId=:sessionId ";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setString("sessionId", sessionId);
        List<Answer> result = (List<Answer>) query.list();
        return result;
    }

    @Override
    public List<Answer> loadAnswer() {
        String hql = "from Answer as a where a.survey.id=1";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        List<Answer> result = (List<Answer>) query.list();
        return result;
    }
    
    
    
    
}
