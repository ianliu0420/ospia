package org.poscomp.survey.dao;


import java.util.List;

import javax.annotation.Resource;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.poscomp.survey.domain.Question;
import org.springframework.stereotype.Repository;

@Repository("questionDao")
public class QuestionDaoImpl implements QuestionDao {

    @Resource
    private SessionFactory sessionFactory;

    public void save(Question question) {
        sessionFactory.getCurrentSession().saveOrUpdate(question);
    }

    public void delete(Question question) {
        sessionFactory.getCurrentSession().delete(question);
    }

    public Question findById(int id) {
        String hql = "from Question as q where q.id=:id";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setInteger("id", id);
        Question result = (Question) query.uniqueResult();
        return result;
    }

    public List<Question> listBySurveyId(int surveyId) {
        String hql = "from Question as q where q.surveyid=:surveyId";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setInteger("surveyId", surveyId);
        List<Question> result = (List<Question>) query.list();
        return result;
    }

    public List<Question> findAll(int pageNo, int pageSize) {
        String hql = "from Question";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        List<Question> result = (List<Question>) query.list();
        return result;
    }

    @Override
    public List<Question> findByContent(String content, String ids) {
        String hql = "from Question as q where q.title LIKE :title";

        if(!"".equals(ids)){
            String[] unavailableIds = ids.split(",");
            for (int i = 0; i < unavailableIds.length; i++) {
                
                if (i == 0) {
                    hql += " and ( q.id <> " + unavailableIds[i];
                } else {
                    hql += " and q.id <> " + unavailableIds[i];
                }
                
            }
            
            if (unavailableIds.length > 0) {
                hql += " ) ";
            }
        }
        

        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setString("title", "%"+content+"%");
        List<Question> result = (List<Question>) query.list();
        return result;
    }
}
