package org.poscomp.survey.dao;


import java.util.List;

import org.poscomp.survey.domain.Question;
import org.poscomp.survey.domain.Survey;

public interface SurveyDao {

    public void save(Survey survey);

    public void update(Survey survey);

    public List<Survey> listByUser(int adminId);

    public Survey findById(int id);

    public List<Survey> findAll(int pageNo, int pageSize);
    
    public void delete(Survey survey);

}
