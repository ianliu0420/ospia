package org.poscomp.eqclinic.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.NoteDao;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Repository(value = "noteDao")
public class NoteDaoImpl implements NoteDao {

    private static final Logger logger = Logger.getLogger(NoteDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveNote(Note note) {
        sessionFactory.getCurrentSession().saveOrUpdate(note);
        logger.info("you have insert a note");
    }

    @Override
    public List<Note> getNoteBySessionId(String sessionId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Note as n where n.sessionId=:sessionId";
        Query query = session.createQuery(hql);
        query.setString("sessionId", sessionId);
        List<Note> result = (List<Note>) query.list();
        return result;
    }

    @Override
    public List<Note> getNoteBySessionIdNoter(String sessionId, int noterType, int noterId) {
        
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Note as n where n.sessionId=:sessionId and noterType=:noterType and noterId=:noterId";
        Query query = session.createQuery(hql);
        query.setString("sessionId", sessionId);
        query.setInteger("noterType", noterType);
        query.setInteger("noterId", noterId);
        List<Note> result = (List<Note>) query.list();
        return result;
    }

    @Override
    public void deleteNote(Note note) {
        sessionFactory.getCurrentSession().delete(note);;
    }

    @Override
    public Note getNoteById(int noteId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Note as n where n.id=:noteId";
        Query query = session.createQuery(hql);
        query.setInteger("noteId", noteId);
        Note result = (Note) query.uniqueResult();
        return result;
    }

    
    // noterType: 1: tutor  2: SP
    // noteType: 1: thumb up  2: thumb down  
    @Override
    public List<Note> getNoteBySessionIdType(String sessionId, int noterType, int noteType) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Note as n where n.sessionId=:sessionId and noterType=:noterType and noteContent=:noteType";
        Query query = session.createQuery(hql);
        query.setString("sessionId", sessionId);
        query.setInteger("noterType", noterType);
        query.setInteger("noteType", noteType);
        List<Note> result = (List<Note>) query.list();
        return result;
    }

    @Override
    public List<Note> getNoteBySessionIdTypeAll(String sessionId, int noterType) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Note as n where n.sessionId=:sessionId and noterType=:noterType";
        Query query = session.createQuery(hql);
        query.setString("sessionId", sessionId);
        query.setInteger("noterType", noterType);
        List<Note> result = (List<Note>) query.list();
        return result;
    }

}
