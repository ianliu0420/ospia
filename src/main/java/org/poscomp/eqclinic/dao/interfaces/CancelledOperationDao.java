package org.poscomp.eqclinic.dao.interfaces;

import org.poscomp.eqclinic.domain.CancelledOperation;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface CancelledOperationDao {

    public void saveCancelledOperation(CancelledOperation operation);

}
