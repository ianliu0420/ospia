package org.poscomp.eqclinic.dao.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.PatientWeeklyEmail;
import org.poscomp.eqclinic.domain.PatientWeeklyEmailClicking;


/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface PatientWeeklyEmailClickingDao {
    
    public void savePatientWeeklyEmailClicking(PatientWeeklyEmailClicking clicking);
    
}
