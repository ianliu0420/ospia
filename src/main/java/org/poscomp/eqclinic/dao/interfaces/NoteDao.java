package org.poscomp.eqclinic.dao.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Note;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface NoteDao {
    
    public void saveNote(Note note);

    public void deleteNote(Note note);
    
    public Note getNoteById(int noteId);
    
    public List<Note> getNoteBySessionId(String sessionId);

    public List<Note> getNoteBySessionIdNoter(String sessionId, int noterType, int noterId);
    
    public List<Note> getNoteBySessionIdType(String sessionId, int noterType, int noteType);
    
    public List<Note> getNoteBySessionIdTypeAll(String sessionId, int noterType);
    
}
