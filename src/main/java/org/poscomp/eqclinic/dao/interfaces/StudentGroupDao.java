package org.poscomp.eqclinic.dao.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.StudentGroup;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface StudentGroupDao {

    public StudentGroup getStudentGroupByStudentId(String studentId);
    
    public StudentGroup getStudentGroupById(int id);

    public List<StudentGroup> getAllUnParticipatedStudent();

	public List<StudentGroup> getAllStudent();

	public List<StudentGroup> searchStudent(String searchContent);

	public void saveStudentGroup(StudentGroup student);
	
	public List<StudentGroup> getStudentByEmailGroupId(int emailGroupId);
	
	public List<StudentGroup> getStudentByImageGroupId(int imageGroupId);
	
	public void clearStudentGroup();
    
}
