package org.poscomp.eqclinic.dao.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.PatientWeeklyEmail;


/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface PatientWeeklyEmailDao {
    
    public int savePatientWeeklyEmail(PatientWeeklyEmail email);
    
    public void updatePatientWeeklyEmail(PatientWeeklyEmail email);
    
    public PatientWeeklyEmail getPatientEmailById(int emailId);
    
}
