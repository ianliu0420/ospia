package org.poscomp.eqclinic.dao.interfaces;

import org.poscomp.eqclinic.domain.AppointmentProgress;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface AppointmentProgressDao {
    
    public void saveAppointmentProgress(AppointmentProgress appPro);

    public AppointmentProgress getAppointmentProgressById(int id);

    public AppointmentProgress getAppointmentProgressByAptIdUserType(int aptId, String userType);
    
    public AppointmentProgress getAppointmentProgressByAptIdUserId(int aptId, String userType, int userId);

    public AppointmentProgress getAppointmentProgressBySessionIdUserId(String sessionId, String userType, int userId);

}
