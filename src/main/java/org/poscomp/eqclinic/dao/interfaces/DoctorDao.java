package org.poscomp.eqclinic.dao.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Doctor;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface DoctorDao {
    
    public Doctor getDoctorByUsername(String username);

    public Doctor getDoctorByDoctorId(int doctorId);

    public List<Doctor> getAllDoctors();

    public void saveDoctor(Doctor doctor);

	public List<Doctor> searchDoctor(String searchContent);
    
}
