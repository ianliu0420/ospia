package org.poscomp.eqclinic.dao.interfaces;

import org.poscomp.eqclinic.domain.Participation;

public interface ParticipationDao {

    public void save(Participation part);

}
