package org.poscomp.eqclinic.dao.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.pojo.AppointmentSearchObject;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface AppointmentDao {
    
    public void saveApp(Appointment app);

    public List<Appointment> getDoctorNextApts(int doctorId);

    public List<Appointment> getDoctorRecentRequest(int doctorId);

    public List<Appointment> getDoctorAptCancelled(int doctorId);

    public List<Appointment> getDoctorApts(int doctorId);

    public List<Appointment> getDoctorAptsStatus(int doctorId, int status);

    public List<Appointment> getDoctorAptsAvailable(int doctorId);

    public List<Appointment> getDoctorAptsWholeDate(int doctorId, String aptdate);

    public Appointment getLatestAptByDoctorId(int doctorId);


    public List<Appointment> getPatientApts(int patientId);

    public List<Appointment> getPatientAptsWholeDate(int patientId, String aptdate);

    public List<Appointment> getPatientPendingRequest(int patientId);

    public List<Appointment> getPatientNextApts(int patientId);


    public Appointment getLatestAptByPatientId(int patientId);

    public Appointment getAppointmentBySessionId(String sessionId);

    public Appointment getAppointmentByAptId(int aptId);

    public List<Appointment> findBetweenTwoTime(Long start, Long end);
    
    public List<Appointment> findAllAvailabel(Long end);

    public List<Appointment> getPreviousAptByDoctorId(int doctorId);
    
    
    public List<Appointment> getAptByDate(String aptdate);
    
    public List<Appointment> getAvailableAptByTime(long time);
    
    public List<Appointment> searchAppointments(AppointmentSearchObject aso);
    
    public List<Appointment> getPendingRequest24H(int from, int to);

	public List<Appointment> getDoctorAptStatusAssessment(int doctorId, int status);
    
	public List<Object> getAppointmentWithStudentPersonalFeedback(int patientId, Long start, Long end);

}
