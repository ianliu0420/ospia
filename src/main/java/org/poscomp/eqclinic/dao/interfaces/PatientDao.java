package org.poscomp.eqclinic.dao.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Patient;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface PatientDao {

    public Patient getPatientByUsername(String username);

    public Patient getPatientByPatientId(int patientid);
    
    public void savePatient(Patient patient);
    
    public Patient getPatientByPhoneNo(String phoneNo);
    
    public List<Patient> getAllPatients();
    
    public List<Patient> searchPatient(String searchContent);
    
    public List<Patient> getPatientInPeriod(Long start, Long end);
    
}
