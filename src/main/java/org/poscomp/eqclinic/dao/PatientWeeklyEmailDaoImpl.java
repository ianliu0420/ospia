package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.dao.interfaces.PatientWeeklyEmailDao;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.PatientWeeklyEmail;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "patientWeeklyEmailDao")
public class PatientWeeklyEmailDaoImpl implements PatientWeeklyEmailDao {

    private static final Logger logger = Logger.getLogger(PatientWeeklyEmailDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	@Override
	public int savePatientWeeklyEmail(PatientWeeklyEmail email) {
		sessionFactory.getCurrentSession().saveOrUpdate(email);
		logger.info("you have insert a patient email");
		return email.getId();
	}

	@Override
	public PatientWeeklyEmail getPatientEmailById(int emailId) {
		Session session = sessionFactory.getCurrentSession();
        String hql = "from PatientWeeklyEmail as p where p.id=:emailId";
        Query query = session.createQuery(hql);
        query.setInteger("emailId", emailId);
        PatientWeeklyEmail email = (PatientWeeklyEmail) query.uniqueResult();
        return email;
	}

	@Override
	public void updatePatientWeeklyEmail(PatientWeeklyEmail email) {
		sessionFactory.getCurrentSession().update(email);
		logger.info("you have update a patient email");
		return;
	}
    
}
