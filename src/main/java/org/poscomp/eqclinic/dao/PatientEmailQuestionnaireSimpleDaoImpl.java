package org.poscomp.eqclinic.dao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.CalibrationUserDao;
import org.poscomp.eqclinic.dao.interfaces.PatientEmailQuestionnaireSimpleDao;
import org.poscomp.eqclinic.domain.CalibrationSurveyAnswer;
import org.poscomp.eqclinic.domain.CalibrationUser;
import org.poscomp.eqclinic.domain.PatientEmailQuestionnaireSimple;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "patientEmailQuestionnaireSimpleDao")
public class PatientEmailQuestionnaireSimpleDaoImpl implements PatientEmailQuestionnaireSimpleDao {

    private static final Logger logger = Logger.getLogger(PatientEmailQuestionnaireSimpleDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	@Override
	public void savePatientEmailQuestionnaireSimple(
			PatientEmailQuestionnaireSimple patientQ) {
		sessionFactory.getCurrentSession().saveOrUpdate(patientQ);
        logger.info("you have insert an calibration user");
	}


}
