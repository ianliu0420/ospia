package org.poscomp.eqclinic.dao;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.CancelledOperationDao;
import org.poscomp.eqclinic.domain.CancelledOperation;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "cancelledOperationDao")
public class CancelledOperationDaoImpl implements CancelledOperationDao {

    private static final Logger logger = Logger.getLogger(CancelledOperationDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveCancelledOperation(CancelledOperation operation) {
        sessionFactory.getCurrentSession().saveOrUpdate(operation);
        logger.info("you have insert a cancelled operation");
    }

}
