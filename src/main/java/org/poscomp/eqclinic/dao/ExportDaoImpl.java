package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.dao.interfaces.ExportDao;
import org.poscomp.eqclinic.domain.Application;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "exportDao")
public class ExportDaoImpl implements ExportDao {

    private static final Logger logger = Logger.getLogger(ExportDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Object> exportAptOffered(long timestamp) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "select p.firstname, p.lastname, COUNT(apt.patientId) from Appointment as apt, Patient as p where apt.patientId=p.patientid and apt.createtime>:timestamp and (apt.status=1 or apt.status=2 or apt.status=3 or apt.status=8) GROUP BY apt.patientId";
        Query query = session.createQuery(hql);
        query.setLong("timestamp", timestamp);
        List<Object> list = (ArrayList<Object>) query.list();
        return list;
        
    }

    @Override
    public List<Object> exportSPRegistration(long timestamp) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "select firstname, lastname, createAt, email from Patient where status=1 order by createAt desc";
        Query query = session.createQuery(hql);
        List<Object> list = (ArrayList<Object>) query.list();
        return list;
    }

    @Override
    public List<Object> exportAptCompleted(long timestamp) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "select d.firstname, d.lastname, d.userId, p.firstname, p.lastname, apt.starttime from Appointment as apt, Patient as p, Doctor as d where apt.patientId=p.patientid and apt.doctorId=d.doctorId and apt.starttime>:timestamp and apt.status=8 and apt.patientname<>'Ian' order by apt.starttime DESC";
        Query query = session.createQuery(hql);
        query.setLong("timestamp", timestamp);
        List<Object> list = (ArrayList<Object>) query.list();
        return list;
    }

    @Override
    public List<Object> exportAptFailed(long timestamp) {
        return null;
    }

    @Override
    public List<Object> exportAptCanceledStu(long timestamp) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "select d.firstname, d.lastname, d.userId, p.firstname, p.lastname, apt.starttime, co.reason from CancelledOperation as co, Appointment as apt, Patient as p, Doctor as d where co.cancelAt>:timestamp and apt.patientId=p.patientid and co.doctorId=d.doctorId and apt.aptid=co.aptId and co.doctorId is not null";
        Query query = session.createQuery(hql);
        query.setLong("timestamp", timestamp);
        List<Object> list = (ArrayList<Object>) query.list();
        return list;
    }
    
    @Override
    public List<Object> exportAptCanceledSP(long timestamp) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "select d.firstname, d.lastname, d.userId, p.firstname, p.lastname, apt.starttime, co.reason from CancelledOperation as co, Appointment as apt, Patient as p, Doctor as d where co.cancelAt>:timestamp and apt.doctorId=d.doctorId and co.patientId=p.patientid and apt.aptid=co.aptId and co.patientId is not null";
        Query query = session.createQuery(hql);
        query.setLong("timestamp", timestamp);
        List<Object> list = (ArrayList<Object>) query.list();
        return list;
    }

    @Override
    public List<Object> exportAptCompletedSP(long timestamp) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "select p.firstname, p.lastname, count(p.patientid) from Appointment as apt, Patient as p, Doctor as d where apt.patientId=p.patientid and apt.doctorId=d.doctorId and apt.starttime>:timestamp and apt.status=8 and apt.patientname<>'Ian' group by p.patientid";
        Query query = session.createQuery(hql);
        query.setLong("timestamp", timestamp);
        List<Object> list = (ArrayList<Object>) query.list();
        return list;
    }

    @Override
    public List<Object> exportAptCompletedStu(long timestamp) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "select d.firstname, d.lastname, d.userId, count(d.doctorId) from Appointment as apt, Patient as p, Doctor as d where apt.patientId=p.patientid and apt.doctorId=d.doctorId and apt.starttime>:timestamp and apt.status=8 and apt.patientname<>'Ian' group by d.doctorId";
        Query query = session.createQuery(hql);
        query.setLong("timestamp", timestamp);
        List<Object> list = (ArrayList<Object>) query.list();
        return list;
    }

    @Override
    public List<Object> exportAptImappropriate(long timestamp) {
        return null;
    }

    @Override
    public List<Object> exportAptConclusion(int status, long timestamp) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as apt where status=:status and starttime>:timestamp";
        Query query = session.createQuery(hql);
        query.setInteger("status", status);
        query.setLong("timestamp", timestamp);
        List<Object> list = (ArrayList<Object>) query.list();
        return list;
    }
    
}
