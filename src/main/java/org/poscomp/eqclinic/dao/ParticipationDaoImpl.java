package org.poscomp.eqclinic.dao;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.ParticipationDao;
import org.poscomp.eqclinic.domain.Participation;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "participationDao")
public class ParticipationDaoImpl implements ParticipationDao {

    private static final Logger logger = Logger.getLogger(ParticipationDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	@Override
	public void save(Participation part) {
		sessionFactory.getCurrentSession().saveOrUpdate(part);
        logger.info("you have insert an participation");
		
	}
    
}
