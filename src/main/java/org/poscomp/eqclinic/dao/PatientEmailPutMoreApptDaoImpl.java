package org.poscomp.eqclinic.dao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.CalibrationUserDao;
import org.poscomp.eqclinic.dao.interfaces.PatientEmailPutMoreApptDao;
import org.poscomp.eqclinic.dao.interfaces.PatientEmailQuestionnaireSimpleDao;
import org.poscomp.eqclinic.domain.CalibrationSurveyAnswer;
import org.poscomp.eqclinic.domain.CalibrationUser;
import org.poscomp.eqclinic.domain.PatientEmailPutMoreAppt;
import org.poscomp.eqclinic.domain.PatientEmailQuestionnaireSimple;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "patientEmailPutMoreApptDao")
public class PatientEmailPutMoreApptDaoImpl implements PatientEmailPutMoreApptDao {

    private static final Logger logger = Logger.getLogger(PatientEmailPutMoreApptDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	@Override
	public void savePatientEmailPutMoreAppt(PatientEmailPutMoreAppt moreAppt) {
		sessionFactory.getCurrentSession().saveOrUpdate(moreAppt);
        logger.info("you have insert an more appt");
	}


}
