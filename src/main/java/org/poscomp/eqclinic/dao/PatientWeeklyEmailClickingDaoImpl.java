package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.dao.interfaces.PatientWeeklyEmailClickingDao;
import org.poscomp.eqclinic.dao.interfaces.PatientWeeklyEmailDao;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.PatientWeeklyEmail;
import org.poscomp.eqclinic.domain.PatientWeeklyEmailClicking;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "patientWeeklyEmailClickingDao")
public class PatientWeeklyEmailClickingDaoImpl implements PatientWeeklyEmailClickingDao {

    private static final Logger logger = Logger.getLogger(PatientWeeklyEmailClickingDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	@Override
	public void savePatientWeeklyEmailClicking(PatientWeeklyEmailClicking clicking) {
		sessionFactory.getCurrentSession().saveOrUpdate(clicking);
		logger.info("you have insert a patient email clicking");
		return;
	}

}
