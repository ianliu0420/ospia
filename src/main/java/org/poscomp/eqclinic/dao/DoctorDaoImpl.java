package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.DoctorDao;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "doctorDao")
@Transactional
public class DoctorDaoImpl implements DoctorDao {

    private static final Logger logger = Logger.getLogger(DoctorDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Doctor getDoctorByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Doctor as d where d.username=:username";
        Query query = session.createQuery(hql);
        query.setString("username", username);
        List<Doctor> list = (ArrayList<Doctor>) query.list();

        if (list != null && list.size() == 1) {
            return list.get(0);
        }

        return null;
    }

    @Override
    public List<Doctor> getAllDoctors() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Doctor ";
        Query query = session.createQuery(hql);
        List<Doctor> list = (ArrayList<Doctor>) query.list();
        return list;
    }

    @Override
    public Doctor getDoctorByDoctorId(int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Doctor as d where d.doctorId=:doctorId";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        Doctor doc = (Doctor) query.uniqueResult();
        return doc;
    }

    @Override
    public void saveDoctor(Doctor d) {
        sessionFactory.getCurrentSession().saveOrUpdate(d);
        logger.info("you have insert an doctor");
    }

	@Override
	public List<Doctor> searchDoctor(String searchContent) {
		searchContent = searchContent.trim().toLowerCase();
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Doctor as d where status=1 and ( lower(d.firstname) like :content1 or lower(d.lastname) like :content2 or lower(d.userId) like :content3)";
        Query query = session.createQuery(hql);
        query.setString("content1", "%"+searchContent+"%");
        query.setString("content2", "%"+searchContent+"%");
        query.setString("content3", "%"+searchContent+"%");
        List<Doctor> doctors = (List<Doctor>) query.list();
        return doctors;
	}

}
