package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.TeacherDao;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "teacherDao")
public class TeacherDaoImpl implements TeacherDao {

    private static final Logger logger = Logger.getLogger(TeacherDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Teacher getTeacherByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Teacher as t where t.username=:username";
        Query query = session.createQuery(hql);
        query.setString("username", username);
        List<Teacher> list = (ArrayList<Teacher>) query.list();

        if (list != null && list.size() == 1) {
            return list.get(0);
        }

        return null;
    }

    @Override
    public Teacher getTeacherByTeacherId(int teacherId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Teacher as t where t.teacherId=:teacherId";
        Query query = session.createQuery(hql);
        query.setInteger("teacherId", teacherId);
        Teacher teacher = (Teacher) query.uniqueResult();
        return teacher;
    }

    @Override
    public void saveTeacher(Teacher teacher) {
        sessionFactory.getCurrentSession().saveOrUpdate(teacher);
        logger.info("you have insert a tutor");
    }

	@Override
	public List<Teacher> getAllTeacher() {
		Session session = sessionFactory.getCurrentSession();
        String hql = "from Teacher ";
        Query query = session.createQuery(hql);
        List<Teacher> list = (ArrayList<Teacher>) query.list();
        return list;
	}

	@Override
	public List<Teacher> searchTeacher(String searchContent) {
		searchContent = searchContent.trim().toLowerCase();
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Teacher as t where status=1 and ( lower(t.firstname) like :content1 or lower(t.lastname) like :content2 or lower(t.username) like :content3)";
        Query query = session.createQuery(hql);
        query.setString("content1", "%"+searchContent+"%");
        query.setString("content2", "%"+searchContent+"%");
        query.setString("content3", "%"+searchContent+"%");
        List<Teacher> teachers = (List<Teacher>) query.list();
        return teachers;
	}

}
