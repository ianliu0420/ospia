package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.PatientDao;
import org.poscomp.eqclinic.domain.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Repository(value = "patientDao")
public class PatientDaoImpl implements PatientDao {

    private static final Logger logger = Logger.getLogger(PatientDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Patient getPatientByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Patient as p where p.username=:username and p.status=1";
        Query query = session.createQuery(hql);
        query.setString("username", username);
        List<Patient> list = (ArrayList<Patient>) query.list();

        if (list != null && list.size() == 1) {
            return list.get(0);
        }

        return null;
    }

    @Override
    public Patient getPatientByPatientId(int patientid) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Patient as p where p.patientid=:patientid and p.status=1";
        Query query = session.createQuery(hql);
        query.setInteger("patientid", patientid);
        Patient patient = (Patient) query.uniqueResult();
        return patient;
    }

    @Override
    public void savePatient(Patient patient) {
        sessionFactory.getCurrentSession().saveOrUpdate(patient);
        logger.info("you have insert a patient");
    }

    @Override
    public Patient getPatientByPhoneNo(String phoneNo) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Patient as p where p.phone=:phoneNo and p.status=1";
        Query query = session.createQuery(hql);
        query.setString("phoneNo", phoneNo);
        Patient patient = (Patient) query.uniqueResult();
        return patient;
    }

    @Override
    public List<Patient> getAllPatients() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Patient where status=1";
        Query query = session.createQuery(hql);
        List<Patient> patients = (List<Patient>) query.list();
        return patients;
    }

    @Override
    public List<Patient> searchPatient(String searchContent) {
        
        searchContent = searchContent.trim().toLowerCase();
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Patient as p where status=1 and ( lower(p.firstname) like :content1 or lower(p.lastname) like :content2 )";
        Query query = session.createQuery(hql);
        query.setString("content1", "%"+searchContent+"%");
        query.setString("content2", "%"+searchContent+"%");
        List<Patient> patients = (List<Patient>) query.list();
        return patients;
    }

    @Override
    public List<Patient> getPatientInPeriod(Long start, Long end) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "select distinct p from Appointment as a, Patient as p where p.status=1 and a.patientId=p.patientid and a.starttime>:start and a.endtime<:end and a.status=8";
        Query query = session.createQuery(hql);
        query.setLong("start", start);
        query.setLong("end", end);
        List<Patient> patients = (List<Patient>) query.list();
        return patients;
    }

}
