package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.ScenarioDao;
import org.poscomp.eqclinic.domain.Scenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "scenarioDao")
public class ScenarioDaoImpl implements ScenarioDao {

    private static final Logger logger = Logger.getLogger(ScenarioDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Scenario> getAllScenario() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Scenario";
        Query query = session.createQuery(hql);
        List<Scenario> result = (List<Scenario>) query.list();
        return result;

    }

    @Override
    public Scenario getScenarioById(int id) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Scenario as s where s.id=:id";
        Query query = session.createQuery(hql);
        query.setInteger("id", id);
        Scenario result = (Scenario) query.uniqueResult();
        return result;
    }

    @Override
    public List<Scenario> getScenarioBySearch(Scenario sce) {

        Session session = sessionFactory.getCurrentSession();

        String hql = "from Scenario as s where 1=1 ";

        if (sce != null) {
            if (sce.getGender() != null) {
                hql += " and s.gender='" + sce.getGender() + "' ";
            }
            if (sce.getPysicality() != null) {
                hql += " and s.pysicality='" + sce.getPysicality() + "' ";
            }

            if (sce.getAge() != 0) {
                hql += " and s.age=" + sce.getAge() + " ";
            }
        }

        System.out.println(hql);

        Query query = session.createQuery(hql);
        List<Scenario> result = (List<Scenario>) query.list();
        return result;
    }

    @Override
    public List<Scenario> getMultiScenarioByIds(String ids) {
        if ("".equals(ids)) {
            return new ArrayList<Scenario>();
        }

        String[] sceIds = ids.trim().split(",");
        if (sceIds.length == 0) {
            return null;
        } else {
            String hql = "from Scenario as s where 1=1 ";
            for (int i = 0; i < sceIds.length; i++) {
                int sceId = Integer.parseInt(sceIds[i]);
                if (i == 0) {
                    hql += " and (s.id=" + sceId + " ";
                } else {
                    hql += " or s.id=" + sceId + " ";
                }
            }
            hql += " ) ";

            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(hql);
            List<Scenario> result = (List<Scenario>) query.list();
            return result;
        }
    }

    @Override
    public List<Scenario> getScenarioByCode(String code) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Scenario as s where s.code like '%" + code + "%' ";
        Query query = session.createQuery(hql);
        List<Scenario> result = (List<Scenario>) query.list();
        return result;
    }

}
