package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.StudentGroupDao;
import org.poscomp.eqclinic.dao.interfaces.TeacherDao;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.StudentGroup;
import org.poscomp.eqclinic.domain.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "studentGroupDao")
public class StudentGroupDaoImpl implements StudentGroupDao {

    private static final Logger logger = Logger.getLogger(StudentGroupDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public StudentGroup getStudentGroupByStudentId(String studentId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from StudentGroup as t where t.studentId=:studentId";
        Query query = session.createQuery(hql);
        query.setString("studentId", studentId);
        StudentGroup result = (StudentGroup) query.uniqueResult();
        return result;
    }

	@Override
	public List<StudentGroup> getAllUnParticipatedStudent() {
		//select studentId from studentgroup where groupNumber=3 and studentId not in ( select doctor.userId from video, doctor where video.videoType=1 and video.doctorId=doctor.doctorId );
		
		Session session = sessionFactory.getCurrentSession();
        String hql = "select sg from StudentGroup as sg where sg.groupNumber=3 and sg.studentId not in (select d.userId from Video as v, Doctor as d where v.videoType=1 and v.doctorId=d.doctorId)";
        Query query = session.createQuery(hql);
        List<StudentGroup> result = (List<StudentGroup>)query.list();
        return result;
	}

	@Override
	public List<StudentGroup> getAllStudent() {
		 Session session = sessionFactory.getCurrentSession();
	        String hql = "from StudentGroup as t where t.status=1";
	        Query query = session.createQuery(hql);
	        List<StudentGroup> result = (List<StudentGroup>) query.list();
	        return result;
	}

	@Override
	public List<StudentGroup> searchStudent(String searchContent) {
		searchContent = searchContent.trim().toLowerCase();
        Session session = sessionFactory.getCurrentSession();
        String hql = "from StudentGroup as d where status=1 and ( lower(d.firstName) like :content1 or lower(d.lastName) like :content2 or lower(d.studentId) like :content3)";
        Query query = session.createQuery(hql);
        query.setString("content1", "%"+searchContent+"%");
        query.setString("content2", "%"+searchContent+"%");
        query.setString("content3", "%"+searchContent+"%");
        List<StudentGroup> doctors = (List<StudentGroup>) query.list();
        return doctors;
	}

	@Override
	public StudentGroup getStudentGroupById(int id) {
		Session session = sessionFactory.getCurrentSession();
        String hql = "from StudentGroup as t where t.id=:id";
        Query query = session.createQuery(hql);
        query.setInteger("id", id);
        StudentGroup result = (StudentGroup) query.uniqueResult();
        return result;
	}

	@Override
	public void saveStudentGroup(StudentGroup student) {
		sessionFactory.getCurrentSession().saveOrUpdate(student);
	}
	
	@Override
	public List<StudentGroup> getStudentByEmailGroupId(int emailGroupId) {
		Session session = sessionFactory.getCurrentSession();
        String hql = "from StudentGroup as t where t.emailGroup=:emailGroupId";
        Query query = session.createQuery(hql);
        query.setInteger("emailGroupId", emailGroupId);
        List<StudentGroup> result = (List<StudentGroup>) query.list();
        return result;
	}
	
	@Override
	public List<StudentGroup> getStudentByImageGroupId(int imageGroupId) {
		Session session = sessionFactory.getCurrentSession();
        String hql = "from StudentGroup as t where t.evaluationImage=:imageGroupId";
        Query query = session.createQuery(hql);
        query.setInteger("imageGroupId", imageGroupId);
        List<StudentGroup> result = (List<StudentGroup>) query.list();
        return result;
	}

	@Override
	public void clearStudentGroup() {
		Session session = sessionFactory.getCurrentSession();
        String hql = "update StudentGroup as t set t.status=1 where t.studentId<>:defaultStudentId";
        Query query = session.createQuery(hql);
        query.setString("defaultStudentId", "z1111112");
        query.executeUpdate();
	}

}
