package org.poscomp.eqclinic.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.AppointmentProgressDao;
import org.poscomp.eqclinic.domain.AppointmentProgress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "appointmentProgressDao")
public class AppointmentProgressDaoImpl implements AppointmentProgressDao {

    private static final Logger logger = Logger.getLogger(AppointmentProgressDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveAppointmentProgress(AppointmentProgress appPro) {
        sessionFactory.getCurrentSession().saveOrUpdate(appPro);
        logger.info("you have insert an application progress");
    }

    @Override
    public AppointmentProgress getAppointmentProgressByAptIdUserId(int aptId, String userType, int userId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from AppointmentProgress as ap where ap.aptid=:aptId and ap.type=:userType and ap.userId=:userId";
        Query query = session.createQuery(hql);
        query.setInteger("aptId", aptId);
        query.setString("userType", userType);
        query.setInteger("userId", userId);
        // AppointmentProgress result = (AppointmentProgress) query.uniqueResult();
        List<AppointmentProgress> results = (List<AppointmentProgress>) query.list();
        AppointmentProgress result = null;
        if(results.size()>0){
        	result = results.get(0);
        }
        return result;
    }

    @Override
    public AppointmentProgress getAppointmentProgressBySessionIdUserId(String sessionId, String userType, int userId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from AppointmentProgress as ap where ap.sessionId=:sessionId and"
                        + " ap.type=:userType and ap.userId=:userId";
        Query query = session.createQuery(hql);
        query.setString("sessionId", sessionId);
        query.setString("userType", userType);
        query.setInteger("userId", userId);
        // AppointmentProgress result = (AppointmentProgress) query.uniqueResult();
        List<AppointmentProgress> results = (List<AppointmentProgress>) query.list();
        AppointmentProgress result = null;
        if(results.size()>0){
        	result = results.get(0);
        }
        
        return result;
    }

    @Override
    public AppointmentProgress getAppointmentProgressById(int id) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from AppointmentProgress as ap where ap.id=:id";
        Query query = session.createQuery(hql);
        query.setInteger("id", id);
        AppointmentProgress result = (AppointmentProgress) query.uniqueResult();
        return result;
    }

    @Override
    public AppointmentProgress getAppointmentProgressByAptIdUserType(int aptId, String userType) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from AppointmentProgress as ap where ap.aptid=:aptId and ap.type=:userType";
        Query query = session.createQuery(hql);
        query.setInteger("aptId", aptId);
        query.setString("userType", userType);
        // AppointmentProgress result = (AppointmentProgress) query.uniqueResult();
        List<AppointmentProgress> results = (List<AppointmentProgress>) query.list();
        AppointmentProgress result = null;
        if(results.size()>0){
        	result = results.get(0);
        }
        return result;
    }

}
