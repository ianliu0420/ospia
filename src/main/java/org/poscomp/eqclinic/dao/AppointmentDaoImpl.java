package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.AppointmentDao;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.pojo.AppointmentSearchObject;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "appointmentDao")
public class AppointmentDaoImpl implements AppointmentDao {

    private static final Logger logger = Logger.getLogger(AppointmentDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveApp(Appointment app) {
        sessionFactory.getCurrentSession().saveOrUpdate(app);
        logger.info("you have insert an appointment");
    }

    @Override
    public List<Appointment> getDoctorApts(int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.doctorId=:doctorId";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }

    @Override
    public List<Appointment> getPatientApts(int patientId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.patientId=:patientId and a.status<>8 ";
        Query query = session.createQuery(hql);
        query.setInteger("patientId", patientId);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }

    @Override
    public Appointment getAppointmentBySessionId(String sessionId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.sessionId=:sessionId or a.doctorSessionIdBaseline=:sessionId or a.patientSessionIdBaseline=:sessionId";
        Query query = session.createQuery(hql);
        query.setString("sessionId", sessionId);
        Appointment result = (Appointment) query.uniqueResult();
        return result;
    }

    @Override
    public List<Appointment> getPatientAptsWholeDate(int patientId, String aptdate) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.patientId=:patientId and a.status<>8 and a.aptdate=:aptdate order by a.starttime";
        Query query = session.createQuery(hql);
        query.setInteger("patientId", patientId);
        query.setString("aptdate", aptdate);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }

    @Override
    public Appointment getAppointmentByAptId(int aptId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.aptid=:aptid";
        Query query = session.createQuery(hql);
        query.setInteger("aptid", aptId);
        Appointment apt = (Appointment) query.uniqueResult();
        return apt;
    }

    @Override
    public List<Appointment> getPatientPendingRequest(int patientId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.patientId=:patientId and a.status=2 order by a.createtime";
        Query query = session.createQuery(hql);
        query.setInteger("patientId", patientId);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }

    @Override
    public List<Appointment> getPatientNextApts(int patientId) {
        Session session = sessionFactory.getCurrentSession();
        long curentTime = System.currentTimeMillis();
        long periodStop = curentTime + 14 * 24 * 60 * 60 * 1000;

//        String hql = "from Appointment as a where a.patientId=:patientId and a.status=3 "
//                        + "and a.starttime>=:curentTime and a.starttime<=:periodStop order by a.starttime";
        String hql = "from Appointment as a where a.patientId=:patientId and a.status=3 and a.starttime<=:periodStop "
              + " order by a.starttime";
        
        Query query = session.createQuery(hql);
        query.setInteger("patientId", patientId);
//        query.setLong("curentTime", curentTime);
        query.setLong("periodStop", periodStop);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }

    @Override
    public List<Appointment> getDoctorNextApts(int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        long curentTime = System.currentTimeMillis();
        long periodStop = curentTime + 14 * 24 * 60 * 60 * 1000;

//        String hql = "from Appointment as a where a.doctorId=:doctorId and a.status=3 "
//                        + "and a.starttime>=:curentTime and a.starttime<=:periodStop order by a.starttime";
        String hql = "from Appointment as a where a.doctorId=:doctorId and a.status=3 and a.starttime<=:periodStop "
                + " order by a.starttime";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
//        query.setLong("curentTime", curentTime);
        query.setLong("periodStop", periodStop);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }

    @Override
    public List<Appointment> getDoctorRecentRequest(int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        long curentTime = System.currentTimeMillis();
        long periodStop = curentTime - 14 * 24 * 60 * 60 * 1000;

        String hql = "from Appointment as a where a.doctorId=:doctorId and a.createtime>=:periodStop "
                        + "and a.createtime<=:curentTime order by a.createtime";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        query.setLong("curentTime", curentTime);
        query.setLong("periodStop", periodStop);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }

    @Override
    public List<Appointment> getDoctorAptCancelled(int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.doctorId=:doctorId and (a.status=5 or a.status=6)";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }


    @Override
    public List<Appointment> getDoctorAptsAvailable(int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where (a.doctorId=:doctorId and a.status<>8) or (a.status=1 and a.starttime>:curTime)";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        query.setLong("curTime", System.currentTimeMillis());
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }

    @Override
    public List<Appointment> getDoctorAptsWholeDate(int doctorId, String aptdate) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where (a.doctorId=:doctorId or a.status=1) and a.status<>8 and a.aptdate=:aptdate order by a.starttime";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        query.setString("aptdate", aptdate);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }

    @Override
    public List<Appointment> getDoctorAptsStatus(int doctorId, int status) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.doctorId=:doctorId and a.status=:status";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        query.setInteger("status", status);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }

    @Override
    public Appointment getLatestAptByDoctorId(int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.doctorId=:doctorId and a.status=:status order by a.starttime";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        query.setInteger("status", 3);
        List<Appointment> list = (ArrayList<Appointment>) query.list();

        if (list != null && list.size() > 0) {
            return list.get(0);
        }

        return null;
    }

    @Override
    public Appointment getLatestAptByPatientId(int patientId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.patientId=:patientId and a.status=:status order by a.starttime";
        Query query = session.createQuery(hql);
        query.setInteger("patientId", patientId);
        query.setInteger("status", 3);
        List<Appointment> list = (ArrayList<Appointment>) query.list();

        if (list != null && list.size() > 0) {
            return list.get(0);
        }

        return null;
    }

    @Override
    public List<Appointment> findBetweenTwoTime(Long start, Long end) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.createtime>:start and a.createtime<:end and a.status=1 order by a.starttime";
        Query query = session.createQuery(hql);
        query.setLong("start", start);
        query.setLong("end", end);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }
    
    @Override
    public List<Appointment> findAllAvailabel(Long end) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.createtime<:end and a.endtime>:aptEnd and a.status=1 order by a.starttime";
        Query query = session.createQuery(hql);
        query.setLong("end", end);
        query.setLong("aptEnd", end);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }

    @Override
    public List<Appointment> getPreviousAptByDoctorId(int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.doctorId=:doctorId and ( a.status=8 or a.status=2 or a.status=3 or a.sessionType=1 or a.sessionType=2)";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;

    }

    @Override
    public List<Appointment> getAptByDate(String aptdate) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.status=3 and a.aptdate=:aptdate";
        Query query = session.createQuery(hql);
        query.setString("aptdate", aptdate);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
    }

	@Override
	public List<Appointment> getAvailableAptByTime(long time) {
		 Session session = sessionFactory.getCurrentSession();
	        String hql = "from Appointment as a where a.status=3 and starttime>:time";
	        Query query = session.createQuery(hql);
	        query.setLong("time", time);
	        List<Appointment> list = (ArrayList<Appointment>) query.list();
	        return list;
	}

	@Override
	public List<Appointment> searchAppointments(AppointmentSearchObject aso) {
		
		String[] searchTypes = aso.getSearchType().split(",");
		
		if("1".equals(searchTypes[0])){
			
			String hql = "from Appointment as a, Patient as p where a.patientId=p.patientid ";
			hql += 	" and ((a.status=1 and a.starttime>"+System.currentTimeMillis()+")";
			
			if("1".equals(searchTypes[1])){
				hql += " or a.status=2";
			}
			
			if("1".equals(searchTypes[2])){
				hql += " or a.status=3";
			}
			
			if("1".equals(searchTypes[3])){
				hql += " or a.status=8";
			}
			hql += " ) ";
			
			if(aso.getSearchContent()!=null && !"".equals(aso.getSearchContent())){
				if(aso.getSearchContentType()==1){
					return null;
				}else if(aso.getSearchContentType()==2){
					return null;
				}else if(aso.getSearchContentType()==3){
					hql += " and (concat(lower(p.firstname),' ',lower(p.lastname))= lower('"+aso.getSearchContent()+"') or lower(p.firstname) = lower('"+aso.getSearchContent()+"') or lower(p.lastname)=lower('"+aso.getSearchContent()+"'))";
				}
			}
			hql += " order by a.starttime";
			System.out.println(hql);
			
			Session session = sessionFactory.getCurrentSession();
	        Query query = session.createQuery(hql);
	        List<Appointment> list = (ArrayList<Appointment>) query.list();
	        return list;
			
		}else{
			
			String hql = "from Appointment as a, Patient as p, Doctor as d where a.doctorId=d.doctorId and a.patientId=p.patientid ";
			boolean hasChecked = false;
			
			if("1".equals(searchTypes[1])){
				if(hasChecked){
					hql += " or a.status=2";
				}else{
					hql += " and ( a.status=2";
				}
				hasChecked=true;
			}
			
			if("1".equals(searchTypes[2])){
				if(hasChecked){
					hql += " or a.status=3";
				}else{
					hql += " and ( a.status=3";
				}
				hasChecked=true;
			}
			
			if("1".equals(searchTypes[3])){
				if(hasChecked){
					hql += " or a.status=8";
				}else{
					hql += " and ( a.status=8";
				}
				hasChecked=true;
			}
			
			if(hasChecked){
				hql += " )";
			}		
			
			if(aso.getSearchContent()!=null && !"".equals(aso.getSearchContent())){
				if(aso.getSearchContentType()==1){
					hql += " and lower(d.userId)= lower('"+aso.getSearchContent()+"')";
				}else if(aso.getSearchContentType()==2){
					hql += " and (concat(lower(d.firstname),' ',lower(d.lastname))= lower('"+aso.getSearchContent()+"') or lower(d.firstname) = lower('"+aso.getSearchContent()+"') or lower(d.lastname)=lower('"+aso.getSearchContent()+"'))";
				}else if(aso.getSearchContentType()==3){
					hql += " and (concat(lower(p.firstname),' ',lower(p.lastname))= lower('"+aso.getSearchContent()+"') or lower(p.firstname) = lower('"+aso.getSearchContent()+"') or lower(p.lastname)=lower('"+aso.getSearchContent()+"'))";
				}
			}
			
			hql += " order by a.starttime";
			Session session = sessionFactory.getCurrentSession();
	        Query query = session.createQuery(hql);
	        List<Appointment> list = (ArrayList<Appointment>) query.list();
	        return list;
			
		}
		
		
		
	}

	@Override
	public List<Appointment> getPendingRequest24H(int from, int to) {
		Session session = sessionFactory.getCurrentSession();
		long currentTime = System.currentTimeMillis();
		long fromTime = from*24*60*60*1000L;
		long toTime = to*24*60*60*1000L;
		System.out.println(fromTime);
		System.out.println(toTime);
        String hql = "from Appointment as a where a.status=2 and (:currentTime-a.apptime)>:fromTime and (:currentTime-a.apptime)<:toTime";
        Query query = session.createQuery(hql);
        query.setLong("currentTime", currentTime);
        query.setLong("fromTime", fromTime);
        query.setLong("toTime", toTime);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
	}

	@Override
	public List<Appointment> getDoctorAptStatusAssessment(int doctorId, int status) {
		Session session = sessionFactory.getCurrentSession();
        String hql = "from Appointment as a where a.doctorId=:doctorId and a.sessionType=2 and a.status=:status";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        query.setInteger("status", status);
        List<Appointment> list = (ArrayList<Appointment>) query.list();
        return list;
	}

	@Override
	public List<Object> getAppointmentWithStudentPersonalFeedback(
			int patientId, Long start, Long end) {
		Session session = sessionFactory.getCurrentSession();
		
		// select * from appointment as a, s_answer as an where a.`status`=8 and a.sessionId=an.sessionId and an.surveyid=5;
        String hql = "from Appointment as ap, Answer as an where ap.patientId=:patientId and ap.status=8 and ap.sessionId=an.sessionId and an.survey.id=5 and ap.starttime>:start and ap.endtime<:end"; // 
        Query query = session.createQuery(hql);
        query.setInteger("patientId", patientId);
        query.setLong("start", start);
        query.setLong("end", end);
        List<Object> list = (ArrayList<Object>) query.list();
        return list;
	}

	
	
	
}
