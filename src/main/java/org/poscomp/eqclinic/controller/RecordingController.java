package org.poscomp.eqclinic.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.AppointmentProgress;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Video;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.processor.DownloadProcessor;
import org.poscomp.eqclinic.processor.DownloadProcessorBaseline;
import org.poscomp.eqclinic.processor.OpenTokProcessor;
import org.poscomp.eqclinic.processor.VideoStartProcessor;
import org.poscomp.eqclinic.processor.VideoStopProcessor;
import org.poscomp.eqclinic.service.interfaces.AppointmentProgressService;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.NoteService;
import org.poscomp.eqclinic.service.interfaces.VideoService;
import org.poscomp.eqclinic.util.SecurityTool;
import org.poscomp.eqclinic.util.TimeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.opentok.Archive;
import com.opentok.ArchiveProperties;
import com.opentok.OpenTok;
import com.opentok.TokenOptions;
import com.opentok.Archive.OutputMode;
import com.opentok.exception.OpenTokException;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class RecordingController {

    private static final Logger logger = Logger.getLogger(RecordingController.class);

    private static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";

    private static final int INTERVAL = 2000;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";

    @Value("#{config['opentok_apiKey']}")
    private String apiKey;

    @Value("#{config['opentok_apiSecret']}")
    private String apiSecret;

    private static OpenTok opentok;

    @Autowired
    private VideoService videoService;

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private AppointmentProgressService appointmentProgressService;

    @Autowired
    private NoteService noteService;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiSecret() {
        return apiSecret;
    }

    public void setApiSecret(String apiSecret) {
        this.apiSecret = apiSecret;
    }

    // the sp click the "start appointment" button on the home page
    @RequestMapping(value = PATIENTPATH + "/spbaseline/{aptId}")
    public String spBaselineWithAptId(HttpServletRequest request, @PathVariable int aptId) {
        Appointment apt = appointmentService.getAppointmentByAptId(aptId);
        String newPatientBaselineSessionId = OpenTokProcessor.generateSessionId();
        apt.setPatientSessionIdBaseline(newPatientBaselineSessionId);
        appointmentService.saveApp(apt);
        
        String token = null;
        if (apt != null) {
            String patientSessionIdCombined = apt.getSessionId();
            String patientSessionIdBaseline = apt.getPatientSessionIdBaseline();
            opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
            try {
                token = opentok.generateToken(patientSessionIdBaseline);
                request.setAttribute("patientApiKeyBaseline", apiKey);
                request.setAttribute("patientSessionIdBaseline", patientSessionIdBaseline);
                request.setAttribute("patientTokenBaseline", token);
                request.setAttribute("redpage", "start_sp");
                request.getSession().setAttribute("patientSessionIdCombined", patientSessionIdCombined);
                return "sp/13_SP_StartApt";
            } catch (OpenTokException e) {
                logger.error(e.getMessage(), e);
            }
        }

        return null;
    }

//    // the sp click the "start apt" button on the menu bar
//    @RequestMapping(value = PATIENTPATH + "/spbaseline")
//    public String spBaseline(HttpServletRequest request) {
//
//        // Patient patient = (Patient)
//        // request.getSession().getAttribute("loginedPatient");
//        Patient patient = (Patient) SecurityTool.getActiveUser();
//        Appointment apt = appointmentService.getLatestAptByPatientId(patient.getPatientid());
//        String token = null;
//        if (apt != null) {
//            String patientSessionId = apt.getSessionId();
//            String patientSessionIdBaseline = apt.getPatientSessionIdBaseline();
//            opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
//            try {
//                token = opentok.generateToken(patientSessionIdBaseline);
//                request.setAttribute("patientApiKeyBaseline", apiKey);
//                request.setAttribute("patientSessionIdBaseline", patientSessionIdBaseline);
//                request.setAttribute("patientTokenBaseline", token);
//                request.setAttribute("redpage", "start_sp");
//                request.getSession().setAttribute("patientSessionIdCombined", patientSessionId);
//                return "sp/13_SP_StartApt";
//            } catch (OpenTokException e) {
//                logger.error(e.getMessage(), e);
//            }
//        }
//
//        return null;
//    }

    // start recording: SP baseline
    @RequestMapping(value = PATIENTPATH + "/startrecordingbaselinesp/{patientSesionIdBaseline}")
    @ResponseBody
    public ReturnValue startArchiveBaselineSp(HttpServletRequest request, @PathVariable String patientSesionIdBaseline) {

        if (opentok == null) {
            System.out.println("opentok is null");
        }
        try {
            String patientArchiveIdBaseline = "";

            long curTime = System.currentTimeMillis();

            ExecutorService pool = Executors.newFixedThreadPool(1);
            Appointment apt = appointmentService.getAppointmentBySessionId(patientSesionIdBaseline);
            Archive patientArchiveBaseline =
                            opentok.startArchive(
                                            apt.getPatientSessionIdBaseline(),
                                            new ArchiveProperties.Builder()
                                                            .name("video_" + curTime + "_patient_baseline")
                                                            .hasAudio(true).hasVideo(true)
                                                            .outputMode(OutputMode.COMPOSED).build());
            
            if (patientArchiveBaseline != null) {
                patientArchiveIdBaseline = patientArchiveBaseline.getId();
                
                System.out.println("patient archieve id: " + patientArchiveIdBaseline);
            }
            pool.shutdown();
            apt.setArchIdPatBaseline(patientArchiveIdBaseline);
            appointmentService.saveApp(apt);

            ReturnValue rv = new ReturnValue();
            rv.setCode(1);
            rv.setContent(patientArchiveIdBaseline);
            return rv;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    // stop recording: SP baseline
    @RequestMapping(value = PATIENTPATH + "/stoprecordingbaselinesp/{patientSesionIdBaseline}")
    @ResponseBody
    public ReturnValue stopArchiveBaselineSp(HttpServletRequest request, @PathVariable String patientSesionIdBaseline) {
        if (opentok == null) {
            System.out.println("opentok is null");
        }
        try {
            Appointment apt = appointmentService.getAppointmentBySessionId(patientSesionIdBaseline);
            String patientArchiveIdBaseline = apt.getArchIdPatBaseline();

            if (patientArchiveIdBaseline != null) {

                apt.setArchIdPatBaseline(patientArchiveIdBaseline);
                appointmentService.saveApp(apt);

                Archive patientArchiveBaseline = opentok.stopArchive(patientArchiveIdBaseline);

                // insert a data to the db, archiveId is the unique key
                Video video = new Video();
                video.setName(patientArchiveBaseline.getName());
                video.setState(1);
                video.setArchiveId(patientArchiveIdBaseline);
                long baselineTime = Long.parseLong(patientArchiveBaseline.getName().split("_")[1]);
                video.setGenerateTime(baselineTime);
                SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
                Date baselineDate = new Date(baselineTime);
                video.setGenerateTimeStr(sdf.format(baselineDate));
                int patientId = apt.getPatientId();
                String patientName = apt.getPatientname();
                String doctorName = apt.getDoctorname();
                int doctorId = apt.getDoctorId();
                video.setPatientId(patientId);
                video.setPatientName(patientName);
                video.setDoctorId(doctorId);
                video.setDoctorName(doctorName);
                video.setSessionId(patientSesionIdBaseline);
                video.setVideoType(4);
                String[] aptDate = TimeTool.convertLongToString(apt.getStarttime()).split(" ");
                video.setAptDate(aptDate[0]);
                video.setAptTime(aptDate[1]);
                video.setSceCode(apt.getScenarioCode());
                videoService.saveVideo(video);

                // download the baseline file
                Thread thread =
                                new Thread(new DownloadProcessorBaseline(patientArchiveIdBaseline, videoService,
                                                appointmentService, noteService, patientSesionIdBaseline));
                thread.start();

                ReturnValue rv = new ReturnValue();
                rv.setCode(1);
                return rv;
            } else {
                ReturnValue rv = new ReturnValue();
                rv.setCode(1);
                return rv;
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            ReturnValue rv = new ReturnValue();
            rv.setCode(0);
            return rv;
        }
    }

    // the student click the "start appointment" button on the home page
    @RequestMapping(value = DOCTORPATH + "/start_stu/{aptId}", method = {RequestMethod.GET})
    public String redirect_start_stu(HttpServletRequest request, @PathVariable int aptId) {
        Appointment apt = appointmentService.getAppointmentByAptId(aptId);

        Integer finishedPreSurvey = apt.getFinishedPreSurvey();

        if (finishedPreSurvey != null && finishedPreSurvey == 1) {
            request.getSession().setAttribute("doctorAptId", apt.getAptid());
            request.getSession().setAttribute("doctorSessionIdCombined", apt.getSessionId());
            return "redirect:" + DOCTORPATH + "/stubaseline";
        } else {
            request.setAttribute("redpage", "start_stu");
            request.getSession().setAttribute("doctorAptId", apt.getAptid());
            request.getSession().setAttribute("doctorSessionIdCombined", apt.getSessionId());
            request.setAttribute("surveyId", 3);
            request.setAttribute("ispreview", 0);
            request.setAttribute("nextpage", DOCTORPATH + "/stubaseline");
            return "stu/20_Stu_StartApt_PreQues";
        }
    }

    // the student click the "start apt" button on the home page
    @RequestMapping(value = DOCTORPATH + "/start_stu", method = {RequestMethod.GET})
    public String redirect_start_stuFromStart(HttpServletRequest request) {
        Doctor doc = (Doctor) SecurityTool.getActiveUser();
        Appointment apt = appointmentService.getLatestAptByDoctorId(doc.getDoctorId());
        request.getSession().setAttribute("doctorSessionIdCombined", apt.getSessionId());
        request.setAttribute("redpage", "start_stu");
        request.getSession().setAttribute("doctorAptId", apt.getAptid());
        return "stu/20_Stu_StartApt_PreQues";
    }

    // go into the baseline page
    @RequestMapping(value = DOCTORPATH + "/stubaseline")
    public String stuBaselineWithAptId(HttpServletRequest request) {
        Integer aptId = (Integer) request.getSession().getAttribute("doctorAptId");
        Appointment apt = appointmentService.getAppointmentByAptId(aptId);
        String newDoctorBaselineSessionId = OpenTokProcessor.generateSessionId();
        apt.setDoctorSessionIdBaseline(newDoctorBaselineSessionId);
        appointmentService.saveApp(apt);
        
        String token = null;
        if (apt != null) {
            String doctorSessionId = apt.getSessionId();
            String doctorSessionIdBaseline = apt.getDoctorSessionIdBaseline();
            opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
            try {
                token = opentok.generateToken(doctorSessionIdBaseline);
                request.setAttribute("doctorApiKeyBaseline", apiKey);
                request.setAttribute("doctorSessionIdBaseline", doctorSessionIdBaseline);
                request.setAttribute("doctorTokenBaseline", token);
                request.setAttribute("redpage", "start_stu");
                // request.getSession().setAttribute("doctorSessionIdCombined",
                // doctorSessionId);
                return "stu/21_Stu_StartApt_Init";
            } catch (OpenTokException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return null;
    }

    // start recording: SP baseline
    @RequestMapping(value = DOCTORPATH + "/startrecordingbaselinestu/{doctorSesionIdBaseline}")
    @ResponseBody
    public ReturnValue startRecordingBaselineStu(HttpServletRequest request, @PathVariable String doctorSesionIdBaseline) {
        if (opentok == null) {
            System.out.println("opentok is null");
        }
        try {
            String doctorArchiveIdBaseline = "";

            long curTime = System.currentTimeMillis();

            ExecutorService pool = Executors.newFixedThreadPool(1);
            Appointment apt = appointmentService.getAppointmentBySessionId(doctorSesionIdBaseline);

            Archive doctorArchiveBaseline =
                            opentok.startArchive(apt.getDoctorSessionIdBaseline(), new ArchiveProperties.Builder()
                                            .name("video_" + curTime + "_doctor_baseline").hasAudio(true)
                                            .hasVideo(true).outputMode(OutputMode.COMPOSED).build());

            if (doctorArchiveBaseline != null) {
                doctorArchiveIdBaseline = doctorArchiveBaseline.getId();
                System.out.println("doctor archieve id: " + doctorArchiveIdBaseline);
            }
            pool.shutdown();
            apt.setArchIdDocBaseline(doctorArchiveIdBaseline);
            appointmentService.saveApp(apt);
            
            ReturnValue rv = new ReturnValue();
            rv.setCode(1);
            rv.setContent(doctorArchiveIdBaseline);
            return rv;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    // stop recording: SP baseline
    @RequestMapping(value = DOCTORPATH + "/stoprecordingbaselinestu/{doctorSesionIdBaseline}")
    @ResponseBody
    public ReturnValue stopArchiveBaselineStu(HttpServletRequest request, @PathVariable String doctorSesionIdBaseline) {

        if (opentok == null) {
            System.out.println("opentok is null");
        }
        try {
            Appointment apt = appointmentService.getAppointmentBySessionId(doctorSesionIdBaseline);
            String doctorArchiveIdBaseline = apt.getArchIdDocBaseline();
            
            if (doctorArchiveIdBaseline != null) {

                apt.setArchIdDocBaseline(doctorArchiveIdBaseline);
                appointmentService.saveApp(apt);

                Archive doctorArchiveBaseline = opentok.stopArchive(doctorArchiveIdBaseline);

                // insert a data to the db, archiveId is the unique key
                Video video = new Video();
                video.setName(doctorArchiveBaseline.getName());
                video.setState(1);
                video.setArchiveId(doctorArchiveIdBaseline);
                long baselineTime = Long.parseLong(doctorArchiveBaseline.getName().split("_")[1]);
                video.setGenerateTime(baselineTime);
                SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
                Date baselineDate = new Date(baselineTime);
                video.setGenerateTimeStr(sdf.format(baselineDate));
                int patientId = apt.getPatientId();
                String patientName = apt.getPatientname();
                String doctorName = apt.getDoctorname();
                int doctorId = apt.getDoctorId();
                video.setPatientId(patientId);
                video.setPatientName(patientName);
                video.setDoctorId(doctorId);
                video.setDoctorName(doctorName);
                video.setSessionId(doctorSesionIdBaseline);
                video.setVideoType(4);
                String[] aptDate = TimeTool.convertLongToString(apt.getStarttime()).split(" ");
                video.setAptDate(aptDate[0]);
                video.setAptTime(aptDate[1]);
                video.setSceCode(apt.getScenarioCode());
                videoService.saveVideo(video);

                // download the baseline file
                Thread thread =
                                new Thread(new DownloadProcessorBaseline(doctorArchiveIdBaseline, videoService,
                                                appointmentService, noteService, doctorSesionIdBaseline));
                thread.start();

                ReturnValue rv = new ReturnValue();
                rv.setCode(1);
                return rv;
            } else {
                ReturnValue rv = new ReturnValue();
                rv.setCode(0);
                return rv;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            ReturnValue rv = new ReturnValue();
            rv.setCode(0);
            return rv;
        }
    }

    // this controller init the session (first user come)
    @RequestMapping(value = DOCTORPATH + "/init")
    public String initSession(HttpServletRequest request) {
        String token = null;
        opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
        String sessionId = (String) request.getSession().getAttribute("doctorSessionIdCombined");

        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        apt.setDoctorLoginAt(System.currentTimeMillis());
        appointmentService.saveApp(apt);

        String connectionMetadata = "doctor";
        try {

            TokenOptions tokenOpts = new TokenOptions.Builder().data(connectionMetadata).build();
            token = opentok.generateToken(sessionId, tokenOpts);

            request.setAttribute("apiKey", apiKey);
            request.setAttribute("sessionId", sessionId);
            request.setAttribute("token", token);
            request.setAttribute("redpage", "start_stu");
            return "stu/21_Stu_StartApt_Init_real";
        } catch (OpenTokException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    // this is the controller to join a conference
    @RequestMapping(value = PATIENTPATH + "/host")
    public String host(HttpServletRequest request) {

        String token = null;
        opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
        String sessionId = (String) request.getSession().getAttribute("patientSessionIdCombined");
        String connectionMetadata = "patient";

        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        apt.setPatientLoginAt(System.currentTimeMillis());
        appointmentService.saveApp(apt);

        try {

            TokenOptions tokenOpts = new TokenOptions.Builder().data(connectionMetadata).build();
            token = opentok.generateToken(sessionId, tokenOpts);

            request.setAttribute("apiKey", apiKey);
            request.setAttribute("sessionId", sessionId);
            request.setAttribute("token", token);
            request.setAttribute("redpage", "start_sp");
            request.setAttribute("apt", apt);
            return "sp/13_SP_StartApt_real";
        } catch (OpenTokException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    @RequestMapping(value = "unloadchatroom/{sessionIdStr}")
    @ResponseBody
    public ReturnValue unloadChatRoom(HttpServletRequest request, @PathVariable String sessionIdStr) {

        String sessionId = "";
        String[] info = sessionIdStr.split("__");
        int type = 1;
        if (info != null && info.length > 1) {
            sessionId = info[0];
            type = Integer.parseInt(info[1]);
        }

        Appointment app = appointmentService.getAppointmentBySessionId(sessionId);
        if (type == 1) {
            app.setPatientRecTime(null);
            app.setDoctorRecTime(null);
            appointmentService.saveApp(app);
            System.out.println("patient unload");
        } else {
            app.setPatientRecTime(null);
            app.setDoctorRecTime(null);
            appointmentService.saveApp(app);
            System.out.println("doctor unload");
        }

        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        return rv;
    }

    @RequestMapping(value = "start/{sessionIdStr}")
    @ResponseBody
    public String[] startArchive(HttpServletRequest request, @PathVariable String sessionIdStr) {

        String sessionId = "";
        String[] info = sessionIdStr.split("__");
        int type = 1;
        if (info != null && info.length > 1) {
            sessionId = info[0];
            type = Integer.parseInt(info[1]);
        }

        long curTime = System.currentTimeMillis();

        if (opentok == null) {
            System.out.println("opentok is null");
        }
        try {
            long timeStamp = System.currentTimeMillis();
            Appointment app = appointmentService.getAppointmentBySessionId(sessionId);

            String[] boolArray = null;
            if (type == 1) { // patient recording time

                app.setPatientRecTime(curTime);
                if (app.getDoctorRecTime() != null) {

                    // here we start archiving patient stream
                    String combinedArchiveId = "";

                    // ExecutorService pool = Executors.newFixedThreadPool(3);
                    // VideoStartProcessor combinedStarter = new
                    // VideoStartProcessor(opentok, sessionId, "video_"
                    // + timeStamp);
                    // Future combineFuture = pool.submit(combinedStarter);
                    //
                    // Archive archive = (Archive) combineFuture.get();
                    //
                    // if (archive != null) {
                    // combinedArchiveId = archive.getId();
                    // }
                    // pool.shutdown();

                    Archive archive =
                                    opentok.startArchive(
                                                    sessionId,
                                                    new ArchiveProperties.Builder().name("video_" + timeStamp)
                                                                    .hasAudio(true).hasVideo(true)
                                                                    .outputMode(OutputMode.INDIVIDUAL).build());
                    if (archive != null) {
                        combinedArchiveId = archive.getId();
                        app.setArchId(combinedArchiveId);
                    } else {
                        combinedArchiveId = app.getArchId();
                    }

                    long recordStartTime = System.currentTimeMillis();
                    app.setStartRecTime(recordStartTime);
                    boolArray = new String[] {combinedArchiveId};
                }
                appointmentService.saveApp(app);
            } else if (type == 2) { // student recording time

                app.setDoctorRecTime(curTime);

                if (app.getPatientRecTime() != null) {

                    String combinedArchiveId = "";

                    // ExecutorService pool = Executors.newFixedThreadPool(3);
                    // VideoStartProcessor combinedStarter = new
                    // VideoStartProcessor(opentok, sessionId, "video_" +
                    // timeStamp);
                    // Future combineFuture = pool.submit(combinedStarter);
                    // Archive archive = (Archive) combineFuture.get();
                    // if (archive != null) {
                    // combinedArchiveId = archive.getId();
                    // }
                    // pool.shutdown();

                    Archive archive =
                                    opentok.startArchive(
                                                    sessionId,
                                                    new ArchiveProperties.Builder().name("video_" + timeStamp)
                                                                    .hasAudio(true).hasVideo(true)
                                                                    .outputMode(OutputMode.INDIVIDUAL).build());
                    if (archive != null) {
                        combinedArchiveId = archive.getId();
                        app.setArchId(combinedArchiveId);
                    } else {
                        combinedArchiveId = app.getArchId();
                    }

                    long recordStartTime = System.currentTimeMillis();
                    app.setStartRecTime(recordStartTime);
                    boolArray = new String[] {combinedArchiveId};
                }
                appointmentService.saveApp(app);
            }
            return boolArray;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }

    }

    @RequestMapping(value = "stop/{sessionIdStr}")
    @ResponseBody
    public ReturnValue stopArchive(HttpServletRequest request, @PathVariable String sessionIdStr) {

        long curTime = System.currentTimeMillis();

        if (opentok == null) {
            System.out.println("opentok is null");
        }
        Appointment app = appointmentService.getAppointmentBySessionId(sessionIdStr);

        int patientId = app.getPatientId();
        String patientName = app.getPatientname();
        int doctorId = app.getDoctorId();
        String doctorName = app.getDoctorname();
        int aptId = app.getAptid();

        // create the appointment progress first
        String userType = "doctor";
        AppointmentProgress doctorAp =
                        appointmentProgressService.getAppointmentProgressByAptIdUserType(aptId, userType);
        if (doctorAp == null) {
            // record doctor's progress
            doctorAp = new AppointmentProgress();
            doctorAp.setAptid(app.getAptid());
            doctorAp.setHasFinish(0);
            doctorAp.setLastOperationAt(curTime);
            doctorAp.setSessionId(sessionIdStr);
            doctorAp.setStage(0);
            doctorAp.setType("doctor");
            doctorAp.setUserId(doctorId);
            appointmentProgressService.saveAppointmentProgress(doctorAp);
        }

        userType = "patient";
        AppointmentProgress patientAp =
                        appointmentProgressService.getAppointmentProgressByAptIdUserType(aptId, userType);
        if (patientAp == null) {
            // record patient's progress
            patientAp = new AppointmentProgress();
            patientAp.setAptid(app.getAptid());
            patientAp.setHasFinish(0);
            patientAp.setLastOperationAt(curTime);
            patientAp.setSessionId(sessionIdStr);
            patientAp.setStage(0);
            patientAp.setType("patient");
            patientAp.setUserId(patientId);
            appointmentProgressService.saveAppointmentProgress(patientAp);
        }

        String archiveId = app.getArchId();

        if (archiveId == null || "".equals(archiveId)) {
            ReturnValue rv = new ReturnValue();
            rv.setCode(0);
            rv.setContent("The recording is not started, please wait for a moment.");
            return rv;
        }

        int hasDownload = app.getHasDonwload();

        if (hasDownload == 0) {

            app.setHasDonwload(1);
            app.setArchId(archiveId);
            app.setStopRecTime(curTime);
            // set the appointment as "finish recording"
            app.setStatus(8);
            appointmentService.saveApp(app);

            // ExecutorService pool = Executors.newFixedThreadPool(3);
            // VideoStopProcessor combinedStoper = new
            // VideoStopProcessor(opentok, archiveId);
            // Future combineFuture = pool.submit(combinedStoper);
            // Archive archive = (Archive) combineFuture.get();
            // pool.shutdown();

            Archive archive = null;
            try {
                archive = opentok.stopArchive(archiveId);
            } catch (OpenTokException e) {

//                Video video = new Video();
//                video.setName("");
//                video.setState(-1);
//                video.setArchiveId(archiveId);
//                video.setGenerateTime(0L);
//                video.setGenerateTimeStr("");
//                video.setPatientId(patientId);
//                video.setPatientName(patientName);
//                video.setDoctorId(doctorId);
//                video.setDoctorName(doctorName);
//                video.setSessionId(app.getSessionId());
//                video.setVideoType(1);
//                String[] aptDate = TimeTool.convertLongToString(app.getStarttime()).split(" ");
//                video.setAptDate(aptDate[0]);
//                video.setAptTime(aptDate[1]);
//                video.setSceCode(app.getScenarioCode());
//                videoService.saveVideo(video);

                logger.error(e.getMessage(), e);
                ReturnValue rv = new ReturnValue();
                rv.setCode(0);
                return rv;
            }

            // insert a data to the database, archiveId is the unique key, this
            // section id storing the combine video
            Video video = new Video();
            video.setName(archive.getName());
            video.setState(1);
            video.setArchiveId(archiveId);
            long combinedArchiveTime = Long.parseLong(archive.getName().split("_")[1]);
            video.setGenerateTime(combinedArchiveTime);
            SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
            Date combinedArchiveDate = new Date(combinedArchiveTime);
            video.setGenerateTimeStr(sdf.format(combinedArchiveDate));
            video.setPatientId(patientId);
            video.setPatientName(patientName);
            video.setDoctorId(doctorId);
            video.setDoctorName(doctorName);
            video.setSessionId(app.getSessionId());
            video.setVideoType(1);
            String[] aptDate = TimeTool.convertLongToString(app.getStarttime()).split(" ");
            video.setAptDate(aptDate[0]);
            video.setAptTime(aptDate[1]);
            video.setSceCode(app.getScenarioCode());
            video.setEmailLevel(0);
            video.setHasSendEmail(0);
            videoService.saveVideo(video);

            // download the video
            Thread thread =
                            new Thread(new DownloadProcessor(archiveId, app.getArchIdPatBaseline(),
                                            app.getArchIdDocBaseline(), videoService, appointmentService, noteService,
                                            sessionIdStr));
            thread.start();
        }

        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        return rv;

    }

    @RequestMapping(value = "requestrecodingtime/{sessionIdStr}")
    @ResponseBody
    public ReturnValue requestRecodingTime(HttpServletRequest request, @PathVariable String sessionIdStr) {

        Appointment app = appointmentService.getAppointmentBySessionId(sessionIdStr);

        Long startRecTime = 0L;
        if (app.getStartRealConversationTime() != null) {
            startRecTime = app.getStartRealConversationTime();
        } else {
            startRecTime = app.getStartRecTime();
        }
        ReturnValue rv = new ReturnValue();
        if (startRecTime == null || startRecTime == 0) {
            rv.setCode(0);
            rv.setContent("");
        } else {
            rv.setCode(1);
            double diff = Math.floor((System.currentTimeMillis() - startRecTime) / 1000.0f / 60);
            if (diff > 2) {
                rv.setContent((int) diff + " minutes");
            } else {
                rv.setContent((int) diff + " minute");
            }
        }

        return rv;
    }


    @RequestMapping(value = DOCTORPATH +"/reviewedrecordingupdate/{videoId}/{section}")
    @ResponseBody
    public ReturnValue reviewedRecordingUpdate(HttpServletRequest request, @PathVariable int videoId,
                    @PathVariable int section) {

        Video video = videoService.getVideoById(videoId);
        if (section == 1) {
            video.setReviewComments(1);
        } else if (section == 2) {
            video.setReviewSoca(1);
        } else if (section == 3) {
            video.setReviewBehav(1);
        } else if (section == 4) {
            // video.setReviewSurvey(1);
        } else if (section == 5) {
            video.setReviewTutorSoca(1);
        }
        videoService.saveVideo(video);
        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        return rv;
    }



}
