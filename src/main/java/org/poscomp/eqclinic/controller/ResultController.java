package org.poscomp.eqclinic.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.pojo.ResultSOCA;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.ApplicationService;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.poscomp.eqclinic.util.EmailTool;
import org.poscomp.eqclinic.util.SMSTool;
import org.poscomp.eqclinic.util.SecurityTool;
import org.poscomp.survey.domain.Answer;
import org.poscomp.survey.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class ResultController {

    private static final Logger logger = Logger.getLogger(ResultController.class);

    @Autowired
    private AnswerService answerService;
    
    @Autowired
    private AppointmentService appointmentService;
    
    @Autowired
    private DoctorService doctorService;
    
    
    @RequestMapping(value = "/allsoca", method = { RequestMethod.GET })
    public String applyAppoitment(HttpServletRequest request) throws Exception{

        List<Answer> socas = answerService.loadAnswer();
        List<ResultSOCA> results = new ArrayList<ResultSOCA>();
        
        
        for (Answer answer : socas) {
            
            String sessionId = answer.getSessionId();
            
            Appointment app = appointmentService.getAppointmentBySessionId(sessionId);
            
            if(app!=null){
                int doctorId = app.getDoctorId();
                
                Doctor doctor = doctorService.getDoctorByDoctorId(doctorId);
                
                if(doctor==null){
                    continue;
                }
                
                
                String answerContent = answer.getContent();
                
                JSONArray jsonArray = new JSONArray(answerContent);
                
                ResultSOCA result = new ResultSOCA();
                
                
                result.setName(doctor.getFirstname()+" "+doctor.getLastname());
                result.setzId(doctor.getUserId());
                int total = 0;
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject json_data = jsonArray.getJSONObject(i);
                    String qid= json_data.getString("qid");
                    String value= json_data.getString("qvalue");
                    
                    if("F".equals(value)){
                        total += 1;
                    }else if("P-".equals(value)){
                        total += 2;
                    }else if("P".equals(value)){
                        total += 3;
                    }else if("P+".equals(value)){
                        total += 4;
                    }
                    
                    if("1".equals(qid)){
                        result.setQ1(value);
                    }else if("4".equals(qid)){
                        result.setQ2(value);
                    }else if("7".equals(qid)){
                        result.setQ3(value);
                    }else if("10".equals(qid)){
                        result.setQ4(value);
                    }
                    
                    if("2".equals(qid)){
                        result.setReason1(value);
                    }else if("5".equals(qid)){
                        result.setReason2(value);
                    }else if("8".equals(qid)){
                        result.setReason3(value);
                    }else if("11".equals(qid)){
                        result.setReason4(value);
                    }
                    
                }
                result.setTotal(total);
                
                results.add(result);
            }
            
        }
        
        request.setAttribute("results", results);
        
        return "survey/listresult";
    }

}
