package org.poscomp.eqclinic.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Note;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Teacher;
import org.poscomp.eqclinic.domain.Video;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.NoteService;
import org.poscomp.eqclinic.service.interfaces.VideoService;
import org.poscomp.eqclinic.util.SecurityTool;
import org.poscomp.eqclinic.util.StringTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class NoteController {

    private static final Logger logger = Logger.getLogger(NoteController.class);

    @Value("#{config['opentok_apiKey']}")
    private String apiKey;

    @Value("#{config['opentok_apiSecret']}")
    private String apiSecret;

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private NoteService noteService;

    @Autowired
    private VideoService videoService;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";
    private static final String TUTORPATH = "/tut";

    @RequestMapping(value = TUTORPATH + "/addnotetutor", method = { RequestMethod.POST },
                    headers = { "Content-type=application/json" })
    @ResponseBody
    public ReturnValue addNoteTutor(HttpServletRequest request, @RequestBody Note note) {
        Teacher teacher = (Teacher) SecurityTool.getActiveUser();
        ReturnValue rv = new ReturnValue();
        String noteContent = StringTool.filterHtml(note.getNoteContent());
        note.setNoteContent(noteContent);
        note.setNoterId(teacher.getTeacherId());
        note.setNoterType(1);
        int result = noteService.saveNoteTutor(note);
        if (result == 1) {
            rv.setCode(1);
        } else {
            rv.setCode(0);
            rv.setContent("Comment fail, please submit again.");
        }

        return rv;
    }

    @RequestMapping(value = PATIENTPATH + "/addnotepatient", method = { RequestMethod.POST },
                    headers = { "Content-type=application/json" })
    @ResponseBody
    public ReturnValue addNotePatient(HttpServletRequest request, @RequestBody Note note) {

        // Patient patient = (Patient) request.getSession().getAttribute("loginedPatient");
        Patient patient = (Patient) SecurityTool.getActiveUser();
        String sessionId = "";
        if (note != null) {
            sessionId = note.getSessionId();
        }
        String noteContent = StringTool.filterHtml(note.getNoteContent());
        note.setNoteContent(noteContent);
        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        
        if(apt==null || apt.getStartRecTime()==null){
        	ReturnValue rv = new ReturnValue();
            rv.setCode(-1);
            rv.setContent("Sorry, you can only leave notes when the student appears");
            return rv;
        }
        
        int result = noteService.saveNotePatient(note, apt, patient);
        ReturnValue rv = new ReturnValue();
        rv.setCode(result);

        return rv;
    }

    @RequestMapping(value = DOCTORPATH + "/loadsessionnote2/{archiveId}", method = { RequestMethod.POST })
    @ResponseBody
    public List<Note> loadSessionNote2(HttpServletRequest request, @PathVariable String archiveId) {

        Video video = videoService.getVideoByArchiveId(archiveId);
        String sessionId = video.getSessionId();
        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        long startRecordingTime = apt.getStartRecTime();
        long stopRecordingTime = apt.getStopRecTime();
        int lengthInMunite = (int) ((stopRecordingTime - startRecordingTime) / 60000.0) + 1;

        List<Note> allNotes = noteService.getNoteBySessionId(sessionId);
        List<Note> noters = new ArrayList<Note>();

        for (Note note : allNotes) {
            int noterType = note.getNoterType();
            int noterId = note.getNoterId();

            boolean hasThisNoter = false;
            for (Note noter : noters) {

                if (noter.getNoterId() == noterId && noter.getNoterType() == noterType) {
                    hasThisNoter = true;
                }
            }

            if (hasThisNoter == false) {
                noters.add(note);
            }
        }

        Note noterType1 = null;
        Note noterType2 = null;

        for (Note noter : noters) {

            if (noter.getNoterType() == 1) {
                noterType1 = noter;
            } else if (noter.getNoterType() == 2) {
                noterType2 = noter;
            }
        }

        List<Note> notesStu = new ArrayList<Note>();
        List<Note> notesTut = new ArrayList<Note>();

        if (noterType1 != null && video.getAllowTutorNotes()==1) {
            notesStu = noteService.getNoteBySessionIdNoter(sessionId, 1, noterType1.getNoterId());
        }
        if (noterType2 != null) {
            notesTut = noteService.getNoteBySessionIdNoter(sessionId, 2, noterType2.getNoterId());
        }

        List<Note> newList = new ArrayList<Note>(notesStu);
        newList.addAll(notesTut);

        double videoLength = lengthInMunite;
        Collections.sort(newList);

        return newList;

    }

    @RequestMapping(value = DOCTORPATH + "/loadsessionnote/{archiveId}", method = { RequestMethod.GET })
    public String loadSessionNote(HttpServletRequest request, @PathVariable String archiveId) {

        Video video = videoService.getVideoByArchiveId(archiveId);
        
        if(video!=null){
            String sessionId = video.getSessionId();
            Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
            Long generateTime = video.getGenerateTime();
            String combinedVideoName = "video_" + generateTime;
            if(apt!=null){
                long startRecordingTime = apt.getStartRecTime();
                long stopRecordingTime = apt.getStopRecTime();
                int lengthInMunite = (int) ((stopRecordingTime - startRecordingTime) / 60000.0) + 1;
                request.setAttribute("videoLength", lengthInMunite);
            }
            request.setAttribute("redpage", "cons_stu");
            request.setAttribute("videoName", combinedVideoName);
            request.setAttribute("archiveId", archiveId);
            request.setAttribute("sessionId", video.getSessionId());
            request.setAttribute("apt", apt);
        }
        
        return "stu/35_Stu_Notes";
    }

    @RequestMapping(value = TUTORPATH + "/loadtutornote", method = { RequestMethod.POST },
                    headers = { "Content-type=application/json" })
    @ResponseBody
    public List<Note> loadTutorNote(HttpServletRequest request, @RequestBody Note note) {

        // Teacher teacher = (Teacher) request.getSession().getAttribute("loginedTeacher");
        Teacher teacher = (Teacher) SecurityTool.getActiveUser();
        int noterType = 1;
        List<Note> notes = noteService.getNoteBySessionIdNoter(note.getSessionId(), noterType, teacher.getTeacherId());
        return notes;
    }

    @RequestMapping(value = TUTORPATH + "/deletecomment/{noteId}", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue deleteComment(HttpServletRequest request, @PathVariable int noteId) {

        Note note = noteService.getNoteById(noteId);
        noteService.deleteNote(note);
        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        rv.setContent("Delete success");
        return rv;
    }

}
