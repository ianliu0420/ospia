package org.poscomp.eqclinic.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.CalibrationSurveyAnswer;
import org.poscomp.eqclinic.domain.CalibrationUser;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Teacher;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.filter.CSRFTokenManager;
import org.poscomp.eqclinic.service.interfaces.CalibrationUserService;
import org.poscomp.eqclinic.util.SecurityTool;
import org.poscomp.survey.domain.Answer;
import org.poscomp.survey.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class PageController {

    private static final Logger logger = Logger.getLogger(PageController.class);

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";
    private static final String TUTORPATH = "/tut";

    @Autowired
    private AnswerService answerService;
    
    @Autowired
    private CalibrationUserService calibrationUserService;
    
    @RequestMapping(value = PATIENTPATH + "/uhome_sp", method = { RequestMethod.GET })
    public String redirect_uhome_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "uhome_sp");
        return "sp/01_SP_home";
    }

    @RequestMapping(value = PATIENTPATH + "/uregister_sp", method = { RequestMethod.GET })
    public String redirect_uregister_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "uregister_sp");
        return "sp/06_SP_Regist";
    }

    @RequestMapping(value = PATIENTPATH + "/ulogin_sp", method = { RequestMethod.GET })
    public String redirect_ulogin_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "ulogin_sp");
        return "sp/02_SP_login";
    }

    @RequestMapping(value = PATIENTPATH + "/home_sp", method = { RequestMethod.GET })
    public String redirect_home_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "home_sp");
        return "sp/07_SP_HomeRegistered";
    }
    
    @RequestMapping(value = PATIENTPATH + "/homeagree_sp", method = { RequestMethod.GET })
    public String redirect_homeagree_sp(HttpServletRequest request) {
        Patient pat = (Patient) SecurityTool.getActiveUser();
        if(request.getSession().getAttribute("homeloaded")==null){
            request.getSession().setAttribute("homeloaded", 0);
        }else{
            int homeLoadedCounter = (Integer)request.getSession().getAttribute("homeloaded");
            homeLoadedCounter++;
            request.getSession().removeAttribute("homeloaded");
            request.getSession().setAttribute("homeloaded", homeLoadedCounter);
        }
        request.setAttribute("redpage", "home_sp");
        return "sp/07_SP_HomeRegistered";
    }
    

    @RequestMapping(value = PATIENTPATH + "/info_sp", method = { RequestMethod.GET })
    public String redirect_info_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "info_sp");
        return "sp/03_SP_ProgInfo2";
    }

    @RequestMapping(value = PATIENTPATH + "/cal_sp", method = { RequestMethod.GET })
    public String redirect_cal_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "cal_sp");
        return "sp/12_SP_MyCalen";
    }

    @RequestMapping(value = PATIENTPATH + "/scedb_sp", method = { RequestMethod.GET })
    public String redirect_scedb_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "scedb_sp");
        return "sp/11_SP_ScenarioDB";
    }

    @RequestMapping(value = PATIENTPATH + "/soca_sp", method = { RequestMethod.GET })
    public String redirect_soca_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "start_sp");
        request.setAttribute("surveyId", 1);
        request.setAttribute("ispreview", 0);
        // request.setAttribute("nextpage", PATIENTPATH + "/autonomy_sp");
        request.setAttribute("nextpage", PATIENTPATH + "/feedback_sp");
        return "sp/14_SP_SOCA";
    }
    
    @RequestMapping(value = PATIENTPATH + "/autonomy_sp", method = { RequestMethod.GET })
    public String redirect_autonomy_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "start_sp");
        request.setAttribute("surveyId", 12);
        request.setAttribute("ispreview", 0);
        request.setAttribute("nextpage", PATIENTPATH + "/feedback_sp");
        return "sp/14_SP_Autonomy";
    }

    @RequestMapping(value = PATIENTPATH + "/feedback_sp", method = { RequestMethod.GET })
    public String redirect_feedback_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "start_sp");
        request.setAttribute("surveyId", 2);
        request.setAttribute("ispreview", 0);
        request.setAttribute("nextpage", PATIENTPATH + "/thanks_sp");
        return "sp/15_SP_OSPIAFeedback";
    }

    @RequestMapping(value = PATIENTPATH + "/thanks_sp", method = { RequestMethod.GET })
    public String redirect_changepwd_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "start_sp");
        return "sp/16_SP_Thanks";
    }
    
    
    @RequestMapping(value = PATIENTPATH + "/appropriate/{sessionId}", method = { RequestMethod.GET })
    public String redirect_appropriate_sp(HttpServletRequest request, @PathVariable String sessionId) {
        request.setAttribute("redpage", "start_sp");
        request.setAttribute("sessionId", sessionId);
        return "sp/18_SP_Appropriate";
    }
    
    @RequestMapping(value = PATIENTPATH + "/inappropriate", method = { RequestMethod.GET })
    public String redirect_inappropriate_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "start_sp");
        return "sp/19_SP_Inappropriate";
    }
    
    
    
    
    @RequestMapping(value = PATIENTPATH + "/changepwd", method = { RequestMethod.GET })
    public String redirect_thanks_sp(HttpServletRequest request) {
        request.setAttribute("redpage", "home_sp");
        return "sp/17_SP_ChangePwd";
    }

    @RequestMapping(value = DOCTORPATH + "/ulogin_stu", method = { RequestMethod.GET })
    public String redirect_ulogin_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "");
        return "stu/17_Stu_login";
    }

    @RequestMapping(value = DOCTORPATH + "/uhome_stu", method = { RequestMethod.GET })
    public String redirect_uhome_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "uhome_stu");
        return "stu/18_Stu_home";
    }

    @RequestMapping(value = DOCTORPATH + "/home_stu", method = { RequestMethod.GET })
    public String redirect_home_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "home_stu");
        
        int homeLoadedCounter = 0;
        if(request.getSession()!=null && request.getSession().getAttribute("homeloaded")!=null){
            homeLoadedCounter = (Integer)request.getSession().getAttribute("homeloaded");
        }
        homeLoadedCounter++;
        request.getSession().removeAttribute("homeloaded");
        request.getSession().setAttribute("homeloaded", homeLoadedCounter);
        return "stu/18_Stu_homeRegisterd";
    }

    @RequestMapping(value = DOCTORPATH + "/homeagree_stu", method = { RequestMethod.GET })
    public String redirect_homeagree_stu(HttpServletRequest request) {
        Doctor doc = (Doctor) SecurityTool.getActiveUser();
        if(request.getSession().getAttribute("homeloaded")==null){
            request.getSession().setAttribute("homeloaded", 0);
        }else{
            int homeLoadedCounter = (Integer)request.getSession().getAttribute("homeloaded");
            homeLoadedCounter++;
            request.getSession().removeAttribute("homeloaded");
            request.getSession().setAttribute("homeloaded", homeLoadedCounter);
        }
        request.setAttribute("redpage", "home_stu");
        return "stu/18_Stu_homeRegisterd";
    }

    @RequestMapping(value = DOCTORPATH + "/cal_stu", method = { RequestMethod.GET })
    public String redirect_cal_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "cal_stu");
        return "stu/19_Stu_MyCalen";
    }

    @RequestMapping(value = DOCTORPATH + "/info_stu", method = { RequestMethod.GET })
    public String redirect_info_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "info_stu");
        return "stu/03_SP_ProgInfo11";
    }
    
    @RequestMapping(value = DOCTORPATH + "/training_stu", method = { RequestMethod.GET })
    public String redirect_training_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "training_stu");
        return "stu/40_Stu_Training";
    }
    
    @RequestMapping(value = DOCTORPATH + "/code_stu", method = { RequestMethod.GET })
    public String redirect_code_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "info_stu");
        return "stu/36_Stu_Code_Conduct";
    }
    
    @RequestMapping(value = DOCTORPATH + "/consent_stu", method = { RequestMethod.GET })
    public String redirect_consent_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "info_stu");
        return "stu/37_Stu_Consent_Form";
    }

    @RequestMapping(value = DOCTORPATH + "/perref1_stu", method = { RequestMethod.GET })
    public String redirect_perref1_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "start_stu");
        request.setAttribute("surveyId", 4);
        request.setAttribute("ispreview", 0);
        request.setAttribute("nextpage", DOCTORPATH + "/autonomy_stu");
        return "stu/23_Stu_StartApt_PerRef";
    }
    
    @RequestMapping(value = DOCTORPATH + "/autonomy_stu", method = { RequestMethod.GET })
    public String redirect_autonomy_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "start_stu");
        request.setAttribute("surveyId", 13);
        request.setAttribute("ispreview", 0);
        request.setAttribute("nextpage", DOCTORPATH + "/selfsoca_stu");
        return "stu/23_Stu_StartApt_Autonomy";
    }
    
    @RequestMapping(value = DOCTORPATH + "/selfsoca_stu", method = { RequestMethod.GET })
    public String redirect_selfsoca_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "start_stu");
        request.setAttribute("surveyId", 9);
        request.setAttribute("ispreview", 0);
        request.setAttribute("nextpage", DOCTORPATH + "/rectype_stu");
        return "stu/23_Stu_StartApt_selfsoca";
    }
    
    @RequestMapping(value = DOCTORPATH + "/perref_second_stu", method = { RequestMethod.GET })
    public String redirect_perref_second_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "start_stu");
        request.setAttribute("surveyId", 6);
        request.setAttribute("ispreview", 0);
        request.setAttribute("nextpage", DOCTORPATH + "/thanks_stu");
        return "stu/26_Stu_StartApt_PerRef_second";
    }

    @RequestMapping(value = DOCTORPATH + "/rectype_stu", method = { RequestMethod.GET })
    public String redirect_rectype_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "start_stu");
        return "stu/24_Stu_StartApt_Type";
    }

    @RequestMapping(value = DOCTORPATH + "/evaluatesp_stu", method = { RequestMethod.GET })
    public String redirect_evaluatesp_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "start_stu");
        request.setAttribute("surveyId", 5);
        request.setAttribute("ispreview", 0);
        request.setAttribute("nextpage", DOCTORPATH + "/feedback_stu");
        return "stu/25_Stu_StartApt_SPRating";
    }
    
    @RequestMapping(value = DOCTORPATH + "/feedback_stu", method = { RequestMethod.GET })
    public String redirect_feedback_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "start_stu");
        request.setAttribute("surveyId", 7);
        request.setAttribute("ispreview", 0);
        String sessionId = (String) request.getSession().getAttribute("doctorSessionIdCombined");
        request.setAttribute("nextpage", DOCTORPATH + "/second_soca_stu/" + sessionId);
        return "stu/26_Stu_StartApt_OSPIAFeedback";
    }
    
    @RequestMapping(value = DOCTORPATH + "/nonverbalfeedback_stu/{sessionId}", method = { RequestMethod.GET })
    public String redirect_nonverbalfeedback_stu(HttpServletRequest request, @PathVariable String sessionId) {
        request.setAttribute("redpage", "cons_stu");
        request.setAttribute("surveyId", 8);
        request.setAttribute("ispreview", 0);
        request.setAttribute("nonverbalSessionId", sessionId);
        request.setAttribute("nextpage", DOCTORPATH + "/learning_stu/" + sessionId);
        return "stu/38_Stu_NonverbalFeedback";
    }
    
    @RequestMapping(value = DOCTORPATH + "/learning_stu/{sessionId}", method = { RequestMethod.GET })
    public String redirect_learning_stu(HttpServletRequest request, @PathVariable String sessionId) {
        request.setAttribute("redpage", "cons_stu");
        request.setAttribute("surveyId", 10);
        request.setAttribute("ispreview", 0);
        request.setAttribute("nonverbalSessionId", sessionId);
        request.setAttribute("nextpage", DOCTORPATH + "/learning_stu2/" + sessionId);
        return "stu/42_Stu_StudentLearning";
    }
    
    @RequestMapping(value = DOCTORPATH + "/learning_stu2/{sessionId}", method = { RequestMethod.GET })
    public String redirect_learning_stu2(HttpServletRequest request, @PathVariable String sessionId) {
        request.setAttribute("redpage", "cons_stu");
        request.setAttribute("surveyId", 11);
        request.setAttribute("ispreview", 0);
        request.setAttribute("nonverbalSessionId", sessionId);
        Doctor loginedDoctor = (Doctor) SecurityTool.getActiveUser();
        request.setAttribute("nextpage", DOCTORPATH + "/cons_stu/" + loginedDoctor.getDoctorId());
        return "stu/42_Stu_StudentLearning2";
    }
    
    
    @RequestMapping(value = DOCTORPATH + "/thanks_stu", method = { RequestMethod.GET })
    public String redirect_thanks_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "start_stu");
        return "stu/27_Stu_StartApt_Thanks";
    }

    @RequestMapping(value = DOCTORPATH + "/notes_stu", method = { RequestMethod.GET })
    public String redirect_notes_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "cons_stu");
        return "stu/35_Stu_Notes";
    }
    
    @RequestMapping(value = DOCTORPATH + "/appropriate", method = { RequestMethod.GET })
    public String redirect_appropriate_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "start_stu");
        return "stu/22_Stu_StartApt_Appropriate";
    }
    
    @RequestMapping(value = DOCTORPATH + "/inappropriate", method = { RequestMethod.GET })
    public String redirect_inappropriate_stu(HttpServletRequest request) {
        request.setAttribute("redpage", "start_stu");
        return "stu/22_Stu_StartApt_Inappropriate";
    }
    
    

    @RequestMapping(value = TUTORPATH + "/uhome_tut", method = { RequestMethod.GET })
    public String redirect_uhome_tut(HttpServletRequest request) {
        request.setAttribute("redpage", "uhome_tut");
        return "tut/31_Tut_home";
    }

    @RequestMapping(value = TUTORPATH + "/ulogin_tut", method = { RequestMethod.GET })
    public String redirect_login_tut(HttpServletRequest request) {
        request.setAttribute("redpage", "login_tut");
        return "tut/31_Tut_login";
    }

    @RequestMapping(value = TUTORPATH + "/home_tut", method = { RequestMethod.GET })
    public String redirect_home_tut(HttpServletRequest request) {
        request.setAttribute("redpage", "home_tut");
        return "tut/31_Tut_home1";
    }

    @RequestMapping(value = TUTORPATH + "/info_tut", method = { RequestMethod.GET })
    public String redirect_info_tut(HttpServletRequest request) {
        request.setAttribute("redpage", "info_tut");
        return "tut/31_Tut_ProgInfo";
    }

    @RequestMapping(value = TUTORPATH + "/review_tut", method = { RequestMethod.GET })
    public String redirect_review_tut(HttpServletRequest request) {
        request.setAttribute("redpage", "review_tut");
        return "tut/32_Tut_search";
    }

    @RequestMapping(value = TUTORPATH + "/soca_tut/{sessionId}", method = { RequestMethod.GET })
    public String redirect_soca_tut(HttpServletRequest request, @PathVariable String sessionId) {
        
        Teacher loginedTeacher = (Teacher) SecurityTool.getActiveUser();
        Answer answer = answerService.loadAnswerBySessionIdUserIdUsertype(sessionId, "teacher",loginedTeacher.getTeacherId(), 1);
        if(answer!=null){
            request.setAttribute("answer", answer);
        }
        request.setAttribute("redpage", "soca_tut");
        request.setAttribute("surveyId", 1);
        request.setAttribute("ispreview", 0);
        request.setAttribute("sessionId", sessionId);
        return "tut/34_Tut_SOCA";
    }
    
    
    @RequestMapping(value = TUTORPATH + "/soca_f2f", method = { RequestMethod.GET })
    public String redirect_soca_f2f_tut(HttpServletRequest request) {
        
        Teacher loginedTeacher = (Teacher) SecurityTool.getActiveUser();
//        Answer answer = answerService.loadAnswerBySessionIdUserIdUsertype(sessionId, "teacher",loginedTeacher.getTeacherId(), 1);
//        if(answer!=null){
//            request.setAttribute("answer", answer);
//        }
        request.setAttribute("redpage", "soca_f2f");
        request.setAttribute("surveyId", 1);
        request.setAttribute("ispreview", 0);
        return "tut/35_Tut_SOCA_f2f";
    }
    
    
    @RequestMapping(value = "/cali", method = { RequestMethod.GET })
    public String redirect_calibrate_main(HttpServletRequest request) {
        return "redirect:/cali/calibrate_info";
    }
    
    
    @RequestMapping(value = "/cali/calibrate_info", method = { RequestMethod.GET })
    public String redirect_calibrate_info(HttpServletRequest request) {
        return "cali/calibrate_info";
    }
    
    @RequestMapping(value = "/cali/calibrate", method = { RequestMethod.GET })
    public String redirect_calibrate(HttpServletRequest request) {
        return "cali/calibrate";
    }
    
    @RequestMapping(value = "/cali/standard/{userId}/{sessionId}", method = { RequestMethod.GET })
    public String redirect_calibrate(HttpServletRequest request, @PathVariable int userId, @PathVariable int sessionId) {
        
        int goldStandardId = 0;

        if (sessionId == 1) {
            goldStandardId = 1;
        } else if (sessionId == 2) {
            goldStandardId = 2;
        } else if (sessionId == 3) {
            goldStandardId = 3;
        }

        CalibrationSurveyAnswer goldAnswer = calibrationUserService.getCalibrationSurveyAnswerById(goldStandardId);
        CalibrationSurveyAnswer testAnswer =
                        calibrationUserService.getCalibrationSurveyAnswerByUserIdSessionId(userId, sessionId);

        if(testAnswer == null){
            return "cali/caliError";
        }
        
        request.setAttribute("answerId", goldAnswer.getId());
        return "cali/standar";
    }
    
    
}
