package org.poscomp.eqclinic.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Note;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.PatientWeeklyEmail;
import org.poscomp.eqclinic.domain.PatientWeeklyEmailClicking;
import org.poscomp.eqclinic.domain.ReflectionLog;
import org.poscomp.eqclinic.domain.StudentGroup;
import org.poscomp.eqclinic.domain.Video;
import org.poscomp.eqclinic.domain.pojo.AttentionTag;
import org.poscomp.eqclinic.domain.pojo.BaselineObj;
import org.poscomp.eqclinic.domain.pojo.EndPoint;
import org.poscomp.eqclinic.domain.pojo.FrownTag;
import org.poscomp.eqclinic.domain.pojo.Loudness;
import org.poscomp.eqclinic.domain.pojo.NonverbalCollection;
import org.poscomp.eqclinic.domain.pojo.ParseContent;
import org.poscomp.eqclinic.domain.pojo.Pitch;
import org.poscomp.eqclinic.domain.pojo.RecordingFile;
import org.poscomp.eqclinic.domain.pojo.ResultSOCA;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.domain.pojo.SelfTouch;
import org.poscomp.eqclinic.domain.pojo.SmileTag;
import org.poscomp.eqclinic.domain.pojo.SoundProperty;
import org.poscomp.eqclinic.domain.pojo.SpeakingPeriod;
import org.poscomp.eqclinic.domain.pojo.SpeechPercentage;
import org.poscomp.eqclinic.domain.pojo.SpeechRate;
import org.poscomp.eqclinic.domain.pojo.Syllable;
import org.poscomp.eqclinic.domain.pojo.TouchObj;
import org.poscomp.eqclinic.domain.pojo.TurnTakingInfo;
import org.poscomp.eqclinic.processor.Cmd;
import org.poscomp.eqclinic.processor.SoundProcessorNoSync;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.ExportService;
import org.poscomp.eqclinic.service.interfaces.NoteService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.PatientWeeklyEmailClickingService;
import org.poscomp.eqclinic.service.interfaces.PatientWeeklyEmailService;
import org.poscomp.eqclinic.service.interfaces.ReflectionLogService;
import org.poscomp.eqclinic.service.interfaces.StudentGroupService;
import org.poscomp.eqclinic.service.interfaces.VideoService;
import org.poscomp.eqclinic.util.ConstantValue;
import org.poscomp.eqclinic.util.EmailTool;
import org.poscomp.eqclinic.util.ExcelConstructor;
import org.poscomp.eqclinic.util.JsonTool;
import org.poscomp.eqclinic.util.ParserAllpoints;
import org.poscomp.eqclinic.util.ParserNodding;
import org.poscomp.eqclinic.util.ParserSmile;
import org.poscomp.eqclinic.util.ParserTilting;
import org.poscomp.eqclinic.util.SecurityTool;
import org.poscomp.eqclinic.util.SpeechPercentageTool;
import org.poscomp.eqclinic.util.Tools;
import org.poscomp.eqclinic.util.TurnTakingAnalyser;
import org.poscomp.survey.domain.Answer;
import org.poscomp.survey.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ian.test.MyNative;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class VideoDetailController {

    private static final Logger logger = Logger.getLogger(VideoDetailController.class);

    @Autowired
    private VideoService videoService;

    @Autowired
    private DoctorService doctorService;
    
    @Autowired
    private PatientService patientService;

    @Autowired
    private AppointmentService appointmentService;
    
    @Autowired
    private PatientWeeklyEmailService patientWeeklyEmailService;

    @Autowired
    private PatientWeeklyEmailClickingService patientWeeklyEmailClickingService;
    
    @Autowired
    private NoteService noteService;
    
    @Autowired
    private AnswerService answerService;
    
    @Autowired
    private ReflectionLogService reflectionLogService;
    
    @Autowired
    private StudentGroupService studentGroupService;
    
    @Autowired
    private ExportService exportService;

    private static final String VIDEOFOLDER = "../webapps/"+ConstantValue.projectName+"/video/";

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";
    private static final String TUTORPATH = "/tut";

    public void trimSingleVideo(String timestamp){
        String projectName = "ospia";
        String fileName = "video_" + timestamp;
        String saveDir = "../webapps/" + projectName + "/video/" + fileName + "/";

        String jsonFileName = JsonTool.getJsonFileName(saveDir);

        ArrayList<RecordingFile> files = new ArrayList<RecordingFile>();
        files = JsonTool.loadJsonObject(saveDir + jsonFileName);

        // start processing
        System.out.println("start processing....");

        Cmd cmd = new Cmd();
        String exePath = "c:\\videos\\ffmpeg.exe";

        if (files.size() > 2) {

            List<RecordingFile> filesDoctor = new ArrayList<RecordingFile>();
            List<RecordingFile> filesPatient = new ArrayList<RecordingFile>();

            for (RecordingFile file : files) {
            	 // assess the actual length of videos
            	float videoLength = cmd.getDurationVideo(exePath, saveDir + file.getOpentokName() + ".webm");
            	int lengthInMilliSec = (int)(videoLength*1000);
            	System.out.println(saveDir + file.getOpentokName() + ".webm" +" : " + videoLength );
            	
            	int start = file.getStartTimeOffset();
            	file.setStopTimeOffset(start + lengthInMilliSec);
            	
                if ("patient".equals(file.getUserType())) {
                    filesPatient.add(file);
                } else if ("doctor".equals(file.getUserType())) {
                    filesDoctor.add(file);
                }
            }

            Collections.sort(filesDoctor);
            Collections.sort(filesPatient);

            List<RecordingFile> filesDoctorTrimmed = new ArrayList<RecordingFile>();
            List<RecordingFile> filesPatientTrimmed = new ArrayList<RecordingFile>();

            RecordingFile rf = new RecordingFile();

            for (RecordingFile fileDoctor : filesDoctor) {
                for (RecordingFile filePatient : filesPatient) {
                    RecordingFile result = rf.getOverlapping(fileDoctor, filePatient);
                    if (result != null) {
                        filesDoctorTrimmed.add(result);
                    }
                }
            }


            for (RecordingFile filePatient : filesPatient) {
                for (RecordingFile fileDoctor : filesDoctor) {
                    RecordingFile result = rf.getOverlapping(filePatient, fileDoctor);
                    if (result != null) {
                        filesPatientTrimmed.add(result);
                    }
                }
            }

            List<Integer> gaps = new ArrayList<Integer>();
            
            for (int i = 0; i < filesPatientTrimmed.size(); i++) {
                if(i==0){
                    gaps.add(filesPatientTrimmed.get(i).getStartTimeOffset());
                }else{
                    int diff = filesPatientTrimmed.get(0).getStartTimeOffset();
                    for (int j = 1; j <= i; j++){
                        diff += filesPatientTrimmed.get(j).getStartTimeOffset()-filesPatientTrimmed.get(j-1).getStopTimeOffset();
                    }
                    gaps.add(diff);
                }
            }

            // access the comments
            Video conbinedVideo = videoService.getVideo(fileName);
            String conbinedSesseionId = conbinedVideo.getSessionId();
            List<Note> notes = noteService.getNoteBySessionId(conbinedSesseionId);

            for (Note note : notes) {
                
                long noteTime = note.getRealTime();
                int number = -1;
                for (int i = 0; i < filesPatientTrimmed.size(); i++){
                    RecordingFile trimmedFile = filesPatientTrimmed.get(i);
                    
                    if(noteTime>=(trimmedFile.getStartTimeOffset()+trimmedFile.getRecordingTime())&&
                            noteTime<=(trimmedFile.getStopTimeOffset()+trimmedFile.getRecordingTime())){
                        number = i;
                    }
                }
                float total = 0;
                if(number!=-1){
                	total = gaps.get(number);
                }
                
                double time = note.getRealTime()-filesPatientTrimmed.get(0).getRecordingTime()-total;
                System.out.println(note.getRealTime());
                System.out.println(filesPatientTrimmed.get(0).getRecordingTime());
                if(time>0){
                    note.setTime(time/1000.0);
                }
                noteService.saveNote(note);
            }
            
            
            String finalFileNamePatient = "";
            String finalFileNameDoctor = "";
            
            List<RecordingFile> filesDoctorTrimmedBK = new ArrayList<RecordingFile>();
            List<RecordingFile> filesPatientTrimmedBK = new ArrayList<RecordingFile>();
            
            for (int i = 0; i < filesDoctorTrimmed.size(); i++) {
                if(filesDoctorTrimmed.get(i).getT()>3){
                    filesDoctorTrimmedBK.add(filesDoctorTrimmed.get(i));
                }
            }
            
            for (int i = 0; i < filesPatientTrimmed.size(); i++) {
                if(filesPatientTrimmed.get(i).getT()>3){
                    filesPatientTrimmedBK.add(filesPatientTrimmed.get(i));
                }
            }
            
            
            for (int i = 0; i < filesPatientTrimmedBK.size(); i++) {

                RecordingFile file = filesPatientTrimmedBK.get(i);

                finalFileNamePatient = file.getFileName();
                
                String inputFile = saveDir + file.getOpentokName() + ".webm";

                String outputFile = saveDir + file.getUserType() + i + ".mp4";

                // covert the webm to mp4
                cmd.webm2mp4(exePath, inputFile, outputFile, file.getSs(), file.getT());
                float videoLength = cmd.getDurationVideo(exePath, saveDir + file.getUserType() + i + ".mp4");

                // extract WAV file
                cmd.extractAudio(exePath, outputFile, saveDir + file.getUserType() + i + "_ori.wav");
                float audioLength = cmd.getDurationVideo(exePath, saveDir + file.getUserType() + i + "_ori.wav");

                if (audioLength < videoLength) {
                    double difference = videoLength - audioLength;
                    cmd.createSilentWav(exePath, difference, saveDir + file.getUserType() + i + "_silent.wav");

                    cmd.concatTwoAudios(exePath, saveDir + file.getUserType() + i + "_silent.wav",
                                    saveDir + file.getUserType() + i + "_ori.wav", saveDir + file.getUserType() + i
                                                    + ".wav");
                } else {
                    double difference = 0.001;
                    cmd.createSilentWav(exePath, difference, saveDir + file.getUserType() + i + "_silent.wav");
                    cmd.concatTwoAudios(exePath, saveDir + file.getUserType() + i + "_silent.wav",
                                    saveDir + file.getUserType() + i + "_ori.wav", saveDir + file.getUserType() + i
                                                    + ".wav");
                }

                new File(saveDir + file.getUserType() + i + "_ori.wav").delete();
                new File(saveDir + file.getUserType() + i + "_silent.wav").delete();
            }
            
            
            for (int i = 0; i < filesDoctorTrimmedBK.size(); i++) {

                RecordingFile file = filesDoctorTrimmedBK.get(i);

                finalFileNameDoctor = file.getFileName();
                
                String inputFile = saveDir + file.getOpentokName() + ".webm";

                String outputFile = saveDir + file.getUserType() + i + ".mp4";

                // covert the webm to mp4
                cmd.webm2mp4(exePath, inputFile, outputFile, file.getSs(), file.getT());
                float videoLength = cmd.getDurationVideo(exePath, saveDir + file.getUserType() + i + ".mp4");

                // extract WAV file
                cmd.extractAudio(exePath, outputFile, saveDir + file.getUserType() + i + "_ori.wav");
                float audioLength = cmd.getDurationVideo(exePath, saveDir + file.getUserType() + i + "_ori.wav");

                if (audioLength < videoLength) {
                    double difference = videoLength - audioLength;
                    cmd.createSilentWav(exePath, difference, saveDir + file.getUserType() + i + "_silent.wav");

                    cmd.concatTwoAudios(exePath, saveDir + file.getUserType() + i + "_silent.wav",
                                    saveDir + file.getUserType() + i + "_ori.wav", saveDir + file.getUserType() + i
                                                    + ".wav");
                } else {
                    double difference = 0.001;
                    cmd.createSilentWav(exePath, difference, saveDir + file.getUserType() + i + "_silent.wav");
                    cmd.concatTwoAudios(exePath, saveDir + file.getUserType() + i + "_silent.wav",
                                    saveDir + file.getUserType() + i + "_ori.wav", saveDir + file.getUserType() + i
                                                    + ".wav");
                }

                new File(saveDir + file.getUserType() + i + "_ori.wav").delete();
                new File(saveDir + file.getUserType() + i + "_silent.wav").delete();
            }
            
            
            cmd.concatMultipleAudios(exePath, saveDir + "doctor", filesDoctorTrimmedBK.size(), saveDir + finalFileNameDoctor+".wav");
            cmd.concatMultipleVideos(exePath, saveDir + "doctor", filesDoctorTrimmedBK.size(), saveDir + finalFileNameDoctor+".mp4");
            cmd.concatMultipleAudios(exePath, saveDir + "patient", filesPatientTrimmedBK.size(), saveDir + finalFileNamePatient+".wav");
            cmd.concatMultipleVideos(exePath, saveDir + "patient", filesPatientTrimmedBK.size(), saveDir + finalFileNamePatient+".mp4");
            
            
//            for (int i = 0; i < filesPatientTrimmedBK.size(); i++) {
//                RecordingFile file = filesPatientTrimmedBK.get(i);
//                new File(saveDir + file.getUserType() + i + ".wav").delete();
//                new File(saveDir + file.getUserType() + i + ".mp4").delete();
//            }
//            
//            for (int i = 0; i < filesDoctorTrimmedBK.size(); i++) {
//                RecordingFile file = filesDoctorTrimmedBK.get(i);
//                new File(saveDir + file.getUserType() + i + ".wav").delete();
//                new File(saveDir + file.getUserType() + i + ".mp4").delete();
//            }
            
            
            
            String video1Name = saveDir + finalFileNameDoctor + ".mp4";
            String video2Name = saveDir + finalFileNamePatient + ".mp4";
            String combinedVideoName = saveDir + fileName + ".mp4";

            // combine two video
            cmd.mergeTwoVideo(exePath, video1Name, video2Name, combinedVideoName);

            String audio1Name = saveDir + finalFileNameDoctor + ".wav";
            String audio2Name = saveDir + finalFileNamePatient + ".wav";
            String combinedAudioName = saveDir + fileName + ".wav";
            // combine two audio
            cmd.mergeTwoAudios(exePath, audio1Name, audio2Name, combinedAudioName);


            // merge the video and audio
            String combinedName = saveDir + fileName + "_combined.mp4";
            cmd.mergeVideoAudio(exePath, combinedVideoName, combinedAudioName, combinedName);

            // if the combined file already created
            if (new File(combinedName).exists()) {
                // delete files
                new File(combinedAudioName).delete();
                new File(combinedVideoName).delete();

                File oldCombinedFile = new File(combinedName);
                File newCombinedFile = new File(combinedVideoName);

                oldCombinedFile.renameTo(newCombinedFile);
            }
            
        } else {

            RecordingFile file_doctor = new RecordingFile();
            RecordingFile file_patient = new RecordingFile();

            // convert webm files to mp4 files
            for (RecordingFile recordingFile : files) {
                String fileNameWebm = recordingFile.getFileName();
                String opentokName = recordingFile.getOpentokName();
                float trimmedLength = recordingFile.getTrimmedLength() / 1000;

                if (recordingFile.getUserType().equals("doctor")) {
                    file_doctor = recordingFile;
                } else {
                    file_patient = recordingFile;
                }

                // extract sound from doctor mp4 file
                String inputFile = saveDir + opentokName + ".webm";
                String outFile = saveDir + fileNameWebm + ".mp4";

                // trimmedLength += 1;

                // here we need to trim the mp4 file, based on the offsite length
                cmd.webm2mp4(exePath, inputFile, outFile, trimmedLength); 

                float videoLength = cmd.getDurationVideo(exePath, saveDir + fileNameWebm + ".mp4");

                // extract WAV file
                cmd.extractAudio(exePath, outFile, saveDir + fileNameWebm + ".wav");
                float audioLength = cmd.getDurationVideo(exePath, saveDir + fileNameWebm + "_ori.wav");

                if (audioLength < videoLength) {
                    double difference = videoLength - audioLength;
                    cmd.createSilentWav(exePath, difference, saveDir + fileNameWebm + "_silent.wav");
                    cmd.concatTwoAudios(exePath, saveDir + fileNameWebm + "_silent.wav", saveDir + fileNameWebm
                                    + "_ori.wav", saveDir + fileNameWebm + ".wav");
                } else {
                    double difference = 0.001;
                    cmd.createSilentWav(exePath, difference, saveDir + fileNameWebm + "_silent.wav");
                    cmd.concatTwoAudios(exePath, saveDir + fileNameWebm + "_silent.wav", saveDir + fileNameWebm
                                    + "_ori.wav", saveDir + fileNameWebm + ".wav");
                }

                new File(saveDir + fileNameWebm + "_ori.wav").delete();
                new File(saveDir + fileNameWebm + "_silent.wav").delete();
            }

            String video1Name = saveDir + file_doctor.getFileName() + ".mp4";
            String video2Name = saveDir + file_patient.getFileName() + ".mp4";
            String combinedVideoName = saveDir + fileName + ".mp4";

            // combine two video
            cmd.mergeTwoVideo(exePath, video1Name, video2Name, combinedVideoName);

            String audio1Name = saveDir + file_doctor.getFileName() + ".wav";
            String audio2Name = saveDir + file_patient.getFileName() + ".wav";
            String combinedAudioName = saveDir + fileName + ".wav";
            // combine two audio
            cmd.mergeTwoAudios(exePath, audio1Name, audio2Name, combinedAudioName);


            // merge the video and audio
            String combinedName = saveDir + fileName + "_combined.mp4";
            cmd.mergeVideoAudio(exePath, combinedVideoName, combinedAudioName, combinedName);

            // if the combined file already created
            if (new File(combinedName).exists()) {
                // delete files
                new File(combinedAudioName).delete();
                new File(combinedVideoName).delete();

                File oldCombinedFile = new File(combinedName);
                File newCombinedFile = new File(combinedVideoName);

                oldCombinedFile.renameTo(newCombinedFile);
            }
        }

         // cmd.splitVideo(exePath, saveDir + fileName + "_doctor.mp4", 30);
    }
    
    
    
    @RequestMapping(value = "/trimall/{timestamp}")
    public void trimAll(HttpServletRequest request, @PathVariable String timestamp) throws Exception {
        
        List<Video> videos = videoService.getVideoByDate(timestamp);
        String videoTimes = "";
        for (Video video : videos) {
            Long generateTime = video.getGenerateTime();
            trimVideo(generateTime+"");
            videoTimes += generateTime+";";
        }
        String projectPath = request.getSession().getServletContext().getRealPath("/");
        String path = projectPath+"video\\";
        String tempFile = "C:/EQClinicDetector/processvideo.txt";
        
        BufferedReader br = null;
		FileReader fr = null;

		try {

			FileWriter writer = new FileWriter(tempFile);
			writer.write(path+"\n");
			if(!videoTimes.equals("")){
				videoTimes = videoTimes.substring(0, videoTimes.length()-1);
			}
			writer.write(videoTimes);
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
				if (fr != null)
					fr.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
        
    }
    
    
    @RequestMapping(value = "/trimvideo/{timestamp}")
    public void trimVideoHttp(HttpServletRequest request, @PathVariable String timestamp) throws Exception {
    	trimVideo(timestamp);
    }

    public void trimVideo(String timestamp) throws Exception {

        String videoName = "video_"+timestamp;
        String baselineFileName = videoName + "_doctor_baseline";
        
        // move baseline video to the folder
        Video video = videoService.getVideo("video_"+timestamp);
        String sesseionId = video.getSessionId();
        Appointment apt = appointmentService.getAppointmentBySessionId(sesseionId);
        
        
        String doctorBaselineSessionId = apt.getDoctorSessionIdBaseline();
        List<Video> doctorBaselineVideos = videoService.getVideoBySessionId(doctorBaselineSessionId);
        Video doctorBaselineVideo = null;
        String doctorBaselineVideoName = "";
        
        String doctorBaselineAudioFileBaseDirStr = VIDEOFOLDER + "baselines/";
        String audioFileBaseDirStr = VIDEOFOLDER + videoName + "/";
        
        
        if (doctorBaselineVideos.size() > 0) {
            doctorBaselineVideo = doctorBaselineVideos.get(0);
            doctorBaselineVideoName = doctorBaselineVideo.getName();

            // ******** move the baseline video and audio to particular folder**********
            File oldVideoFileBaseline = new File(doctorBaselineAudioFileBaseDirStr + doctorBaselineVideoName + ".mp4");
            File oldAudioFileBaseline = new File(doctorBaselineAudioFileBaseDirStr + doctorBaselineVideoName + ".wav");

            File newVideoFileBaseline = new File(audioFileBaseDirStr + baselineFileName + ".mp4");
            File newAudioFileBaseline = new File(audioFileBaseDirStr + baselineFileName + ".wav");

            if (!newVideoFileBaseline.exists()) {
                oldVideoFileBaseline.renameTo(newVideoFileBaseline);
            }

            if (!newAudioFileBaseline.exists()) {
                oldAudioFileBaseline.renameTo(newAudioFileBaseline);
            }
        }
        
        
        trimSingleVideo(timestamp);

    }
    
    
    // -Xms1024M  -Xmx1024M -XX:PermSize=256m -XX:MaxPermSize=256m
    // @Scheduled(cron = "30 57 21 ? * *")
    @RequestMapping(value = "/processvideo/{timestamp}")
    public void processVideo(HttpServletRequest request, @PathVariable String timestamp) throws Exception {
        
        List<Video> videos = videoService.getVideoByDate(timestamp);
        
        for (Video video : videos) {
            
            String fileName = video.getName();
            
            Cmd cmd = new Cmd();
            String exePath = "c:\\videos\\ffmpeg.exe";
            String saveDir = "../webapps/ospia/video/" + fileName + "/";
//            cmd.splitVideo(exePath, saveDir + fileName + "_doctor.mp4", 30);
            
            System.out.println(video.getId());
            
            int FPS = 25;
            String baselineFileName = video.getName() + "_doctor_baseline";

            Video conbinedVideo = videoService.getVideo(fileName);
            String conbinedSesseion = conbinedVideo.getSessionId();
            Appointment apt = appointmentService.getAppointmentBySessionId(conbinedSesseion);
            
            // check whether analyse this video: start
            int doctorId = apt.getDoctorId();
            int patientId = apt.getPatientId();
            Doctor doctor = doctorService.getDoctorByDoctorId(doctorId);
            Patient patient = patientService.getPatientByPatientId(patientId);
//            if(doctor.getAgreement()!=1 || patient.getAgreement()!=1){
//                continue;
//            }
            // check whether analyse this video: end 
            
            String doctorBaselineSessionId = apt.getDoctorSessionIdBaseline();
            List<Video> doctorBaselineVideos = videoService.getVideoBySessionId(doctorBaselineSessionId);
            Video doctorBaselineVideo = null;
            String doctorBaselineVideoName = "";
            
            String doctorBaselineAudioFileBaseDirStr = VIDEOFOLDER + "baselines/";
            String audioFileBaseDirStr = VIDEOFOLDER + fileName + "/";

            
            if (doctorBaselineVideos.size() > 0) {
                doctorBaselineVideo = doctorBaselineVideos.get(0);
                doctorBaselineVideoName = doctorBaselineVideo.getName();

//                // ******** move the baseline video and audio to particular folder**********
//                File oldVideoFileBaseline = new File(doctorBaselineAudioFileBaseDirStr + doctorBaselineVideoName + ".mp4");
//                File oldAudioFileBaseline = new File(doctorBaselineAudioFileBaseDirStr + doctorBaselineVideoName + ".wav");
//
//                File newVideoFileBaseline = new File(audioFileBaseDirStr + baselineFileName + ".mp4");
//                File newAudioFileBaseline = new File(audioFileBaseDirStr + baselineFileName + ".wav");
//
//                if (!newVideoFileBaseline.exists()) {
//                    oldVideoFileBaseline.renameTo(newVideoFileBaseline);
//                }
//
//                if (!newAudioFileBaseline.exists()) {
//                    oldAudioFileBaseline.renameTo(newAudioFileBaseline);
//                }
                
               
                // ************* baseline processing start*****************
                SoundProcessorNoSync processorDoctorBaseline =
                                new SoundProcessorNoSync(audioFileBaseDirStr, baselineFileName, audioFileBaseDirStr,
                                                baselineFileName);
                processorDoctorBaseline.run();

//                MyNative mnBaseline = new MyNative();
//                mnBaseline.parseAllPoints(audioFileBaseDirStr, baselineFileName, ".mp4", 25, 1);
//                mnBaseline.parseSmiles(audioFileBaseDirStr, baselineFileName, ".mp4", 25, 1);
//                mnBaseline.parseTouches(audioFileBaseDirStr, baselineFileName, ".mp4", 25, 1);
//                mnBaseline.parseMoves(audioFileBaseDirStr, baselineFileName, ".mp4", 25, 1);

                ParserAllpoints apBaseline = new ParserAllpoints();
                 apBaseline.getFaceSizeFromAll(audioFileBaseDirStr + baselineFileName + "_allpoints.txt", audioFileBaseDirStr
                                 + baselineFileName + "_leanings.txt");
                 apBaseline.getFaceMovement(audioFileBaseDirStr + baselineFileName + "_allpoints.txt", audioFileBaseDirStr + baselineFileName + "_movement.txt");
 
                 // -------------------------- tilting -----------------------------------------------
                 ParserTilting tdBaseline = new ParserTilting();
                 int totalLineNoBaseline = tdBaseline.getFileLineNos(audioFileBaseDirStr + baselineFileName + "_allpoints.txt");
                 tdBaseline.generateTiltingAngleFile(audioFileBaseDirStr + baselineFileName + "_allpoints.txt",
                                 audioFileBaseDirStr + baselineFileName + "_allpoints_angle", totalLineNoBaseline, FPS);
                 tdBaseline.generateDurationFile(audioFileBaseDirStr + baselineFileName + "_allpoints_angle_posnag",
                                 audioFileBaseDirStr + baselineFileName + "_allpoints_turns");
 
                // ************** baseline processing end****************
             }
            
            System.out.println("finish baseline");
            
            
            String doctorFile = fileName + "_doctor";
            String patientFile = fileName + "_patient";

            System.out.println("before audio proc: " + new Date());
            SoundProcessorNoSync processorDoctor =
                            new SoundProcessorNoSync(audioFileBaseDirStr, doctorFile, audioFileBaseDirStr, doctorFile);
            processorDoctor.run();

            SoundProcessorNoSync processorPatient =
                            new SoundProcessorNoSync(audioFileBaseDirStr, patientFile, audioFileBaseDirStr, patientFile);
            processorPatient.run();
            System.out.println("after audio proc: " + new Date());
            
            
            
             float duration = cmd.getDurationVideo("C:/videos/ffmpeg.exe", audioFileBaseDirStr + doctorFile + ".mp4");
             int interval = 30;
             int splitNumber = (int) (duration / interval);
 
//             MyNative mn = new MyNative();
//             System.out.println("before image proc:" + new Date());
// 
//             for (int i = 0; i <= splitNumber; i++) {
//                 if (i == splitNumber) {
//                     mn.parseAllPoints(audioFileBaseDirStr, doctorFile + i, ".mp4", 25, 1);
//                     mn.parseSmiles(audioFileBaseDirStr, doctorFile + i, ".mp4", 25, 1);
//                     mn.parseTouches(audioFileBaseDirStr, doctorFile + i, ".mp4", 25, 1);
//                     mn.parseMoves(audioFileBaseDirStr, doctorFile + i, ".mp4", 25, 1);
//                 } else {
//                     mn.parseAllPoints(audioFileBaseDirStr, doctorFile + i, ".mp4", 25, 0);
//                     mn.parseSmiles(audioFileBaseDirStr, doctorFile + i, ".mp4", 25, 0);
//                     mn.parseTouches(audioFileBaseDirStr, doctorFile + i, ".mp4", 25, 0);
//                     mn.parseMoves(audioFileBaseDirStr, doctorFile + i, ".mp4", 25, 0);
//                 }
//             }
//             System.out.println("after image proc:" + new Date());
// 
//             List<String> inputFilesFrown = new ArrayList<String>();
//             List<String> inputFilesAllpoints = new ArrayList<String>();
//             List<String> inputFilesSmile = new ArrayList<String>();
//             List<String> inputFilesMoves = new ArrayList<String>();
//             List<String> inputFilesTouches = new ArrayList<String>();
// 
//             // merge the files
//             for (int i = 0; i <= splitNumber; i++) {
//                 inputFilesFrown.add(audioFileBaseDirStr + doctorFile + i + "_frown.txt");
//                 inputFilesAllpoints.add(audioFileBaseDirStr + doctorFile + i + "_allpoints.txt");
//                 inputFilesSmile.add(audioFileBaseDirStr + doctorFile + i + "_smile.txt");
//                 inputFilesMoves.add(audioFileBaseDirStr + doctorFile + i + "_movement.txt");
//                 inputFilesTouches.add(audioFileBaseDirStr + doctorFile + i + "_selftouch_percentage.txt");
//             }
//             Tools.mergeTextFiles(inputFilesFrown, audioFileBaseDirStr + doctorFile + "_frown.txt");
//             Tools.mergeTextFiles(inputFilesAllpoints, audioFileBaseDirStr + doctorFile + "_allpoints.txt");
//             Tools.mergeTextFiles(inputFilesSmile, audioFileBaseDirStr + doctorFile + "_smile.txt");
//             Tools.mergeTextFiles(inputFilesTouches, audioFileBaseDirStr + doctorFile + "_selftouch_percentage.txt");
//             Tools.mergeTextFilesWithLine(inputFilesMoves, audioFileBaseDirStr + doctorFile + "_movement.txt", interval
//                             * FPS);
 
 
             ParserAllpoints ap = new ParserAllpoints();
             ap.getFaceSizeFromAll(audioFileBaseDirStr + doctorFile + "_allpoints.txt", audioFileBaseDirStr + doctorFile
                             + "_leanings.txt");
             ap.getNosePointFromAll(audioFileBaseDirStr + doctorFile + "_allpoints.txt", audioFileBaseDirStr + doctorFile
                             + "_nosepoint.txt");
             ap.getFaceMovement(audioFileBaseDirStr + doctorFile + "_allpoints.txt", audioFileBaseDirStr + doctorFile + "_movement.txt");
             
         //  -------------------------- nodding -----------------------------------------------
             ParserNodding tn = new ParserNodding();
            // detect the nodding
            tn.manipXYNodding(audioFileBaseDirStr + doctorFile + "_nosepoint.txt", audioFileBaseDirStr + doctorFile
                            + "_nodding.txt", FPS);
            // detect the shaking
             tn.manipXYShaking(audioFileBaseDirStr + doctorFile + "_nosepoint.txt", audioFileBaseDirStr + doctorFile
                     + "_shaking.txt", 25.0f);
             
             tn.deleteRepeat(audioFileBaseDirStr + doctorFile+ "_shaking.txt", 
                     audioFileBaseDirStr + doctorFile + "_nodding.txt", 
                     audioFileBaseDirStr + doctorFile+ "_shakingnew.txt", 
                     audioFileBaseDirStr + doctorFile + "_noddingnew.txt");
             
 
             // -------------------------- tilting -----------------------------------------------
             ParserTilting td = new ParserTilting();
             int totalLineNo = td.getFileLineNos(audioFileBaseDirStr + doctorFile + "_allpoints.txt");
             td.generateTiltingAngleFile(audioFileBaseDirStr + doctorFile + "_allpoints.txt", audioFileBaseDirStr
                             + doctorFile + "_allpoints_angle", totalLineNo, FPS);
             td.generateDurationFile(audioFileBaseDirStr + doctorFile + "_allpoints_angle_posnag", audioFileBaseDirStr
                             + doctorFile + "_allpoints_turns");
 
            // analyze the smile intensity
            ParserSmile st = new ParserSmile();
            st.analysisSmile(audioFileBaseDirStr + doctorFile + "_smile.txt", audioFileBaseDirStr + doctorFile
                            + "_smileana.txt", 25); 
            System.out.println("finish processing: " + new Date());
            
        }
       

    }


    @RequestMapping(value = DOCTORPATH + "/video/{archiveId}")
    @ResponseBody
    public Video getVideoByArchive(HttpServletRequest request, @PathVariable String archiveId) {
        Video video = videoService.getVideoByArchiveId(archiveId);
        return video;
    }

    public BaselineObj getBaselineLevel(String vname) {
        BaselineObj bo = new BaselineObj();

        String fileFolder = VIDEOFOLDER + vname + "/";
        String fileNameBaselineLoudness = vname + "_doctor_baseline_loudness.txt";
        String fileNameBaselinePitch = vname + "_doctor_baseline_pitch.txt";
        String fileNameBaselineLeaning = vname + "_doctor_baseline_leanings.txt";
        String fileNameBaselineTilting = vname + "_doctor_baseline_allpoints_turns";
        String fileNameBaselineMovement = vname + "_doctor_baseline_movement.txt";

        BufferedReader br;
        String line = "";
        try {
            // access loudness baseline information
            br = new BufferedReader(new FileReader(fileFolder + fileNameBaselineLoudness));
            line = br.readLine();
            line = br.readLine();
            if (line != null && !"".equals(line.trim())) {
                float biggestVolum = 0;
                String[] volumeStr = line.split(",");
                for (String string : volumeStr) {
                    if (Float.parseFloat(string) > biggestVolum) {
                        biggestVolum = Float.parseFloat(string);
                    }
                }

                bo.setLoudnessTop(biggestVolum);
                bo.setLoudnessBottom(0);
            }
            br.close();

            // access pitch baseline information
            br = new BufferedReader(new FileReader(fileFolder + fileNameBaselinePitch));
            line = br.readLine();
            if (line != null && !"".equals(line.trim())) {
                float biggestPitch = 0;
                String[] volumeStr = line.split(",");
                for (String string : volumeStr) {
                    if (Float.parseFloat(string) > biggestPitch && Float.parseFloat(string) < 70) {
                        biggestPitch = Float.parseFloat(string);
                    }
                }

                bo.setPitchTop(biggestPitch);
                bo.setPitchBottom(0);
            }
            br.close();

            // access leaning information
            br = new BufferedReader(new FileReader(fileFolder + fileNameBaselineLeaning));
            line = br.readLine();
            float biggestLeaning = 0;
            float smallestLeaning = 1000000;
            while (line != null) {
                float leaningValue = Float.parseFloat(line);
                
                if(leaningValue<10000){
                    line = br.readLine();
                    continue;
                }
                
                if (leaningValue > biggestLeaning) {
                    biggestLeaning = leaningValue;
                }
                if (leaningValue < smallestLeaning) {
                    smallestLeaning = leaningValue;
                }
                line = br.readLine();
            }
            bo.setLeaningTop(biggestLeaning);
            bo.setLeaningBottom(smallestLeaning);
            br.close();

            // access leaning information
            br = new BufferedReader(new FileReader(fileFolder + fileNameBaselineTilting));
            line = br.readLine();
            float biggestTilting = 0;
            float smallestTilting = 1000000;
            while (line != null) {
                String[] values = line.split(",");
                float tiltingValue = Float.parseFloat(values[5]);
                if (tiltingValue > biggestTilting) {
                    biggestTilting = tiltingValue;
                }
                if (tiltingValue < smallestTilting) {
                    smallestTilting = tiltingValue;
                }
                line = br.readLine();
            }
            bo.setTiltingTop(biggestTilting);
            bo.setTiltingBottom(smallestTilting);
            br.close();

            // access leaning information
            br = new BufferedReader(new FileReader(fileFolder + fileNameBaselineMovement));
            line = br.readLine();
            float biggestMovement = 0;
            while (line != null) {
                float movementValue = Float.parseFloat(line);
                if (movementValue > biggestMovement) {
                    biggestMovement = movementValue;
                }
                line = br.readLine();
            }
            
            bo.setMovementTop(biggestMovement);
            bo.setMovementBottom(0);
            br.close();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return bo;

    }


    @RequestMapping(value = DOCTORPATH + "/vdetail/{vname}")
    public String videoDetail(HttpServletRequest request, @PathVariable String vname) {

        String[] temp = vname.split("&");
        vname = temp[0];
        BaselineObj baselineObject = getBaselineLevel(vname);

        // access the comments
        Video conbinedVideo = videoService.getVideo(vname);
        String conbinedSesseionId = conbinedVideo.getSessionId();
        List<Note> notes = noteService.getNoteBySessionId(conbinedSesseionId);

        // access NVB reflection survey answers
        Answer nvbAnswer = answerService.loadAnswerBySessionIdUserId(conbinedSesseionId, "doctor", 4);
        JSONArray nvbJsonArray = JSONArray.fromObject(nvbAnswer.getContent());
        
        int speakRatioNo = 0;
        int volumeRaisedNo = 0; 
        int pitchRaisedNo = 0; 
        int interruptedNo = 0;
        int smileNo = 0; 
        int frownNo = 0; 
        int nodNo = 0;
        int shakeNo = 0;
        int leanNo = 0;
        int noLookNo = 0;
        int tiltNo = 0;
        
        for (int i = 0; i < nvbJsonArray.size(); i++) {
        	JSONObject jAnswer = nvbJsonArray.getJSONObject(i);
        	int questionId = jAnswer.getInt("qid");
        	String questionValue = jAnswer.getString("qvalue");
        	if(questionId==69){
        		try{
        			speakRatioNo = Integer.parseInt(questionValue);
        		}catch (Exception e){
        			// 
        		}
        	} else if(questionId==128){
        		try{
        			volumeRaisedNo = Integer.parseInt(questionValue);
        		}catch (Exception e){
        			// 
        		}
        	} else if(questionId==129){
        		try{
        			pitchRaisedNo = Integer.parseInt(questionValue);
        		}catch (Exception e){
        			// 
        		}
        	} else if(questionId==130){
        		try{
        			interruptedNo = Integer.parseInt(questionValue);
        		}catch (Exception e){
        			// 
        		}
        	} else if(questionId==131){
        		try{
        			smileNo = Integer.parseInt(questionValue);
        		}catch (Exception e){
        			// 
        		}
        	}else if(questionId==132){
        		try{
        			frownNo = Integer.parseInt(questionValue);
        		}catch (Exception e){
        			// 
        		}
        	}else if(questionId==133){
        		try{
        			nodNo = Integer.parseInt(questionValue);
        		}catch (Exception e){
        			// 
        		}
        	}else if(questionId==134){
        		try{
        			shakeNo = Integer.parseInt(questionValue);
        		}catch (Exception e){
        			// 
        		}
        	}else if(questionId==135){
        		try{
        			leanNo = Integer.parseInt(questionValue);
        		}catch (Exception e){
        			// 
        		}
        	}else if(questionId==136){
        		try{
        			noLookNo = Integer.parseInt(questionValue);
        		}catch (Exception e){
        			// 
        		}
        	}else if(questionId==137){
        		try{
        			tiltNo = Integer.parseInt(questionValue);
        		}catch (Exception e){
        			// 
        		}
        	}
        }
        
        NonverbalCollection estimatedNC = new NonverbalCollection();
        estimatedNC.setSpeakRatioNo(speakRatioNo*10);
        estimatedNC.setVolumeRaisedNo(volumeRaisedNo);
        estimatedNC.setPitchRaisedNo(pitchRaisedNo);
        estimatedNC.setInterruptedNo(interruptedNo);
        estimatedNC.setSmileNo(smileNo);
        estimatedNC.setFrownNo(frownNo);
        estimatedNC.setNodNo(nodNo);
        estimatedNC.setShakeNo(shakeNo);
        estimatedNC.setLeanNo(leanNo);
        estimatedNC.setNoLookNo(noLookNo);
        estimatedNC.setTiltNo(tiltNo);

        for (Note note : notes) {
            if ("1".equals(note.getNoteContent())) {
                note.setNoteContent("good");
            } else if ("2".equals(note.getNoteContent())) {
                note.setNoteContent("bad");
            }
        }

        JSONArray jsonArray = JSONArray.fromObject(notes);
        String notesString = jsonArray.toString();

        String fileFolder = VIDEOFOLDER + vname + "/";
        // vname = "video_1436761313842";
        // get the name of the video
        String videoName = vname;

        String videoToDisplay = videoName + ".mp4";
        String fileNameNodding = videoName + "_doctor_noddingnew.txt";
        String fileNameShaking = videoName + "_doctor_shakingnew.txt";

        // detecting smile
        BufferedReader brNodding;
        BufferedReader brShaking;
        List<ParseContent> pcs = new ArrayList<ParseContent>();
        int noddingNo = 0;
        int shakingNo = 0;
        String noddingline = "";
        String shakingline = "";
        try {
            brNodding = new BufferedReader(new FileReader(fileFolder + fileNameNodding));
            String lineNodding = brNodding.readLine();

            while (lineNodding != null && !"".equals(lineNodding.trim())) {
                String[] lineSp = lineNodding.split(":");
                String content = lineSp[0];
                String time = lineSp[1];
                String timeStart = lineSp[2];
                String moveValue = lineSp[3];
                if ("nodding".equals(content) && Float.parseFloat(moveValue) > 10) {
                    if (noddingline.equals("")) {
                        noddingline += time + ";" + timeStart+";"+moveValue;
                    } else {
                        noddingline += "," + time + ";" + timeStart+";"+moveValue;
                    }
                    noddingNo++;
                }

                ParseContent pc = new ParseContent();
                pc.setContent(content);
                pc.setTime(time);

                pcs.add(pc);
                lineNodding = brNodding.readLine();
            }
            brNodding.close();

            brShaking = new BufferedReader(new FileReader(fileFolder + fileNameShaking));
            String lineShaking = brShaking.readLine();

            while (lineShaking != null && !"".equals(lineShaking.trim())) {
                String[] lineSp = lineShaking.split(":");
                String content = lineSp[0];
                String time = lineSp[1];
                String timeStart = lineSp[2];
                String moveValue = lineSp[3];
                if ("shaking".equals(content) && Float.parseFloat(moveValue) > 10) {
                    if (shakingline.equals("")) {
                        shakingline += time + ";" + timeStart+";"+moveValue;
                    } else {
                        shakingline += "," + time + ";" + timeStart+";"+moveValue;
                    }
                    shakingNo++;
                }

                ParseContent pc = new ParseContent();
                pc.setContent(content);
                pc.setTime(time);

                pcs.add(pc);
                lineShaking = brShaking.readLine();
            }
            brShaking.close();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }


        String fileNameWava = vname + "_doctor_waveproperty.txt";
        BufferedReader brWave;
        int totalFrame = 0;
        float totalLength = 0;
        try {
            brWave = new BufferedReader(new FileReader(fileFolder + fileNameWava));
            // get the content of length of speech
            String line = brWave.readLine();
            if (line != null && !"".equals(line.trim())) {
                totalLength = Float.parseFloat(line);
            }

            // get the content of loudness tags
            line = brWave.readLine();
            if (line != null && !"".equals(line.trim())) {
                totalFrame = Integer.parseInt(line);
            }
            brWave.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        BufferedReader br;
        // load speaking period doctor: start
        String endPointDoctorFileName = vname + "_doctor_endpoint_new.txt";
        String endPointsDoctor = "";

        try {
            br = new BufferedReader(new FileReader(fileFolder + endPointDoctorFileName));
            String line = br.readLine();
            while (line != null) {
                endPointsDoctor += line + ";";
                line = br.readLine();
            }
            if (!"".equals(endPointsDoctor)) {
                endPointsDoctor = endPointsDoctor.substring(0, endPointsDoctor.length() - 1);
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        // load speaking period: end

        // load speaking period patient: start
        String endPointPatientFileName = vname + "_patient_endpoint_new.txt";
        String endPointsPatient = "";

        try {
            br = new BufferedReader(new FileReader(fileFolder + endPointPatientFileName));
            String line = br.readLine();
            while (line != null) {
                endPointsPatient += line + ";";
                line = br.readLine();
            }
            if (!"".equals(endPointsPatient)) {
                endPointsPatient = endPointsPatient.substring(0, endPointsPatient.length() - 1);
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        // load speaking period: end

        // analysis turn taking
        TurnTakingAnalyser tta = new TurnTakingAnalyser();
        ArrayList<SpeakingPeriod> sps = tta.turntakingAnalysis(endPointsDoctor, endPointsPatient);
        ArrayList<SpeakingPeriod> shrinkSps = tta.shrink(sps);
        ArrayList<TurnTakingInfo> infos = tta.extractTurnTakingInfo(shrinkSps, sps);

        request.setAttribute("baselineObject", baselineObject);
        request.setAttribute("notesString", notesString);
        request.setAttribute("noddingNo", noddingNo);
        request.setAttribute("shakingNo", shakingNo);
        request.setAttribute("leftVideo", videoToDisplay);
        // request.setAttribute("selftouchline", selfTouchLine);
        request.setAttribute("noddingline", noddingline);
        request.setAttribute("shakingline", shakingline);
        request.setAttribute("video_name", vname);
        request.setAttribute("endPoints_doctor", endPointsDoctor);
        request.setAttribute("endPoints_patient", endPointsPatient);
        request.getSession().setAttribute("pcs", pcs);
        request.setAttribute("docInfo", infos.get(0));
        request.setAttribute("patInfo", infos.get(1));
        request.setAttribute("redpage", "cons_stu");
        request.setAttribute("totalFrame", totalFrame);
        request.setAttribute("totalLength", totalLength);
        request.setAttribute("sesseionId", conbinedSesseionId);
        request.setAttribute("estimatedNC", estimatedNC);
        if (temp.length == 3) {
            request.setAttribute("items", temp[2]);
            
            Doctor doc = (Doctor) SecurityTool.getActiveUser();
            ReflectionLog reflectionLog = new ReflectionLog();
            reflectionLog.setDoctorId(doc.getDoctorId());
            reflectionLog.setStartTime(System.currentTimeMillis());
            reflectionLog.setSessionId(conbinedSesseionId);
            reflectionLog.setNonverbalType(temp[2]);
            reflectionLogService.save(reflectionLog);
            request.setAttribute("reflectionLogId", reflectionLog.getLogId());
            
            return "stu/39_Stu_StartApt_result_nonverbalCombine";
        } else {
            return "stu/29_Stu_StartApt_result_nonverbal";
        }

    }

    @RequestMapping(value = DOCTORPATH + "/frown/{vname}")
    @ResponseBody
    public List<FrownTag> getFrown(HttpServletRequest request, @PathVariable String vname) {

        String fileFolder = VIDEOFOLDER + vname + "/";

        String fileName = vname + "_doctor_frown.txt";
        BufferedReader br;
        List<FrownTag> frowns = new ArrayList<FrownTag>();
        try {
            br = new BufferedReader(new FileReader(fileFolder + fileName));
            String line = br.readLine();
            int lineNo = 1;
            while (line != null && !"".equals(line.trim())) {
                String[] lineSp = line.split(":");
                float value = Float.parseFloat(line);
                float time = lineNo / 25.0f;
                FrownTag st = new FrownTag(lineNo, value, time);
                frowns.add(st);
                line = br.readLine();
                lineNo++;
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return frowns;
    }

    @RequestMapping(value = DOCTORPATH + "/attention/{vname}")
    @ResponseBody
    public List<AttentionTag> getAttention(HttpServletRequest request, @PathVariable String vname) {

        String fileFolder = VIDEOFOLDER + vname + "/";

        String fileName = vname + "_doctor_attention.txt";
        BufferedReader br;
        List<AttentionTag> attentions = new ArrayList<AttentionTag>();
        try {
            br = new BufferedReader(new FileReader(fileFolder + fileName));
            String line = br.readLine();
            int lineNo = 1;
            while (line != null && !"".equals(line.trim())) {
                String[] lineSp = line.split(":");
                float value = Float.parseFloat(line);
                float time = lineNo / 25.0f;
                AttentionTag st = new AttentionTag(lineNo, value, time);
                attentions.add(st);
                line = br.readLine();
                lineNo++;
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return attentions;
    }
    
    @RequestMapping(value = DOCTORPATH + "/stretch/{vname}")
    @ResponseBody
    public List<AttentionTag> getStretch(HttpServletRequest request, @PathVariable String vname) {

        String fileFolder = VIDEOFOLDER + vname + "/";

        String fileName = vname + "_doctor_stretch.txt";
        BufferedReader br;
        List<AttentionTag> attentions = new ArrayList<AttentionTag>();
        try {
            br = new BufferedReader(new FileReader(fileFolder + fileName));
            String line = br.readLine();
            int lineNo = 1;
            while (line != null && !"".equals(line.trim())) {
                String[] lineSp = line.split(":");
                float value = Float.parseFloat(line);
                float time = lineNo / 25.0f;
                AttentionTag st = new AttentionTag(lineNo, value, time);
                attentions.add(st);
                line = br.readLine();
                lineNo++;
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return attentions;
    }
    
    
    @RequestMapping(value = DOCTORPATH + "/selftouch/{vname}")
    @ResponseBody
    public List<SelfTouch> getSelftouch(HttpServletRequest request, @PathVariable String vname) {

        String fileFolder = VIDEOFOLDER + vname + "/";

        String fileName = vname + "_doctor_selftouch.txt";

        BufferedReader br;
        List<SelfTouch> touches = new ArrayList<SelfTouch>();
        try {
            br = new BufferedReader(new FileReader(fileFolder + fileName));
            String line = br.readLine();
            while (line != null && !"".equals(line.trim())) {
                String[] lineSp = line.split(",");
                float start = Float.parseFloat(lineSp[0]);
                float end = Float.parseFloat(lineSp[1]);
                SelfTouch st = new SelfTouch(start, end);
                touches.add(st);
                line = br.readLine();
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return touches;
    }

    @RequestMapping(value = DOCTORPATH + "/smiles/{vname}")
    @ResponseBody
    public List<SmileTag> getSmiles(HttpServletRequest request, @PathVariable String vname) {

        String fileFolder = VIDEOFOLDER + vname + "/";
        String fileName = vname + "_doctor_smileana.txt";

        BufferedReader br;
        List<SmileTag> smiles = new ArrayList<SmileTag>();
        try {
            br = new BufferedReader(new FileReader(fileFolder + fileName));
            String line = br.readLine();
            while (line != null && !"".equals(line.trim())) {
                String[] lineSp = line.split(":");
                int tagId = Integer.parseInt(lineSp[0]);
                float value = Float.parseFloat(lineSp[1]);

                SmileTag st = new SmileTag(tagId, value);
                smiles.add(st);
                line = br.readLine();
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return smiles;
    }



    @RequestMapping(value = DOCTORPATH + "/leanings/{vname}")
    @ResponseBody
    public List<String> getLeanings(HttpServletRequest request, @PathVariable String vname) {

        String fileFolder = VIDEOFOLDER + vname + "/";
        String fileName = vname + "_doctor_leanings.txt";
        BufferedReader br;
        List<String> leanings = new ArrayList<String>();
        try {

            br = new BufferedReader(new FileReader(fileFolder + fileName));

            String line = br.readLine();
            while (line != null && !"".equals(line.trim())) {
                leanings.add(line);
                line = br.readLine();
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return leanings;
    }

    @RequestMapping(value = DOCTORPATH + "/tiltings/{vname}")
    @ResponseBody
    public List<String> getTiltings(HttpServletRequest request, @PathVariable String vname) {

//        String fileFolder = VIDEOFOLDER + vname + "/";
//        String fileName = vname + "_doctor_allpoints_turns";
//        BufferedReader br;
//        List<String> tiltings = new ArrayList<String>();
//        try {
//            // -------------------------------------
//            br = new BufferedReader(new FileReader(fileFolder + fileName));
//            String line = br.readLine();
//            while (line != null && !"".equals(line.trim())) {
//                tiltings.add(line);
//                line = br.readLine();
//            }
//            br.close();
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//        }
//
//        return tiltings;
        
        
        String fileFolder = VIDEOFOLDER + vname + "/";
        String fileName = vname + "_doctor_allpoints_angle";

        BufferedReader br;
        List<String> tiltings = new ArrayList<String>();
        try {
            // -------------------------------------
            br = new BufferedReader(new FileReader(fileFolder + fileName));
            String line = br.readLine();
            while (line != null && !"".equals(line.trim())) {
                tiltings.add(line);
                line = br.readLine();
            }
            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return tiltings;
        
    }

    @RequestMapping(value = DOCTORPATH + "/soundproperty/{vname}")
    @ResponseBody
    public SoundProperty getSoundProperty(HttpServletRequest request, @PathVariable String vname) {
        String fileFolder = VIDEOFOLDER + vname + "/";
        String fileName = vname + "_doctor_waveproperty.txt";
        BufferedReader br;
        SoundProperty pro = new SoundProperty();
        try {
            br = new BufferedReader(new FileReader(fileFolder + fileName));
            // get the content of length of speech
            String line = br.readLine();
            if (line != null && !"".equals(line.trim())) {
                pro.setTotalLength(Float.parseFloat(line));
            }

            // get the content of loudness tags
            line = br.readLine();
            if (line != null && !"".equals(line.trim())) {
                pro.setTotalFrame(Integer.parseInt(line));
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return pro;
    }



    @RequestMapping(value = DOCTORPATH + "/loudness/{vname}")
    @ResponseBody
    public Loudness getLoudness(HttpServletRequest request, @PathVariable String vname) {
        String fileFolder = VIDEOFOLDER + vname + "/";
        String fileName = vname + "_doctor_loudness.txt";

        BufferedReader br;
        Loudness loudness = new Loudness();
        try {
            br = new BufferedReader(new FileReader(fileFolder + fileName));
            // get the content of length of speech
            String line = br.readLine();

            if (line != null && !"".equals(line.trim())) {
                loudness.setTotalLength(line);
            }

            // get the content of loudness tags
            line = br.readLine();
            if (line != null && !"".equals(line.trim())) {
                loudness.setLoudness(line);
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return loudness;
    }

    @RequestMapping(value = DOCTORPATH + "/pitch/{vname}")
    @ResponseBody
    public Pitch getPitch(HttpServletRequest request, @PathVariable String vname) {
        String fileFolder = VIDEOFOLDER + vname + "/";
        String fileName = vname + "_doctor_pitch.txt";

        BufferedReader br;
        Pitch pitch = new Pitch();
        try {
            br = new BufferedReader(new FileReader(fileFolder + fileName));
            // get the content of length of speech
            String line = br.readLine();
            if (line != null && !"".equals(line.trim())) {
                pitch.setPitch(line);
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return pitch;
    }

    @RequestMapping(value = DOCTORPATH + "/touches/{vname}")
    @ResponseBody
    public List<TouchObj> getTouches(HttpServletRequest request, @PathVariable String vname) {
        String fileFolder = VIDEOFOLDER + vname + "/";
        String fileName = vname + "_doctor_selftouch_percentage.txt";

        BufferedReader br;
        List<TouchObj> touches = new ArrayList<TouchObj>();
        try {

            br = new BufferedReader(new FileReader(fileFolder + fileName));
            // get the content of length of speech
            String line = br.readLine();
            int lineNo = 0;
            while (line != null && !"".equals(line.trim())) {
                lineNo++;
                String[] values = line.split(":");
                double value = Double.parseDouble(values[3]);

                TouchObj touch = new TouchObj();
                touch.setLineNo(lineNo);
                touch.setPersentage(value);

                touches.add(touch);

                line = br.readLine();
            }
            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return touches;
    }


    @RequestMapping(value = DOCTORPATH + "/moves/{vname}")
    @ResponseBody
    public List<TouchObj> getMoves(HttpServletRequest request, @PathVariable String vname) {
        String fileFolder = VIDEOFOLDER + vname + "/";
        String fileName = vname + "_doctor_movement.txt";

        BufferedReader br;
        List<TouchObj> touches = new ArrayList<TouchObj>();
        try {

            br = new BufferedReader(new FileReader(fileFolder + fileName));
            // get the content of length of speech
            String line = br.readLine();
            int lineNo = 0;

            while (line != null && !"".equals(line.trim())) {
//                String[] values = line.split(":");
//                lineNo = Integer.parseInt(values[0]);
//                double value = Double.parseDouble(values[1]);
//
//                TouchObj touch = new TouchObj();
//                touch.setLineNo(lineNo);
//                if (value < 0) {
//                    touch.setPersentage(0);
//                } else {
//                    touch.setPersentage(value);
//                }

                TouchObj touch = new TouchObj();
                touch.setLineNo(25*lineNo);
                touch.setPersentage(Float.parseFloat(line));
                touches.add(touch);
                
                lineNo++;
                line = br.readLine();
            }
            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return touches;
    }



    @RequestMapping(value = DOCTORPATH + "/speechrate/{vname}")
    @ResponseBody
    public ArrayList<SpeechRate> getSpeechrate(HttpServletRequest request, @PathVariable String vname) {

        String fileFolder = VIDEOFOLDER + vname + "/";
        String endPointFileName = vname + "_doctor_endpoint.txt";
        String syllableFileName = vname + "_doctor_syllable.txt";

        ArrayList<EndPoint> endPoints = new ArrayList<EndPoint>();
        ArrayList<Syllable> syllables = new ArrayList<Syllable>();

        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(fileFolder + endPointFileName));
            String line = br.readLine();
            while (line != null) {
                String[] temp = line.split(",");
                if (temp.length == 2) {
                    EndPoint ep = new EndPoint();
                    ep.setStart(Float.parseFloat(temp[0]));
                    ep.setEnd(Float.parseFloat(temp[1]));
                    endPoints.add(ep);
                }
                line = br.readLine();
            }
            br.close();

            br = new BufferedReader(new FileReader(fileFolder + syllableFileName));
            line = br.readLine();
            while (line != null) {
                String[] temp = line.split(",");
                if (temp.length == 2) {
                    Syllable sy = new Syllable();
                    sy.setStart(Float.parseFloat(temp[0]));
                    sy.setEnd(Float.parseFloat(temp[1]));
                    syllables.add(sy);
                }
                line = br.readLine();
            }
            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        for (EndPoint endpoint : endPoints) {
            float epStart = endpoint.getStart();
            float epEnd = endpoint.getEnd();
            int syllableNo = endpoint.getSyllableNo();
            for (Syllable syllable : syllables) {

                float syStart = syllable.getStart();
                float syEnd = syllable.getEnd();

                if ((epStart <= syStart || Math.abs(epStart - syStart) < 0.1)
                                && (epEnd >= syEnd || Math.abs(epEnd - syEnd) < 0.1)) {
                    syllableNo++;
                    endpoint.setSyllableNo(syllableNo);
                }
            }
        }

        // get total length the speech
        float totalLength = 0f;
        String propertyFileName = vname + "_doctor_waveproperty.txt";
        try {
            br = new BufferedReader(new FileReader(fileFolder + propertyFileName));
            // get the content of length of speech
            String line = br.readLine();
            if (line != null) {
                totalLength = Float.parseFloat(line);
            }
            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        float[] timeslots = new float[(int) (totalLength / 0.1)];
        for (EndPoint endpoint : endPoints) {

            float epStart = endpoint.getStart();
            float epEnd = endpoint.getEnd();

            float diff = epEnd - epStart;
            float rate = (1 / diff) * 60 * endpoint.getSyllableNo();

            int start = (int) (epStart / 0.1);
            int end = (int) (epEnd / 0.1);

            for (int i = start; i < end; i++) {
                timeslots[i] = rate;
            }
        }

        ArrayList<SpeechRate> srs = new ArrayList<SpeechRate>();
        for (int i = 0; i < timeslots.length; i++) {
            SpeechRate sr = new SpeechRate();
            sr.setTime(i * 0.1f);
            sr.setRate(timeslots[i]);
            srs.add(sr);
        }

        return srs;
    }

    @RequestMapping(value = DOCTORPATH + "/totallength/{vname}")
    @ResponseBody
    public Float gettotalLength(HttpServletRequest request, @PathVariable String vname) {
        String fileFolder = VIDEOFOLDER + vname + "/";
        String propertyFileName = vname + "_doctor_waveproperty.txt";
        BufferedReader br;
        float totalLength = 0f;

        try {
            br = new BufferedReader(new FileReader(fileFolder + propertyFileName));
            String line = br.readLine();
            if (line != null) {
                totalLength = Float.parseFloat(line);
            }
            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return totalLength;
    }

    @RequestMapping(value = DOCTORPATH + "/speechpercentage/{vname}/{splitLength}")
    @ResponseBody
    public ArrayList<SpeechPercentage> getSpeechPercetage(HttpServletRequest request, @PathVariable String vname,
                    @PathVariable int splitLength) {
        ArrayList<SpeechPercentage> sps = new ArrayList<SpeechPercentage>();

        String fileFolder = VIDEOFOLDER + vname + "/";
        String endPointDoctorFileName = vname + "_doctor_endpoint_new.txt";
        String endPointPatientFileName = vname + "_patient_endpoint_new.txt";
        String propertyFileName = vname + "_doctor_waveproperty.txt";
        BufferedReader br;
        float totalLength = 0f;
        ArrayList<Float> stopPoints = new ArrayList<Float>();

        try {
            br = new BufferedReader(new FileReader(fileFolder + propertyFileName));
            String line = br.readLine();
            if (line != null) {
                totalLength = Float.parseFloat(line);
                stopPoints = SpeechPercentageTool.getStopPoints(totalLength);
            }
            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        BufferedReader doctorBr;
        BufferedReader patientBr;

        try {
            doctorBr = new BufferedReader(new FileReader(fileFolder + endPointDoctorFileName));
            patientBr = new BufferedReader(new FileReader(fileFolder + endPointPatientFileName));

            ArrayList<EndPoint> endPointsDoctor = new ArrayList<EndPoint>();
            ArrayList<EndPoint> endPointsPatient = new ArrayList<EndPoint>();

            String line = doctorBr.readLine();
            while (line != null) {
                String[] temp = line.split(",");
                if (temp.length == 2) {
                    EndPoint ep = new EndPoint();
                    ep.setStart(Float.parseFloat(temp[0]));
                    ep.setEnd(Float.parseFloat(temp[1]));
                    endPointsDoctor.add(ep);
                }
                line = doctorBr.readLine();
            }
            doctorBr.close();

            line = patientBr.readLine();
            while (line != null) {
                String[] temp = line.split(",");
                if (temp.length == 2) {
                    EndPoint ep = new EndPoint();
                    ep.setStart(Float.parseFloat(temp[0]));
                    ep.setEnd(Float.parseFloat(temp[1]));
                    endPointsPatient.add(ep);
                }
                line = patientBr.readLine();
            }
            patientBr.close();
            // sps = SpeechPercentageTool.getSpeechPercentage(stopPoints, endPointsDoctor,
            // endPointsPatient);
            sps =
                            SpeechPercentageTool.getSpeechPercentageSplit(stopPoints, endPointsDoctor,
                                            endPointsPatient, splitLength);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return sps;
    }

    @RequestMapping(value = DOCTORPATH + "/parse")
    @ResponseBody
    public List<ParseContent> parsing(HttpServletRequest request) {

        MyNative mn = new MyNative();

        String content = mn.sayHello();

        String[] marks = content.split(",");

        List<ParseContent> cons = new ArrayList<ParseContent>();

        for (String mark : marks) {
            ParseContent pc = new ParseContent();
            pc.setContent(mark);
            cons.add(pc);
        }

        return cons;
    }

    @RequestMapping(value = DOCTORPATH + "/cons_stu/{doctorId}")
    public String cons_stu(HttpServletRequest request, @PathVariable int doctorId) {
    	
    	Doctor doc = (Doctor) SecurityTool.getActiveUser();
    	int docId = doc.getDoctorId();
    	if(doc==null || doc.getDoctorId()!=doctorId){
    		String redirectUrl = DOCTORPATH + "/logoff";
    		return "redirect:" + redirectUrl;
    	}
    	
    	Doctor updatedDoc = doctorService.getDoctorByDoctorId(docId);
    	int agreement = updatedDoc.getAgreement();
    	
        List<Object> videos = videoService.getVideoByDoctorIdWithProgress(doctorId);
        request.setAttribute("videoList", videos);
        request.setAttribute("redpage", "cons_stu");
        request.setAttribute("agreement", agreement);
        
        ReflectionLog reflectionLog = new ReflectionLog();
        reflectionLog.setDoctorId(doc.getDoctorId());
        reflectionLog.setStartTime(System.currentTimeMillis());
        reflectionLog.setSessionId("");
        reflectionLog.setNonverbalType(41+"");
        reflectionLogService.save(reflectionLog);
        
        return "stu/30_Stu_Consultations";
    }

    @RequestMapping(value = PATIENTPATH + "/cons_sp")
    @ResponseBody
    public List<Object> cons_sp(HttpServletRequest request) {
        Patient patient = (Patient) SecurityTool.getActiveUser();
        int patientId = patient.getPatientid();
        List<Object> videos = videoService.getVideoByPatientIdWithProgress(patientId);
        return videos;
    }

    @RequestMapping(value = TUTORPATH + "/searchvideo")
    public String searchVideoByStudentName(HttpServletRequest request) {
        String doctorUserName = request.getParameter("searchContent");

        Doctor doc = doctorService.getDoctorByUsername(doctorUserName);
        if (doc != null) {
            List<Video> videos = videoService.getVideoByDoctorId(doc.getDoctorId());
            request.setAttribute("videoList", videos);
        }
        request.setAttribute("redpage", "review_tut");
        request.setAttribute("searchContent", doctorUserName);
        return "tut/32_Tut_search";
    }
    
    @RequestMapping(value = TUTORPATH + "/enablepostcheck/{sessionId}/{enable}")
    @ResponseBody
    public ReturnValue enablePostCheck(HttpServletRequest request, @PathVariable String sessionId, @PathVariable int enable) {
        
        List<Video> videos = videoService.getVideoBySessionId(sessionId);
        for (Video video : videos) {
            video.setAllowTutorNotes(enable);
            videoService.saveVideo(video);
        }
        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        return rv;
    }
    
    @RequestMapping(value = "/sendRecordingFinishEmail/{processDate}")
    @ResponseBody
    public ReturnValue sendRecordingFinishEmail(HttpServletRequest request, @PathVariable String processDate) {
        System.out.println(processDate);
        if(processDate!=null && !"".equals(processDate)){
            List<Video> videos = videoService.getVideoByDate(processDate);
            for (Video video : videos) {
                if(video!=null && video.getAllowFeedback()!=null&& video.getAllowFeedback()==1){
                    String conbinedSesseion = video.getSessionId();
                    Appointment apt = appointmentService.getAppointmentBySessionId(conbinedSesseion);
                    
                    // check whether analyse this video: start
                    int doctorId = apt.getDoctorId();
                    int patientId = apt.getPatientId();
                    Doctor doctor = doctorService.getDoctorByDoctorId(doctorId);
                    Patient patient = patientService.getPatientByPatientId(patientId);
                    if(doctor.getAgreement()!=1 || patient.getAgreement()!=1){
                        continue;
                    }
                    // check whether analyse this video: end 
                    video.setAllowFeedback(1);
                    videoService.saveVideo(video);
                    EmailTool.nvbFeedbackReady(apt, doctor, patient);
                }
            }
        }
        
        return null;
    }
    
    
    
    
    @RequestMapping(value = "/sendRecordingFinishEmailDiff/{processDate}")
    @ResponseBody
    public ReturnValue sendRecordingFinishEmailDiff(HttpServletRequest request, @PathVariable String processDate) {
        System.out.println(processDate);
        if(processDate!=null && !"".equals(processDate)){
            List<Video> videos = videoService.getVideoByDate(processDate);
            for (Video video : videos) {
                if(video!=null && video.getAllowFeedback()!=null){
                    
                    int feedbackType = video.getAllowFeedback(); 
                    String conbinedSesseion = video.getSessionId();
                    Appointment apt = appointmentService.getAppointmentBySessionId(conbinedSesseion);
                    
                    // check whether analyse this video: start
                    int doctorId = apt.getDoctorId();
                    int patientId = apt.getPatientId();
                    Doctor doctor = doctorService.getDoctorByDoctorId(doctorId);
                    Patient patient = patientService.getPatientByPatientId(patientId);
                    if(doctor.getAgreement()!=1 || patient.getAgreement()!=1){
                        continue;
                    }
                    
                    String studentId = doctor.getUserId();
                    
                    StudentGroup sg = studentGroupService.getStudentGroupByStudentId(studentId);
                    int emailGroup = sg.getEmailGroup();  
                    
                    // check whether analyse this video: end 
                    video.setAllowFeedback(1);
                    videoService.saveVideo(video);
                    
                    String sessionId = apt.getSessionId();
//                    List<Note> thumbUp = noteService.getNoteBySessionIdType(sessionId, 2, 1);
//                    List<Note> thumbDown = noteService.getNoteBySessionIdType(sessionId, 2, 2);
                    List<Note> notes = noteService.getNoteBySessionIdTypeAll(sessionId, 2);
                    
                    // SP's evaluation
                    Answer answer = answerService.loadAnswerBySessionIdUserId(sessionId, "patient", 1);
                    
                    int emailLevel = video.getEmailLevel();
                    if(video.getReviewComments()==null && video.getReviewSoca()==null && video.getReviewBehav()==null && video.getHasSendEmail()==0){
                        EmailTool.nvbFeedbackReadyDiff(apt, doctor, patient,feedbackType, notes, answer, emailLevel, video, emailGroup);
                        video.setHasSendEmail(1);
                        videoService.saveVideo(video);
                    }
                }
            }
        }
        
        return null;
    }
    
    @Scheduled(cron = "00 00 14 ? * *")
    @RequestMapping(value = "/sendRecordingFinishEmailLevels")
    @ResponseBody
    public void sendRecordingFinishEmailDiff() {
            List<Video> videos = videoService.getAllUnreviewedVideo();
            for (Video video : videos) {
                if(video!=null && video.getAllowFeedback()!=null){
                    
                    int feedbackType = video.getAllowFeedback(); 
                    String conbinedSesseion = video.getSessionId();
                    Appointment apt = appointmentService.getAppointmentBySessionId(conbinedSesseion);
                    
                    // check whether analyse this video: start
                    int doctorId = apt.getDoctorId();
                    int patientId = apt.getPatientId();
                    Doctor doctor = doctorService.getDoctorByDoctorId(doctorId);
                    Patient patient = patientService.getPatientByPatientId(patientId);
//                    if(doctor.getAgreement()!=1 || patient.getAgreement()!=1){
//                        continue;
//                    }
                    
                    String studentId = doctor.getUserId();
                    StudentGroup sg = studentGroupService.getStudentGroupByStudentId(studentId);
                    int emailGroup = sg.getEmailGroup();  
                    
                    // check whether analyse this video: end 
                    video.setAllowFeedback(1);
                    videoService.saveVideo(video);
                    
                    String sessionId = apt.getSessionId();
                    List<Note> notes = noteService.getNoteBySessionIdTypeAll(sessionId, 2);
                    
                    // SP's evaluation
                    Answer answer = answerService.loadAnswerBySessionIdUserId(sessionId, "patient", 1);
                    
                    int emailLevel = video.getEmailLevel();
                    if((video.getReviewComments()==null || video.getReviewSoca()==null || video.getReviewBehav()==null)&& video.getEmailLevel()!=0 && video.getHasSendEmail()==0){
                        EmailTool.nvbFeedbackReadyDiff(apt, doctor, patient,feedbackType, notes, answer, emailLevel, video, emailGroup);
                        video.setHasSendEmail(1);
                        videoService.saveVideo(video);
                    }
                }
            }
    }
    
    @Scheduled(cron = "00 55 13 ? * *")
    @RequestMapping(value = "/upgradeEmailLevel")
    @ResponseBody
    public void upgradeEmailLevel() throws Exception{
        System.out.println("upgrade email level.");
    	
        List<Video> videos = videoService.getAllUnreviewedVideo();
        
        for (Video video : videos) {
            
            int emailLevel = video.getEmailLevel();
            int hasSend = video.getHasSendEmail();
            
            SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            
            String generatedDateStr = timeFormat.format(video.getGenerateTime()).split(" ")[0];
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String currentDateStr = df.format(new Date());            
            
            Date generatedDate = timeFormat.parse(generatedDateStr+" 00:00:00)");
            Date currentDate = timeFormat.parse(currentDateStr+" 00:00:00)");
            
            long generatedTime = generatedDate.getTime();
            long currentTime = currentDate.getTime();
            
            System.out.println("generatedDate: " + generatedDate);
            System.out.println("currentDate: " + currentDate);
            
            int gapInday = (int)Math.round((currentTime-generatedTime)/(1000*24*3600*1.0));
            
            int LEVEL1 = 3;
            int LEVEL2 = 5;
            int LEVEL3 = 7;
            
            if(emailLevel == 0 && gapInday==LEVEL1){
                emailLevel = 1;
                video.setHasSendEmail(0);
            }else if(emailLevel == 1 && hasSend==1 && gapInday==LEVEL2){
                emailLevel = 2;
                video.setHasSendEmail(0);
            }else if(emailLevel == 2 && hasSend==1 && gapInday==LEVEL3){
                emailLevel = 3;
                video.setHasSendEmail(0);
            }else{
                // return null;
            }
            
            video.setEmailLevel(emailLevel);
            videoService.saveVideo(video);
            
        }
        
//        return null;
    }
    
    @Scheduled(cron = "00 45 13 ? * *")
    @RequestMapping(value = "/setAllowFeedback")
    @ResponseBody
    public void setAllowFeedback() throws Exception{
        System.out.println("upgrade allow feedback");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String currentDateStr = df.format(System.currentTimeMillis()-24*3600*1000);            
        List<Video> videos = videoService.getVideoByDate(currentDateStr);
       
        for (Video video : videos) {
        	if(video.getId()!=2107 && video.getId()!=2175 && video.getId()!=2189){
        		video.setAllowFeedback(1);
        		videoService.saveVideo(video);
        	}
        }
    }
    
//    // send a thanks email to all participated SP in previous week.
////    @Scheduled(cron = "00 25 06 * * MON")
//    @RequestMapping(value = "/sendThanksEmail")
////    @ResponseBody
//    public void sendThanksEmail() throws Exception{
//        
//    	SimpleDateFormat timeFormatShort = new SimpleDateFormat("yyyy-MM-dd");
//        String dateShort = timeFormatShort.format(new Date());
//    	
//        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        Date endTimeDate = timeFormat.parse(dateShort+" 00:00:00)");
//        long endTime = endTimeDate.getTime();
//        long startTime = endTime - 7*24*3600*1000;
//        
//        List<Patient> patients = patientService.getPatientInPeriod(startTime, endTime);
//
//        for (Patient patient : patients) {
//            EmailTool.sendThankEmailToSP(patient);
//        }  
//    } 
    
    @Scheduled(cron = "00 25 06 * * MON")
    @RequestMapping(value = "/sendThanksEmailFeedback")
    @ResponseBody
    public void sendThanksEmailFeedback(HttpServletRequest request) throws Exception{
        
    	SimpleDateFormat timeFormatShort = new SimpleDateFormat("yyyy-MM-dd");
        String dateShort = timeFormatShort.format(new Date());
    	
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date endTimeDate = timeFormat.parse(dateShort+" 00:00:00)");
        long endTime = endTimeDate.getTime();
        long startTime = endTime - 7*24*3600*1000;
        

        List<Patient> patients = patientService.getPatientInPeriod(startTime, endTime);
//        List<Patient> patients = new ArrayList<Patient>();
//        Patient testPatient = patientService.getPatientByPatientId(29);
//        Patient testPatient = patientService.getPatientByPatientId(217);
//        patients.add(testPatient);

	for (Patient patient : patients) {
        	PatientWeeklyEmail patientEmail = new PatientWeeklyEmail();
            patientEmail.setCreateAt(System.currentTimeMillis());
            patientEmail.setDoctorIds("");
            patientEmail.setEmailType(1);
            patientEmail.setHasBeenAnswered(0);
            patientEmail.setHasBeenClick(0);
            patientEmail.setPatientId(patient.getPatientid());
            patientEmail.setStatus(1);
            
            int emailId = patientWeeklyEmailService.savePatientWeeklyEmail(patientEmail);
        	
            EmailTool.sendThankEmailToSPFeedback(patient, emailId);
        }  
    }
    
    
    @RequestMapping(value = "/patientEmailClick/{emailId}")
    public String patientEmailClick(HttpServletRequest request, @PathVariable int emailId) throws Exception {
        
    	PatientWeeklyEmail email = patientWeeklyEmailService.getPatientEmailById(emailId);
    	email.setHasBeenClick(1);
    	patientWeeklyEmailService.updatePatientWeeklyEmail(email);
    	
    	PatientWeeklyEmailClicking clicking = new PatientWeeklyEmailClicking();
    	clicking.setClickAt(System.currentTimeMillis());
    	clicking.setEmailId(emailId);
    	patientWeeklyEmailClickingService.savePatientWeeklyEmailClicking(clicking);
    	
    	request.setAttribute("emailId", emailId);
    	
    	return "patientEmail" ;
    	
    }
    
    @RequestMapping(value = "/patientEmailClickWithPersonalFeedback/{emailId}")
    public String patientEmailClickWithPersonalFeedback(HttpServletRequest request, @PathVariable int emailId) throws Exception {
        
    	// List<Patient> patients = aptService.getPatientInPeriod(startTime, endTime);
    	
    	
    	PatientWeeklyEmail email = patientWeeklyEmailService.getPatientEmailById(emailId);
    	email.setHasBeenClick(1);
    	patientWeeklyEmailService.updatePatientWeeklyEmail(email);
    	
    	PatientWeeklyEmailClicking clicking = new PatientWeeklyEmailClicking();
    	clicking.setClickAt(System.currentTimeMillis());
    	clicking.setEmailId(emailId);
    	patientWeeklyEmailClickingService.savePatientWeeklyEmailClicking(clicking);
    	
    	request.setAttribute("emailId", emailId);
    	
    	
    	SimpleDateFormat timeFormatShort = new SimpleDateFormat("yyyy-MM-dd");
        String dateShort = timeFormatShort.format(email.getCreateAt());
    	
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date endTimeDate = timeFormat.parse(dateShort+" 00:00:00)");
        long endTime = endTimeDate.getTime();
//        long startTime = endTime - 20L*24*3600*1000;
//        List<Object> appFeedback = appointmentService.getAppointmentWithStudentPersonalFeedback(282, startTime, endTime);
        long startTime = endTime - 7L*24*3600*1000;
        List<Object> appFeedback = appointmentService.getAppointmentWithStudentPersonalFeedback(email.getPatientId(), startTime, endTime);
        
        ArrayList<Appointment> aptList = new ArrayList<Appointment>();
        ArrayList<Answer> answerList = new ArrayList<Answer>();
        ArrayList<Doctor> docList = new ArrayList<Doctor>();
        
        for(int i=0; i<appFeedback.size();i++){
        	Object[] hql_result = (Object[]) appFeedback.get(i);
        	Appointment apt = (Appointment)(hql_result[0]);
        	Answer answer = (Answer)(hql_result[1]);
        	Doctor doc = doctorService.getDoctorByDoctorId(apt.getDoctorId());
        	
        	if(answer.getContent()!=null && !answer.getContent().trim().equals("")){
        		String answerContent = answer.getContent();
        		JSONArray jsonArray = JSONArray.fromObject(answerContent);
        		
        		for (int k = 0; k < jsonArray.size(); k++) {
                	JSONObject jAnswer = jsonArray.getJSONObject(k);
                	int questionId = jAnswer.getInt("qid");
                	String questionValue = jAnswer.getString("qvalue");
                	if(questionId==143){
                		answer.setContent(questionValue);
                	}
        		}
        		aptList.add(apt);
        		answerList.add(answer);
        		docList.add(doc);
        	}
        }
        
        
        request.setAttribute("aptList", aptList);
        request.setAttribute("answerList", answerList);
        request.setAttribute("docList", docList);
        
    	return "patientEmail" ;
    	
    }
    
    // send a thanks email to all participated SP in previous week.
    // @Scheduled(cron = "00 40 06 * * MON")
    @RequestMapping(value = "/sendUnparticipatedStudentNotification")
    @ResponseBody
    public void sendUnparticipatedStudentNotification() throws Exception{
        
    	List<StudentGroup> students = studentGroupService.getAllUnParticipatedStudent();
    	
    	int aptNum = 0;
    	List<Appointment> apts = appointmentService.getAvailableAptByTime(System.currentTimeMillis());
    	if(apts!=null){
    		aptNum = apts.size();
    	}
    	
    	EmailTool.sendUnparticipatedStudentNotification(students, aptNum);
    	
    } 
    
    @Scheduled(cron = "00 40 05 * * MON")
    @RequestMapping(value = "/sendWeeklyReport")
    @ResponseBody
    public void sendWeeklyReport() throws Exception{
        
        SimpleDateFormat timeFormatShort = new SimpleDateFormat("ddMMyyyy");
        String dateShort = timeFormatShort.format(new Date());
        SimpleDateFormat timeFormat = new SimpleDateFormat("ddMMyyyy hh:mm:ss");
        Date endTimeDate;
        try {
            endTimeDate = timeFormat.parse(dateShort+" 00:00:00)");
            long endTime = endTimeDate.getTime();
            long timestamp = endTime - 7*24*3600*1000;
            
            List<Object> spRegistration =  exportService.exportSPRegistration(timestamp);
            List<Object> aptOffered =  exportService.exportAptOffered( timestamp);
            List<Object> aptCompleted =  exportService.exportAptCompleted( timestamp);
            List<Object> aptFailed =  exportService.exportAptFailed( timestamp);
            List<Object> aptCanceledStu =  exportService.exportAptCanceledStu( timestamp);
            List<Object> aptCanceledSP =  exportService.exportAptCanceledSP( timestamp);
            List<Object> aptCompletedSP =  exportService.exportAptCompletedSP( timestamp);
            List<Object> aptCompletedStu =  exportService.exportAptCompletedStu( timestamp);
            List<Object> aptImappropriate =  exportService.exportAptImappropriate( timestamp);
            List<Object> aptConclusion1 =  exportService.exportAptConclusion(1,  timestamp);
            List<Object> aptConclusion2 =  exportService.exportAptConclusion(2,  timestamp);
            List<Object> aptConclusion3 =  exportService.exportAptConclusion(3,  timestamp);
            
            ArrayList<Integer> aptConclusions = new ArrayList<Integer>();
            aptConclusions.add(aptConclusion1.size());
            aptConclusions.add(aptConclusion2.size());
            aptConclusions.add(aptConclusion3.size());
            
            ExcelConstructor.constructWeeklyReport(spRegistration,aptOffered,aptCompleted,aptFailed,aptCanceledStu,aptCanceledSP,aptCompletedSP,aptCompletedStu,aptImappropriate,aptConclusions);
            
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
    } 
    
    @Scheduled(cron = "00 45 07 ? * *")
    @RequestMapping(value = "/sendSecondReminder")
    @ResponseBody
    public void sendSecondReminder() throws Exception{
    	List<Appointment> pendingApts = appointmentService.getPendingRequest24H(1, 2);
    	for(int i =0; i<pendingApts.size(); i++){
    		Appointment apt = pendingApts.get(i);
    		Doctor doctor = doctorService.getDoctorByDoctorId(apt.getDoctorId());
    		Patient patient = patientService.getPatientByPatientId(apt.getPatientId());
    		EmailTool.sendSecondReminder(apt, patient);
    	}
    } 
    
    public static void main(String[] args) throws Exception{
        
    	System.out.print(System.currentTimeMillis());
    	/*
        String audioFileBaseDirStr = "E:/apache-tomcat-7.0.63/webapps/eqclinic/video/video_1447891456155/";
        String doctorFile = "video_1447891456155_doctor";
        int FPS = 25;
     // -------------------------- nodding -----------------------------------------------
        ParserNodding tn = new ParserNodding();
        // detect the nodding
        tn.manipXYNodding(audioFileBaseDirStr + doctorFile + "_nosepoint.txt", audioFileBaseDirStr + doctorFile
                        + "_nodding.txt", FPS);
        // detect the shaking
        tn.manipXYShaking(audioFileBaseDirStr + doctorFile + "_nosepoint.txt", audioFileBaseDirStr + doctorFile
                + "_shaking.txt", 25.0f);
        
        tn.deleteRepeat(audioFileBaseDirStr + doctorFile+ "_shaking.txt", 
                audioFileBaseDirStr + doctorFile + "_nodding.txt", 
                audioFileBaseDirStr + doctorFile+ "_shakingnew.txt", 
                audioFileBaseDirStr + doctorFile + "_noddingnew.txt");
    */
    }
    
    
}
