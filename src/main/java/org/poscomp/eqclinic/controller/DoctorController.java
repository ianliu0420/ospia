package org.poscomp.eqclinic.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.LoginLog;
import org.poscomp.eqclinic.domain.StudentGroup;
import org.poscomp.eqclinic.domain.Teacher;
import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.AvailableStudentGroupService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.LoginLogService;
import org.poscomp.eqclinic.service.interfaces.StudentGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class DoctorController {

    private static final Logger logger = Logger.getLogger(DoctorController.class);


    @Autowired
    private DoctorService doctorService;
    
    @Autowired
    private LoginLogService loginLogService;

    @Autowired
    private StudentGroupService studentGroupService;

    @Autowired
    private AvailableStudentGroupService availableStudentGroupService;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";
    private static final String SURVEYPATH = "/survey";

    @RequestMapping(value = DOCTORPATH + "/updateagreement", method = {RequestMethod.POST},
                    headers = {"Content-type=application/json"})
    @ResponseBody
    public ReturnValue savePatient(HttpServletRequest request, @RequestBody Doctor doctor) {
        ReturnValue rv = new ReturnValue();
        if (doctor != null) {
            Doctor doctorInDb = doctorService.getDoctorByDoctorId(doctor.getDoctorId());
            doctorInDb.setAgreement(doctor.getAgreement());
            doctorService.saveDoctor(doctorInDb);
            rv.setCode(1);

            MyUserPrincipal activeUser =
                            (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            activeUser.setDoctor(doctorInDb);
        } else {
            rv.setCode(0);
        }
        return rv;
    }

    @RequestMapping(value = DOCTORPATH + "/finishtraining/{doctorId}", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue finishTraning(HttpServletRequest request, @PathVariable int doctorId) {
        ReturnValue rv = new ReturnValue();

        Doctor doctorInDb = doctorService.getDoctorByDoctorId(doctorId);
        if (doctorInDb != null) {
            doctorInDb.setTraining(1);
            doctorService.saveDoctor(doctorInDb);
            rv.setCode(1);

            MyUserPrincipal activeUser =
                            (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            activeUser.setDoctor(doctorInDb);
        } else {
            rv.setCode(0);
        }
        return rv;

    }

    @RequestMapping(value = "/verifyUserDetails", method = {RequestMethod.POST},
                    headers = {"Content-type=application/json"})
    @ResponseBody
    public Doctor verifyUserDetails(HttpServletRequest request, @RequestBody Doctor doctor) {

        String allowGroupsStr = availableStudentGroupService.getAllAvailableGroups();

        String studentId = doctor.getUserId();
        StudentGroup sGroup = studentGroupService.getStudentGroupByStudentId(studentId);

        if (sGroup == null || sGroup.getStatus()==0) {
            return null;
        }

        int groupNumber = sGroup.getGroupNumber();

        boolean isAllowed = false;
        String[] allowGroups = allowGroupsStr.split(",");


        for (int i = 0; i < allowGroups.length; i++) {
            if (allowGroups[i].equals(groupNumber + "")) {
                isAllowed = true;
                break;
            }
        }

        if (isAllowed == false) {
            return null;
        }

        Doctor doctorInDb = doctorService.getDoctorByUsername(doctor.getUsername());
        if (doctorInDb == null) {
            String password = UUID.randomUUID().toString();
            Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
            String hashedPassword = passwordEncoder.encodePassword(password, doctor.getUsername());
            doctor.setPassword(hashedPassword);
            doctor.setAgreement(0);
            doctor.setStatus(1);
            doctor.setLastLoginAt(0L);
            doctor.setTraining(0);
            doctorService.saveDoctor(doctor);
            doctor.setPassword(password);
            
            LoginLog loginLog = new LoginLog();
            loginLog.setDoctorId(doctor.getDoctorId());
            loginLog.setLoginAt(System.currentTimeMillis());
            loginLogService.saveLoginLog(loginLog);
            
            return doctor;

        } else {

            String password = UUID.randomUUID().toString();
            Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
            String hashedPassword = passwordEncoder.encodePassword(password, doctor.getUsername());
            doctorInDb.setPassword(hashedPassword);
            doctorService.saveDoctor(doctorInDb);
            doctorInDb.setPassword(password);
            
            LoginLog loginLog = new LoginLog();
            loginLog.setDoctorId(doctorInDb.getDoctorId());
            loginLog.setLoginAt(System.currentTimeMillis());
            loginLogService.saveLoginLog(loginLog);
            
            return doctorInDb;

        }
    }

    @RequestMapping(value = SURVEYPATH + "/students", method = { RequestMethod.GET })
    public String allStudent(HttpServletRequest request) {
    	
        String searchContent = request.getParameter("searchContent");
        List<StudentGroup> students = null;
        if (searchContent == null || "".equals(searchContent)) {
        	students = studentGroupService.getAllStudent();
        } else {
        	students = studentGroupService.searchStudent(searchContent);
        }
        request.setAttribute("students", students);
        request.setAttribute("redpage", "students_admin");
        request.setAttribute("searchContent", searchContent);
        return "survey/liststudent";
    }
    
    @RequestMapping(value = SURVEYPATH + "/disablestudent/{studentGroupId}", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue disableStudent(HttpServletRequest request, @PathVariable int studentGroupId) {

        ReturnValue rv = new ReturnValue();
        if (studentGroupId != 0) {
            StudentGroup student = studentGroupService.getStudentGroupById(studentGroupId);
            student.setStatus(0);
            studentGroupService.saveStudentGroup(student);
            rv.setCode(1);
        } else {
            rv.setCode(0);
        }

        return rv;
    }
    
    @RequestMapping(value = SURVEYPATH + "/uploadExcelFileStudentMajor", method = {RequestMethod.POST})
    public String uploadFileMajor(@RequestParam(value="filenameMajor") MultipartFile file, HttpServletRequest request,HttpServletResponse response) throws IOException {
    	
    	if(file==null) return null;
    	InputStream is = file.getInputStream();
    	XSSFWorkbook hssfWorkbook = new XSSFWorkbook(is);  
    	
    	for (int n = 0; n < hssfWorkbook.getNumberOfSheets(); n++) { 
    		XSSFSheet sheet = hssfWorkbook.getSheetAt(n);  
            if (sheet == null) {  
                continue;  
            }  
    		
            // clear studentGroup table
            studentGroupService.clearStudentGroup();
            
            for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {  
            	Row row = sheet.getRow(rowNum); 
            	
            	if(row==null){
            		continue;
            	}
            	
            	StudentGroup sg = new StudentGroup();
                for (int i = 0; i < row.getLastCellNum(); i++) {  
                	Cell cell = row.getCell(i);  
                	String cellValue = cell.getStringCellValue();
                	cellValue = cellValue.trim();
                	
                	if(cellValue!=null && !"".equals(cellValue)){
                		if(i==0){
                    		sg.setFirstName(cellValue);	
                    	}else if(i==1){
                    		sg.setLastName(cellValue);
                    	}else if(i==2){
                    		
                    		sg.setStudentId(cellValue);
                    		sg.setEmail(cellValue+"@unsw.edu.au");
                    		sg.setStatus(1);
                    		sg.setGroupNumber(7);
                    		
                    		List<StudentGroup> emailGroup1 = studentGroupService.getStudentByEmailGroupId(1);
                    		List<StudentGroup> emailGroup2 = studentGroupService.getStudentByEmailGroupId(2);
                    		
                    		if(emailGroup1==null){
                    			sg.setEmailGroup(1);
                    		}
                    		if(emailGroup2==null){
                    			sg.setEmailGroup(2);
                    		}
                    		
                    		if(emailGroup1!=null && emailGroup2!=null && emailGroup1.size()>emailGroup2.size()){
                    			sg.setEmailGroup(2);
                    		}else{
                    			sg.setEmailGroup(1);
                    		}
                    		
                    		List<StudentGroup> imageGroup1 = studentGroupService.getStudentByImageGroupId(1);
                    		List<StudentGroup> imageGroup2 = studentGroupService.getStudentByImageGroupId(2);
                    		
                    		if(imageGroup1==null){
                    			sg.setEvaluationImage(1);
                    		}
                    		if(imageGroup2==null){
                    			sg.setEvaluationImage(2);
                    		}
                    		
                    		if(imageGroup1!=null && imageGroup2!=null && imageGroup1.size()>imageGroup2.size()){
                    			sg.setEvaluationImage(2);
                    		}else{
                    			sg.setEvaluationImage(1);
                    		}
                    	}
                	}
                	
                	StudentGroup studentInDB = studentGroupService.getStudentGroupByStudentId(sg.getStudentId());
                    if(studentInDB==null && sg.getStudentId()!=null && sg.getEmail()!=null){
                    	studentGroupService.saveStudentGroup(sg);
                    }
                }
            }
    	}
    	is.close();
    	String redirectUrl = SURVEYPATH + "/students";
		return "redirect:" + redirectUrl;
    } 
    
    @RequestMapping(value = SURVEYPATH + "/uploadExcelFileStudentMinor", method = {RequestMethod.POST})
    public String uploadFileMinor(@RequestParam(value="filenameMinor") MultipartFile file, HttpServletRequest request,HttpServletResponse response) throws IOException {
    	
    	if(file==null) return null;
    	InputStream is = file.getInputStream();
    	XSSFWorkbook hssfWorkbook = new XSSFWorkbook(is);  
    	
    	for (int n = 0; n < hssfWorkbook.getNumberOfSheets(); n++) { 
    		
    		XSSFSheet sheet = hssfWorkbook.getSheetAt(n);  
            if (sheet == null) {  
                continue;  
            }  
    		
            for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {  
            	Row row = sheet.getRow(rowNum);  
            	
            	if(row==null){
            		continue;
            	}
            	
            	StudentGroup sg = new StudentGroup();
            	
                for (int i = 0; i < row.getLastCellNum(); i++) {  
                	Cell cell = row.getCell(i);  
                	
                	if(cell!=null){
                		String cellValue = cell.getStringCellValue();
                		cellValue = cellValue.trim();
                		if(cellValue!=null && !"".equals(cellValue)){
                			
                			if(i==0){
                				sg.setFirstName(cellValue);	
                			}else if(i==1){
                				sg.setLastName(cellValue);
                			}else if(i==2){
                				
                				sg.setStudentId(cellValue);
                				sg.setEmail(cellValue+"@unsw.edu.au");
                				sg.setStatus(1);
                				sg.setGroupNumber(7);
                				
                				List<StudentGroup> emailGroup1 = studentGroupService.getStudentByEmailGroupId(1);
                				List<StudentGroup> emailGroup2 = studentGroupService.getStudentByEmailGroupId(2);
                				
                				if(emailGroup1==null){
                					sg.setEmailGroup(1);
                				}
                				if(emailGroup2==null){
                					sg.setEmailGroup(2);
                				}
                				
                				if(emailGroup1!=null && emailGroup2!=null && emailGroup1.size()>emailGroup2.size()){
                					sg.setEmailGroup(2);
                				}else{
                					sg.setEmailGroup(1);
                				}
                				
                				List<StudentGroup> imageGroup1 = studentGroupService.getStudentByImageGroupId(1);
                				List<StudentGroup> imageGroup2 = studentGroupService.getStudentByImageGroupId(2);
                				
                				if(imageGroup1==null){
                					sg.setEvaluationImage(1);
                				}
                				if(imageGroup2==null){
                					sg.setEvaluationImage(2);
                				}
                				
                				if(imageGroup1!=null && imageGroup2!=null && imageGroup1.size()>imageGroup2.size()){
                					sg.setEvaluationImage(2);
                				}else{
                					sg.setEvaluationImage(1);
                				}
                			}
                		}
                		
                	}
                	
                	 StudentGroup studentInDB = studentGroupService.getStudentGroupByStudentId(sg.getStudentId());
                     if(studentInDB==null && sg.getStudentId()!=null && sg.getEmail()!=null){
                     	studentGroupService.saveStudentGroup(sg);
                     }else{
                    	 if(studentInDB!=null){
                    		 studentInDB.setStatus(1);
                        	 studentGroupService.saveStudentGroup(studentInDB);
                    	 }
                     }
                }
            }
    	}
    	is.close();
    	String redirectUrl = SURVEYPATH + "/students";
		return "redirect:" + redirectUrl;
    } 
    
}
