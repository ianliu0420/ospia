package org.poscomp.eqclinic.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.poscomp.eqclinic.util.SecurityTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class ScenarioController {

    private static final Logger logger = Logger.getLogger(ScenarioController.class);

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";

    @Autowired
    private ScenarioService scenarioService;

    @Autowired
    private PatientService patientService;

    @RequestMapping(value = PATIENTPATH + "/getscenarios", method = { RequestMethod.POST },
                    headers = { "Content-type=application/json" })
    @ResponseBody
    public List<Scenario> getScenario(HttpServletRequest request, @RequestBody Scenario scenario) {

        List<Scenario> scenarios = scenarioService.getScenarioBySearch(scenario);
        return scenarios;

    }

    @RequestMapping(value = PATIENTPATH + "/getallscenarios", method = { RequestMethod.POST })
    @ResponseBody
    public List<Scenario> getAllScenarios(HttpServletRequest request) {
        // Patient patient = (Patient) request.getSession().getAttribute("loginedPatient");
        Patient patient = (Patient) SecurityTool.getActiveUser();
        List<Scenario> sces = scenarioService.getMultiScenarioByIds(patient.getScenarioids(), patient);
        return sces;
    }

    @RequestMapping(value = PATIENTPATH + "/train_sp", method = { RequestMethod.GET })
    public String train_sp(HttpServletRequest request) {
        // Patient patient = (Patient) request.getSession().getAttribute("loginedPatient");
        Patient patient = (Patient) SecurityTool.getActiveUser();
        List<Scenario> sces = scenarioService.getMultiScenarioByIds(patient.getScenarioids(), patient);
        request.setAttribute("trainings", sces);

        int rowSize = 3;
        int rowNo = 1;
        if (sces.size() % rowSize == 0) {
            rowNo = sces.size() / rowSize;
        } else {
            rowNo = sces.size() / rowSize + 1;
        }

        request.setAttribute("rowNo", rowNo);
        request.setAttribute("rowSize", rowSize);
        request.setAttribute("redpage", "train_sp");
        return "sp/09_SP_Training";
    }

    @RequestMapping(value = PATIENTPATH + "/scenario/{sceId}", method = { RequestMethod.GET })
    public String getScenarios(HttpServletRequest request, @PathVariable int sceId) {

        // Patient patient = (Patient) request.getSession().getAttribute("loginedPatient");
        Patient patient = (Patient) SecurityTool.getActiveUser();

        String trainedId = patient.getTrainedsceids();

        Scenario sce = scenarioService.getScenarioById(sceId);

        if (trainedId != null && !"".equals(trainedId)) {

            String[] trainedIdsStr = trainedId.split(",");
            ArrayList<Integer> trainedIds = new ArrayList<Integer>();

            for (String id : trainedIdsStr) {
                trainedIds.add(Integer.parseInt(id));
            }

            for (Integer id : trainedIds) {
                if (sceId == id) {
                    sce.setTrained(1);
                }
            }
        }

        request.setAttribute("scenario", sce);
        request.setAttribute("redpage", "train_sp");
        return "sp/10_SP_TrainingDetail";
    }

    @RequestMapping(value = PATIENTPATH + "/scesearch", method = { RequestMethod.POST })
    public String getScenarios(HttpServletRequest request) {
        String content = request.getParameter("content");
        request.setAttribute("content", content);

        List<Scenario> sces = scenarioService.getScenarioByCode(content);
        request.setAttribute("trainings", sces);

        int rowSize = 3;
        int rowNo = 1;
        if (sces.size() % rowSize == 0) {
            rowNo = sces.size() / rowSize;
        } else {
            rowNo = sces.size() / rowSize + 1;
        }

        request.setAttribute("rowNo", rowNo);
        request.setAttribute("rowSize", rowSize);
        request.setAttribute("redpage", "train_sp");
        request.setAttribute("search", 1);
        return "sp/09_SP_Training";
    }

    @RequestMapping(value = PATIENTPATH + "/scequestions/{scenarioId}", method = { RequestMethod.GET })
    public String sceQues(HttpServletRequest request, @PathVariable int scenarioId) {

        Scenario sce = scenarioService.getScenarioById(scenarioId);
        request.setAttribute("scenario", sce);
        request.setAttribute("redpage", "train_sp");
        return "sp/10_SP_Training_Ques";
    }

    @RequestMapping(value = PATIENTPATH + "/sceanswer", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue sceAns(HttpServletRequest request) {

        // add the trained scenario id to the patient
        int sceId = Integer.parseInt(request.getParameter("sceId"));

        // here we need something to check the answer of sp
        // Patient patient = (Patient) request.getSession().getAttribute("loginedPatient");
        Patient patient = (Patient) SecurityTool.getActiveUser();

        String trainedSces = patient.getTrainedsceids();
        if (trainedSces == null || "".equals(trainedSces)) {
            trainedSces = sceId + "";
        } else {
            trainedSces += "," + sceId;
        }
        patient.setTrainedsceids(trainedSces);
        patientService.savePatient(patient);

        // update the logined patient
        HttpSession session = request.getSession();
        session.setAttribute("loginedPatient", patient);

        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        return rv;
    }

}
