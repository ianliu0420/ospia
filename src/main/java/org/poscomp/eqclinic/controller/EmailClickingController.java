package org.poscomp.eqclinic.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.EmailClicking;
import org.poscomp.eqclinic.domain.Participation;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.ApplicationService;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.EmailClickingService;
import org.poscomp.eqclinic.service.interfaces.ParticipationService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.poscomp.eqclinic.util.EmailTool;
import org.poscomp.eqclinic.util.SMSTool;
import org.poscomp.eqclinic.util.SecurityTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class EmailClickingController {

    private static final Logger logger = Logger.getLogger(EmailClickingController.class);

    @Autowired
    private EmailClickingService emailClickingService;
    
    @Autowired
    private ParticipationService participationService;
    
    
    
    
    @RequestMapping(value = "/feedback/{doctorId}/{level}/{type}", method = {RequestMethod.GET})
    @ResponseBody
    public ModelAndView recordClicking(HttpServletRequest request, @PathVariable int doctorId, @PathVariable int level, @PathVariable int type) {
        
        EmailClicking clicking = new EmailClicking();
        
        clicking.setDoctorId(doctorId);
        clicking.setEmailLevel(level);
        clicking.setEmailType(type);
        clicking.setClickAt(System.currentTimeMillis());
        emailClickingService.saveEmailClicking(clicking);
        
        String url = "https://emed.med.unsw.edu.au";
        
        return new ModelAndView("redirect:" + url);

    }
    
    
    @RequestMapping(value = "/saveparticipation", method = {RequestMethod.POST}, headers = {"Content-type=application/json"})
    @ResponseBody
    public ReturnValue saveparticipation(HttpServletRequest request,  @RequestBody Participation participation) {
        
    	ReturnValue rv = new ReturnValue();
    	if(participation!=null){
    		// participation.setAgree(1);
    		participation.setSubmitAt(System.currentTimeMillis());
    		participation.setSpEmail("");
    		participationService.save(participation);
    		
    		rv.setCode(1);
    	}else{
    		rv.setCode(0);
    	}
    	
    	return rv;
    }

}
