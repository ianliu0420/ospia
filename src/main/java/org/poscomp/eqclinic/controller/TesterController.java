package org.poscomp.eqclinic.controller;

import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;
import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.Video;
import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.filter.CSRFTokenManager;
import org.poscomp.eqclinic.processor.DownloadProcessorBaseline;
import org.poscomp.eqclinic.processor.OpenTokProcessor;
import org.poscomp.eqclinic.processor.VideoStopProcessor;
import org.poscomp.eqclinic.service.interfaces.ApplicationService;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.poscomp.eqclinic.util.EmailTool;
import org.poscomp.eqclinic.util.SMSTool;
import org.poscomp.eqclinic.util.SecurityTool;
import org.poscomp.eqclinic.util.TimeTool;
import org.poscomp.eqclinic.ws.AllData;
import org.poscomp.eqclinic.ws.AllDataServiceLocator;
import org.poscomp.eqclinic.ws.AssessmentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.opentok.Archive;
import com.opentok.ArchiveProperties;
import com.opentok.OpenTok;
import com.opentok.Archive.OutputMode;
import com.opentok.exception.OpenTokException;
import com.twilio.sdk.verbs.Message;
import com.twilio.sdk.verbs.TwiMLException;
import com.twilio.sdk.verbs.TwiMLResponse;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class TesterController {

    private static final Logger logger = Logger.getLogger(TesterController.class);

    private static OpenTok opentok;
    
    @RequestMapping(value = "/tester", method = { RequestMethod.GET})
    public String getDoctorRecentRequest(HttpServletRequest request) {

        String sessionId = OpenTokProcessor.generateSessionId();
        String apiKey="45493902";
        String apiSecret="54d8c8370b3828f633154dc04810e3af4535646f";
        opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
        String token = null;
        
        try {
            token = opentok.generateToken(sessionId);
            request.setAttribute("testerToken", token);
            request.setAttribute("testerSession", sessionId);
            request.setAttribute("testerApiKey", apiKey);
            
            return "tester";
            
        } catch (OpenTokException e) {
            logger.error(e.getMessage(), e);
        }
        
        return "tester";
        
    }

    
    @RequestMapping(value = "/ospiatesterstart/{sessionId}")
    @ResponseBody
    public ReturnValue startRecordingBaselineStu(HttpServletRequest request, @PathVariable String sessionId) {
        
        System.out.println(sessionId);
        
        if (opentok == null) {
            System.out.println("opentok is null");
        }
        try {
            long curTime = System.currentTimeMillis();
            Archive doctorArchiveBaseline = opentok.startArchive(sessionId,  new ArchiveProperties.Builder()
            .name("video_" + curTime + "_doctor_baseline")
            .hasAudio(true)
            .hasVideo(true)
            .outputMode(OutputMode.COMPOSED)
            .build());
            
            
            String doctorArchiveIdBaseline = "";
            if (doctorArchiveBaseline != null) {
                doctorArchiveIdBaseline = doctorArchiveBaseline.getId();
            }
            
            ReturnValue rv = new ReturnValue();
            rv.setCode(1);
            rv.setContent(doctorArchiveIdBaseline);
            return rv;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            ReturnValue rv = new ReturnValue();
            rv.setCode(0);
            rv.setContent(null);
            return rv;
        }
    }

    // stop recording: SP baseline
    @RequestMapping(value = "/ospiatesterstop/{archiveId}")
    @ResponseBody
    public ReturnValue stopArchiveBaselineStu(HttpServletRequest request, @PathVariable String archiveId) {

        ReturnValue rv = new ReturnValue();
        
        if (opentok == null) {
            System.out.println("opentok is null");
        }
        try {

            if (archiveId != null) {

                ExecutorService pool = Executors.newFixedThreadPool(3);
                VideoStopProcessor doctorStoperBaseline = new VideoStopProcessor(opentok, archiveId);
                Future doctorFutureBaseline = pool.submit(doctorStoperBaseline);
                Archive doctorArchiveBaseline = (Archive) doctorFutureBaseline.get();
                pool.shutdown();
                
                
                while (true) {
                    doctorArchiveBaseline = opentok.getArchive(archiveId);

                    if (doctorArchiveBaseline != null) {
                        String url = doctorArchiveBaseline.getUrl();
                        if (url != null && !"".equals(url)) {
                            request.setAttribute("redpage", "start_sp");
                            rv.setCode(1);
                            rv.setContent(url);
                            break;
                        } else {
                           Thread.sleep(2000);
                        }
                    }
                }
            }
            
            rv.setCode(1);
            return rv;

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            rv.setCode(0);
            return rv;
        }
    }

    @RequestMapping(value = "/userDetails")
    public void getUserDetails(HttpServletRequest request, HttpServletResponse response) {
        String callbackName = (String)request.getParameter("jsoncallback");
        String jsonString = "{\"firstName\":\"Ian\", \"lastName\":\"Liu\", \"fullId\":\"z1111111\", \"email\":\"z1111111@student.unsw.edu.au\", \"year\":\"1\", \"phase\":\"1\"}";
        try {
            response.getWriter().write(callbackName+"("+jsonString+")");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    @RequestMapping(value = "/testws")
    public void testws(HttpServletRequest request, HttpServletResponse response) {
        AllDataServiceLocator serviceLocator = new AllDataServiceLocator();
        
        try {
            AllData service =  serviceLocator.getDomino();
            // to use Basic HTTP Authentication:
            ((Stub) service)._setProperty(Call.USERNAME_PROPERTY, "z1111116");
            ((Stub) service)._setProperty(Call.PASSWORD_PROPERTY, "medstudent");
            AssessmentList assessmentList = service.getAssessments();
            System.out.println(assessmentList.getAssessmentList().length);
            response.getWriter().write("{\"firstName\":\"Ian\", \"lastName\":\"Liu\", \"fullId\":\"z1111111\", \"email\":\"z1111111@student.unsw.edu.au\", \"year\":\"1\", \"phase\":\"1\"}");
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    
}
