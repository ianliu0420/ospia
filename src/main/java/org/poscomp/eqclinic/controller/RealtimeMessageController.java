package org.poscomp.eqclinic.controller;

import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import net.sf.json.JSONObject;

import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;
import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.Video;
import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.filter.CSRFTokenManager;
import org.poscomp.eqclinic.processor.DownloadProcessorBaseline;
import org.poscomp.eqclinic.processor.OpenTokProcessor;
import org.poscomp.eqclinic.processor.VideoStopProcessor;
import org.poscomp.eqclinic.service.interfaces.ApplicationService;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.poscomp.eqclinic.util.EmailTool;
import org.poscomp.eqclinic.util.SMSTool;
import org.poscomp.eqclinic.util.SecurityTool;
import org.poscomp.eqclinic.util.TimeTool;
import org.poscomp.eqclinic.ws.AllData;
import org.poscomp.eqclinic.ws.AllDataServiceLocator;
import org.poscomp.eqclinic.ws.AssessmentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.opentok.Archive;
import com.opentok.ArchiveProperties;
import com.opentok.OpenTok;
import com.opentok.Archive.OutputMode;
import com.opentok.exception.OpenTokException;
import com.twilio.sdk.verbs.Message;
import com.twilio.sdk.verbs.TwiMLException;
import com.twilio.sdk.verbs.TwiMLResponse;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
@ServerEndpoint("/realtimeMsg/{sendUser}")  
public class RealtimeMessageController {

    private static final Logger logger = Logger.getLogger(RealtimeMessageController.class);

    private Session session;
    private static ConcurrentHashMap<String, RealtimeMessageController> webSocketMap = new ConcurrentHashMap<String, RealtimeMessageController>();  
    private String sendUser;
    private String toUser;
    private String message;
    
    @OnOpen  
    public void onOpen(@PathParam("sendUser") String sendUser,Session session) throws IOException {  
        this.sendUser = sendUser;  
        this.session = session;
        webSocketMap.put(sendUser, this);
        System.out.println("a new user added:" + sendUser);  
    }  
    
    @OnClose  
    public void onClose() throws IOException {  
        webSocketMap.remove(sendUser);  
    }  
    
    @OnMessage  
    public void onMessage(String jsonMsg, Session session) throws IOException {  
        JSONObject jsonOject = JSONObject.fromObject(jsonMsg);  
        sendUser = jsonOject.getString("sendUser");  
        
        String[] values = sendUser.split("&");
        
        int patientId = -1;
        int doctorId = -1;
        
        if(values.length==2){
        	String openTokSessionId = values[0];
        	char isPatient = values[1].charAt(0);
        	if(isPatient=='p'){
        		patientId = Integer.parseInt(values[1].substring(1));
        	}else if(isPatient=='d'){
        		doctorId = Integer.parseInt(values[1].substring(1));
        	}
        	
        	if(patientId!=-1){
        		// access doctor Id according to sessionId 
        		doctorId = 111;
        		toUser = openTokSessionId + "&d"+doctorId;
        	}else{
        		patientId = 222;
        		toUser = openTokSessionId + "&p"+patientId;
        	}
        	
	        message = jsonOject.getString("message");  
	        // message = "来自:" + sendUser + "用户发给" + toUser + "用户的信息:" + message + " \r\n";  
	        RealtimeMessageController user = webSocketMap.get(toUser);  
	        user.sendMessage("send",message);  
        }
    }  
    
    
    public void sendMessage(String type,String message) throws IOException {  
        this.session.getBasicRemote().sendText(message);
    }  
    
}
