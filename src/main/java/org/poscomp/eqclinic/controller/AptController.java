package org.poscomp.eqclinic.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.pojo.AppointmentOrderObject;
import org.poscomp.eqclinic.domain.pojo.AppointmentSearchObject;
import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.filter.CSRFTokenManager;
import org.poscomp.eqclinic.service.interfaces.ApplicationService;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.poscomp.eqclinic.util.EmailTool;
import org.poscomp.eqclinic.util.SMSTool;
import org.poscomp.eqclinic.util.SecurityTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.twilio.sdk.verbs.Message;
import com.twilio.sdk.verbs.TwiMLException;
import com.twilio.sdk.verbs.TwiMLResponse;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class AptController {

    private static final Logger logger = Logger.getLogger(AptController.class);

    @Autowired
    private PatientService patientService;

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private ScenarioService scenarioService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private DoctorService doctorService;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";
    private static final String SURVEYPATH = "/survey";

    /*
     * get all the recent requests of the student
     */
    @RequestMapping(value = DOCTORPATH + "/getdoctorrecentrequest", method = { RequestMethod.POST })
    @ResponseBody
    public List<Appointment> getDoctorRecentRequest(HttpServletRequest request) {

        Doctor loginedDoctor = (Doctor) SecurityTool.getActiveUser();
        int doctorId = loginedDoctor.getDoctorId();
        List<Appointment> apts = appointmentService.doctorHomePageApts(doctorId, 1);
        return apts;
    }

    /*
     * get all the confirmed appointments in the next two weeks
     */
    @RequestMapping(value = DOCTORPATH + "/getdoctornextapts", method = { RequestMethod.POST })
    @ResponseBody
    public List<Appointment> getDoctorNextApts(HttpServletRequest request) {
        Doctor loginedDoctor = (Doctor) SecurityTool.getActiveUser();
        int doctorId = loginedDoctor.getDoctorId();
        List<Appointment> apts = appointmentService.doctorHomePageApts(doctorId, 2);
        return apts;
    }

    /*
     * get appointment according to the sessionId
     */
    @RequestMapping(value = "/getapt/{sessionId}", method = { RequestMethod.POST })
    @ResponseBody
    public Appointment getApt(HttpServletRequest request, @PathVariable String sessionId) {

        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        return apt;
    }

    /*
     * get all the cancelled requests of the student
     */
    @RequestMapping(value = DOCTORPATH + "/getdoctorcancelledrequest", method = { RequestMethod.POST })
    @ResponseBody
    public List<Appointment> getDoctorCancelledRequest(HttpServletRequest request) {
        Doctor loginedDoctor = (Doctor) SecurityTool.getActiveUser();
        int doctorId = loginedDoctor.getDoctorId();
        List<Appointment> apts = appointmentService.doctorHomePageApts(doctorId, 3);
        return apts;
    }

    /*
     * access the doctor calendar
     */
    @RequestMapping(value = DOCTORPATH + "/doctorcalendar", method = { RequestMethod.POST })
    @ResponseBody
    public List<Appointment> doctorCalendar(HttpServletRequest request) {
        Doctor loginedDoctor = (Doctor) SecurityTool.getActiveUser();
        int doctorId = loginedDoctor.getDoctorId();
        List<Appointment> apts = appointmentService.getDoctorAptAvailable(doctorId);

        List<Application> applications = applicationService.findPendingByDoctorId(doctorId);

        for (Appointment apt : apts) {
            int aptId = apt.getAptid();
            for (Application application : applications) {
                if (aptId == application.getAptId()) {
                    apt.setStatus(2);
                }
            }
        }

        return apts;
    }

    /*
     * access all the appointments of a whole date (click date slot)
     */
    @RequestMapping(value = DOCTORPATH + "/getdoctoraptswholedate", method = { RequestMethod.POST })
    @ResponseBody
    public List<Appointment> getDoctorAptsWholeDate(HttpServletRequest request) {
        Doctor loginedDoctor = (Doctor) SecurityTool.getActiveUser();
        int doctorId = loginedDoctor.getDoctorId();
        String getEventWholeDateDate = (String) request.getParameter("getEventWDDate");
        List<Appointment> apts = appointmentService.getDoctorAptsWholeDate(doctorId, getEventWholeDateDate);

        List<Application> applications = applicationService.findPendingByDoctorId(doctorId);

        for (Appointment apt : apts) {
            int aptId = apt.getAptid();
            for (Application application : applications) {
                if (aptId == application.getAptId()) {
                    apt.setStatus(2);
                }
            }
        }

        return apts;
    }

    /*
     * doctor send a request for booking a appointment
     */
    @RequestMapping(value = DOCTORPATH + "/doctorrequestapt", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue doctorRequestApt(HttpServletRequest request) {
        Doctor loginedDoctor = (Doctor) SecurityTool.getActiveUser();
        int doctorId = loginedDoctor.getDoctorId();

        // check whether student has 2 pending request
        List<Appointment> apts = appointmentService.getDoctorAptStatus(doctorId, 3);

        ReturnValue rv = new ReturnValue();
        if (apts.size() >= 1) {
            System.out.println(apts.size());
            rv.setCode(-3);
            rv.setContent("Thanks for completing one OSPIA! Curretly, we are unable to offer more appointments to you.");
        } else {
            int aptId = Integer.parseInt((String) request.getParameter("aptId"));
            Appointment apt = appointmentService.getAppointmentByAptId(aptId);

            int result = appointmentService.doctorBookApt(loginedDoctor, apt);

            if (result == 1) {
                rv.setCode(1);
                Patient pat = patientService.getPatientByPatientId(apt.getPatientId());
                Scenario sce = scenarioService.getScenarioById(apt.getScenario());

            } else if (result == 0) {
                rv.setCode(0);
                rv.setContent("request failed, please try again");
            } else if (result == -1) {
                rv.setCode(-1);
                rv.setContent("request failed, you have to send the request 3 days ahead.");
            } else if (result == -2) {
                rv.setCode(-2);
                rv.setContent("request failed, you can only request the apt in next 30 days.");
            }
        }
        return rv;
    }

    /*
     * doctor start the formal conversation
     */
    @RequestMapping(value = DOCTORPATH + "/getArchiveId/{sessionId}", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue getArchiveId(HttpServletRequest request, @PathVariable String sessionId) {
        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        ReturnValue rv = new ReturnValue();
        if(apt!=null && apt.getArchId()!= null && !apt.getAptid().equals("")){
            rv.setCode(1);
            rv.setContent(apt.getArchId());
        }else{
            rv.setCode(0);
            rv.setContent("");
        }
        
        return rv;
    }
    
    
    /*
     * doctor start the formal conversation
     */
    @RequestMapping(value = DOCTORPATH + "/doctorstartformalconversation/{sessionId}", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue doctorStartFormalConversation(HttpServletRequest request, @PathVariable String sessionId) {
        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        ReturnValue rv = new ReturnValue();
        if(apt!=null && apt.getArchId()!= null && !apt.getAptid().equals("")){
            apt.setStartRealConversationTime(System.currentTimeMillis());
            appointmentService.saveApp(apt);
            rv.setCode(1);
        }else{
            rv.setCode(0);
            rv.setContent("Please wait for a moment for the SP");
        }
        
        return rv;
    }
    
    
    /*
     * doctor cancel an confirmed appointment
     */
    @RequestMapping(value = DOCTORPATH + "/cancelapt", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue doctorCancelApt(HttpServletRequest request) {
        int aptId = Integer.parseInt((String) request.getParameter("aptId"));
        int cancelPerson = Integer.parseInt((String) request.getParameter("cancelPerson"));
        
        Doctor doctor = null;
        Patient patient = null;
        
        String cancelReason = (String) request.getParameter("cancelReason");
        Appointment apt = appointmentService.getAppointmentByAptId(aptId);
        
        doctor = doctorService.getDoctorByDoctorId(apt.getDoctorId());
        patient = patientService.getPatientByPatientId(apt.getPatientId());
        String patientName = patient.getFirstname();
        String doctorName = doctor.getFirstname();
        
        int result = appointmentService.cancelApt(apt, cancelPerson, cancelReason);

        if(cancelPerson == 1){ // doctor cancel, send email to patient 
            EmailTool.sendCancelApt(apt, null, patient, cancelReason, patientName, doctorName);
            SMSTool.sendSMSCancelled(apt, patient);
        }else if(cancelPerson == 2){ // patient cancel, send email to doctor 
            EmailTool.sendCancelApt(apt, doctor, null, cancelReason, patientName, doctorName);
        }
        
        ReturnValue rv = new ReturnValue();
        rv.setCode(result);
        return rv;
    }

    /*
     * patient cancel an confirmed appointment
     */
    @RequestMapping(value = PATIENTPATH + "/cancelapt", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue patientCancelApt(HttpServletRequest request) {
        int aptId = Integer.parseInt((String) request.getParameter("aptId"));
        int cancelPerson = Integer.parseInt((String) request.getParameter("cancelPerson"));
        
        Doctor doctor = null;
        Patient patient = null;
        
        String cancelReason = (String) request.getParameter("cancelReason");
        Appointment apt = appointmentService.getAppointmentByAptId(aptId);
        
        doctor = doctorService.getDoctorByDoctorId(apt.getDoctorId());
        patient = patientService.getPatientByPatientId(apt.getPatientId());
        String patientName = patient.getFirstname();
        String doctorName = doctor.getFirstname();
        int result = appointmentService.cancelApt(apt, cancelPerson, cancelReason);

        if(cancelPerson == 1){ // doctor cancel, send email to patient 
            EmailTool.sendCancelApt(apt, null, patient, cancelReason, patientName, doctorName);
        }else if(cancelPerson == 2){ // patient cancel, send email to doctor 
            EmailTool.sendCancelApt(apt, doctor, null, cancelReason, patientName, doctorName);
        }
        
        ReturnValue rv = new ReturnValue();
        rv.setCode(result);
        return rv;
    }

    /*
     * get all the pending request of a patient
     */
    @RequestMapping(value = PATIENTPATH + "/getpatientpendingrequest", method = { RequestMethod.POST })
    @ResponseBody
    public List<Appointment> getPatientPendingRequest(HttpServletRequest request) {
        Patient patient = (Patient) SecurityTool.getActiveUser();
        // Patient patient = (Patient)
        // request.getSession().getAttribute("loginedPatient");
        int patientId = patient.getPatientid();
        List<Appointment> apts = appointmentService.patientHomePageApts(patientId, 1);
        return apts;
    }

    /*
     * get appointment of a patient in the next two weeks
     */
    @RequestMapping(value = PATIENTPATH + "/getpatientnextapts", method = { RequestMethod.POST })
    @ResponseBody
    public List<Appointment> getPatientNextApts(HttpServletRequest request) {
        Patient patient = (Patient) SecurityTool.getActiveUser();
        int patientId = patient.getPatientid();
        List<Appointment> apts = appointmentService.patientHomePageApts(patientId, 2);
        return apts;
    }

    /*
     * access the patient calendar
     */
    @RequestMapping(value = PATIENTPATH + "/patientcalendar", method = { RequestMethod.POST })
    @ResponseBody
    public List<Appointment> patientCalendar(HttpServletRequest request) {
        Patient patient = (Patient) SecurityTool.getActiveUser();
        int spId = patient.getPatientid();
        List<Appointment> apts = appointmentService.getPatientApt(spId);
        return apts;
    }

    /*
     * access all the appointments of a whole date (click date slot)
     */
    @RequestMapping(value = PATIENTPATH + "/getpatientaptswholedate", method = { RequestMethod.POST })
    @ResponseBody
    public List<Appointment> spGetEventWD(HttpServletRequest request) {
        Patient patient = (Patient) SecurityTool.getActiveUser();
        int patientId = patient.getPatientid();
        String getEventWholeDateDate = (String) request.getParameter("getEventWDDate");
        List<Appointment> apts = appointmentService.getPatientAptsWholeDate(patientId, getEventWholeDateDate);
        return apts;
    }

    /*
     * patient create an appointment
     */
    @RequestMapping(value = PATIENTPATH + "/patientcreateapt", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue patientCreateApt(HttpServletRequest request) {

        String aptidStr = (String) request.getParameter("aptid");
        Patient patient = (Patient) SecurityTool.getActiveUser();
        String insertDateDate = (String) request.getParameter("insertDateDate");
        String insertDateTimeStart = (String) request.getParameter("insertDateTimeStart");
        String insertDateTimeEnd = (String) request.getParameter("insertDateTimeEnd");
        int scenarioId = Integer.parseInt((String) request.getParameter("scenario"));
        Scenario sce = scenarioService.getScenarioById(scenarioId);

        ReturnValue returnValue = appointmentService.patientCreateAptForUnavailablePeriod(patient, aptidStr, insertDateDate, insertDateTimeStart, insertDateTimeEnd, sce);
        if(returnValue!=null && returnValue.getCode()==-2){
            return returnValue;
        }
        
        int result = appointmentService.patientCreateApt(patient, aptidStr, insertDateDate, insertDateTimeStart, insertDateTimeEnd, sce);

        ReturnValue rv = new ReturnValue();
        rv.setCode(result);
        if (result == 0) {
            rv.setContent("You should make an appointment 48 hours to 90 days ahead.");
        }else if(result == -1){
            rv.setCode(0);
            rv.setContent("Please allow 15 minutes minimum between appointments.");
        }else if(result == -2){
            rv.setCode(0);
            rv.setContent("You cannot provide ");
        }
        return rv;

    }

    /*
     * patient delete an appointment
     */
    @RequestMapping(value = PATIENTPATH + "/patientdeleteapt", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue patientDeleteApt(HttpServletRequest request) {
        int aptId = Integer.parseInt((String) request.getParameter("aptId"));
        Appointment apt = appointmentService.getAppointmentByAptId(aptId);
        ReturnValue rv = new ReturnValue();
        if (apt != null) {
            
            if(apt.getApplicationNo()>0){
                rv.setCode(0);
                rv.setContent("There are "+apt.getApplicationNo()+" applications on this appointment, you cannot edit it.");
            }else{
                apt.setStatus(7);
                appointmentService.saveApp(apt);
                rv.setCode(1);
            }
        } else {
            rv.setCode(0);
            rv.setContent("Confirm failed, please try again");
        }
        return rv;
    }

    /*
     * patient accept or decline the appointment request
     */
    @RequestMapping(value = PATIENTPATH + "/patientconfirmapt", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue patientConfirmApt(HttpServletRequest request) {

        int aptId = Integer.parseInt((String) request.getParameter("aptId"));
        int type = Integer.parseInt((String) request.getParameter("type"));

        int result = appointmentService.patientConfirmApt(aptId, type);

        if (result == 1) {
            if (type == 1) {
                // send email to the student (accepted)
                Appointment apt = appointmentService.getAppointmentByAptId(aptId);
                if (apt != null) {
                    int doctorId = apt.getDoctorId();
                    Doctor doctor = doctorService.getDoctorByDoctorId(doctorId);

                    // send an email to the student
                    EmailTool.sendConfirmToDoctor(apt, doctor,1);
                }
            } else {
                // send email to the student (decline)
                Appointment apt = appointmentService.getAppointmentByAptId(aptId);
                if (apt != null) {
                    int doctorId = apt.getDoctorId();
                    Doctor doctor = doctorService.getDoctorByDoctorId(doctorId);

                    // send an email to the student
                    EmailTool.sendConfirmToDoctor(apt, doctor,0);
                }
            }
        }

        ReturnValue rv = new ReturnValue();
        rv.setCode(result);
        return rv;

    }

    /*
     * patient accept the appointment request through email
     */
    @RequestMapping(value = "/patientconfirmaptemail", method = { RequestMethod.GET })
    public String acceptAptEmail(HttpServletRequest request) {

        System.out.println("Patient confirm by email....");
        
        long curTime = Long.parseLong(request.getParameter("time"));

        // to prevent machine click
        // long requestTime = curTime - (48 * 60 * 60 * 100);
        if ((System.currentTimeMillis()) - curTime < 30 * 1000) {
            request.setAttribute("returnContent", "Sorry, we are unable to process your request. Please confirm your appointment again 2 minutes later.");
            return "sp/01_SP_home_confirm";
            // return null;
        }

        int aptId = Integer.parseInt(request.getParameter("aptId"));
        String patientName = request.getParameter("patient");

        int result = appointmentService.patientConfirmAptEmail(curTime, aptId, patientName);
        if (result == 1) {
            // send email to the student
            Appointment apt = appointmentService.getAppointmentByAptId(aptId);
            if (apt != null) {
                int doctorId = apt.getDoctorId();
                Doctor doctor = doctorService.getDoctorByDoctorId(doctorId);

                // send an email to the student
                EmailTool.sendConfirmToDoctor(apt, doctor, 1);
            }
            request.setAttribute("returnContent", "Thanks for accepting this appointment request.");
        } else {

            // send email to the student
            Appointment apt = appointmentService.getAppointmentByAptId(aptId);
            if (apt != null) {
                int doctorId = apt.getDoctorId();
                Doctor doctor = doctorService.getDoctorByDoctorId(doctorId);

                // send an email to the student
                // EmailTool.sendConfirmToDoctor(apt, doctor, 0);
            }

            request.setAttribute("returnContent", "This appointment has been comfirmed, please <a href='http://eqclinic.poscomp.org/ospia/sp/ulogin_sp'>log in </a> the system to check it.");
        }
        return "sp/01_SP_home_confirm";

    }
    
//    @RequestMapping(value = "/testHttps", method = { RequestMethod.POST })
//    public void testHttps(HttpServletRequest request) {
//    	
//    	System.out.println("fffffffff");
//    	
//    }
    

    /*
     * patient accept the appointment request through sms
     */
    @RequestMapping(value = "/patientconfirmaptsms", method = { RequestMethod.POST })
    public void acceptAptSMS(HttpServletRequest request) {
    	
    	System.out.println("request");
    	
        String fromNumber = request.getParameter("From");
        String incomeMsg = request.getParameter("Body");

        System.out.println(fromNumber + " : " + incomeMsg);
        
        Patient patient = patientService.getPatientByPhoneNo(fromNumber);
        if (patient != null) {
//            String aptIdsStr = patient.getAppliedApt();
//            if (!"".equals(aptIdsStr)) {
//                String[] aptIds = aptIdsStr.split(",");
//                
//                for (String aptIdStr : aptIds) {
//                    int aptId = Integer.parseInt(aptIdStr);
//                    if (aptIdStr.equals(incomeMsg)) {
//                        
//                        String newAppliedApt = deleteNumberFromString(patient.getAppliedApt(),aptIdStr);
//                        patient.setAppliedApt(newAppliedApt);
//                        patientService.savePatient(patient);
//                        // send email to the student
//                        Appointment apt = appointmentService.getAppointmentByAptId(aptId);
//                        if (apt != null) {
//                            int doctorId = apt.getDoctorId();
//                            Doctor doctor = doctorService.getDoctorByDoctorId(doctorId);
//
//                            // send an email to the student
//                            EmailTool.sendConfirmToDoctor(apt, doctor, 1);
//                            SMSTool.sendReplySMS(patient);
//                        }
//                        int result = appointmentService.patientConfirmApt(aptId, 1);
//                    }
//                }
//            }
            
        int aptId = Integer.parseInt(incomeMsg);
        Appointment apt = appointmentService.getAppointmentByAptId(aptId);
         if (apt != null) {
              int doctorId = apt.getDoctorId();
              Doctor doctor = doctorService.getDoctorByDoctorId(doctorId);
    
              // send an email to the student
              EmailTool.sendConfirmToDoctor(apt, doctor, 1);
              SMSTool.sendReplySMS(patient);
          }
          int result = appointmentService.patientConfirmApt(aptId, 1);
            
            
        } else {

        }
    }

    
    /*
     * doctor cancel an confirmed appointment
     */
    @RequestMapping(value = DOCTORPATH + "/patientnoshow/{sessionId}", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue patientNoShow(HttpServletRequest request, @PathVariable String sessionId) {
        Doctor doctor = null;
        Patient patient = null;
        int cancelPerson = 2;
        String cancelReason = "No show";

        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        
        doctor = doctorService.getDoctorByDoctorId(apt.getDoctorId());
        patient = patientService.getPatientByPatientId(apt.getPatientId());
        
        int result = appointmentService.noShowApt(apt, cancelPerson, cancelReason);
        ReturnValue rv = new ReturnValue();
        rv.setCode(result);
        return rv;
    }
    
    
    /*
     * doctor cancel an confirmed appointment
     */
    @RequestMapping(value = PATIENTPATH + "/doctornoshow/{sessionId}", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue doctorNoShow(HttpServletRequest request, @PathVariable String sessionId) {
        Doctor doctor = null;
        Patient patient = null;
        int cancelPerson = 1;
        String cancelReason = "No show";
        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        
        doctor = doctorService.getDoctorByDoctorId(apt.getDoctorId());
        patient = patientService.getPatientByPatientId(apt.getPatientId());
        
        int result = appointmentService.noShowApt(apt, cancelPerson, cancelReason);
        ReturnValue rv = new ReturnValue();
        rv.setCode(result);
        return rv;
    }
    
    
    public String deleteNumberFromString(String original, String deleteNo){
        
        String[] numbers = original.split(",");
        String result = "";
        for (String number : numbers) {
            
            if(!number.equals(deleteNo)){
                if(result.equals("")){
                    result = number;
                }else{
                    result += ","+number;
                }
            }
        }
        
        return result;
    }
    
    // send an email to all the students
    @Scheduled(cron = "00 30 07 ? * *")
    public void sendnotificationEmail(){
    	
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        String aptdate = dateFormat.format(cal.getTime()); 
        
        List<Appointment> apts = appointmentService.getAptByDate(aptdate);
        
        for (Appointment apt : apts) {
            
            Doctor doctor =  doctorService.getDoctorByDoctorId(apt.getDoctorId());
            Patient patient = patientService.getPatientByPatientId(apt.getPatientId());
            
            EmailTool.sendNotificationToDoctor(apt, doctor, patient);
            EmailTool.sendNotificationToPatient(apt, doctor, patient);
            
        }
        
    }
    
    /*
     * doctor submit the type of this video (assessment/practice)
     */
    @RequestMapping(value = DOCTORPATH + "/submitappropriatevalue/{appropriateValue}", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue submitAppropriateValue(HttpServletRequest request, @PathVariable int appropriateValue) {
        ReturnValue rv = new ReturnValue();
        String sessionId = (String) request.getSession().getAttribute("doctorSessionIdCombined");

        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        apt.setAppropriateStu(appropriateValue);
        appointmentService.saveApp(apt);

        if(appropriateValue == 0){ // inappropriate
            rv.setContent("appropriate");
            rv.setCode(0);
            
        }else if(appropriateValue == 1){
            rv.setContent("perref1_stu");
            rv.setCode(1);
        }
        return rv;
    }
    
    
    /*
     * doctor submit the type of this video (assessment/practice)
     */
    @RequestMapping(value = PATIENTPATH + "/submitappropriatevaluepatient/{combineValue}", method = { RequestMethod.POST })
    @ResponseBody
    public ReturnValue submitAppropriateValueSp(HttpServletRequest request, @PathVariable String combineValue) {
        ReturnValue rv = new ReturnValue();
        
        String[] temps = combineValue.split(",");
        
        String sessionId = temps[0];
        int appropriateValue = Integer.parseInt(temps[1]);

        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        apt.setAppropriateSp(appropriateValue);
        appointmentService.saveApp(apt);

        if(appropriateValue == 0){ // inappropriate
            rv.setCode(0);
            
        }else if(appropriateValue == 1){
            rv.setCode(1);
        }
        return rv;
    }
    
    // the search function of the admin
    @RequestMapping(value = SURVEYPATH + "/apts",method = { RequestMethod.GET })
    public String editPatient(HttpServletRequest request) {
    	request.setAttribute("redpage", "apts_admin");
    	return "survey/listapts";
    }
    
    // the search function of the admin
    @RequestMapping(value = SURVEYPATH + "/apts",method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
    @ResponseBody
    public List<Appointment> editPatient(HttpServletRequest request, @RequestBody AppointmentSearchObject aso) {
    	if(aso==null){
    		return null;
    	}else{
    		request.setAttribute("searchContent", aso.getSearchContent());
    		return appointmentService.searchAppointments(aso);
    	}
    }
    
    
    
}
