package org.poscomp.eqclinic.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONArray;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.domain.pojo.SurveySoca;
import org.poscomp.eqclinic.service.interfaces.AppointmentProgressService;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.util.SecurityTool;
import org.poscomp.eqclinic.ws.AllData;
import org.poscomp.eqclinic.ws.AllDataServiceLocator;
import org.poscomp.eqclinic.ws.Assessment;
import org.poscomp.eqclinic.ws.AssessorDetails;
import org.poscomp.eqclinic.ws.Criterion;
import org.poscomp.eqclinic.ws.CriterionGrade;
import org.poscomp.eqclinic.ws.SubmitAssessmentResponse;
import org.poscomp.eqclinic.ws.TemplateField;
import org.poscomp.survey.domain.Answer;
import org.poscomp.survey.domain.Question;
import org.poscomp.survey.domain.Survey;
import org.poscomp.survey.service.AnswerService;
import org.poscomp.survey.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

//import net.sf.json.JSONArray;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class FormController {

    private static final Logger logger = Logger.getLogger(FormController.class);

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private AppointmentProgressService appointmentProgressService;

    @Autowired
    private AnswerService answerService;
    
    @Autowired
    private SurveyService surveyService;
    
    @Autowired
    private DoctorService doctorService;

    @Autowired
    private PatientService patientService;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";

    @RequestMapping(value = DOCTORPATH + "/second_soca_stu/{sessionId}", method = {RequestMethod.GET})
    public String redirect_second_soca_stu(HttpServletRequest request, @PathVariable String sessionId) {

        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        Answer answer = answerService.loadAnswerBySessionIdUserId(sessionId, "patient", 1);

        if (answer == null) {
            request.setAttribute("answerId", "");
        } else {
            request.setAttribute("answerId", answer.getId());
        }
        request.setAttribute("nextpage", DOCTORPATH + "/perref_second_stu");
        return "stu/26_Stu_StartApt_result_second_SOCA";
    }


    @RequestMapping(value = DOCTORPATH + "/review_tutor_soca/{sessionId}", method = {RequestMethod.GET})
    public String redirect_review_tutor_soca(HttpServletRequest request, @PathVariable String sessionId) {

        Answer answer = answerService.loadAnswerBySessionIdUserId(sessionId, "teacher", 1);
        if (answer == null) {
            request.setAttribute("answerId", "");
        } else {
            request.setAttribute("answerId", answer.getId());
        }
        request.setAttribute("redpage", "cons_stu");
        return "stu/41_Tut_SOCA";
    }


    /*
     * patient submit system feedback
     */
    @RequestMapping(value = PATIENTPATH + "/submitpatientsystemfeedback", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue submitPatientSystemFeedback(HttpServletRequest request) {
        ReturnValue rv = new ReturnValue();
        String sessionId = (String) request.getSession().getAttribute("patientSessionIdCombined");

        // save the appointment progress of the patient
        // Patient patient = (Patient) request.getSession().getAttribute("loginedPatient");
        Patient patient = (Patient) SecurityTool.getActiveUser();
        int patientId = patient.getPatientid();
        String userType = "patient";
        int result =
                        appointmentProgressService.updateAppointmentProgressState(sessionId, userType, patientId,
                                        "patientSystemFeedback");
        rv.setCode(result);
        return rv;
    }

    /*
     * doctor submit the first personal reflection form
     */
    @RequestMapping(value = DOCTORPATH + "/submitdoctorperreffirst", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue submitDoctorPerRefFirst(HttpServletRequest request) {
        ReturnValue rv = new ReturnValue();
        String sessionId = (String) request.getSession().getAttribute("doctorSessionIdCombined");

        // save the appointment progress of the doctor
        Doctor doctor = (Doctor) SecurityTool.getActiveUser();
        int doctorId = doctor.getDoctorId();
        String userType = "doctor";
        int result =
                        appointmentProgressService.updateAppointmentProgressState(sessionId, userType, doctorId,
                                        "doctorPerRefFirst");
        rv.setCode(result);
        return rv;
    }

    /*
     * doctor submit the type of this video (assessment/practice)
     */
    @RequestMapping(value = DOCTORPATH + "/submitdoctorrecordingtype/{sessionType}", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue submitDoctorRecordingType(HttpServletRequest request, @PathVariable int sessionType) {
        ReturnValue rv = new ReturnValue();
        String sessionId = (String) request.getSession().getAttribute("doctorSessionIdCombined");

        Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
        apt.setSessionType(sessionType);
        appointmentService.saveApp(apt);

        Answer answer = answerService.loadAnswerBySessionIdUserId(sessionId, "patient", 1);
        // save answer to eMed
        
        if(sessionType==2 && answer!=null && answer.getReceiptNo()!=null && "".equals(answer.getReceiptNo())){
            
            System.out.println("update receip number from session type");
            
            int doctorIdNumber = apt.getDoctorId();
            int patientIdNumber = apt.getPatientId();

            Doctor doctorForEmed = doctorService.getDoctorByDoctorId(doctorIdNumber);
            Patient patientForEmed = patientService.getPatientByPatientId(patientIdNumber);
            
            String result = submitToEmed(answer.getContent(), doctorForEmed.getUserId(), patientForEmed.getFirstname(),
                            patientForEmed.getLastname());
            
            answer.setReceiptNo(result);
            
            answerService.save(answer);
        }

        // save the appointment progress of the doctor
        Doctor doctor = (Doctor) SecurityTool.getActiveUser();
        int doctorId = doctor.getDoctorId();
        String userType = "doctor";

        int result = appointmentProgressService.updateAppointmentProgressState(sessionId, userType, doctorId,
                                        "doctorRecordingType");
        rv.setCode(result);

        return rv;
    }

    /*
     * doctor submit the evaluation of SP and scenario
     */
    @RequestMapping(value = DOCTORPATH + "/submitdoctorevaluatepatient", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue submitDoctorEvaluatePatient(HttpServletRequest request) {
        ReturnValue rv = new ReturnValue();
        String sessionId = (String) request.getSession().getAttribute("doctorSessionIdCombined");

        // save the appointment progress of the doctor
        Doctor doctor = (Doctor) SecurityTool.getActiveUser();
        int doctorId = doctor.getDoctorId();
        String userType = "doctor";

        int result =
                        appointmentProgressService.updateAppointmentProgressState(sessionId, userType, doctorId,
                                        "doctorEvaluatePatient");
        rv.setCode(result);

        return rv;
    }

    /*
     * doctor submit system feedback
     */
    @RequestMapping(value = DOCTORPATH + "/submitdoctorsystemfeedback", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue submitDoctorSystemFeedback(HttpServletRequest request) {
        ReturnValue rv = new ReturnValue();
        String sessionId = (String) request.getSession().getAttribute("doctorSessionIdCombined");

        // save the appointment progress of the doctor
        Doctor doctor = (Doctor) SecurityTool.getActiveUser();
        int doctorId = doctor.getDoctorId();
        String userType = "doctor";
        int result =
                        appointmentProgressService.updateAppointmentProgressState(sessionId, userType, doctorId,
                                        "doctorSystemFeedback");
        rv.setCode(result);
        return rv;
    }

    /*
     * doctor submit second personal reflection
     */
    @RequestMapping(value = DOCTORPATH + "/submitdoctorperrefsecond", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue submitDoctorPerRefSecond(HttpServletRequest request) {
        ReturnValue rv = new ReturnValue();
        String sessionId = (String) request.getSession().getAttribute("doctorSessionIdCombined");

        // save the appointment progress of the doctor
        Doctor doctor = (Doctor) SecurityTool.getActiveUser();
        int doctorId = doctor.getDoctorId();
        String userType = "doctor";
        int result =
                        appointmentProgressService.updateAppointmentProgressState(sessionId, userType, doctorId,
                                        "doctorPerRefSecond");
        rv.setCode(result);
        return rv;
    }

    /*
     * doctor submit second personal reflection
     */
    @RequestMapping(value = DOCTORPATH + "/refsoca_stu/{sessionId}", method = {RequestMethod.GET})
    public String ref(HttpServletRequest request, @PathVariable String sessionId) {
        Answer answer = answerService.loadAnswerBySessionIdUserId(sessionId, "patient", 1);
        if (answer == null) {
            request.setAttribute("answerId", "");
        } else {
            request.setAttribute("answerId", answer.getId());
        }

        Answer answerSecond = answerService.loadAnswerBySessionIdUserId(sessionId, "doctor", 6);
        if (answerSecond == null) {
            request.setAttribute("answerIdSecond", "");
        } else {
            request.setAttribute("answerIdSecond", answerSecond.getId());
        }

        request.setAttribute("reflogSessionId", answer.getSessionId());
        request.setAttribute("redpage", "cons_stu");
        return "stu/28_Stu_StartApt_result_SOCA";
    }



    public String submitToEmed(String myAnswer, String studentId, String patientFirstName, String patientLastName) {

        try {
            JSONArray answerArray = new JSONArray(myAnswer);

            Survey survey = surveyService.findById(1);
            Set<Question> questions = survey.getQuestions();

            String orderOfQuestion = survey.getOrderOfQuestions();

            String[] orders = orderOfQuestion.split(",");

            CriterionGrade[] criterionGradeList = new CriterionGrade[6];

            for (int i = 0; i < orders.length - 1; i = i + 2) {
                int quesionId = Integer.parseInt(orders[i]);
                int quesionId_next = Integer.parseInt(orders[i + 1]);

                org.json.JSONObject answer = null;
                org.json.JSONObject answer_next = null;

                for (int j = 0; j < answerArray.length(); j++) {
                    org.json.JSONObject temp = (org.json.JSONObject) answerArray.get(j);
                    if (temp.getInt("qid") == quesionId) {
                        answer = temp;
                    }
                }

                for (int j = 0; j < answerArray.length(); j++) {
                    org.json.JSONObject temp = (org.json.JSONObject) answerArray.get(j);
                    if (temp.getInt("qid") == quesionId_next) {
                        answer_next = temp;
                    }
                }

                CriterionGrade grade = new CriterionGrade();
                grade.setGrade(answer.getString("qvalue"));
                String reasons[] = {answer_next.getString("qvalue")};
                grade.setGradeReason(reasons);

                if (i / 2 == 4) {
                    criterionGradeList[i / 2] = grade;

                    String[] grades = answer.getString("qvalue").split(",");
                    String feedbackReasons = "";
                    if (grades.length > 0) {
                        feedbackReasons = "You need to focus on following skills: ";
                    }

                    for (int j = 0; j < grades.length; j++) {

                        if (j == 0) {
                            if ("1".equals(grades[j])) {
                                feedbackReasons = "Provide structure";
                            } else if ("2".equals(grades[j])) {
                                feedbackReasons = "Gather information";
                            } else if ("3".equals(grades[j])) {
                                feedbackReasons = "Build relationships & developing rapport";
                            } else if ("4".equals(grades[j])) {
                                feedbackReasons =
                                                "Ensure a shared understanding of patient's needs and perspective/impact of problem";
                            }
                        } else {
                            if ("1".equals(grades[j])) {
                                feedbackReasons += "; Provide structure";
                            } else if ("2".equals(grades[j])) {
                                feedbackReasons += "; Gather information";
                            } else if ("3".equals(grades[j])) {
                                feedbackReasons += "; Build relationships & developing rapport";
                            } else if ("4".equals(grades[j])) {
                                feedbackReasons +=
                                                "; Ensure a shared understanding of patient's needs and perspective/impact of problem";
                            }
                        }

                    }
                    grade.setGrade(feedbackReasons);

                } else {
                    criterionGradeList[i / 2] = grade;
                }

            }

            Locale locale = Locale.US;
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", locale);
            String currentDate = sdf.format(new Date());
            System.out.println(currentDate);
            for (int j = 0; j < criterionGradeList.length - 1; j++) {
                CriterionGrade grade = criterionGradeList[j];
                String reasons = grade.getGradeReason()[0];

                String[] newReason = null;
                if (j == 4) {
                    newReason = grade.getGradeReason();
                } else {
                    newReason = parseReason(j, reasons);
                }
                grade.setGradeReason(newReason);
                grade.setCriterionNum((j + 1) + "");
                grade.setDateCreated(currentDate);
                grade.setDateModified(currentDate);
                TemplateField tf = new TemplateField();
                if (j == 0) {
                    tf.setValue("Provide structure");
                } else if (j == 1) {
                    tf.setValue("Gather information");
                } else if (j == 2) {
                    tf.setValue("Build relationships & developing rapport");
                } else if (j == 3) {
                    tf.setValue("Ensure a shared understanding of patient's needs and perspective/impact of problem");
                } else if (j == 4) {
                    tf.setValue("Overall feedback");
                }
                grade.setTitle(tf);
            }

            CriterionGrade grade_reflection = new CriterionGrade();
            grade_reflection.setGrade("No Reflection");
            String reflection[] = {};
            grade_reflection.setGradeReason(reflection);
            criterionGradeList[5] = grade_reflection;
            criterionGradeList[5].setCriterionNum("6");
            TemplateField tf = new TemplateField();
            tf.setValue("Reflection answer");
            grade_reflection.setTitle(tf);


            AllDataServiceLocator serviceLocator = new AllDataServiceLocator();

            try {
                AllData service = serviceLocator.getDomino();
                // to use Basic HTTP Authentication:
                ((Stub) service)._setProperty(Call.USERNAME_PROPERTY, "WebServiceOSPIA");
                ((Stub) service)._setProperty(Call.PASSWORD_PROPERTY, "WebServiceOSPIA");

                Assessment assessment = new Assessment();
                // set the student Id
                assessment.setStudentId(studentId);

                // set assessor Id (SP name, or tutor name)
                assessment.setAssessorId("");
                AssessorDetails assessorDetails = new AssessorDetails();

                TemplateField assessorName = new TemplateField();
                assessorName.setLabel(patientFirstName);
                assessorName.setValue(patientLastName);
                assessorDetails.setNameOfAssessor(assessorName);
                assessment.setAssessorDetails(assessorDetails);
                // set the status
                assessment.setStatus("Completed");

                // set the receipt number
                assessment.setReceiptNumber(null);

                assessment.setAssessmentType("OSPIA");

                // set submission date
                System.out.println(currentDate);
                assessment.setSubmissionDate(currentDate);

                // set date modified
                assessment.setDateStatusModified(currentDate);

                assessment.setCriterionList(createCrition());
                assessment.setGradedCriterionList(criterionGradeList);

                assessment.setAssessmentTitle("Patient-student communication assessment");

                SubmitAssessmentResponse response = service.submitAssessment(assessment);

                System.out.println(response.getMessage());
                System.out.println(response.getMissingCount());
                System.out.println(response.getReceiptNumber());

                logger.info(response.getMessage());
                logger.info(response.getMissingCount());
                logger.info(response.getReceiptNumber());

                return response.getReceiptNumber();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }

        return "";

    }

    public String[] parseReason(int questionId, String oReasons) {
        ArrayList<String> reasons = new ArrayList<String>();
        ArrayList<String> reasons_result = new ArrayList<String>();
        if (questionId == 0) {
            reasons.add("A. Initiates the session appropriately with introductions, defining of the purpose and agenda");
            reasons.add("B. Clarifies and summarises at key points during the interview");
            reasons.add("C. Uses transitions and signposting");
            reasons.add("D. Manages time effectively");
            reasons.add("E. Closes the session appropriately with a plan and/or summary");
        } else if (questionId == 1) {
            reasons.add("A. Encourages the patient to tell their story in their own words");
            reasons.add("B. Explores the patient's problems and perspectives (beliefs, worries, feelings, goals)");
            reasons.add("C. Uses open questions initially, listens attentively, and then synthesizes closed questions as appropriate");
            reasons.add("D. Facilitates patient's responses using encouragement, pause/silence, repetition, paraphrasing, interpretation - with limited interruptions");
            reasons.add("E. Avoids using jargon and requests clarification and further information where needed");
        } else if (questionId == 2) {
            reasons.add("A. Picks up and acknowledges patient's non-verbal behaviour (e.g. body language, speech, facial expressions, affect)");
            reasons.add("B. Demonstrates respectful, encouraging and non-controlling non-verbal behavior (eye contact, facial expressions, posture, position, movement) and vocal rate, volume and tone");
            reasons.add("C. Acknowledges patient′s perspective and efforts to cope and is non-judgemental");
            reasons.add("D. Handles uncomfortable topics sensitively");
            reasons.add("E. Involves the patient, and shares own thinking as appropriate - ideas, thought processes, dilemmas");
        } else if (questionId == 3) {
            reasons.add("A. Explores impacts, concerns and expectations");
            reasons.add("B. Relates subsequent questioning and explanations to previously elicited ideas, concerns or expectations");
            reasons.add("C. Checks student's interpretation of information with the patient - clarifying and asking for any corrections or questions");
            reasons.add("D. Recognises and prioritises patient′s needs during interview");
        }

        if (oReasons == null || "".equals(oReasons)) {
            return null;
        }

        String[] str = oReasons.split(",");
        for (int i = 0; i < str.length; i++) {
            for (int j = 0; j < reasons.size(); j++) {
                if (reasons.get(j).startsWith(str[i])) {
                    reasons_result.add(reasons.get(j));
                }
            }
        }

        String[] result = new String[reasons_result.size()];

        for (int i = 0; i < reasons_result.size(); i++) {
            result[i] = reasons_result.get(i);
        }

        return result;

    }
    
    public Criterion[] createCrition() {

        CriterionGrade grade_f = new CriterionGrade();
        grade_f.setGrade("F");
        CriterionGrade grade_pm = new CriterionGrade();
        grade_f.setGrade("P-");
        CriterionGrade grade_p = new CriterionGrade();
        grade_f.setGrade("P");
        CriterionGrade grade_pp = new CriterionGrade();
        grade_f.setGrade("P+");
        CriterionGrade[] totalGradeList = { grade_f, grade_pm, grade_p, grade_pp };

        CriterionGrade focus_1 = new CriterionGrade();
        focus_1.setGrade("Provide structure");
        CriterionGrade focus_2 = new CriterionGrade();
        focus_2.setGrade("Gather information");
        CriterionGrade focus_3 = new CriterionGrade();
        focus_3.setGrade("Build relationships & developing rapport");
        CriterionGrade focus_4 = new CriterionGrade();
        focus_4.setGrade("Ensure a shared understanding of patient's needs and perspective/impact of problem");
        CriterionGrade[] overallGrades = { focus_1, focus_2, focus_3, focus_4 };

        
        String question1= "How you felt the interview went for you at the time?";
        String question2= "How this compares with the grade and comments entered by the assessor?";
        String question3= "What this means to you for how you will continue to develop your communication skills?";
        
        CriterionGrade ref_1 = new CriterionGrade();
        ref_1.setGrade(question1);
        CriterionGrade ref_2 = new CriterionGrade();
        ref_2.setGrade(question2);
        CriterionGrade ref_3 = new CriterionGrade();
        ref_3.setGrade(question3);
        CriterionGrade[] reflectionGrades = { ref_1, ref_2, ref_3};
        
        Criterion cris_1 = new Criterion();
        cris_1.setCriterionNum("1");
        TemplateField title_1 = new TemplateField();
        title_1.setLabel("Provide structure");
        title_1.setValue("Provide structure");
        cris_1.setTitle(title_1);
        cris_1.setDescription(title_1);
        cris_1.setCriterionGradeList(totalGradeList);

        Criterion cris_2 = new Criterion();
        cris_2.setCriterionNum("2");
        TemplateField title_2 = new TemplateField();
        title_2.setLabel("Gather information");
        title_2.setValue("Gather information");
        cris_2.setTitle(title_2);
        cris_2.setDescription(title_2);
        cris_2.setCriterionGradeList(totalGradeList);

        Criterion cris_3 = new Criterion();
        cris_3.setCriterionNum("3");
        TemplateField title_3 = new TemplateField();
        title_3.setLabel("Build relationships & developing rapport");
        title_3.setValue("Build relationships & developing rapport");
        cris_3.setTitle(title_3);
        cris_3.setDescription(title_3);
        cris_3.setCriterionGradeList(totalGradeList);

        Criterion cris_4 = new Criterion();
        cris_4.setCriterionNum("4");
        TemplateField title_4 = new TemplateField();
        title_4.setLabel("Ensure a shared understanding of patient's needs and perspective/impact of problem");
        title_4.setValue("Ensure a shared understanding of patient's needs and perspective/impact of problem");
        cris_4.setTitle(title_4);
        cris_4.setDescription(title_4);
        cris_4.setCriterionGradeList(totalGradeList);

        Criterion cris_5 = new Criterion();
        cris_5.setCriterionNum("5");
        TemplateField title_5 = new TemplateField();
        title_5.setLabel("Overall feedback");
        title_5.setValue("Overall feedback");
        cris_5.setTitle(title_5);
        cris_5.setDescription(title_5);
        cris_5.setCriterionGradeList(overallGrades);

        Criterion cris_6 = new Criterion();
        cris_6.setCriterionNum("6");
        TemplateField title_6 = new TemplateField();
        title_6.setLabel("Reflection answer");
        title_6.setValue("Reflection answer");
        cris_6.setTitle(title_6);
        cris_6.setDescription(title_6);
        cris_6.setCriterionGradeList(reflectionGrades);
        
        Criterion[] cris = { cris_1, cris_2, cris_3, cris_4, cris_5, cris_6 };
        return cris;
    }


}
