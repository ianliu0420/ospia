package org.poscomp.eqclinic.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.poscomp.eqclinic.domain.pojo.PasswordObj;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.filter.CSRFTokenManager;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.poscomp.survey.domain.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class PatientController {

    private static final Logger logger = Logger.getLogger(PatientController.class);

    @Autowired
    private PatientService patientService;

    @Autowired
    private ScenarioService scenarioService;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";
    private static final String SURVEYPATH = "/survey";

    
    @RequestMapping(value = PATIENTPATH + "/updateagreement", method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
    @ResponseBody
    public ReturnValue updatePatientAgreement(HttpServletRequest request, @RequestBody Patient patient) {
        ReturnValue rv = new ReturnValue();
        if (patient != null) {
            Patient patientInDb = patientService.getPatientByPatientId(patient.getPatientid());
            patientInDb.setAgreement(patient.getAgreement());
            patientService.savePatient(patientInDb);
            rv.setCode(1);

            MyUserPrincipal activeUser = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            activeUser.setPatient(patientInDb);
        } else {
            rv.setCode(0);
        }
        return rv;
    }
    
    
    @RequestMapping(value = PATIENTPATH + "/getpatient/{patientId}", method = {RequestMethod.POST})
    @ResponseBody
    public Patient getPatient(HttpServletRequest request, @PathVariable int patientId) {
        Patient patient = patientService.getPatientByPatientId(patientId);
        return patient;
    }

    @RequestMapping(value = PATIENTPATH + "/savepatient",method = {RequestMethod.POST}, headers = {"Content-type=application/json"})
    @ResponseBody
    public ReturnValue savePatient(HttpServletRequest request, @RequestBody Patient patient) {
        ReturnValue rv = new ReturnValue();
        if (patient != null) {

            Patient patientInDb = patientService.getPatientByUsername(patient.getUsername());
            
            String phoneNo = patient.getPhone();
            
            if(phoneNo!=null && !"".equals(phoneNo)){
                Patient patientInDbPhone = patientService.getPatientByPhoneNo("+61" + patient.getPhone().substring(1));
                if(patientInDbPhone !=null){ 
                    rv.setCode(-1);
                    rv.setContent("Your phone number already exist");
                    return rv;
                }
            }
            
            if (patientInDb != null) {
                rv.setCode(-1);
                rv.setContent("Your email already exist");
            }else {
                // update phone number
                phoneNo = patient.getPhone();
                
                if(phoneNo!=null && !"".equals(phoneNo)){
                    patient.setPhone("+61" + phoneNo.substring(1));
                }

                List<Scenario> scenarios = scenarioService.getScenarioByPatient(patient);

                Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
                String hashedPassword = passwordEncoder.encodePassword(patient.getPassword(), patient.getUsername());

                patient.setPassword(hashedPassword);
                patient.setAgreement(0);
                patient.setCreateAt(System.currentTimeMillis());
                patientService.createPatient(patient, scenarios);
                rv.setCode(1);
            }

        } else {
            rv.setCode(0);
            rv.setContent("Register fail, please try again");
        }
        return rv;
    }
    
    
    @RequestMapping(value = SURVEYPATH + "/editpatient", method = {RequestMethod.POST},
                    headers = {"Content-type=application/json"})
    @ResponseBody
    public ReturnValue editPatient(HttpServletRequest request, @RequestBody Patient patient) {
        Patient oldPatient = patientService.getPatientByPatientId(patient.getPatientid());
        oldPatient.setTitle(patient.getTitle());
        oldPatient.setFirstname(patient.getFirstname());
        oldPatient.setLastname(patient.getLastname());
        oldPatient.setGender(patient.getGender());
        oldPatient.setAge(patient.getAge());
        oldPatient.setPhone(patient.getPhone());
        oldPatient.setEmail(patient.getEmail());
        oldPatient.setStreet(patient.getStreet());
        oldPatient.setSuburb(patient.getSuburb());
        oldPatient.setPostcode(patient.getPostcode());
        oldPatient.setHealth(patient.getHealth());
        oldPatient.setLanguage(patient.getLanguage());
        oldPatient.setReason(patient.getReason());
        oldPatient.setNotes(patient.getNotes());
        patientService.savePatient(oldPatient);
        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        return rv;
    }
    
    

    @RequestMapping(value = PATIENTPATH + "/agreescenario/{patientId}", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue agreeScenario(HttpServletRequest request, @PathVariable int patientId) {

        Patient patient = patientService.getPatientByPatientId(patientId);
        ReturnValue rv = new ReturnValue();

        if (patient != null) {
            String scenarios = patient.getScenarioids();
            patient.setTrainedsceids(scenarios);
            patientService.savePatient(patient);
            rv.setCode(1);
            MyUserPrincipal activeUser =
                            (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            activeUser.setPatient(patient);
        } else {
            rv.setCode(0);
            rv.setContent("Operation fail, please try again");
        }
        return rv;
    }

    @RequestMapping(value = PATIENTPATH + "/changepassword", method = {RequestMethod.POST},
                    headers = {"Content-type=application/json"})
    @ResponseBody
    public ReturnValue changePassword(HttpServletRequest request, @RequestBody PasswordObj po) {
        ReturnValue rv = new ReturnValue();
        if (po != null) {
            Patient patient = patientService.getPatientByPatientId(po.getUserId());

            Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
            String hashedOldPassword = passwordEncoder.encodePassword(po.getOldPassword(), patient.getUsername());

            if (patient.getPassword().equals(hashedOldPassword)) {
                String hashedNewPassword = passwordEncoder.encodePassword(po.getNewPassword(), patient.getUsername());
                patient.setPassword(hashedNewPassword);
                patientService.savePatient(patient);
                MyUserPrincipal activeUser =
                                (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                activeUser.setPatient(patient);
                rv.setCode(0);
                rv.setContent("Operation success");
            } else {
                rv.setCode(0);
                rv.setContent("Operation fail, your old password is not right");
            }
        } else {
            rv.setCode(0);
            rv.setContent("Operation fail, please try again");
        }
        return rv;
    }


    @RequestMapping(value = SURVEYPATH + "/patients", method = {RequestMethod.GET})
    public String getAllPatient(HttpServletRequest request) {

        String searchContent = request.getParameter("searchContent");
        List<Patient> patients = null;
        if (searchContent == null || "".equals(searchContent)) {
            patients = patientService.getAllPatients();
        } else {
            patients = patientService.searchPatient(searchContent);
        }

        request.setAttribute("patients", patients);
        request.setAttribute("redpage", "sp_admin");
        request.setAttribute("searchContent", searchContent);
        return "survey/listpatient";

    }


    @RequestMapping(value = SURVEYPATH + "/disablepatient/{patientId}", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue disablePatient(HttpServletRequest request, @PathVariable int patientId) {

        ReturnValue rv = new ReturnValue();

        if (patientId != 0) {
            Patient patient = patientService.getPatientByPatientId(patientId);
            patient.setStatus(0);
            patientService.savePatient(patient);
            rv.setCode(1);
        } else {
            rv.setCode(0);
        }

        return rv;
    }
    
    
    @RequestMapping(value = SURVEYPATH + "/editpatient/{patientId}")
    public String editPatient(HttpServletRequest request, @PathVariable int patientId) {
        Patient patient = patientService.getPatientByPatientId(patientId);
        request.setAttribute("patient", patient);
        request.setAttribute("redpage", "sp_admin");
        return "survey/editpatient";
    }


}
