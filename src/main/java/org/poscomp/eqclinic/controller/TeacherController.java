package org.poscomp.eqclinic.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.Teacher;
import org.poscomp.eqclinic.domain.Video;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.poscomp.eqclinic.service.interfaces.TeacherService;
import org.poscomp.eqclinic.service.interfaces.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class TeacherController {

    private static final Logger logger = Logger.getLogger(TeacherController.class);

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";
    private static final String TUTORPATH = "/tut";
    private static final String SURVEYPATH = "/survey";

    @Autowired
    private ScenarioService scenarioService;

    @Autowired
    private VideoService videoService;
    
    @Autowired
    private TeacherService teacherService;

    @RequestMapping(value = TUTORPATH + "/reviewscreen_tut/{archiveId}", method = { RequestMethod.GET })
    public String redirect_reviewscreen_tut(HttpServletRequest request, @PathVariable String archiveId) {
        System.out.println(archiveId);
        request.setAttribute("redpage", "review_tut");
        Video video = videoService.getVideoByArchiveId(archiveId);
        request.setAttribute("video", video);

        String sceCode = video.getSceCode();
        List<Scenario> sce = scenarioService.getScenarioByCode(sceCode);
        request.setAttribute("scenario", sce.get(0));

        return "tut/33_Tut_SP_screen";
    }

    @RequestMapping(value = SURVEYPATH + "/assessors", method = { RequestMethod.GET })
    public String assessorAdmin(HttpServletRequest request) {
    	
        String searchContent = request.getParameter("searchContent");
        List<Teacher> assessors = null;
        if (searchContent == null || "".equals(searchContent)) {
        	assessors = teacherService.getAllTeacher();
        } else {
        	assessors = teacherService.searchTeacher(searchContent);
        }

        request.setAttribute("assessors", assessors);
        request.setAttribute("redpage", "assessors_admin");
        request.setAttribute("searchContent", searchContent);
        return "survey/listassessor";
        

    }
    
    @RequestMapping(value = SURVEYPATH + "/disableassessor/{assessorId}", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue disableAssessor(HttpServletRequest request, @PathVariable int assessorId) {

        ReturnValue rv = new ReturnValue();

        if (assessorId != 0) {
            Teacher teacher = teacherService.getTeacherByTeacherId(assessorId);
            teacher.setStatus(0);
            teacherService.saveTeacher(teacher);
            rv.setCode(1);
        } else {
            rv.setCode(0);
        }

        return rv;
    }
    
    
    @RequestMapping(value = SURVEYPATH + "/uploadExcelFileAssessor", method = {RequestMethod.POST})
    public String uploadFile(@RequestParam(value="filename") MultipartFile file, HttpServletRequest request,HttpServletResponse response) throws IOException {
    	
    	if(file==null) return null;
    	InputStream is = file.getInputStream();
    	XSSFWorkbook hssfWorkbook = new XSSFWorkbook(is);  
    	
    	for (int n = 0; n < hssfWorkbook.getNumberOfSheets(); n++) { 
    		XSSFSheet sheet = hssfWorkbook.getSheetAt(n);  
            if (sheet == null) {  
                continue;  
            }  
    		
            for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {  
            	Row row = sheet.getRow(rowNum);  
            	
            	if(row==null){
            		continue;
            	}
            	
            	Teacher teacher = new Teacher();
                for (int i = 0; i < row.getLastCellNum(); i++) {  
                	Cell cell = row.getCell(i);  
                	String cellValue = cell.getStringCellValue();
                	cellValue = cellValue.trim();
                	
                	if(cellValue!=null && !"".equals(cellValue)){
                		if(i==0){
                    		teacher.setFirstname(cellValue);	
                    	}else if(i==1){
                    		teacher.setLastname(cellValue);
                    	}else if(i==2){
                    		Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
                    	    String hashedPassword = passwordEncoder.encodePassword("Demonstrator",cellValue);
                    		teacher.setUsername(cellValue);
                    		teacher.setStatus(1);
                    		teacher.setPassword(hashedPassword);
                    	}
                	}
                }
                
                Teacher teacherInDB = teacherService.getTeacherByUsername(teacher.getUsername());
                
                if(teacherInDB==null){
                	teacherService.saveTeacher(teacher);
                }else{
                	teacherInDB.setStatus(1);
                	teacherService.saveTeacher(teacherInDB);
                }
            }
    	}
    	is.close();
    	String redirectUrl = SURVEYPATH + "/assessors";
		return "redirect:" + redirectUrl;
    } 
    
    
}
