package org.poscomp.eqclinic.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.PatientEmailPutMoreAppt;
import org.poscomp.eqclinic.domain.PatientEmailQuestionnaireSimple;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.poscomp.eqclinic.domain.pojo.PasswordObj;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.filter.CSRFTokenManager;
import org.poscomp.eqclinic.service.interfaces.PatientEmailPutMoreApptService;
import org.poscomp.eqclinic.service.interfaces.PatientEmailQuestionnaireSimpleService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.poscomp.survey.domain.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class PatientEmailPutMoreApptController {

    private static final Logger logger = Logger.getLogger(PatientEmailPutMoreApptController.class);

    @Autowired
    private PatientEmailPutMoreApptService patientEmailPutMoreApptService;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";
    private static final String SURVEYPATH = "/survey";

    
    @RequestMapping(value = "/putMoreApptYes/{emailId}",method = {RequestMethod.GET})
    public String putMoreApptYes(HttpServletRequest request, @PathVariable int emailId) {
    	ReturnValue rv = new ReturnValue();
    	PatientEmailPutMoreAppt moreAppt = new PatientEmailPutMoreAppt();
    	moreAppt.setEmailId(emailId);
    	moreAppt.setPutMore(1);
    	moreAppt.setSubmitAt(System.currentTimeMillis());
    	patientEmailPutMoreApptService.savePatientEmailPutMoreAppt(moreAppt);
        return "redirect:/sp/cal_sp";
    }
    
    @RequestMapping(value = "/putMoreApptNo/{emailId}",method = {RequestMethod.GET})
    public String putMoreApptNo(HttpServletRequest request, @PathVariable int emailId) {
    	ReturnValue rv = new ReturnValue();
    	PatientEmailPutMoreAppt moreAppt = new PatientEmailPutMoreAppt();
    	moreAppt.setEmailId(emailId);
    	moreAppt.setPutMore(-1);
    	moreAppt.setSubmitAt(System.currentTimeMillis());
    	patientEmailPutMoreApptService.savePatientEmailPutMoreAppt(moreAppt);
        return "redirect:/patientEmailThanks.jsp";
    }
    
}
