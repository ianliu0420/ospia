package org.poscomp.eqclinic.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.PatientEmailQuestionnaireSimple;
import org.poscomp.eqclinic.domain.PatientWeeklyEmail;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.poscomp.eqclinic.domain.pojo.PasswordObj;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.filter.CSRFTokenManager;
import org.poscomp.eqclinic.service.interfaces.PatientEmailQuestionnaireSimpleService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.PatientWeeklyEmailService;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.poscomp.survey.domain.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class PatientEmailQuestionnaireSimpleController {

    private static final Logger logger = Logger.getLogger(PatientEmailQuestionnaireSimpleController.class);

    @Autowired
    private PatientService patientService;

    @Autowired
    private PatientWeeklyEmailService patientWeeklyEmailService;
    
    @Autowired
    private PatientEmailQuestionnaireSimpleService patientEmailQuestionnaireSimpleService;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";
    private static final String SURVEYPATH = "/survey";

    
    @RequestMapping(value = "/savepatientq",method = {RequestMethod.POST}, headers = {"Content-type=application/json"})
    @ResponseBody
    public ReturnValue savePatient(HttpServletRequest request, @RequestBody PatientEmailQuestionnaireSimple patientQ) {
        ReturnValue rv = new ReturnValue();
        
        patientQ.setSubmitAt(System.currentTimeMillis());
        patientEmailQuestionnaireSimpleService.savePatientEmailQuestionnaireSimple(patientQ);
        
        PatientWeeklyEmail email = patientWeeklyEmailService.getPatientEmailById(patientQ.getEmailId());
        email.setHasBeenAnswered(1);
        patientWeeklyEmailService.updatePatientWeeklyEmail(email);
        
        rv.setCode(1);
        return rv;
    }
    
}
