package org.poscomp.eqclinic.service;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.ReflectionLog;
import org.poscomp.eqclinic.service.interfaces.ReflectionLogService;
import org.poscomp.survey.dao.ReflectionLogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "reflectionLogService")
public class ReflectionLogServiceImpl implements ReflectionLogService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private ReflectionLogDao reflectionLogDao;

    @Override
    public void save(ReflectionLog reflectionLog) {
        reflectionLogDao.save(reflectionLog);
    }

    @Override
    public ReflectionLog getById(int reflectionLogId) {
         return reflectionLogDao.getById(reflectionLogId);
    }


}
