package org.poscomp.eqclinic.service;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.AppointmentProgressDao;
import org.poscomp.eqclinic.domain.AppointmentProgress;
import org.poscomp.eqclinic.service.interfaces.AppointmentProgressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "appointmentProgressService")
public class AppointmentProgressServiceImpl implements AppointmentProgressService {

    private static final Logger logger = Logger.getLogger(AppointmentProgressServiceImpl.class);

    @Autowired
    private AppointmentProgressDao appointmentProgressDao;

    @Override
    public void saveAppointmentProgress(AppointmentProgress appPro) {
        appointmentProgressDao.saveAppointmentProgress(appPro);
    }

    @Override
    public int updateAppointmentProgressState(String sessionId, String userType, int userId, String step) {
        AppointmentProgress apt =
                        appointmentProgressDao.getAppointmentProgressBySessionIdUserId(sessionId, userType, userId);

        if ("doctorPerRefFirst".equals(step)) {
            apt.setStage(1);
        } else if ("doctorRecordingType".equals(step)) {
            apt.setStage(2);
        } else if ("doctorEvaluatePatient".equals(step)) {
            apt.setStage(3);
        } else if ("doctorSystemFeedback".equals(step)) {
            apt.setStage(4);
        } else if ("doctorPerRefSecond".equals(step)) {
            apt.setStage(5);
            apt.setHasFinish(1);
        } else if("doctorSelfSoca".equals(step)){
            apt.setStage(6);
        } else if("doctorAutonomy".equals(step)){
            apt.setStage(7);
        } else if ("patientSoca".equals(step)) {
            apt.setStage(1);
        } else if ("patientAutonomy".equals(step)) {
            apt.setStage(2);
        } else if ("patientSystemFeedback".equals(step)) {
            apt.setStage(3);
            apt.setHasFinish(1);
        }
        this.saveAppointmentProgress(apt);
        return 1;
    }

    @Override
    public AppointmentProgress getAppointmentProgressById(int id) {
        return appointmentProgressDao.getAppointmentProgressById(id);
    }

    @Override
    public AppointmentProgress getAppointmentProgressByAptIdUserId(int aptId, String userType, int userId) {
        return appointmentProgressDao.getAppointmentProgressByAptIdUserId(aptId, userType, userId);
    }

    @Override
    public AppointmentProgress getAppointmentProgressByAptIdUserType(int aptId, String userType) {
        return appointmentProgressDao.getAppointmentProgressByAptIdUserType(aptId, userType);
    }

}
