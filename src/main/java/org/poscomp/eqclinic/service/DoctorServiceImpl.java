package org.poscomp.eqclinic.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.DoctorDao;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "doctorService")
public class DoctorServiceImpl implements DoctorService {

    private static final Logger logger = Logger.getLogger(DoctorServiceImpl.class);

    @Autowired
    private DoctorDao doctorDao;

    @Override
    public Doctor doctorLogin(String username, String password) {
        Doctor doctor = doctorDao.getDoctorByUsername(username);
        if (doctor != null && password.equals(doctor.getPassword())) {
            return doctor;
        }
        return null;
    }

    @Override
    public List<Doctor> getAllDoctors() {
        return doctorDao.getAllDoctors();
    }

    @Override
    public Doctor getDoctorByDoctorId(int doctorId) {
        return doctorDao.getDoctorByDoctorId(doctorId);
    }

    @Override
    public void saveDoctor(Doctor doctor) {
        doctorDao.saveDoctor(doctor);

    }

    @Override
    public Doctor getDoctorByUsername(String username) {
        return doctorDao.getDoctorByUsername(username);
    }

	@Override
	public List<Doctor> searchDoctor(String searchContent) {
		return doctorDao.searchDoctor(searchContent);
	}
}
