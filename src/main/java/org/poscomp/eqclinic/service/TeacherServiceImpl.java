package org.poscomp.eqclinic.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.TeacherDao;
import org.poscomp.eqclinic.domain.Teacher;
import org.poscomp.eqclinic.service.interfaces.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "teacherService")
public class TeacherServiceImpl implements TeacherService {

    private static final Logger logger = Logger.getLogger(TeacherServiceImpl.class);

    @Autowired
    private TeacherDao teacherDao;

    @Override
    public Teacher getTeacherByUsername(String username) {
        return teacherDao.getTeacherByUsername(username);
    }

    @Override
    public Teacher getTeacherByTeacherId(int teacherId) {
        return teacherDao.getTeacherByTeacherId(teacherId);
    }

    @Override
    public Teacher teacherLogin(String username, String password) {
        Teacher teacher = teacherDao.getTeacherByUsername(username);
        if (teacher != null && password.equals(teacher.getPassword())) {
            return teacher;
        }
        return null;
    }

    @Override
    public void saveTeacher(Teacher teacher) {
        teacherDao.saveTeacher(teacher);
    }

	@Override
	public List<Teacher> getAllTeacher() {
		return teacherDao.getAllTeacher();
	}

	@Override
	public List<Teacher> searchTeacher(String searchContent) {
		return teacherDao.searchTeacher(searchContent);
	}

}
