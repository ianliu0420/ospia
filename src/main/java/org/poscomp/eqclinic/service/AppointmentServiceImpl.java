package org.poscomp.eqclinic.service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.dao.interfaces.AppointmentDao;
import org.poscomp.eqclinic.dao.interfaces.CancelledOperationDao;
import org.poscomp.eqclinic.dao.interfaces.DoctorDao;
import org.poscomp.eqclinic.dao.interfaces.PatientDao;
import org.poscomp.eqclinic.dao.interfaces.StudentGroupDao;
import org.poscomp.eqclinic.dao.interfaces.UnavailablePeriodDao;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.CancelledOperation;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.StudentGroup;
import org.poscomp.eqclinic.domain.UnavailablePeriod;
import org.poscomp.eqclinic.domain.pojo.AppointmentOrderObject;
import org.poscomp.eqclinic.domain.pojo.AppointmentSearchObject;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.processor.OpenTokProcessor;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.util.EmailTool;
import org.poscomp.eqclinic.util.TimeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "appointmentService")
public class AppointmentServiceImpl implements AppointmentService {

    private static final Logger logger = Logger.getLogger(ApplicationServiceImpl.class);

    @Autowired
    private AppointmentDao appointmentDao;

    @Autowired
    private DoctorDao doctorDao;
    
    @Autowired
    private PatientDao patientDao;

    @Autowired
    private ApplicationDao applicationDao;

    @Autowired
    private CancelledOperationDao cancelledOperationDao;

    @Autowired
    private UnavailablePeriodDao unavailablePeriodDao;

    @Autowired
    private StudentGroupDao studentGroupDao;
    
    @Override
    public void saveApp(Appointment app) {
        appointmentDao.saveApp(app);
    }

    /*
     * aptType: 1. recent request 2. next two weeks appointments 3. call cancelled appointments
     */
    @Override
    public List<Appointment> doctorHomePageApts(int doctorId, int aptType) {

        List<Appointment> apts = new ArrayList<Appointment>();

        if (aptType == 1) { // get recent request
            apts = appointmentDao.getDoctorRecentRequest(doctorId);
        } else if (aptType == 2) { // appointments in the next two weeks
            apts = appointmentDao.getDoctorNextApts(doctorId);
        } else if (aptType == 3) { // all the canceled appointments
            apts = appointmentDao.getDoctorAptCancelled(doctorId);
        }

        return apts;
    }

    @Override
    public List<Appointment> getDoctorAptsWholeDate(int doctorId, String aptdate) {
        return appointmentDao.getDoctorAptsWholeDate(doctorId, aptdate);
    }

    @Override
    public int doctorBookApt(Doctor doctor, Appointment apt) {
        int doctorId = doctor.getDoctorId();

        if (apt != null) {
            long startTime = apt.getStarttime();
            long currentTime = System.currentTimeMillis();

            long minInterval = 60 * 60 * 1000 * 24 * 3L;
            long maxInterval = 60 * 60 * 1000 * 24 * 30L;
            long interval = startTime - currentTime;

            if (interval > minInterval && interval < maxInterval) {
                apt.setDoctorId(doctorId);
                apt.setDoctorname(doctor.getFirstname());
                apt.setApptime(System.currentTimeMillis());
                apt.setStatus(2);
                this.saveApp(apt);
                return 1;

            }
            if (interval < minInterval) {
                return -1;
            }

            if (interval > maxInterval) {
                return -2;
            }
        } else {
            return 0;
        }
        return 0;

    }

    @Override
    public int cancelApt(Appointment apt, int cancelPerson, String cancelReason) {
        if (apt != null) {
            if (cancelPerson == 1) { // student cancel
                int doctorId = apt.getDoctorId();
                apt.setStatus(1);
                apt.setDoctorId(null);
                apt.setDoctorname(null);
                apt.setApplicationNo(0);
                this.saveApp(apt);
                
//                // the application will be delete
//                Application app = applicationDao.findByAptIdDorctorId(apt.getAptid(), doctorId);
//                applicationDao.deleteApplication(app);
                
                // delete all the applications
                List<Application> applications = applicationDao.findByAptId(apt.getAptid());
                for (Application application : applications) {
                	applicationDao.deleteApplication(application);
				}
                
                CancelledOperation operation = new CancelledOperation();
                operation.setAptId(apt.getAptid());
                operation.setCancelAt(System.currentTimeMillis());
                operation.setDoctorId(doctorId);
                operation.setPatientId(null);
                operation.setReason(cancelReason);
                cancelledOperationDao.saveCancelledOperation(operation);
                
                logger.info("Cancel request:" + doctorId + ":" + System.currentTimeMillis() + ":" + cancelReason);
            } else if (cancelPerson == 2) { // sp cancel
                int patientId = apt.getPatientId();
                apt.setCanceltime(System.currentTimeMillis());
                apt.setStatus(5);
                apt.setCancelreason(cancelReason);
                apt.setApplicationNo(0);
                this.saveApp(apt);
                
                CancelledOperation operation = new CancelledOperation();
                operation.setAptId(apt.getAptid());
                operation.setCancelAt(System.currentTimeMillis());
                operation.setDoctorId(null);
                operation.setPatientId(patientId);
                operation.setReason(cancelReason);
                cancelledOperationDao.saveCancelledOperation(operation);
            }
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public List<Appointment> patientHomePageApts(int patientId, int aptType) {
        List<Appointment> apts = new ArrayList<Appointment>();

        if (aptType == 1) { // get recent request
            apts = appointmentDao.getPatientPendingRequest(patientId);
        } else if (aptType == 2) { // appointments in the next two weeks
            apts = appointmentDao.getPatientNextApts(patientId);
        }

        return apts;

    }

    @Override
    public int patientCreateApt(Patient patient, String aptidStr, String insertDateDate, String insertDateTimeStart,
                    String insertDateTimeEnd, Scenario scenario) {
        
        
        String[] temp =  insertDateDate.split(",");
        insertDateDate = temp[0];
        Float difference = Float.parseFloat(temp[1]);
        long differenceInMili = (long)(difference*60*60*1000);
        long startTime = TimeTool.convertStringToTime(insertDateDate + " " + insertDateTimeStart) + differenceInMili;
        long intervalStart = System.currentTimeMillis() + 2 * 24 * 3600 * 1000L;
        long intervalEnd = System.currentTimeMillis() + 90 * 24 * 3600 * 1000L;

        
        if (startTime<System.currentTimeMillis()) {
        // if ((startTime < intervalStart || startTime > intervalEnd) && aptidStr == null && (patient.getEmail().equals("chunfeng.liu@sydney.edu.au") && patient.getEmail().equals("silas.taylor@unsw.edu.au") && patient.getEmail().equals("renee.lim@sydney.edu.au"))) {
            return 0;
        } else {
            
            // check whetherr there is enough space for this apt
            List<Appointment> patientApts =  getPatientAptsWholeDate(patient.getPatientid(), insertDateDate);
            
            for (Appointment appointment : patientApts) {
                
                if(Math.abs(appointment.getStarttime()-startTime)<45*60*1000 && appointment.getStatus()!=4 && appointment.getStatus()!=5&& appointment.getStatus()!=6&& appointment.getStatus()!=7 && aptidStr == null){
                    return -1;
                }
            }
            
            long endTime = TimeTool.convertStringToTime(insertDateDate + " " + insertDateTimeEnd) + differenceInMili;
            insertDateDate = TimeTool.convertLongToString2(startTime);
            
            Date aptDate = TimeTool.convertStringToDate(insertDateDate);

            Appointment apt = null;
            
            if (aptidStr != null) {
                apt = appointmentDao.getAppointmentByAptId(Integer.parseInt(aptidStr));
            }else{
                apt = new Appointment();
            }
            apt.setPatientId(patient.getPatientid());
            apt.setPatientname(patient.getFirstname());
            apt.setAptdate(aptDate);
            apt.setStarttime(startTime);
            apt.setEndtime(endTime);
            apt.setStatus(1);
            apt.setCreatetime(System.currentTimeMillis());
            apt.setApptime(0);
            apt.setConfirmtime(0);
            apt.setCanceltime(0);
            apt.setScenario(scenario.getId());
            apt.setScenarioCode(scenario.getCode());
            apt.setSessionId(OpenTokProcessor.generateSessionId());
//            apt.setDoctorSessionIdBaseline(OpenTokProcessor.generateSessionId());
//            apt.setPatientSessionIdBaseline(OpenTokProcessor.generateSessionId());
            apt.setSessionType(0);
            apt.setHasDonwload(0);
            apt.setApplicationNo(0);
            apt.setAppropriateSp(1);
            apt.setAppropriateStu(1);
            
            this.saveApp(apt);
            return 1;
        }
    }
    
    
    @Override
    public ReturnValue patientCreateAptForUnavailablePeriod(Patient patient, String aptidStr, String insertDateDate, String insertDateTimeStart,
                    String insertDateTimeEnd, Scenario scenario) {
        
        String[] temp =  insertDateDate.split(",");
        insertDateDate = temp[0];
        Float difference = Float.parseFloat(temp[1]);
        long differenceInMili = (long)(difference*60*60*1000);
        long startTime = TimeTool.convertStringToTime(insertDateDate + " " + insertDateTimeStart) + differenceInMili;
        
        List<UnavailablePeriod> unavailablePeriods = unavailablePeriodDao.getUnavailablePeriod();
        
        UnavailablePeriod up = null;
        
        for (UnavailablePeriod unavailablePeriod : unavailablePeriods) {
            long unavailablePeriodStart = unavailablePeriod.getStartDate();
            long unavailablePeriodEnd = unavailablePeriod.getEndDate();
            
            if(startTime>=unavailablePeriodStart && startTime<=unavailablePeriodEnd){
                up = unavailablePeriod;
                break;
            }
        }        
        
        ReturnValue rv = new ReturnValue();
        if(up == null){
            rv.setCode(1);
        }else{
            rv.setCode(-2);
            rv.setContent("Unfortunately, "+up.getStartDateStr() +" to " + up.getEndDateStr() +" is not available for booking. ");
        }
        
        return rv;
    }

    @Override
    public List<Appointment> getPatientApt(int patientId) {
        return appointmentDao.getPatientApts(patientId);
    }

    @Override
    public Appointment getAppointmentBySessionId(String sessionId) {
        return appointmentDao.getAppointmentBySessionId(sessionId);
    }

    @Override
    public List<Appointment> getPatientAptsWholeDate(int patientId, String aptdate) {
        return appointmentDao.getPatientAptsWholeDate(patientId, aptdate);
    }

    @Override
    public Appointment getAppointmentByAptId(int aptId) {
        return appointmentDao.getAppointmentByAptId(aptId);
    }

    @Override
    public List<Appointment> getDoctorAptAvailable(int doctorId) {
        return appointmentDao.getDoctorAptsAvailable(doctorId);
    }

    @Override
    public List<Appointment> getDoctorAptStatus(int doctorId, int status) {
        return appointmentDao.getDoctorAptsStatus(doctorId, status);
    }

    @Override
    public Appointment getLatestAptByDoctorId(int doctorId) {
        return appointmentDao.getLatestAptByDoctorId(doctorId);
    }

    @Override
    public Appointment getLatestAptByPatientId(int patientId) {
        return appointmentDao.getLatestAptByPatientId(patientId);
    }

    @Override
    public int patientConfirmApt(int aptId, int type) {
        Appointment apt = this.getAppointmentByAptId(aptId);

        if (apt != null) {
            if (type == 0) { // sp decline
                apt.setConfirmtime(System.currentTimeMillis());
                apt.setStatus(4);
                this.saveApp(apt);
            } else if (type == 1) { // sp accept request
                
                int doctorId = apt.getDoctorId();
                Doctor doctor = doctorDao.getDoctorByDoctorId(doctorId);
                String zId = doctor.getUserId();
                StudentGroup sGroup = studentGroupDao.getStudentGroupByStudentId(zId);
                int evaluationImage = 1;
                if(sGroup!=null){
                    evaluationImage = sGroup.getEvaluationImage();
                }
                apt.setEvaluationImage(evaluationImage);
                
                apt.setConfirmtime(System.currentTimeMillis());
                apt.setStatus(3);
                this.saveApp(apt);
            }
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int patientConfirmAptEmail(long endTime, int aptId, String patientName) {
        try {
            patientName = URLDecoder.decode(patientName, "utf-8");
            Appointment apt = this.getAppointmentByAptId(aptId);
            if (apt.getStatus()==2 && patientName.equals(apt.getPatientname())) {
                
                int doctorId = apt.getDoctorId();
                Doctor doctor = doctorDao.getDoctorByDoctorId(doctorId);
                String zId = doctor.getUserId();
                StudentGroup sGroup = studentGroupDao.getStudentGroupByStudentId(zId);
                int evaluationImage = 1;
                if(sGroup!=null){
                    evaluationImage = sGroup.getEvaluationImage();
                }
                apt.setEvaluationImage(evaluationImage);
                
                apt.setConfirmtime(System.currentTimeMillis());
                apt.setStatus(3);
                this.saveApp(apt);
                return 1;
            } else {
                return 0;
            }
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
    }

    @Override
    public List<Appointment> findBetweenTwoTime(Long start, Long end) {
        return appointmentDao.findBetweenTwoTime(start, end);
    }

    /**
     * @param applications
     *            : all the candidates for that appointment
     */
    @Override
    public Doctor allocateApt(List<Application> applications, Appointment apt) {
        long DLEVEL = 5000000000L;
        long PLEVEL = 10000000000L;
        long ALEVEL = 1000000000000L;

        List<AppointmentOrderObject> aoos = new ArrayList<AppointmentOrderObject>();
        Patient patient = patientDao.getPatientByPatientId(apt.getPatientId());
        
        if (applications != null && applications.size() > 0) {

            for (Application application : applications) {
                int doctorId = application.getDoctorId();
                Doctor doctor = doctorDao.getDoctorByDoctorId(doctorId);
                StudentGroup sg = studentGroupDao.getStudentGroupByStudentId(doctor.getUserId());
                
                
                List<Appointment> preApts = appointmentDao.getPreviousAptByDoctorId(doctorId);
                long score = 0;
                score += application.getApplyAt();
                
//                // only for special group 6 student who already finished an OSCE, this is the special case for 2017 beginning practise
//                if(sg.getGroupNumber()==6){
//                	score += ALEVEL;
//                }
                
                for (Appointment appointment : preApts) {

                    int sessionType = appointment.getSessionType();
                    int status = appointment.getStatus();
                    if (sessionType == 1) {
                        score += PLEVEL;
                    } else if (sessionType == 2) {
                        score += ALEVEL;
                    }
                    
                    if(status == 2 || status == 3){
                        score += DLEVEL;
                    }
                }
                AppointmentOrderObject aoo = new AppointmentOrderObject(score, application);
                aoos.add(aoo);
            }

            Collections.sort(aoos);

            Application selectedApplication = aoos.get(0).getApplication();
            apt.setApptime(selectedApplication.getApplyAt());
            int doctorId = selectedApplication.getDoctorId();
            Doctor doctor = doctorDao.getDoctorByDoctorId(doctorId);
            apt.setDoctorId(doctorId);
            apt.setDoctorname(doctor.getFirstname());
            apt.setStatus(2);
            appointmentDao.saveApp(apt);

            for (int i = 0; i < aoos.size(); i++) {
                Application application = aoos.get(i).getApplication();
                if (i == 0) {
                    application.setResult(1);
                } else {
                    application.setResult(-1);
                }
                applicationDao.saveApplication(application);
            }
            
            // send email to all the students who are not selected by the program
            if(doctor!=null){
                for (Application application : applications) {
                    
                    int canDoctorId = application.getDoctorId();
                    Doctor canDoctor = doctorDao.getDoctorByDoctorId(canDoctorId);
                    
                    if(canDoctorId!=doctor.getDoctorId()){
                        EmailTool.sendAptNotSelected(apt, canDoctor, patient);
                    }
                }
            }
            
            return doctor;
        } else {
            return null;
        }

    }

    @Override
    public List<Appointment> findAllAvailabel(Long end) {
        return appointmentDao.findAllAvailabel(end);
    }

    @Override
    public int noShowApt(Appointment apt, int cancelPerson, String cancelReason) {
        
        String reason = "";
        
        if(cancelPerson == 1){
            apt.setStatus(6);
            reason = "doctor no show";
            apt.setCancelreason(reason);
        }else if(cancelPerson == 2){
            apt.setStatus(5);
            reason = "patient no show";
            apt.setCancelreason(reason);
        }
        
        
        appointmentDao.saveApp(apt);
        
        return 1;
    }

    @Override
    public List<Appointment> getAptByDate(String aptdate) {
        return appointmentDao.getAptByDate(aptdate);
    }

	@Override
	public List<Appointment> getAvailableAptByTime(long time) {
		return appointmentDao.getAvailableAptByTime(time);
	}

	@Override
	public List<Appointment> searchAppointments(AppointmentSearchObject aso) {
		return appointmentDao.searchAppointments(aso);
	}

	@Override
	public List<Appointment> getPendingRequest24H(int from, int to) {
		return appointmentDao.getPendingRequest24H(from, to);
	}

	@Override
	public List<Appointment> getDoctorAptStatusAssessment(int doctorId, int i) {
		return appointmentDao.getDoctorAptStatusAssessment(doctorId, i);
	}

	@Override
	public List<Object> getAppointmentWithStudentPersonalFeedback(
			int patientId, Long start, Long end) {
		return appointmentDao. getAppointmentWithStudentPersonalFeedback(patientId,start,end);
	}

}
