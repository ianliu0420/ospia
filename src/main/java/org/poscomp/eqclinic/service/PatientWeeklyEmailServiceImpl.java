package org.poscomp.eqclinic.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.dao.interfaces.PatientWeeklyEmailDao;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.PatientWeeklyEmail;
import org.poscomp.eqclinic.service.interfaces.ApplicationService;
import org.poscomp.eqclinic.service.interfaces.PatientWeeklyEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "patientWeeklyEmailService")
public class PatientWeeklyEmailServiceImpl implements PatientWeeklyEmailService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private PatientWeeklyEmailDao patientWeeklyEmailDao;

	@Override
	public int savePatientWeeklyEmail(PatientWeeklyEmail email) {
		return patientWeeklyEmailDao.savePatientWeeklyEmail(email);
	}

	@Override
	public PatientWeeklyEmail getPatientEmailById(int emailId) {
		return patientWeeklyEmailDao.getPatientEmailById(emailId);
	}

	@Override
	public void updatePatientWeeklyEmail(PatientWeeklyEmail email) {
		patientWeeklyEmailDao.updatePatientWeeklyEmail(email);
		return;
	}


}
