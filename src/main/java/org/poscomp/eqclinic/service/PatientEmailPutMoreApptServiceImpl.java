package org.poscomp.eqclinic.service;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.CalibrationUserDao;
import org.poscomp.eqclinic.dao.interfaces.PatientEmailPutMoreApptDao;
import org.poscomp.eqclinic.dao.interfaces.PatientEmailQuestionnaireSimpleDao;
import org.poscomp.eqclinic.domain.CalibrationSurveyAnswer;
import org.poscomp.eqclinic.domain.CalibrationUser;
import org.poscomp.eqclinic.domain.PatientEmailPutMoreAppt;
import org.poscomp.eqclinic.domain.PatientEmailQuestionnaireSimple;
import org.poscomp.eqclinic.service.interfaces.CalibrationUserService;
import org.poscomp.eqclinic.service.interfaces.PatientEmailPutMoreApptService;
import org.poscomp.eqclinic.service.interfaces.PatientEmailQuestionnaireSimpleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "patientEmailPutMoreApptService")
public class PatientEmailPutMoreApptServiceImpl implements PatientEmailPutMoreApptService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private PatientEmailPutMoreApptDao patientEmailPutMoreApptDao;

	@Override
	public void savePatientEmailPutMoreAppt(PatientEmailPutMoreAppt moreAppt) {

		patientEmailPutMoreApptDao.savePatientEmailPutMoreAppt(moreAppt);
	}

}
