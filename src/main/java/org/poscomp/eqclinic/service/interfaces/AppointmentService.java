package org.poscomp.eqclinic.service.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.pojo.AppointmentSearchObject;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface AppointmentService {

    public List<Appointment> doctorHomePageApts(int doctorId, int aptType);

    public List<Appointment> getDoctorAptsWholeDate(int doctorId, String aptdate);

    public int doctorBookApt(Doctor doctor, Appointment apt);

    public int cancelApt(Appointment apt, int cancelPerson, String cancelReason);

    public List<Appointment> getDoctorAptStatus(int doctorId, int status);

    public List<Appointment> getDoctorAptAvailable(int doctorId);
    
    public Appointment getLatestAptByDoctorId(int doctorId);
    
    
    public List<Appointment> patientHomePageApts(int patientId, int aptType);

    public int patientCreateApt(Patient patient, String aptidStr, String insertDateDate, String insertDateTimeStart,
                    String insertDateTimeEnd, Scenario scenario);
    
    public ReturnValue patientCreateAptForUnavailablePeriod(Patient patient, String aptidStr, String insertDateDate, String insertDateTimeStart,
                    String insertDateTimeEnd, Scenario scenario);

    public int patientConfirmApt(int aptId, int type);

    public int patientConfirmAptEmail(long endTime, int aptId, String patientName);
    
    public List<Appointment> getPatientApt(int patientId);

    public List<Appointment> getPatientAptsWholeDate(int patientId, String aptdate);
    
    public Appointment getLatestAptByPatientId(int patientId);
    
    
    
    public void saveApp(Appointment app);
    
    public Appointment getAppointmentBySessionId(String sessionId);

    public Appointment getAppointmentByAptId(int aptId);
    
    public List<Appointment> findBetweenTwoTime(Long start, Long end);
    
    public List<Appointment> findAllAvailabel(Long end);
    
    public Doctor allocateApt(List<Application> applications, Appointment apt);
    
    public int noShowApt(Appointment apt, int cancelPerson, String cancelReason);
   
    
    public List<Appointment> getAptByDate(String aptdate);
    
    public List<Appointment> getAvailableAptByTime(long time);
    
    public List<Appointment> searchAppointments(AppointmentSearchObject aso);
    
    public List<Appointment> getPendingRequest24H(int from, int to);

	public List<Appointment> getDoctorAptStatusAssessment(int doctorId, int status);

	public List<Object> getAppointmentWithStudentPersonalFeedback(int patientId, Long start, Long end);
}
