package org.poscomp.eqclinic.service.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.Participation;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface ParticipationService {

	public void save(Participation part);
}
