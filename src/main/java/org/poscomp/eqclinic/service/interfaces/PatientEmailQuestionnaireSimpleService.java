package org.poscomp.eqclinic.service.interfaces;

import org.poscomp.eqclinic.domain.CalibrationSurveyAnswer;
import org.poscomp.eqclinic.domain.CalibrationUser;
import org.poscomp.eqclinic.domain.PatientEmailQuestionnaireSimple;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface PatientEmailQuestionnaireSimpleService {

	public void savePatientEmailQuestionnaireSimple(PatientEmailQuestionnaireSimple patientQ);
    
}
