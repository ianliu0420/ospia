package org.poscomp.eqclinic.service.interfaces;

import org.poscomp.eqclinic.domain.EmailClicking;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface EmailClickingService {

    public void saveEmailClicking(EmailClicking clicking);

}
