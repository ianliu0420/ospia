package org.poscomp.eqclinic.service.interfaces;


/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface AvailableStudentGroupService {

    public String getAllAvailableGroups();
    
}
