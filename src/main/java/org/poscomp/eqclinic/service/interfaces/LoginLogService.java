package org.poscomp.eqclinic.service.interfaces;

import org.poscomp.eqclinic.domain.LoginLog;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface LoginLogService {

    public void saveLoginLog(LoginLog loginLog);

}
