package org.poscomp.eqclinic.service.interfaces;

import java.util.Date;
import java.util.List;

import org.poscomp.eqclinic.domain.Video;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface VideoService {
    public void saveVideo(Video video);

    public Video getVideoById(int videoId);
    
    public Video getVideo(String videoName);

    public List<Video> getVideoByDoctorId(int doctorId);

    public List<Object> getVideoByDoctorIdWithProgress(int doctorId);

    public List<Object> getVideoByPatientIdWithProgress(int patientId);

    public Video getVideoByArchiveId(String archiveId);
    
    public List<Video> getVideoBySessionId(String sessionId);
    
    public List<Video> getVideoByDate();
    
    public List<Video> getVideoByDate(String dateStr);
    
    public List<Video> getAllUnreviewedVideo();
}
