package org.poscomp.eqclinic.service;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.ParticipationDao;
import org.poscomp.eqclinic.domain.Participation;
import org.poscomp.eqclinic.service.interfaces.ParticipationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "participationService")
public class ParticipationServiceImpl implements ParticipationService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private ParticipationDao participationDao;

	@Override
	public void save(Participation part) {
		participationDao.save(part);
	}

}
