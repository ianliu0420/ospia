package org.poscomp.eqclinic.service;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.AvailableStudentGroupDao;
import org.poscomp.eqclinic.service.interfaces.AvailableStudentGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "availableStudentGroupService")
public class AvailableStudentGroupServiceImpl implements AvailableStudentGroupService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private AvailableStudentGroupDao availableStudentGroupDao;

    @Override
    public String getAllAvailableGroups() {
        return availableStudentGroupDao.getAllAvailableGroups();
    }
}
