package org.poscomp.eqclinic.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.dao.interfaces.PatientWeeklyEmailClickingDao;
import org.poscomp.eqclinic.dao.interfaces.PatientWeeklyEmailDao;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.PatientWeeklyEmail;
import org.poscomp.eqclinic.domain.PatientWeeklyEmailClicking;
import org.poscomp.eqclinic.service.interfaces.ApplicationService;
import org.poscomp.eqclinic.service.interfaces.PatientWeeklyEmailClickingService;
import org.poscomp.eqclinic.service.interfaces.PatientWeeklyEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "patientWeeklyEmailClickingService")
public class PatientWeeklyEmailClickingServiceImpl implements PatientWeeklyEmailClickingService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private PatientWeeklyEmailClickingDao patientWeeklyEmailClickingDao;

	@Override
	public void savePatientWeeklyEmailClicking(	PatientWeeklyEmailClicking clicking) {
		patientWeeklyEmailClickingDao.savePatientWeeklyEmailClicking(clicking);
		return;
	}


}
