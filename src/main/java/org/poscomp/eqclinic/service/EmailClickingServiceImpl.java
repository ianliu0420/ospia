package org.poscomp.eqclinic.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.dao.interfaces.EmailClickingDao;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.EmailClicking;
import org.poscomp.eqclinic.service.interfaces.ApplicationService;
import org.poscomp.eqclinic.service.interfaces.EmailClickingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "emailClickingService")
public class EmailClickingServiceImpl implements EmailClickingService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private EmailClickingDao emailClickingDao;


    @Override
    public void saveEmailClicking(EmailClicking clicking) {
        emailClickingDao.saveEmailClicking(clicking);
    }

}
