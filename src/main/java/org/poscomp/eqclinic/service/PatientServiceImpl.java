package org.poscomp.eqclinic.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.PatientDao;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "patientService")
public class PatientServiceImpl implements PatientService {

    private static final Logger logger = Logger.getLogger(PatientServiceImpl.class);

    @Autowired
    private PatientDao patientDao;
    
    @Override
    public Patient patientLogin(String username, String password) {
        Patient patient = patientDao.getPatientByUsername(username);
        if (patient != null && password.equals(patient.getPassword())&& patient.getStatus()==1) {
            return patient;
        }
        return null;
    }

    @Override
    public Patient getPatientByPatientId(int patientid) {
        return patientDao.getPatientByPatientId(patientid);
    }

    public void savePatient(Patient patient) {
        patientDao.savePatient(patient);
    }

    @Override
    public void createPatient(Patient patient, List<Scenario> scenarios) {
        String scenarioids = "";
        for (Scenario scenario : scenarios) {
            if ("".equals(scenarioids)) {
                scenarioids += scenario.getId();
            } else {
                scenarioids += "," + scenario.getId();
            }
        }
        patient.setScenarioids(scenarioids);
        this.savePatient(patient);
    }

    @Override
    public Patient getPatientByUsername(String username) {
        return patientDao.getPatientByUsername(username);
    }

    @Override
    public Patient getPatientByPhoneNo(String phoneNo) {
        return patientDao.getPatientByPhoneNo(phoneNo);
    }

    @Override
    public List<Patient> getAllPatients() {
        return patientDao.getAllPatients();
    }

    @Override
    public List<Patient> searchPatient(String searchContent) {
        return patientDao.searchPatient(searchContent);
    }

    @Override
    public List<Patient> getPatientInPeriod(Long start, Long end) {
        return patientDao.getPatientInPeriod(start, end);
    }

}
