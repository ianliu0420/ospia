package org.poscomp.eqclinic.service;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.CalibrationUserDao;
import org.poscomp.eqclinic.dao.interfaces.PatientEmailQuestionnaireSimpleDao;
import org.poscomp.eqclinic.domain.CalibrationSurveyAnswer;
import org.poscomp.eqclinic.domain.CalibrationUser;
import org.poscomp.eqclinic.domain.PatientEmailQuestionnaireSimple;
import org.poscomp.eqclinic.service.interfaces.CalibrationUserService;
import org.poscomp.eqclinic.service.interfaces.PatientEmailQuestionnaireSimpleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "patientEmailQuestionnaireSimpleService")
public class PatientEmailQuestionnaireSimpleServiceImpl implements PatientEmailQuestionnaireSimpleService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private PatientEmailQuestionnaireSimpleDao patientEmailQuestionnaireSimpleDao;

	public void savePatientEmailQuestionnaireSimple(
			PatientEmailQuestionnaireSimple patientQ) {
		patientEmailQuestionnaireSimpleDao.savePatientEmailQuestionnaireSimple(patientQ);
	}

}
