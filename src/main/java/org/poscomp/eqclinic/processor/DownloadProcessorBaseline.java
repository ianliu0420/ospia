package org.poscomp.eqclinic.processor;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Note;
import org.poscomp.eqclinic.domain.Video;
import org.poscomp.eqclinic.domain.pojo.RecordingFile;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.NoteService;
import org.poscomp.eqclinic.service.interfaces.VideoService;
import org.poscomp.eqclinic.util.ConstantValue;
import org.poscomp.eqclinic.util.JsonTool;
import org.poscomp.eqclinic.util.ParserSmile;
import org.poscomp.eqclinic.util.StringTool;
import org.poscomp.eqclinic.util.ZipTool;

import com.opentok.Archive;
import com.opentok.OpenTok;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class DownloadProcessorBaseline implements Runnable {

    private static final Logger logger = Logger.getLogger(DownloadProcessorBaseline.class);

    private static Archive processingArchive;
    private VideoService videoService;
    private AppointmentService appointmentService;
    private NoteService noteService;
    private String sessionId;
    private static Archive archiveDoctor;
    private static Archive archivePatient;
    private static Archive archiveDoctorBaseline;
    private static Archive archivePatientBaseline;
    public static final int FPS = 25;
    public static final int THRESHOLD = 25000;
    public static String projectName = ConstantValue.projectName;

    private String archiveId;

    public DownloadProcessorBaseline(String archiveId, VideoService videoService,
                    AppointmentService appointmentService, NoteService noteService, String sessionId) {
        this.archiveId = archiveId;
        this.videoService = videoService;
        this.appointmentService = appointmentService;
        this.noteService = noteService;
        this.sessionId = sessionId;
    }

    public static void main(String[] args) {
        
        String exePath = "c:\\videos\\ffmpeg.exe";
        Cmd cmd = new Cmd();
        cmd.extractAudio(exePath, "E:/apache-tomcat-7.0.63/webapps/ospia/video/baselines/video_1471845594519_doctor_baseline.mp4",
                        "E:/apache-tomcat-7.0.63/webapps/ospia/video/baselines/video_1471845594519_doctor_baseline.wav");
    }


    public void run() {

        String apiKey = "45493902";
        String apiSecret = "54d8c8370b3828f633154dc04810e3af4535646f";
        OpenTok opentok;
        opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
        try {

            while (true) {
                processingArchive = opentok.getArchive(archiveId);

                if (processingArchive != null) {
                    String url = processingArchive.getUrl();
                    if (url != null && !"".equals(url)) {
                        int videoLength = processingArchive.getDuration();
                        Video video = videoService.getVideoByArchiveId(archiveId);
                        video.setLength(videoLength);
                        videoService.saveVideo(video);
                        break;
                    } else {
                        Thread.sleep(5000);
                    }
                }
            }

            // below, we download the file, and process the file
            String fileName = processingArchive.getName();

            String saveDir = "../webapps/" + projectName + "/video/baselines/";

            if (!saveDir.endsWith(File.separator)) {
                saveDir = saveDir + File.separator;
            }
            File dir = new File(saveDir);
            if (!dir.exists()) {
                if (dir.mkdirs()) {
                    // do nothing
                }
            }

            String fileExt = ".mp4";
            // check whether the video already download
            File file = new File(saveDir + "/" + fileName + fileExt);
            if (!file.exists()) {

                String url = processingArchive.getUrl();

                int result = 0;

                if (url == null || "".equals(url)) {
                    result = 1;
                } else {
                    result = HttpDownloadUtility.downloadFile(fileName + fileExt, url, saveDir);
                }
                if (result == 1) {
                    Cmd cmd = new Cmd();
                    String exePath = "c:\\videos\\ffmpeg.exe";
                    cmd.extractAudio(exePath, saveDir + fileName + fileExt, saveDir + fileName + ".wav");
                }

            } else {
                
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }
}
