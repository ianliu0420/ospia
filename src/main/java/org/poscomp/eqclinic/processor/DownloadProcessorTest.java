package org.poscomp.eqclinic.processor;

import java.io.File;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.pojo.RecordingFile;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.NoteService;
import org.poscomp.eqclinic.service.interfaces.VideoService;
import org.poscomp.eqclinic.util.ConstantValue;
import org.poscomp.eqclinic.util.JsonTool;
import org.poscomp.eqclinic.util.ZipTool;

import com.opentok.Archive;
import com.opentok.OpenTok;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class DownloadProcessorTest {

    private static final Logger logger = Logger.getLogger(DownloadProcessorTest.class);

    private static Archive processingArchive;
    public static final int FPS = 25;
    public static final int THRESHOLD = 25000;
    public static String projectName = ConstantValue.projectName;

    public static void main(String[] args) {
        
        DownloadProcessorTest test = new DownloadProcessorTest();
        test.run();
        
    }

    public void run() {

        String apiKey = "45493902";
        String apiSecret = "54d8c8370b3828f633154dc04810e3af4535646f";
        OpenTok opentok;
        opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);

        String archiveId = "9704c494-e048-4965-8a93-2a4212c18225";
        
        try {

            while (true) {
                processingArchive = opentok.getArchive(archiveId);
                
                if (processingArchive != null) {
                    String url = processingArchive.getUrl();
                    if (url != null && !"".equals(url)) {
                        int videoLength = processingArchive.getDuration();
                        break;
                    } else {
                        Thread.sleep(5000);
                    }
                }
            }

            // below, we download the file, and process the file
            String fileName = processingArchive.getName();

            String saveDir = "C:/videos/";

            if (!saveDir.endsWith(File.separator)) {
                saveDir = saveDir + File.separator;
            }
            File dir = new File(saveDir);
            if (!dir.exists()) {
                if (dir.mkdirs()) {
                    // do nothing
                }
            }

            String fileExt = ".mp4";
            // check whether the video already download
            File file = new File(saveDir + "/" + fileName + fileExt);
            ArrayList<RecordingFile> files = new ArrayList<RecordingFile>();

            if (!file.exists()) {

                String url = processingArchive.getUrl();

                // get doctor's audio file name
                String audioFileBaseDirStr = "../webapps/" + projectName + "/video/" + fileName + "/";
                int result = 0;
                int resultPatientBaseline = 0;
                int resultDoctorBaseline = 0;

                if (url == null || "".equals(url)) {
                    result = 1;
                    resultPatientBaseline = 1;
                    resultDoctorBaseline = 1;
                } else {
                    String zipFileExt = ".zip";
                    result = HttpDownloadUtility.downloadFile(fileName + zipFileExt, url, saveDir);
                    // unzip the file
//                    ZipTool.unZipFiles(saveDir + fileName + zipFileExt, saveDir);
//                    String jsonFileName = JsonTool.getJsonFileName(saveDir);
//                    
//                    files = JsonTool.loadJsonObject(saveDir + jsonFileName);
                }

            } else {
                // do nothing
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);

        }

    }
}
