package org.poscomp.eqclinic.processor;

import java.util.ArrayList;
import java.util.List;

import org.poscomp.eqclinic.util.SoundTool;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class SoundProcessorNoSync{

    private String audioFileDir;
    private String audioFileName;
    private String saveFileDir;
    private String saveFileName;

    public SoundProcessorNoSync(String audioFileDir, String audioFileName, String saveFileDir, String saveFileName) {
        this.audioFileDir = audioFileDir;
        this.audioFileName = audioFileName;
        this.saveFileDir = saveFileDir;
        this.saveFileName = saveFileName;
    }

    public void run() {

        if (audioFileDir.indexOf(".wav") > 0) {
            String[] temp = audioFileDir.split("\\.");
            audioFileDir = temp[0];
        }

        SoundTool tool = new SoundTool();

        Cmd cmd = new Cmd();
        int fileNumber = cmd.cutSoundForProcessing("c:\\videos\\ffmpeg.exe", audioFileDir, audioFileName);
        
        List<String> inputFilesPitch = new ArrayList<String>();
        List<String> inputFilesLoudness = new ArrayList<String>();
        List<String> inputFilesEndpoints = new ArrayList<String>();
        
        if (audioFileName.contains("doctor")) {
            
          tool = new SoundTool();
          float totalLength = tool.processSoundProperty(audioFileDir, audioFileName, saveFileDir, saveFileName);
          tool = null;
            
            if(fileNumber==1){
                
                tool = new SoundTool();
                tool.processPitch(audioFileDir, audioFileName, saveFileDir, saveFileName);
                
                tool = new SoundTool();
                tool.processLoudness(audioFileDir, audioFileName, saveFileDir, saveFileName, totalLength);
                
                tool = new SoundTool();
                tool.processEndpoint(audioFileDir, audioFileName, saveFileDir, saveFileName, 0);
                
            }else{
                for (int i = 0; i < fileNumber; i++) {
                  tool = new SoundTool();
                  tool.processPitch(audioFileDir, audioFileName+i, saveFileDir, saveFileName+i);
                  inputFilesPitch.add(audioFileDir + "/" +audioFileName+i+"_pitch.txt");
                  tool = null;
                }
                cmd.mergePitchPowerFiles(audioFileDir, audioFileName, "pitch", inputFilesPitch);
                
                for (int i = 0; i < fileNumber; i++) {
                    tool = new SoundTool();
                    tool.processLoudness(audioFileDir, audioFileName+i, saveFileDir, saveFileName+i);
                    inputFilesLoudness.add(audioFileDir + "/" +audioFileName+i+"_loudness.txt");
                    tool = null;
                }
                cmd.mergePitchPowerFiles(audioFileDir, audioFileName, "loudness", inputFilesLoudness, totalLength);
                
                for (int i = 0; i < fileNumber; i++) {
                    tool = new SoundTool();
                    tool.processEndpoint(audioFileDir, audioFileName+i, saveFileDir, saveFileName+i, i);
                    inputFilesEndpoints.add(audioFileDir + "/" +audioFileName+i+"_endpoint.txt");
                    tool = null;
                }
                cmd.mergeEndpointsFiles(audioFileDir, audioFileName, "endpoint", inputFilesEndpoints);
                
            }
            
            tool = new SoundTool();
            tool.shrinkEndpointFile(saveFileDir + "/" + saveFileName + "_endpoint.txt", saveFileDir + "/" + saveFileName
                            + "_endpoint_new.txt", 0.3f);
            
//            tool.processLoudness(audioFileDir, audioFileName, saveFileDir, saveFileName);
//            tool = null;
//
////            tool = new SoundTool();
////            tool.processPitch(audioFileDir, audioFileName, saveFileDir, saveFileName);
////            tool = null;
//            
//            cmd.processPitchAudio("c:\\videos\\ffmpeg.exe", audioFileDir, audioFileName);
//            
//            tool = new SoundTool();
//            tool.processSoundProperty(audioFileDir, audioFileName, saveFileDir, saveFileName);
//            tool = null;
        }else{
            
            if(fileNumber==1){
                tool = new SoundTool();
                tool.processEndpoint(audioFileDir, audioFileName, saveFileDir, saveFileName, 0);
            }else{
                for (int i = 0; i < fileNumber; i++) {
                    tool = new SoundTool();
                    tool.processEndpoint(audioFileDir, audioFileName+i, saveFileDir, saveFileName+i, i);
                    inputFilesEndpoints.add(audioFileDir + "/" +audioFileName+i+"_endpoint.txt");
                    tool = null;
                }
                cmd.mergeEndpointsFiles(audioFileDir, audioFileName, "endpoint", inputFilesEndpoints);
            }
          tool = new SoundTool();
          tool.shrinkEndpointFile(saveFileDir + "/" + saveFileName + "_endpoint.txt", saveFileDir + "/" + saveFileName
                          + "_endpoint_new.txt", 0.3f);

            
        }

//        tool = new SoundTool();
//        tool.processEndpoint(audioFileDir, audioFileName, saveFileDir, saveFileName);
//        tool = null;
//
//        tool = new SoundTool();
//        tool.shrinkEndpointFile(saveFileDir + "/" + saveFileName + "_endpoint.txt", saveFileDir + "/" + saveFileName
//                        + "_endpoint_new.txt", 0.3f);

    }

}
