package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "scenario", catalog = "ospia")
public class Scenario implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String code;
    private String gender;
    private int age;
    private String pysicality;
    private int status;
    private String videoLoc;
    private String detail;
    // the attribute to mark whether the scenario is trained by the user, this
    // attribute is not store in DB
    private int trained;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "gender", length=30)
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Column(name = "age")
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Column(name = "pysicality", length=300)
    public String getPysicality() {
        return pysicality;
    }

    public void setPysicality(String pysicality) {
        this.pysicality = pysicality;
    }

    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Column(name = "videoLoc", length=300)
    public String getVideoLoc() {
        return videoLoc;
    }

    public void setVideoLoc(String videoLoc) {
        this.videoLoc = videoLoc;
    }

    @Column(name = "detail", length = 10000)
    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Column(name = "trained")
    public int getTrained() {
        return trained;
    }

    public void setTrained(int trained) {
        this.trained = trained;
    }

}
