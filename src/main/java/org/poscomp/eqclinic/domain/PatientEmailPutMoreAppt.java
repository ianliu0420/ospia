package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "patientemailputmoreappt", catalog = "ospia")
public class PatientEmailPutMoreAppt implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer emailId;
    private Long submitAt;
    private Integer putMore;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name = "putMore")
	public Integer getPutMore() {
		return putMore;
	}

	public void setPutMore(Integer putMore) {
		this.putMore = putMore;
	}

	@Column(name = "emailId")
	public Integer getEmailId() {
		return emailId;
	}

	public void setEmailId(Integer emailId) {
		this.emailId = emailId;
	}

	@Column(name = "submitAt")
	public Long getSubmitAt() {
		return submitAt;
	}

	public void setSubmitAt(Long submitAt) {
		this.submitAt = submitAt;
	}

	
}
