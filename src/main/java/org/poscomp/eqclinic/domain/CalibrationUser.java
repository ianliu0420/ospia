package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "calibrationuser", catalog = "ospia")
public class CalibrationUser implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String zpass;
    private String clinicalSite;
    private String position;
    private String name;
    private String email;
    private Long crateAt;
    private Integer userType;
    private int agreement;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "zpass", length = 100)
    public String getZpass() {
        return zpass;
    }

    public void setZpass(String zpass) {
        this.zpass = zpass;
    }

    @Column(name = "clinicalSite", length = 100)
    public String getClinicalSite() {
        return clinicalSite;
    }

    public void setClinicalSite(String clinicalSite) {
        this.clinicalSite = clinicalSite;
    }

    @Column(name = "position", length = 100)
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Column(name = "name", length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "email", length = 100)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "createAt")
    public Long getCrateAt() {
        return crateAt;
    }

    public void setCrateAt(Long crateAt) {
        this.crateAt = crateAt;
    }

    @Column(name = "userType")
    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    @Column(name = "agreement")
    public int getAgreement() {
        return agreement;
    }

    public void setAgreement(int agreement) {
        this.agreement = agreement;
    }
    
    

}

