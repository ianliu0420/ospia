package org.poscomp.eqclinic.domain;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "patient", catalog = "ospia")
public class Patient implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer patientid;
    private String health;
    private String title;
    private String firstname;
    private String lastname;
    private Integer status;
    private String username;
    private String password;
    private Integer gender;
    private Integer age;
    private Long birthday;
    private String phone;
    private String email;
    private String street;
    private String suburb;
    private String state;
    private String postcode;
    private String country;
    private String language;
    private String reason;
    private String scenarioids;
    // changed when agree on the training page
    private String trainedsceids;
    private Long lastLoginAt;
    private String appliedApt;
    // whether sign the agreement
    private Integer agreement;
    private Long createAt;
    private String notes;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "patientid", unique = true, nullable = false)
    public Integer getPatientid() {
        return this.patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }

    @Column(name = "firstname", length=300)
    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Column(name = "lastname", length=300)
    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Column(name = "status")
    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Column(name = "username", length=300)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password", length=300)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "gender")
    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    @Column(name = "age")
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Column(name = "birthday")
    public Long getBirthday() {
        return birthday;
    }

    public void setBirthday(Long birthday) {
        this.birthday = birthday;
    }

    @Column(name = "phone", length = 20)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "email", length = 300)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "scenarioids" , length=300)
    public String getScenarioids() {
        return scenarioids;
    }

    public void setScenarioids(String scenarioids) {
        this.scenarioids = scenarioids;
    }

    @Column(name = "lastLoginAt")
    public Long getLastLoginAt() {
        return lastLoginAt;
    }

    public void setLastLoginAt(Long lastLoginAt) {
        this.lastLoginAt = lastLoginAt;
    }

    @Column(name = "trainedsceids", length=300)
    public String getTrainedsceids() {
        return trainedsceids;
    }

    public void setTrainedsceids(String trainedsceids) {
        this.trainedsceids = trainedsceids;
    }

    @Column(name = "health", length = 1000)
    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    @Column(name = "title", length = 50)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "street", length = 300)
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Column(name = "suburb", length = 50)
    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    @Column(name = "postcode", length = 10)
    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Column(name = "country", length = 100)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Column(name = "language", length = 100)
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Column(name = "reason", length = 500)
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Column(name = "appliedApt", length = 10000)
    public String getAppliedApt() {
        return appliedApt;
    }

    public void setAppliedApt(String appliedApt) {
        this.appliedApt = appliedApt;
    }

    @Column(name = "agreement")
    public Integer getAgreement() {
        return agreement;
    }

    public void setAgreement(Integer agreement) {
        this.agreement = agreement;
    }

    @Column(name = "state", length=50)
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Column(name = "createAt")
    public Long getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Long createAt) {
        this.createAt = createAt;
    }

    @Column(name = "notes", length = 3000)
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

    
    
    
}
