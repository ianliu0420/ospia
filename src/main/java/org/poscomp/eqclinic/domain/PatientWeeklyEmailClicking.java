package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "patientweeklyemailclicking", catalog = "ospia")
public class PatientWeeklyEmailClicking implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private Long clickAt;
    private Integer emailId;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "clickAt")
	public Long getClickAt() {
		return clickAt;
	}

	public void setClickAt(Long clickAt) {
		this.clickAt = clickAt;
	}
	
	@Column(name = "emailId")
	public Integer getEmailId() {
		return emailId;
	}

	public void setEmailId(Integer emailId) {
		this.emailId = emailId;
	}

}

