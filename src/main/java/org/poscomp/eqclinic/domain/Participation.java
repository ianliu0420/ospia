package org.poscomp.eqclinic.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "participation", catalog = "ospia")
public class Participation {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String spName;
    private String spEmail;
    // 1: agree, 2 decline
    private Integer agree;
    private Long submitAt;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "spName", length=200)
	public String getSpName() {
		return spName;
	}

	public void setSpName(String spName) {
		this.spName = spName;
	}

	@Column(name = "spEmail", length=200)
	public String getSpEmail() {
		return spEmail;
	}

	public void setSpEmail(String spEmail) {
		this.spEmail = spEmail;
	}

	@Column(name = "agree")
	public Integer getAgree() {
		return agree;
	}

	public void setAgree(Integer agree) {
		this.agree = agree;
	}

	@Column(name = "submitAt")
	public Long getSubmitAt() {
		return submitAt;
	}

	public void setSubmitAt(Long submitAt) {
		this.submitAt = submitAt;
	}

}