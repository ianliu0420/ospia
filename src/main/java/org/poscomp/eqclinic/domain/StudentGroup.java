package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "studentgroup", catalog = "ospia")
public class StudentGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String studentId;
    private String firstName;
    private String lastName;
    private String email;
    private int groupNumber;
    // 0: no email 1: group1 2: group2 3: group3
    private int emailGroup;
    private int evaluationImage;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "studentId")
    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    @Column(name = "firstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "lastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "groupNumber")
    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    @Column(name = "evaluationImage")
    public int getEvaluationImage() {
        return evaluationImage;
    }

    public void setEvaluationImage(int evaluationImage) {
        this.evaluationImage = evaluationImage;
    }

    @Column(name = "emailGroup")
	public int getEmailGroup() {
		return emailGroup;
	}

	public void setEmailGroup(int emailGroup) {
		this.emailGroup = emailGroup;
	}

	 @Column(name = "status")
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	
}
