package org.poscomp.eqclinic.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "appointment", catalog = "ospia")
public class Appointment implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Integer aptid;
    private Integer doctorId;
    private Integer patientId;
    private Date aptdate;
    private long starttime;
    private long endtime;
    private String sessionId;
    private String patientname;
    private String doctorname;
    private String doctorSessionIdBaseline;
    private String patientSessionIdBaseline;
    // 1: sp available 2: student applied 3: apply approved 4: apply declined 
    // 5: sp cancelled 6: student cancelled 7.sp delete 8: recording finished
    private Integer status;
    private long createtime;
    private long apptime;
    private long confirmtime;
    private long canceltime;
    private String cancelreason;
    private Integer scenario;
    private String scenarioCode;
    private Long patientRecTime;
    private Long doctorRecTime;
    private String archId;
    private Long startRecTime;
    private Long stopRecTime;
    private String archIdDocBaseline;
    private String archIdPatBaseline;
    // what type this student use this interaction (1: practice 2: assessment)
    private Integer sessionType;
    // mark whether this appointment has been download
    private Integer hasDonwload;
    
    private Long doctorLoginAt;
    private Long doctorLogoutAt;
    private Long patientLoginAt;
    private Long patientLogoutAt;
    // indicate how many application
    private Integer applicationNo;
    
    // indicate the timestamp when start the formal conversation
    private Long startRealConversationTime;
    
    // 1: thumbup/thumbdown  2: happy/sad face
    private Integer evaluationImage;
    
    private Integer finishedPreSurvey;
    
    private Integer appropriateStu;
    
    private Integer appropriateSp;
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "aptid", unique = true, nullable = false)
    public Integer getAptid() {
        return this.aptid;
    }

    public void setAptid(Integer aptid) {
        this.aptid = aptid;
    }

    @Column(name = "doctorId")
    public Integer getDoctorId() {
        return this.doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    @Column(name = "patientid")
    public Integer getPatientId() {
        return this.patientId;
    }

    public void setPatientId(Integer patientid) {
        this.patientId = patientid;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "aptdate")
    public Date getAptdate() {
        return this.aptdate;
    }

    public void setAptdate(Date aptdate) {
        this.aptdate = aptdate;
    }

    @Column(name = "starttime")
    public long getStarttime() {
        return this.starttime;
    }

    public void setStarttime(long starttime) {
        this.starttime = starttime;
    }

    @Column(name = "endtime")
    public long getEndtime() {
        return this.endtime;
    }

    public void setEndtime(long endtime) {
        this.endtime = endtime;
    }

    @Column(name = "sessionId", length=300)
    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Column(name = "patientname" , length=300)
    public String getPatientname() {
        return patientname;
    }

    public void setPatientname(String patientname) {
        this.patientname = patientname;
    }

    @Column(name = "doctorname", length=300)
    public String getDoctorname() {
        return doctorname;
    }

    public void setDoctorname(String doctorname) {
        this.doctorname = doctorname;
    }

    @Column(name = "status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Column(name = "createtime")
    public long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(long createtime) {
        this.createtime = createtime;
    }

    @Column(name = "apptime")
    public long getApptime() {
        return apptime;
    }

    public void setApptime(long apptime) {
        this.apptime = apptime;
    }

    @Column(name = "confirmtime")
    public long getConfirmtime() {
        return confirmtime;
    }

    public void setConfirmtime(long confirmtime) {
        this.confirmtime = confirmtime;
    }

    @Column(name = "canceltime")
    public long getCanceltime() {
        return canceltime;
    }

    public void setCanceltime(long canceltime) {
        this.canceltime = canceltime;
    }

    @Column(name = "cancelreason", length = 10000)
    public String getCancelreason() {
        return cancelreason;
    }

    public void setCancelreason(String cancelreason) {
        this.cancelreason = cancelreason;
    }

    @Column(name = "scenario")
    public Integer getScenario() {
        return scenario;
    }

    public void setScenario(Integer scenario) {
        this.scenario = scenario;
    }

    @Column(name = "scenarioCode", length=30)
    public String getScenarioCode() {
        return scenarioCode;
    }

    public void setScenarioCode(String scenarioCode) {
        this.scenarioCode = scenarioCode;
    }

    @Column(name = "patientRecTime")
    public Long getPatientRecTime() {
        return patientRecTime;
    }

    public void setPatientRecTime(Long patientRecTime) {
        this.patientRecTime = patientRecTime;
    }

    @Column(name = "doctorRecTime")
    public Long getDoctorRecTime() {
        return doctorRecTime;
    }

    public void setDoctorRecTime(Long doctorRecTime) {
        this.doctorRecTime = doctorRecTime;
    }

    @Column(name = "archId")
    public String getArchId() {
        return archId;
    }

    public void setArchId(String archId) {
        this.archId = archId;
    }

    @Column(name = "startRecTime")
    public Long getStartRecTime() {
        return startRecTime;
    }

    public void setStartRecTime(Long startRecTime) {
        this.startRecTime = startRecTime;
    }

    @Column(name = "stopRecTime")
    public Long getStopRecTime() {
        return stopRecTime;
    }

    public void setStopRecTime(Long stopRecTime) {
        this.stopRecTime = stopRecTime;
    }
    
    @Column(name = "doctorSessionIdBaseline", length=300)
    public String getDoctorSessionIdBaseline() {
        return doctorSessionIdBaseline;
    }

    public void setDoctorSessionIdBaseline(String doctorSessionIdBaseline) {
        this.doctorSessionIdBaseline = doctorSessionIdBaseline;
    }

    @Column(name = "patientSessionIdBaseline", length=300)
    public String getPatientSessionIdBaseline() {
        return patientSessionIdBaseline;
    }

    public void setPatientSessionIdBaseline(String patientSessionIdBaseline) {
        this.patientSessionIdBaseline = patientSessionIdBaseline;
    }

    @Column(name = "archIdDocBaseline", length=300)
    public String getArchIdDocBaseline() {
        return archIdDocBaseline;
    }

    public void setArchIdDocBaseline(String archIdDocBaseline) {
        this.archIdDocBaseline = archIdDocBaseline;
    }

    @Column(name = "archIdPatBaseline", length=300)
    public String getArchIdPatBaseline() {
        return archIdPatBaseline;
    }

    public void setArchIdPatBaseline(String archIdPatBaseline) {
        this.archIdPatBaseline = archIdPatBaseline;
    }

    @Column(name = "sessionType")
    public Integer getSessionType() {
        return sessionType;
    }

    public void setSessionType(Integer sessionType) {
        this.sessionType = sessionType;
    }

    @Column(name = "hasDonwload")
    public Integer getHasDonwload() {
        return hasDonwload;
    }

    public void setHasDonwload(Integer hasDonwload) {
        this.hasDonwload = hasDonwload;
    }

    @Column(name = "doctorLoginAt")
    public Long getDoctorLoginAt() {
        return doctorLoginAt;
    }

    public void setDoctorLoginAt(Long doctorLoginAt) {
        this.doctorLoginAt = doctorLoginAt;
    }

    @Column(name = "doctorLogoutAt")
    public Long getDoctorLogoutAt() {
        return doctorLogoutAt;
    }

    public void setDoctorLogoutAt(Long doctorLogoutAt) {
        this.doctorLogoutAt = doctorLogoutAt;
    }

    @Column(name = "patientLoginAt")
    public Long getPatientLoginAt() {
        return patientLoginAt;
    }

    public void setPatientLoginAt(Long patientLoginAt) {
        this.patientLoginAt = patientLoginAt;
    }

    @Column(name = "patientLogoutAt")
    public Long getPatientLogoutAt() {
        return patientLogoutAt;
    }

    public void setPatientLogoutAt(Long patientLogoutAt) {
        this.patientLogoutAt = patientLogoutAt;
    }

    @Column(name = "applicationNo")
    public Integer getApplicationNo() {
        return applicationNo;
    }

    public void setApplicationNo(Integer applicationNo) {
        this.applicationNo = applicationNo;
    }

    @Column(name = "startRealConversationTime")
    public Long getStartRealConversationTime() {
        return startRealConversationTime;
    }

    public void setStartRealConversationTime(Long startRealConversationTime) {
        this.startRealConversationTime = startRealConversationTime;
    }

    @Column(name = "evaluationImage")
    public Integer getEvaluationImage() {
        return evaluationImage;
    }

    public void setEvaluationImage(Integer evaluationImage) {
        this.evaluationImage = evaluationImage;
    }

    @Column(name = "finishedPreSurvey")
    public Integer getFinishedPreSurvey() {
        return finishedPreSurvey;
    }

    public void setFinishedPreSurvey(Integer finishedPreSurvey) {
        this.finishedPreSurvey = finishedPreSurvey;
    }

    @Column(name = "appropriateStu")
    public Integer getAppropriateStu() {
        return appropriateStu;
    }

    public void setAppropriateStu(Integer appropriateStu) {
        this.appropriateStu = appropriateStu;
    }

    @Column(name = "appropriateSp")
    public Integer getAppropriateSp() {
        return appropriateSp;
    }

    public void setAppropriateSp(Integer appropriateSp) {
        this.appropriateSp = appropriateSp;
    }

    
    
    
    
    
}
