package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "patientemailquestionnairesimple", catalog = "ospia")
public class PatientEmailQuestionnaireSimple implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer q1;
    private Integer q2;
    private Integer q3;
    private String q4;
    private String q5;
    private Integer emailId;
    private Long submitAt;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "q1")
    public Integer getQ1() {
		return q1;
	}

	public void setQ1(Integer q1) {
		this.q1 = q1;
	}

	@Column(name = "q2")
	public Integer getQ2() {
		return q2;
	}

	public void setQ2(Integer q2) {
		this.q2 = q2;
	}

	@Column(name = "q3")
	public Integer getQ3() {
		return q3;
	}

	public void setQ3(Integer q3) {
		this.q3 = q3;
	}

	@Column(name = "q4", length=1000)
	public String getQ4() {
		return q4;
	}

	public void setQ4(String q4) {
		this.q4 = q4;
	}

	@Column(name = "q5", length=1000)
	public String getQ5() {
		return q5;
	}

	public void setQ5(String q5) {
		this.q5 = q5;
	}

	@Column(name = "emailId")
	public Integer getEmailId() {
		return emailId;
	}

	public void setEmailId(Integer emailId) {
		this.emailId = emailId;
	}

	@Column(name = "submitAt")
	public Long getSubmitAt() {
		return submitAt;
	}

	public void setSubmitAt(Long submitAt) {
		this.submitAt = submitAt;
	}

	
}
