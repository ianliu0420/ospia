package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "appointmentprogress", catalog = "ospia")
public class AppointmentProgress implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private Integer aptid;
    private String sessionId;
    // type should be doctor/patient
    private String type;
    // doctor id / patient id
    private Integer userId;
    private Integer stage;
    private Long lastOperationAt;
    private Integer hasFinish;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "aptid")
    public Integer getAptid() {
        return this.aptid;
    }

    public void setAptid(Integer aptid) {
        this.aptid = aptid;
    }

    @Column(name = "sessionId", length=300)
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Column(name = "type", length=30)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "userId")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "stage")
    public Integer getStage() {
        return stage;
    }

    public void setStage(Integer stage) {
        this.stage = stage;
    }

    @Column(name = "lastOperationAt")
    public Long getLastOperationAt() {
        return lastOperationAt;
    }

    public void setLastOperationAt(Long lastOperationAt) {
        this.lastOperationAt = lastOperationAt;
    }

    @Column(name = "hasFinish")
    public Integer getHasFinish() {
        return hasFinish;
    }

    public void setHasFinish(Integer hasFinish) {
        this.hasFinish = hasFinish;
    }

}
