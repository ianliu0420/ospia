package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "patientweeklyemail", catalog = "ospia")
public class PatientWeeklyEmail implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private Long createAt;
    private Integer emailType;
    private Integer patientId;
    private String doctorIds;
    private Integer hasBeenClick;
    private Integer hasBeenAnswered;
    private Integer status;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "createAt")
	public Long getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Long createAt) {
		this.createAt = createAt;
	}

	@Column(name = "emailType")
	public Integer getEmailType() {
		return emailType;
	}

	public void setEmailType(Integer emailType) {
		this.emailType = emailType;
	}

	@Column(name = "patientId")
	public Integer getPatientId() {
		return patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	@Column(name = "doctorIds")
	public String getDoctorIds() {
		return doctorIds;
	}

	public void setDoctorIds(String doctorIds) {
		this.doctorIds = doctorIds;
	}

	@Column(name = "hasBeenClick")
	public Integer getHasBeenClick() {
		return hasBeenClick;
	}

	public void setHasBeenClick(Integer hasBeenClick) {
		this.hasBeenClick = hasBeenClick;
	}

	@Column(name = "hasBeenAnswered")
	public Integer getHasBeenAnswered() {
		return hasBeenAnswered;
	}

	public void setHasBeenAnswered(Integer hasBeenAnswered) {
		this.hasBeenAnswered = hasBeenAnswered;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}

