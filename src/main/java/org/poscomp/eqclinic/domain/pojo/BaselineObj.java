package org.poscomp.eqclinic.domain.pojo;

import java.io.Serializable;

public class BaselineObj implements Serializable {

    private static final long serialVersionUID = 1L;

    // loudness properties
    private float loudnessBottom;
    private float loudnessTop;

    // pitch properties
    private float pitchBottom;
    private float pitchTop;

    // smile intensity
    private float smileBottom;
    private float smileTop;

    // frown intensity
    private float frownBottom;
    private float frownTop;

    // leaning
    private float leaningBottom;
    private float leaningTop;

    // tilting
    private float tiltingBottom;
    private float tiltingTop;

    // touches
    private float touchBottom;
    private float touchTop;

    // touches
    private float movementBottom;
    private float movementTop;

    public float getLoudnessBottom() {
        return loudnessBottom;
    }

    public void setLoudnessBottom(float loudnessBottom) {
        this.loudnessBottom = loudnessBottom;
    }

    public float getLoudnessTop() {
        return loudnessTop;
    }

    public void setLoudnessTop(float loudnessTop) {
        this.loudnessTop = loudnessTop;
    }

    public float getPitchBottom() {
        return pitchBottom;
    }

    public void setPitchBottom(float pitchBottom) {
        this.pitchBottom = pitchBottom;
    }

    public float getPitchTop() {
        return pitchTop;
    }

    public void setPitchTop(float pitchTop) {
        this.pitchTop = pitchTop;
    }

    public float getSmileBottom() {
        return smileBottom;
    }

    public void setSmileBottom(float smileBottom) {
        this.smileBottom = smileBottom;
    }

    public float getSmileTop() {
        return smileTop;
    }

    public void setSmileTop(float smileTop) {
        this.smileTop = smileTop;
    }

    public float getFrownBottom() {
        return frownBottom;
    }

    public void setFrownBottom(float frownBottom) {
        this.frownBottom = frownBottom;
    }

    public float getFrownTop() {
        return frownTop;
    }

    public void setFrownTop(float frownTop) {
        this.frownTop = frownTop;
    }

    public float getLeaningBottom() {
        return leaningBottom;
    }

    public void setLeaningBottom(float leaningBottom) {
        this.leaningBottom = leaningBottom;
    }

    public float getLeaningTop() {
        return leaningTop;
    }

    public void setLeaningTop(float leaningTop) {
        this.leaningTop = leaningTop;
    }

    public float getTiltingBottom() {
        return tiltingBottom;
    }

    public void setTiltingBottom(float tiltingBottom) {
        this.tiltingBottom = tiltingBottom;
    }

    public float getTiltingTop() {
        return tiltingTop;
    }

    public void setTiltingTop(float tiltingTop) {
        this.tiltingTop = tiltingTop;
    }

    public float getTouchBottom() {
        return touchBottom;
    }

    public void setTouchBottom(float touchBottom) {
        this.touchBottom = touchBottom;
    }

    public float getTouchTop() {
        return touchTop;
    }

    public void setTouchTop(float touchTop) {
        this.touchTop = touchTop;
    }

    public float getMovementBottom() {
        return movementBottom;
    }

    public void setMovementBottom(float movementBottom) {
        this.movementBottom = movementBottom;
    }

    public float getMovementTop() {
        return movementTop;
    }

    public void setMovementTop(float movementTop) {
        this.movementTop = movementTop;
    }
    
    public void printObj() {
        
        System.out.println("---------------------------");
        System.out.println("loudness top: "+this.loudnessTop);
        System.out.println("loudness bottom: "+this.loudnessBottom);
        System.out.println("pitch top: "+this.pitchTop);
        System.out.println("pitch bottom: "+this.pitchBottom);
        System.out.println("leaning top: "+this.leaningTop);
        System.out.println("leaning bottom: "+this.leaningBottom);
        System.out.println("tilting top: "+this.tiltingTop);
        System.out.println("tilting bottom: "+this.tiltingBottom);
        System.out.println("movement top: "+this.movementTop);
        System.out.println("movement bottom: "+this.movementBottom);
        System.out.println("---------------------------");
        
    }

}
