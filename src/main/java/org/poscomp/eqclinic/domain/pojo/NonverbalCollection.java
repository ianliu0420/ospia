package org.poscomp.eqclinic.domain.pojo;

public class NonverbalCollection {

    private int speakRatioNo = 0;
    private int volumeRaisedNo = 0;
    private int pitchRaisedNo = 0;
    private int interruptedNo = 0;
    private int smileNo = 0;
    private int frownNo = 0;
    private int nodNo = 0;
    private int shakeNo = 0;
    private int leanNo = 0;
    private int noLookNo = 0;
    private int tiltNo = 0;

    public int getSpeakRatioNo() {
        return speakRatioNo;
    }

    public void setSpeakRatioNo(int speakRatioNo) {
        this.speakRatioNo = speakRatioNo;
    }

    public int getVolumeRaisedNo() {
        return volumeRaisedNo;
    }

    public void setVolumeRaisedNo(int volumeRaisedNo) {
        this.volumeRaisedNo = volumeRaisedNo;
    }

    public int getPitchRaisedNo() {
        return pitchRaisedNo;
    }

    public void setPitchRaisedNo(int pitchRaisedNo) {
        this.pitchRaisedNo = pitchRaisedNo;
    }

    public int getInterruptedNo() {
        return interruptedNo;
    }

    public void setInterruptedNo(int interruptedNo) {
        this.interruptedNo = interruptedNo;
    }

    public int getSmileNo() {
        return smileNo;
    }

    public void setSmileNo(int smileNo) {
        this.smileNo = smileNo;
    }

    public int getFrownNo() {
        return frownNo;
    }

    public void setFrownNo(int frownNo) {
        this.frownNo = frownNo;
    }

    public int getNodNo() {
        return nodNo;
    }

    public void setNodNo(int nodNo) {
        this.nodNo = nodNo;
    }

    public int getShakeNo() {
        return shakeNo;
    }

    public void setShakeNo(int shakeNo) {
        this.shakeNo = shakeNo;
    }

    public int getLeanNo() {
        return leanNo;
    }

    public void setLeanNo(int leanNo) {
        this.leanNo = leanNo;
    }

    public int getNoLookNo() {
        return noLookNo;
    }

    public void setNoLookNo(int noLookNo) {
        this.noLookNo = noLookNo;
    }

    public int getTiltNo() {
        return tiltNo;
    }

    public void setTiltNo(int tiltNo) {
        this.tiltNo = tiltNo;
    }

}
