package org.poscomp.eqclinic.domain.pojo;

import java.io.Serializable;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class SelfTouch implements Serializable {

    private static final long serialVersionUID = 1L;

    private float start;
    private float end;

    public SelfTouch(float start, float end) {
        this.start = start;
        this.end = end;
    }

    public float getStart() {
        return start;
    }

    public void setStart(float start) {
        this.start = start;
    }

    public float getEnd() {
        return end;
    }

    public void setEnd(float end) {
        this.end = end;
    }

}
