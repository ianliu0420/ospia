package org.poscomp.eqclinic.domain.pojo;

import java.io.Serializable;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class SpeechRate implements Serializable {

    private static final long serialVersionUID = 1L;

    private float time;
    private float rate;

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

}
