package org.poscomp.eqclinic.domain.pojo;

import java.io.Serializable;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class SmileTag implements Serializable {

    private static final long serialVersionUID = 1L;

    private int tagId;
    private float value;

    public SmileTag(int tagId, float value) {
        this.tagId = tagId;
        this.value = value;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

}
