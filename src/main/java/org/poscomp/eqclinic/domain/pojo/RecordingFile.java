package org.poscomp.eqclinic.domain.pojo;

import java.io.Serializable;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class RecordingFile implements Serializable, Comparable<RecordingFile> {

    private static final long serialVersionUID = 1L;

    private String fileName;
    private String opentokName;
    private String userType;
    private Long recordingTime;
    private int size;
    private int startTimeOffset;
    private int stopTimeOffset;
    private float trimmedLength;

    private float ss;
    private float t;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOpentokName() {
        return opentokName;
    }

    public void setOpentokName(String opentokName) {
        this.opentokName = opentokName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getStartTimeOffset() {
        return startTimeOffset;
    }

    public void setStartTimeOffset(int startTimeOffset) {
        this.startTimeOffset = startTimeOffset;
    }

    public int getStopTimeOffset() {
        return stopTimeOffset;
    }

    public void setStopTimeOffset(int stopTimeOffset) {
        this.stopTimeOffset = stopTimeOffset;
    }

    public float getTrimmedLength() {
        return trimmedLength;
    }

    public void setTrimmedLength(float trimmedLength) {
        this.trimmedLength = trimmedLength;
    }

    public Long getRecordingTime() {
        return recordingTime;
    }

    public void setRecordingTime(Long recordingTime) {
        this.recordingTime = recordingTime;
    }

    public float getSs() {
        return ss;
    }

    public void setSs(float ss) {
        this.ss = ss;
    }

    public float getT() {
        return t;
    }

    public void setT(float t) {
        this.t = t;
    }

    public RecordingFile getOverlapping(RecordingFile self, RecordingFile other) {

        if (self.getStartTimeOffset() > other.getStopTimeOffset()
                        || self.getStopTimeOffset() < other.getStartTimeOffset()) {
            return null;
        } else {

            int startTime = 0;
            int stopTime = 0;

            if (self.getStartTimeOffset() > other.getStartTimeOffset()) {
                startTime = self.getStartTimeOffset();
            } else {
                startTime = other.getStartTimeOffset();
            }

            if (self.getStopTimeOffset() < other.getStopTimeOffset()) {
                stopTime = self.getStopTimeOffset();
            } else {
                stopTime = other.getStopTimeOffset();
            }

            int startDiff = self.getStartTimeOffset() - startTime;
            float ss = 0;
            if (startDiff >= 0) {
                ss = 0;
            } else {
                ss = Math.abs(startDiff)/1000f;
            }


            float t = (stopTime - startTime)/1000f;

            RecordingFile file = this.copy(self);
            file.setSs(ss);
            file.setT(t);
            file.setStartTimeOffset(startTime);
            file.setStopTimeOffset(stopTime);
            return file;
        }
    }
    
    
    public RecordingFile copy(RecordingFile self){
        
        RecordingFile result = new RecordingFile();
        result.setFileName(self.getFileName());
        result.setOpentokName(self.opentokName);
        result.setUserType(self.userType);
        result.setRecordingTime(self.recordingTime);
        result.setSize(self.size);
        result.setStartTimeOffset(self.startTimeOffset);
        result.setStopTimeOffset(self.stopTimeOffset);
        result.setTrimmedLength(self.trimmedLength);
        result.setSs(self.ss);
        result.setT(self.t);
        return result;
        
    }
    
    @Override
    public int compareTo(RecordingFile o) {
        return this.getStartTimeOffset() - o.getStartTimeOffset();
    }


    @Override
    public String toString() {
        return this.getOpentokName()+"-----"+" ss: " + ss + "   t: " + t;
    }
    
    
}
