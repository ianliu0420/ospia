package org.poscomp.eqclinic.domain.pojo;

import org.poscomp.eqclinic.domain.Application;

public class AppointmentSearchObject {

	private static final long serialVersionUID = 1L;

	// 0 = all apts, 1 = all available apts, 2 = all pending available apts, 3= confirmed apts, 8 = completed apts
    private String searchType;
    
    // search by student zid = 1, search by student name =2, search by patient name =3
    private Integer searchContentType;
    
    private String searchContent;

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public Integer getSearchContentType() {
		return searchContentType;
	}

	public void setSearchContentType(Integer searchContentType) {
		this.searchContentType = searchContentType;
	}

	public String getSearchContent() {
		return searchContent;
	}

	public void setSearchContent(String searchContent) {
		this.searchContent = searchContent;
	}
    
    
    
	
}
