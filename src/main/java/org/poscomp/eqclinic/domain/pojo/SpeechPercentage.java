package org.poscomp.eqclinic.domain.pojo;

import java.io.Serializable;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class SpeechPercentage implements Serializable {

    private static final long serialVersionUID = 1L;

    private float time;
    private float doctorPercentage;
    private float patientPercentage;

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public float getDoctorPercentage() {
        return doctorPercentage;
    }

    public void setDoctorPercentage(float doctorPercentage) {
        this.doctorPercentage = doctorPercentage;
    }

    public float getPatientPercentage() {
        return patientPercentage;
    }

    public void setPatientPercentage(float patientPercentage) {
        this.patientPercentage = patientPercentage;
    }

}
