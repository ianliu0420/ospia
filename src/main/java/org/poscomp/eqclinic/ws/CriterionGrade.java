/**
 * CriterionGrade.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class CriterionGrade  extends org.poscomp.eqclinic.ws.Criterion  implements java.io.Serializable {
    private java.lang.String grade;

    private org.poscomp.eqclinic.ws.TemplateField comment;

    private boolean commentMandatory;

    private java.lang.String[] gradeReason;

    private java.lang.String dateCreated;

    private java.lang.String dateModified;

    public CriterionGrade() {
    }

    public CriterionGrade(
           java.lang.String criterionNum,
           org.poscomp.eqclinic.ws.TemplateField title,
           org.poscomp.eqclinic.ws.TemplateField description,
           org.poscomp.eqclinic.ws.CriterionGrade[] criterionGradeList,
           java.lang.String defaultGrade,
           java.lang.String grade,
           org.poscomp.eqclinic.ws.TemplateField comment,
           boolean commentMandatory,
           java.lang.String[] gradeReason,
           java.lang.String dateCreated,
           java.lang.String dateModified) {
        super(
            criterionNum,
            title,
            description,
            criterionGradeList,
            defaultGrade);
        this.grade = grade;
        this.comment = comment;
        this.commentMandatory = commentMandatory;
        this.gradeReason = gradeReason;
        this.dateCreated = dateCreated;
        this.dateModified = dateModified;
    }


    /**
     * Gets the grade value for this CriterionGrade.
     * 
     * @return grade
     */
    public java.lang.String getGrade() {
        return grade;
    }


    /**
     * Sets the grade value for this CriterionGrade.
     * 
     * @param grade
     */
    public void setGrade(java.lang.String grade) {
        this.grade = grade;
    }


    /**
     * Gets the comment value for this CriterionGrade.
     * 
     * @return comment
     */
    public org.poscomp.eqclinic.ws.TemplateField getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this CriterionGrade.
     * 
     * @param comment
     */
    public void setComment(org.poscomp.eqclinic.ws.TemplateField comment) {
        this.comment = comment;
    }


    /**
     * Gets the commentMandatory value for this CriterionGrade.
     * 
     * @return commentMandatory
     */
    public boolean isCommentMandatory() {
        return commentMandatory;
    }


    /**
     * Sets the commentMandatory value for this CriterionGrade.
     * 
     * @param commentMandatory
     */
    public void setCommentMandatory(boolean commentMandatory) {
        this.commentMandatory = commentMandatory;
    }


    /**
     * Gets the gradeReason value for this CriterionGrade.
     * 
     * @return gradeReason
     */
    public java.lang.String[] getGradeReason() {
        return gradeReason;
    }


    /**
     * Sets the gradeReason value for this CriterionGrade.
     * 
     * @param gradeReason
     */
    public void setGradeReason(java.lang.String[] gradeReason) {
        this.gradeReason = gradeReason;
    }

    public java.lang.String getGradeReason(int i) {
        return this.gradeReason[i];
    }

    public void setGradeReason(int i, java.lang.String _value) {
        this.gradeReason[i] = _value;
    }


    /**
     * Gets the dateCreated value for this CriterionGrade.
     * 
     * @return dateCreated
     */
    public java.lang.String getDateCreated() {
        return dateCreated;
    }


    /**
     * Sets the dateCreated value for this CriterionGrade.
     * 
     * @param dateCreated
     */
    public void setDateCreated(java.lang.String dateCreated) {
        this.dateCreated = dateCreated;
    }


    /**
     * Gets the dateModified value for this CriterionGrade.
     * 
     * @return dateModified
     */
    public java.lang.String getDateModified() {
        return dateModified;
    }


    /**
     * Sets the dateModified value for this CriterionGrade.
     * 
     * @param dateModified
     */
    public void setDateModified(java.lang.String dateModified) {
        this.dateModified = dateModified;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CriterionGrade)) return false;
        CriterionGrade other = (CriterionGrade) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.grade==null && other.getGrade()==null) || 
             (this.grade!=null &&
              this.grade.equals(other.getGrade()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment()))) &&
            this.commentMandatory == other.isCommentMandatory() &&
            ((this.gradeReason==null && other.getGradeReason()==null) || 
             (this.gradeReason!=null &&
              java.util.Arrays.equals(this.gradeReason, other.getGradeReason()))) &&
            ((this.dateCreated==null && other.getDateCreated()==null) || 
             (this.dateCreated!=null &&
              this.dateCreated.equals(other.getDateCreated()))) &&
            ((this.dateModified==null && other.getDateModified()==null) || 
             (this.dateModified!=null &&
              this.dateModified.equals(other.getDateModified())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getGrade() != null) {
            _hashCode += getGrade().hashCode();
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        _hashCode += (isCommentMandatory() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getGradeReason() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGradeReason());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGradeReason(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDateCreated() != null) {
            _hashCode += getDateCreated().hashCode();
        }
        if (getDateModified() != null) {
            _hashCode += getDateModified().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CriterionGrade.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "CriterionGrade"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("grade");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Grade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentMandatory");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CommentMandatory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gradeReason");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GradeReason"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateCreated");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateCreated"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateModified");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateModified"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
