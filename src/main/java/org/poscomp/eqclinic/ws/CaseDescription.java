/**
 * CaseDescription.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class CaseDescription  implements java.io.Serializable {
    private org.poscomp.eqclinic.ws.TemplateField title;

    private org.poscomp.eqclinic.ws.TemplateField patientAge;

    private org.poscomp.eqclinic.ws.TemplateField patientGender;

    private org.poscomp.eqclinic.ws.TemplateField caseComplexity;

    private org.poscomp.eqclinic.ws.TemplateField caseContext;

    private org.poscomp.eqclinic.ws.TemplateField caseFocus;

    private org.poscomp.eqclinic.ws.TemplateField capability;

    private org.poscomp.eqclinic.ws.TemplateField assessorId;

    public CaseDescription() {
    }

    public CaseDescription(
           org.poscomp.eqclinic.ws.TemplateField title,
           org.poscomp.eqclinic.ws.TemplateField patientAge,
           org.poscomp.eqclinic.ws.TemplateField patientGender,
           org.poscomp.eqclinic.ws.TemplateField caseComplexity,
           org.poscomp.eqclinic.ws.TemplateField caseContext,
           org.poscomp.eqclinic.ws.TemplateField caseFocus,
           org.poscomp.eqclinic.ws.TemplateField capability,
           org.poscomp.eqclinic.ws.TemplateField assessorId) {
           this.title = title;
           this.patientAge = patientAge;
           this.patientGender = patientGender;
           this.caseComplexity = caseComplexity;
           this.caseContext = caseContext;
           this.caseFocus = caseFocus;
           this.capability = capability;
           this.assessorId = assessorId;
    }


    /**
     * Gets the title value for this CaseDescription.
     * 
     * @return title
     */
    public org.poscomp.eqclinic.ws.TemplateField getTitle() {
        return title;
    }


    /**
     * Sets the title value for this CaseDescription.
     * 
     * @param title
     */
    public void setTitle(org.poscomp.eqclinic.ws.TemplateField title) {
        this.title = title;
    }


    /**
     * Gets the patientAge value for this CaseDescription.
     * 
     * @return patientAge
     */
    public org.poscomp.eqclinic.ws.TemplateField getPatientAge() {
        return patientAge;
    }


    /**
     * Sets the patientAge value for this CaseDescription.
     * 
     * @param patientAge
     */
    public void setPatientAge(org.poscomp.eqclinic.ws.TemplateField patientAge) {
        this.patientAge = patientAge;
    }


    /**
     * Gets the patientGender value for this CaseDescription.
     * 
     * @return patientGender
     */
    public org.poscomp.eqclinic.ws.TemplateField getPatientGender() {
        return patientGender;
    }


    /**
     * Sets the patientGender value for this CaseDescription.
     * 
     * @param patientGender
     */
    public void setPatientGender(org.poscomp.eqclinic.ws.TemplateField patientGender) {
        this.patientGender = patientGender;
    }


    /**
     * Gets the caseComplexity value for this CaseDescription.
     * 
     * @return caseComplexity
     */
    public org.poscomp.eqclinic.ws.TemplateField getCaseComplexity() {
        return caseComplexity;
    }


    /**
     * Sets the caseComplexity value for this CaseDescription.
     * 
     * @param caseComplexity
     */
    public void setCaseComplexity(org.poscomp.eqclinic.ws.TemplateField caseComplexity) {
        this.caseComplexity = caseComplexity;
    }


    /**
     * Gets the caseContext value for this CaseDescription.
     * 
     * @return caseContext
     */
    public org.poscomp.eqclinic.ws.TemplateField getCaseContext() {
        return caseContext;
    }


    /**
     * Sets the caseContext value for this CaseDescription.
     * 
     * @param caseContext
     */
    public void setCaseContext(org.poscomp.eqclinic.ws.TemplateField caseContext) {
        this.caseContext = caseContext;
    }


    /**
     * Gets the caseFocus value for this CaseDescription.
     * 
     * @return caseFocus
     */
    public org.poscomp.eqclinic.ws.TemplateField getCaseFocus() {
        return caseFocus;
    }


    /**
     * Sets the caseFocus value for this CaseDescription.
     * 
     * @param caseFocus
     */
    public void setCaseFocus(org.poscomp.eqclinic.ws.TemplateField caseFocus) {
        this.caseFocus = caseFocus;
    }


    /**
     * Gets the capability value for this CaseDescription.
     * 
     * @return capability
     */
    public org.poscomp.eqclinic.ws.TemplateField getCapability() {
        return capability;
    }


    /**
     * Sets the capability value for this CaseDescription.
     * 
     * @param capability
     */
    public void setCapability(org.poscomp.eqclinic.ws.TemplateField capability) {
        this.capability = capability;
    }


    /**
     * Gets the assessorId value for this CaseDescription.
     * 
     * @return assessorId
     */
    public org.poscomp.eqclinic.ws.TemplateField getAssessorId() {
        return assessorId;
    }


    /**
     * Sets the assessorId value for this CaseDescription.
     * 
     * @param assessorId
     */
    public void setAssessorId(org.poscomp.eqclinic.ws.TemplateField assessorId) {
        this.assessorId = assessorId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CaseDescription)) return false;
        CaseDescription other = (CaseDescription) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.patientAge==null && other.getPatientAge()==null) || 
             (this.patientAge!=null &&
              this.patientAge.equals(other.getPatientAge()))) &&
            ((this.patientGender==null && other.getPatientGender()==null) || 
             (this.patientGender!=null &&
              this.patientGender.equals(other.getPatientGender()))) &&
            ((this.caseComplexity==null && other.getCaseComplexity()==null) || 
             (this.caseComplexity!=null &&
              this.caseComplexity.equals(other.getCaseComplexity()))) &&
            ((this.caseContext==null && other.getCaseContext()==null) || 
             (this.caseContext!=null &&
              this.caseContext.equals(other.getCaseContext()))) &&
            ((this.caseFocus==null && other.getCaseFocus()==null) || 
             (this.caseFocus!=null &&
              this.caseFocus.equals(other.getCaseFocus()))) &&
            ((this.capability==null && other.getCapability()==null) || 
             (this.capability!=null &&
              this.capability.equals(other.getCapability()))) &&
            ((this.assessorId==null && other.getAssessorId()==null) || 
             (this.assessorId!=null &&
              this.assessorId.equals(other.getAssessorId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getPatientAge() != null) {
            _hashCode += getPatientAge().hashCode();
        }
        if (getPatientGender() != null) {
            _hashCode += getPatientGender().hashCode();
        }
        if (getCaseComplexity() != null) {
            _hashCode += getCaseComplexity().hashCode();
        }
        if (getCaseContext() != null) {
            _hashCode += getCaseContext().hashCode();
        }
        if (getCaseFocus() != null) {
            _hashCode += getCaseFocus().hashCode();
        }
        if (getCapability() != null) {
            _hashCode += getCapability().hashCode();
        }
        if (getAssessorId() != null) {
            _hashCode += getAssessorId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CaseDescription.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "CaseDescription"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Title"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PatientAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientGender");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PatientGender"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("caseComplexity");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CaseComplexity"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("caseContext");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CaseContext"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("caseFocus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CaseFocus"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("capability");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Capability"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assessorId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AssessorId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
