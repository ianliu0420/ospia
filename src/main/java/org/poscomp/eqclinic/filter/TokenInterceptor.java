package org.poscomp.eqclinic.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class TokenInterceptor implements HandlerInterceptor {

    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
                    throws Exception {
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
                    throws Exception {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();
        String token = CSRFTokenManager.getTokenForSession(session);
        request.setAttribute("csrftoken", token);
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse arg1, Object arg2) throws Exception {

    	String requestUrl = request.getRequestURL().toString();
    	if(requestUrl.contains("saveparticipation")||requestUrl.contains("patientconfirmaptsms")||requestUrl.contains("uploadExcelFile")){
    		return true;
    	}
    	
        if ("POST".equals(request.getMethod())) {
            if (CSRFTokenManager.isValidCsrfHeaderToken(request)) {
                return true;
            }
            return false;
        } else {
            return true;
        }
    }

}
