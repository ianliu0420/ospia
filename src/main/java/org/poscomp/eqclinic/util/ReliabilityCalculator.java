package org.poscomp.eqclinic.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.poscomp.eqclinic.domain.CalibrationSurveyAnswer;

public class ReliabilityCalculator {

    public static double calculate(CalibrationSurveyAnswer gold, CalibrationSurveyAnswer test) {

        String goldContent = gold.getSurveyAnswer();
        String testContent = test.getSurveyAnswer();

        System.out.println(goldContent);
        System.out.println(testContent);
        ArrayList<Integer> goldValues = extractValues(goldContent);
        ArrayList<Integer> testValues = extractValues(testContent);

        List<List<Integer>> list = new ArrayList<List<Integer>>();
        list.add(goldValues);
        list.add(testValues);
        
        
        // float result = calculateReliability(goldValues, testValues);
        double result = computeKappa(list);
        
        return result;
    }
    
    
    public static double computeKappa(List<List<Integer>> scores) {
        if (scores.size() != 2) {
            throw new IllegalArgumentException("Only scores from two annotators are valid for Cohen's Kappa");
        }

        if (scores.get(0).size() != scores.get(1).size()) {
            throw new IllegalArgumentException("The scores from both annotators must have the same length");
        }

        Map<Integer, Map<Integer, Integer>> table = new HashMap<Integer, Map<Integer, Integer>>();

        int tableSize = 0;

        for (int i = 0; i < scores.get(0).size(); i++) {
            int first = scores.get(0).get(i);
            int second = scores.get(1).get(i);

            if (!table.containsKey(first)) {
                table.put(first, new HashMap<Integer, Integer>());
            }

            if (!table.get(first).containsKey(second)) {
                table.get(first).put(second, 0);
            }

            table.get(first).put(second, table.get(first).get(second) + 1);

            if (first > tableSize) {
                tableSize = first + 1;
            }

            if (second > tableSize) {
                tableSize = second + 1;
            }
        }

        boolean DEBUG = true;
        if (DEBUG) {
            System.out.println(table);
        }


        // sum the rows and cols
        Map<Integer, Integer> sumRows = new HashMap<Integer, Integer>();
        Map<Integer, Integer> sumCols = new HashMap<Integer, Integer>();

        for (Map.Entry<Integer, Map<Integer, Integer>> entry : table.entrySet()) {
            int rowNumber = entry.getKey();
            int sumRow = 0;

            for (Map.Entry<Integer, Integer> rowEntry : entry.getValue().entrySet()) {
                int colNumber = rowEntry.getKey();
                int value = rowEntry.getValue();

                sumRow += value;

                if (!sumCols.containsKey(colNumber)) {
                    sumCols.put(colNumber, 0);
                }

                sumCols.put(colNumber, sumCols.get(colNumber) + value);
            }

            sumRows.put(rowNumber, sumRow);
        }

        // sum the total
        int sumTotal = 0;
        for (Integer rowSum : sumRows.values()) {
            sumTotal += rowSum;
        }

        if (DEBUG) {
//            System.out.println("Row sums: " + sumRows);
//            System.out.println("Col sums: " + sumCols);
//            System.out.println("Total: " + sumTotal);
//            System.out.println("table size: " + tableSize);
        }

        // sum diagonal
        int sumDiagonal = 0;
        for (int i = 1; i <= tableSize; i++) {
            int value = 0;
            if (table.containsKey(i) && table.get(i).containsKey(i)) {
                value = table.get(i).get(i);
            }

            sumDiagonal += value;
        }

        if (DEBUG) {
//            System.out.println("Sum diagonal: " + sumDiagonal);
        }


        double p = (double) sumDiagonal / (double) sumTotal;

        if (DEBUG) {
//            System.out.println("p: " + p);
        }


        double peSum = 0;
        for (int i = 1; i <= tableSize; i++) {
            if (sumRows.containsKey(i) && sumCols.containsKey(i)) {
                peSum += (double) sumRows.get(i) * (double) sumCols.get(i);
            }
        }

        if (DEBUG) {
//            System.out.println("pesum: " + peSum);
        }

        double pe = peSum / (sumTotal * sumTotal);

        if (DEBUG) {
//            System.out.println("pe: " + pe);
        }


        return (p - pe) / (1.0d - pe);
    }

    

    public static float calculateReliability(ArrayList<Integer> goldValues, ArrayList<Integer> testValues){
        
        FleissKappa fk  =new FleissKappa();
        float kappaValue = fk.computeKappa(fk.convertToArray(goldValues, testValues));
        return kappaValue;
    }
    

    public static ArrayList<Integer> extractValues(String answer) {

        try {
            JSONArray jsonArray = new JSONArray(answer);
            int[] values = new int[100];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                int qid = object.getInt("qid");
                int qvalue = convert(object.getString("qvalue"));
                values[qid] = qvalue;
            }
            
            ArrayList<Integer> result = new ArrayList<Integer>();
            for (int i = 0; i < values.length; i++) {
                if(values[i]!=0){
                    result.add(values[i]);
                }
            }
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;

    }

    
    public static int convert(String value){
        
        if("F".equals(value)){
            return 1;
        }else if("P-".equals(value)){
            return 2;
        }else if("P".equals(value)){
            return 3;
        }else if("P+".equals(value)){
            return 4;
        }
        
        return 0;
    }
    


}
