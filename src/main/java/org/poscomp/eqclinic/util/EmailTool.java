package org.poscomp.eqclinic.util;

import java.io.File;
import java.net.URLEncoder;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Note;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.PatientWeeklyEmail;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.StudentGroup;
import org.poscomp.eqclinic.domain.Video;
import org.poscomp.survey.domain.Answer;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class EmailTool {

    private static final Logger logger = Logger.getLogger(EmailTool.class);
    
    private static String from = "ospia.unsw@gmail.com";
    private static String password = "medstudent";
    private static String senderName = "OSPIA Administrator";
    private static String reply_to = "csadmin@unsw.edu.au";
    
    public static void sendAptNotSelected(Appointment apt, Doctor doctor, Patient patient) {
        
        String subject = "OSPIA: your following appointment request was not selected";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from, senderName);

            String dateInfo = TimeTool.convertLongToString(apt.getStarttime());
            String[] infos = dateInfo.split(" ");

            String date = infos[0];
            String startTime = infos[1];
            String length = (apt.getEndtime() - apt.getStarttime()) / (1000 * 60) + " minutes";
            long requestTime = apt.getApptime();

            String str = "";
            str = "<html><body><h3>Sorry, as many student request the following appointment, your request was not selected. Feel free to request another request.</h3>";
            
            String to = "";
            
            if(doctor!=null){
                to = doctor.getUsername();
            }
            
            str += "<table rules='all' style='border-color: #666;' cellpadding='10'>";
            str += "<tr style='background: #eee;'><td><strong>SP Name:</strong> </td><td>" + apt.getPatientname() + "</td></tr>";
            str += "<tr><td><strong>Date:</strong> </td><td>" + date + "</td></tr>";
            str += "<tr><td><strong>Start Time</strong> </td><td>" + startTime + " (Sydney Time)</td></tr>";
            str += "<tr><td><strong>Length:</strong> </td><td>" + length + "</td></tr>";
            // str += "<tr><td><strong>Cancel reason:</strong> </td><td>" + cancelReason + "</td></tr>";
            str += "</table>";
            str += "</body></html>";
            
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }
    
    public static void sendCancelApt(Appointment apt, Doctor doctor, Patient patient, String cancelReason, String patientName, String doctorName) {
        
        String subject = "OSPIA: you have an appointment has been cancelled";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String dateInfo = TimeTool.convertLongToString(apt.getStarttime());
            String[] infos = dateInfo.split(" ");

            String date = infos[0];
            String startTime = infos[1];
            String length = (apt.getEndtime() - apt.getStarttime()) / (1000 * 60) + " minutes";
            long requestTime = apt.getApptime();

            String str = "";
            String to = "";
            
            if(doctor!=null){ // cancel by student
                str = "<html><body>Unfortunately, the following OSPIA appointment has been cancelled by the simulated patient. Feel free to request another appointment.</h3>";
                to = doctor.getUsername();
            }
            
            if(patient!=null){
                if(apt.getStarttime()-System.currentTimeMillis()<48*60*60*1000){
                    str = "<html><body><h3>Unfortunately, the following OSPIA appointment has been cancelled by the student. Feel free to return to the website to indicate other times you are available.</h3>"; 
                }else{
                    str = "<html><body><h3>Unfortunately, the following OSPIA appointment has been cancelled by the student. Another student may book this appointment. Please await further updates.</h3>";
                }
                to = patient.getEmail();
            }
            
            str += "<table rules='all' style='border-color: #666;' cellpadding='10'>";
            str += "<tr style='background: #eee;'><td><strong>SP Name:</strong> </td><td>" + patientName + "</td></tr>";
            str += "<tr style='background: #eee;'><td><strong>Student Name:</strong> </td><td>" + doctorName + "</td></tr>";
            str += "<tr><td><strong>Date:</strong> </td><td>" + date + "</td></tr>";
            str += "<tr><td><strong>Start Time</strong> </td><td>" + startTime + " (Sydney Time)</td></tr>";
            str += "<tr><td><strong>Length:</strong> </td><td>" + length + "</td></tr>";
            // str += "<tr><td><strong>Cancel reason:</strong> </td><td>" + cancelReason + "</td></tr>";
            str += "</table>";
            str += "</body></html>";
            
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }
    
    
    public static void sendConfirmToDoctor(Appointment apt, Doctor doctor, int result) {

        String to = doctor.getUsername();
        
        String subject = "";
        
        if(result == 1){
            subject = "OSPIA: you have an appointment has been accepted";
        }else if(result == 0){
            subject = "OSPIA: you have an appointment has been declined";
        }

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String dateInfo = TimeTool.convertLongToString(apt.getStarttime());
            String[] infos = dateInfo.split(" ");

            String date = infos[0];
            String startTime = infos[1];
            String length = (apt.getEndtime() - apt.getStarttime()) / (1000 * 60) + " minutes";
            String studentName = apt.getDoctorname();
//            String scenario = apt.getScenario() + "";

            long requestTime = apt.getApptime();
            long endTime = (48 * 60 * 60 * 100) + requestTime;
           
            // String callBackURL = "http://www.google.com";

            String str = "";
            str = "<html><body>";
            
            if(result == 1){
                str += "<h3>You have an appointment has been accepted.</h3>";
            }else if(result == 0){
                str += "<h3>Unfortunately, the following OSPIA appointment has been declined by the simulated patient. Feel free to request another appointment.</h3>";
            }
            
            
            str += "<table rules='all' style='border-color: #666;' cellpadding='10'>";
            str +=
                            "<tr style='background: #eee;'><td><strong>SP Name:</strong> </td><td>" + apt.getPatientname()
                                            + "</td></tr>";
            // str += "<tr><td><strong>Scenario:</strong> </td><td>" + sce.getCode() + "</td></tr>";
            str += "<tr><td><strong>Date:</strong> </td><td>" + date + "</td></tr>";
            str += "<tr><td><strong>Start Time</strong> </td><td>" + startTime + " (Sydney Time)</td></tr>";
            str += "<tr><td><strong>Length:</strong> </td><td>" + length + "</td></tr>";
            str += "</table>";
            str += "</body></html>";
            
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }
    
    public static void send(Appointment apt, Patient pat, Scenario sce) {

        String to = pat.getEmail();
        String subject = "OSPIA: You have an appointment request from a student";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String dateInfo = TimeTool.convertLongToString(apt.getStarttime());
            String[] infos = dateInfo.split(" ");

            String date = infos[0];
            String startTime = infos[1];
            String length = (apt.getEndtime() - apt.getStarttime()) / (1000 * 60) + " minutes";
            String studentName = apt.getDoctorname();
//            String scenario = apt.getScenario() + "";

//            long requestTime = apt.getApptime();
//            long endTime = (48 * 60 * 60 * 100) + requestTime;
            long curTime = System.currentTimeMillis();
            String patient = URLEncoder.encode(apt.getPatientname(), "utf-8");

            String callBackURL =
                    ConstantValue.serverName+"/patientconfirmaptemail?time=" + curTime + "&aptId="
                                            + apt.getAptid() + "&patient=" + patient;
            // String callBackURL = "http://www.google.com";

            String str = "";
            str = "<html><body>";
            str += "<h3>You have an appointment request from a student.</h3>";
            str += "<table rules='all' style='border-color: #666;' cellpadding='10'>";
            str +="<tr style='background: #eee;'><td><strong>Student Name:</strong> </td><td>" + studentName
                                            + "</td></tr>";
            // str += "<tr><td><strong>Scenario:</strong> </td><td>" + sce.getCode() + "</td></tr>";
            str += "<tr><td><strong>Date:</strong> </td><td>" + date + "</td></tr>";
            str += "<tr><td><strong>Start Time</strong> </td><td>" + startTime + " (Sydney Time)</td></tr>";
            str += "<tr><td><strong>Length:</strong> </td><td>" + length + "</td></tr>";
            str += "</table>";
            str += "</body></html>";
            str += "You can confirm this appointment by clicking <a href='" + callBackURL + "'>here</a>";

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }
    
    public static void sendSecondReminder(Appointment apt, Patient pat) {

        String to = pat.getEmail();
        String subject = "OSPIA: You have an appointment request from a student";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String dateInfo = TimeTool.convertLongToString(apt.getStarttime());
            String[] infos = dateInfo.split(" ");

            String date = infos[0];
            String startTime = infos[1];
            String length = (apt.getEndtime() - apt.getStarttime()) / (1000 * 60) + " minutes";
            String studentName = apt.getDoctorname();
//            String scenario = apt.getScenario() + "";

//            long requestTime = apt.getApptime();
//            long endTime = (48 * 60 * 60 * 100) + requestTime;
            long curTime = System.currentTimeMillis();
            String patient = URLEncoder.encode(apt.getPatientname(), "utf-8");

            String callBackURL =
                    ConstantValue.serverName+"/patientconfirmaptemail?time=" + curTime + "&aptId="
                                            + apt.getAptid() + "&patient=" + patient;
            // String callBackURL = "http://www.google.com";

            String str = "";
            str = "<html><body>";
            
            str += "Thanks for providing the appointment below. A student is now waiting for you to confirm. Thank you for doing so as soon as possible by clicking <a href='" + callBackURL + "'>here</a>.";
            str += "<br>-----------------------------<br>";
            str += "<h3>You have an appointment request from a student.</h3>";
            str += "<table rules='all' style='border-color: #666;' cellpadding='10'>";
            str +="<tr style='background: #eee;'><td><strong>Student Name:</strong> </td><td>" + studentName
                                            + "</td></tr>";
            // str += "<tr><td><strong>Scenario:</strong> </td><td>" + sce.getCode() + "</td></tr>";
            str += "<tr><td><strong>Date:</strong> </td><td>" + date + "</td></tr>";
            str += "<tr><td><strong>Start Time</strong> </td><td>" + startTime + " (Sydney Time)</td></tr>";
            str += "<tr><td><strong>Length:</strong> </td><td>" + length + "</td></tr>";
            str += "</table>";
            str += "</body></html>";
            str += "You can confirm this appointment by clicking <a href='" + callBackURL + "'>here</a>";

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }

    public static void nvbFeedbackReady(Appointment apt, Doctor doctor, Patient pat) {

        String to = doctor.getUsername();
        String subject = "OSPIA: Your OSPIA feedback is ready to be reviewed";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String dateInfo = TimeTool.convertLongToString(apt.getStarttime());
            String[] infos = dateInfo.split(" ");

            String date = infos[0];
            String startTime = infos[1];
            String length = (apt.getEndtime() - apt.getStarttime()) / (1000 * 60) + " minutes";
            String studentName = apt.getDoctorname();
            long curTime = System.currentTimeMillis();
            String patient = URLEncoder.encode(apt.getPatientname(), "utf-8");

            String str = "";
            str = "<html><body>";
            str += "<h3>Dear student,</h3>";
            str += "<p>Your feedback information of following consultation has been generated, and is ready to be reviewed. Please log in your OPSIA account and find the feedback under the 'My Consultations' tab.<p>";
            
            str += "<p>The feedback information includes: the video recroding of the consultation with the comments provided by the simulated patient, the SOCA result assessed by the SP, and a report of your nonverbal behaviour generated by the machine. Due the lighting condition has a great influence on the result of nonverbal detection, and the result may not accurate.<p>";
            
            str += "<p>After reviewing the feedback, please fill out the Reflective Survey (under My Consultations tab). Your feedback is important for us for improving the platform.<p>";
            
            str += "<p>Due to the network connection condition, you consultation may not be properly recorded. We really sorry about that.<p>";
            
            str += "<table rules='all' style='border-color: #666;' cellpadding='10'>";
            str +="<tr style='background: #eee;'><td><strong>Student Name:</strong> </td><td>" + studentName
                                            + "</td></tr>";
            // str += "<tr><td><strong>Scenario:</strong> </td><td>" + sce.getCode() + "</td></tr>";
            str += "<tr><td><strong>Date:</strong> </td><td>" + date + "</td></tr>";
            str += "<tr><td><strong>Start Time</strong> </td><td>" + startTime + "</td></tr>";
            str += "<tr><td><strong>Length:</strong> </td><td>" + length + "</td></tr>";
            str += "</table>";
            str += "</body></html>";

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }
    
    
    public static void nvbFeedbackReadyDiff(Appointment apt, Doctor doctor, Patient pat, int feedbackType, List<Note> notes, Answer answer, int emailLevel, Video video, int emailGroup) {

        if(emailLevel==0){
            return;
        }
        
        String to = doctor.getUsername();
//        String to = "ianliu0420@gmail.com";
        
        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String doctorName = doctor.getFirstname();
            String emailContent = "";
            String subject = "";
            String emailClickingURL = "https://ospia.med.unsw.edu.au/feedback/"+doctor.getDoctorId()+"/"+emailLevel+"/"+emailGroup;
            String emailClickingLink = "<p><a href='"+emailClickingURL+"'>This is the link to OSPIA. </a></p>";
            
            
            String autonomySubject = "Your OSPIA feedback is ready for review";
            String autonomyEmail = "<html><body>Dear "+ doctorName +",<br/>";
            autonomyEmail += "<p>Feedback on your OSPIA video interview with a simulated patient is now available for you to review.</p>";
            autonomyEmail += "<p>We know you must be quite busy, but think this is important. Our research has shown that this feedback helps students reflect and improve their communication during interviews. Just spending a bit of time going over what you did, and reviewing the feedback will probably help you develop this important skill.</p>";
            autonomyEmail += "<p>At your convenience, please log into OSPIA via eMed and find your personalised feedback under the 'My Consultations' tab.</p>";
            autonomyEmail += "<p>After reviewing the feedback, we would be grateful if you could fill out the Reflective Survey. Your feedback is important to us so that we can improve how the OSPIA platform works for you.</p>";
            autonomyEmail += emailClickingLink;
            autonomyEmail += "<br/><p>With regards</p>";
            autonomyEmail += "<p>OSPIA team</p></body></html>";
            
            String controllingSubject = "Login now to review your personalised OSPIA feedback!";
            String controllingEmail = "<html><body>Dear "+ doctorName +",<br/>";
            controllingEmail += "<p>Feedback on your OSPIA video interview with a simulated patient is now ready and awaiting your review. You will need to access it within a week. You should spend at least 5 minutes on it, and when you are done complete the reflective survey.</p>";
            controllingEmail += "<p>Log into OSPIA via eMed and find your individual feedback under the 'My Consultations' tab.</p>";
            controllingEmail += "<p>After reviewing the feedback, you need to fill out the Reflective Survey.</p>";
            controllingEmail += emailClickingLink;
            controllingEmail += "<br/><p>With regards</p>";
            controllingEmail += "<p>OSPIA team</p></body></html>";
            
            String controlSubject = "Your OSPIA feedback is ready for review.";
            String controlEmail = "<html><body>Dear "+ doctorName +",<br/>";
            controlEmail += "<p>Your feedback of the OSPIA video interview with a simulated patient is ready to review.</p>";
            controlEmail += "<p>Please log into OSPIA via eMed and find your individual feedback under the 'My Consultations' tab.</p>";
            controlEmail +="<p>After reviewing the feedback, please fill out the Reflective Survey. Your feedback is important to us so that we can improve how the OSPIA platform works for you.</p>";
            controlEmail += emailClickingLink;
            controlEmail += "<br/><p>With regards</p>";
            controlEmail += "<p>OSPIA team</p></body></html>";
            
            if(emailLevel==1){
                if(emailGroup == 1){
                    emailContent = autonomyEmail;
                    subject = autonomySubject;
                }else if(emailGroup == 2){
                    emailContent = controllingEmail;
                    subject = controllingSubject;
                }
            }else if(emailLevel==2){
                if(emailGroup == 1){
                    emailContent = autonomyEmail;
                    subject = autonomySubject;
                }else if(emailGroup == 2){
                    emailContent = controllingEmail;
                    subject = controllingSubject;
                }
            }else if(emailLevel==3){
                if(emailGroup == 1){
                    emailContent = autonomyEmail;
                    subject = autonomySubject;
                }else if(emailGroup == 2){
                    emailContent = controllingEmail;
                    subject = controllingSubject;
                }
            }
            
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(emailContent, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
    
    
    
    
    public static void sendNotificationToDoctor(Appointment apt, Doctor doctor, Patient pat) {

        String to = doctor.getUsername();
    	// String to = "chunfeng.liu@sydney.edu.au";
        String subject = "OSPIA: you will have an appointment today";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String dateInfo = TimeTool.convertLongToString(apt.getStarttime());
            String[] infos = dateInfo.split(" ");

            String date = infos[0];
            String startTime = infos[1];
            String length = (apt.getEndtime() - apt.getStarttime()) / (1000 * 60) + " minutes";
            String spName = apt.getPatientname();
            long curTime = System.currentTimeMillis();
            String patient = URLEncoder.encode(apt.getPatientname(), "utf-8");


            String str = "";
            str = "<html><body>";
            str += "<h3>Dear student,</h3>";
            str += "<p>Thanks very much for your participating. You will have an appointment today:<p>";
            
            str += "<table rules='all' style='border-color: #666;' cellpadding='10'>";
            str +="<tr style='background: #eee;'><td><strong>SP Name:</strong> </td><td>" + spName
                                            + "</td></tr>";
            // str += "<tr><td><strong>Scenario:</strong> </td><td>" + sce.getCode() + "</td></tr>";
            str += "<tr><td><strong>Date:</strong> </td><td>" + date + "</td></tr>";
            str += "<tr><td><strong>Start Time</strong> </td><td>" + startTime + " (Sydney Time)</td></tr>";
            str += "<tr><td><strong>Length:</strong> </td><td>" + length + "</td></tr>";
            str += "</table>";
            str += "</body></html>";

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }
    
    
    public static void sendNotificationToPatient(Appointment apt, Doctor doctor, Patient pat) {

        String to = pat.getUsername();
        String subject = "OSPIA: you will have an appointment today";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String dateInfo = TimeTool.convertLongToString(apt.getStarttime());
            String[] infos = dateInfo.split(" ");

            String date = infos[0];
            String startTime = infos[1];
            String length = (apt.getEndtime() - apt.getStarttime()) / (1000 * 60) + " minutes";
            String studentName = apt.getDoctorname();
            long curTime = System.currentTimeMillis();
            String patient = URLEncoder.encode(apt.getPatientname(), "utf-8");

            String str = "";
            str = "<html><body>";
            str += "<h3>Dear simulated patient,</h3>";
            str += "<p>Thanks very much for your participating. You will have an appointment today:<p>";
            
            str += "<table rules='all' style='border-color: #666;' cellpadding='10'>";
            str +="<tr style='background: #eee;'><td><strong>Student Name:</strong> </td><td>" + studentName
                                            + "</td></tr>";
            // str += "<tr><td><strong>Scenario:</strong> </td><td>" + sce.getCode() + "</td></tr>";
            str += "<tr><td><strong>Date:</strong> </td><td>" + date + "</td></tr>";
            str += "<tr><td><strong>Start Time</strong> </td><td>" + startTime + " (Sydney Time)</td></tr>";
            str += "<tr><td><strong>Length:</strong> </td><td>" + length + "</td></tr>";
            str += "</table>";
            str += "</body></html>";

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }
    
    
    public static void sendUnparticipatedStudentNotification(List<StudentGroup> students, int availableApts) {

    	String to = "";
    	
    	if(students!=null){
    		for (int i = 0; i < students.size(); i++) {
        		if(i==0){
        			to += students.get(i).getEmail();
        		}else{
        			to += "," + students.get(i).getEmail();
        		}
    		}
    	}else{
    		
    	}
    	
    	to="ianliu0420@gmail.com,chunfeng.liu@sydney.edu.au,cliu0474@uni.sydney.edu.au";
    	
    	
    	String[] recipientList = to.split(",");
    	InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];

    	for (int i = 0; i < recipientAddress.length; i++) {
    		try {
				recipientAddress[i] = new InternetAddress(recipientList[i].trim());
			} catch (AddressException e) {
				e.printStackTrace();
			}
		}
    	
        String subject = "OSPIA alert";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String str = "";
            str = "<html><body>";
            str += "<h3>Dear students,</h3>";
            str += "<p>OSPIA alert: there are currently "+availableApts+" SP appointments available this week. Go to OSPIA on the eMed quicklinks <a href='https://emed.med.unsw.edu.au'>https://emed.med.unsw.edu.au</a> to access your calendar and secure an interview.<p>";
            
            str += "<br/><p>Thanks!</p>";
            str += "<p>The Clinical Skills team</p>";
            str += "<p>csadmin@unsw.edu.au</p></body></html>";

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            // message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.addRecipients(Message.RecipientType.TO, recipientAddress);
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }
    
    
    public static void sendThankEmailToSP(Patient pat) {

        String to = pat.getUsername();
        // String to = "chunfeng.liu@sydney.edu.au";
        String subject = "OSPIA: Thanks for your participation last week";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String str = "";
            str = "<html><body>";
            str += "<h3>Dear simulated patient,</h3>";
            str += "<p>Thank you very much for participating in OSPIA appointments this week. Feedback from students tells us that they value the opportunity to interact with you, and the feedback and assessment you give. <p>";
            str += "<p>If you have a moment, please <a href='https://ospia.med.unsw.edu.au/sp/cal_sp'>make more appointments now </a>! Otherwise we look forward to seeing you back on this site soon.</p>";
            str += "<br/><p>With best wishes.</p>";
            str += "<p>OSPIA team</p></body></html>";
            
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } 

    }
    
    public static void sendThankEmailToSPFeedback_period1(Patient pat, int emailId) {

        String to = pat.getUsername();
        // String to = "chunfeng.liu@sydney.edu.au";
        String subject = "OSPIA Week 9: Thanks for your participation";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String str = "";
            str = "<html><body>";
            str += "Dear "+pat.getFirstname()+",";
            str += "<p>Thank you very much for participating in OSPIA appointments last week. Feedback from students tells us that they value the opportunity to interact with you, and the feedback and assessment you give. <p>";
            str += "<p>Your feedback is very valuable to us. Please click on the following button to take a short 1-2 minute survey to help us improve your experience.</p>";
            
            String emailClickingURL = "https://ospia.med.unsw.edu.au/patientEmailClick/"+emailId;
            String buttonStyle="style='background-color: #4CAF50;border: none;color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;'";
            String emailClickingLink = "<a href='"+emailClickingURL+"'><button "+buttonStyle+">Take the survey</button></a>";
            
            str += emailClickingLink;
            str += "<br/><p>With best wishes.</p>";
            str += "<p>OSPIA team</p></body></html>";
            
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } 

    }
    
    public static void sendThankEmailToSPFeedback(Patient pat, int emailId) {

        String to = pat.getUsername();
        // String to = "chunfeng.liu@sydney.edu.au";
        String subject = "OSPIA Week 10: Thanks for your participation";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String str = "";
            str = "<html><body>";
            str += "Dear "+pat.getFirstname()+",";
            str += "<p>Thank you very much for participating in OSPIA appointments last week. Feedback from students tells us that they value the opportunity to interact with you, and the feedback and assessment you give. <p>";
            str += "<p>You have received personal message/s from the student/s who interacted with you during last week's OSPIA session. Please note that leaving personal messages is optional for the students, so you will receive messages from the students who chose to do so after the OSPIA session.</p>";
            str += "<p>Please click the following link to see the personal message/s, followed by a short survey.</p>";
            
            String emailClickingURL = "https://ospia.med.unsw.edu.au/patientEmailClickWithPersonalFeedback/"+emailId;
            String buttonStyle="style='background-color: #4CAF50;border: none;color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;'";
            String emailClickingLink = "<a href='"+emailClickingURL+"'><button "+buttonStyle+">Take the survey</button></a>";
            
            str += emailClickingLink;
            str += "<br/><p>With best wishes.</p>";
            str += "<p>OSPIA team</p></body></html>";
            
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } 

    }
    
    
    public static void sendweeklyReport(String weeklyReportName) {
        
        String to = "ianliu0420@gmail.com,csadmin@unsw.edu.au,silas.taylor@unsw.edu.au";
        
        String[] recipientList = to.split(",");
        InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];

        for (int i = 0; i < recipientAddress.length; i++) {
            try {
                recipientAddress[i] = new InternetAddress(recipientList[i].trim());
            } catch (AddressException e) {
                e.printStackTrace();
            }
        }
        
        String subject = "Weekly report";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);
            
            String str = "";
            str = "Hi Silas and Kiran\n";
            str += "\t  Attached the weekly report.\n";
            str += "Cheers\n";
            str += "Ian";
            
            Message message = new MimeMessage(session);
            message.setSubject("Weekly report");
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText(str);
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            String filename = weeklyReportName;
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            String[] names = filename.split("/");
            messageBodyPart.setFileName(names[2]);
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.setContent(multipart);
            message.addRecipients(Message.RecipientType.TO, recipientAddress);
            message.setReplyTo(InternetAddress.parse("chunfeng.liu@sydney.edu.au"));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } 

    }

    public static void sendResetPwdToSP(Patient pat) {

        String to = pat.getUsername();
        String subject = "OSPIA: Resetting Password";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        Transport transport;
        try {
            transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from,senderName);

            String str = "";
            str = "<html><body>";
            str += "<h3>Dear simulated patient,</h3>";
            str += "<p>Thank you very much for participating in OSPIA program. <p>";
            str += "<p>Please click <a href='https://ospia.med.unsw.edu.au/sp/resetNewPwd/"+pat.getPatientid()+"'>this link</a> to process your password resetting! </p>";
            str += "<br/><p>With best wishes.</p>";
            str += "<p>OSPIA team</p></body></html>";
            
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(str, "text/html;charset=utf-8");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(addressFrom);
            message.setSubject(subject);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setContent(mp);
            message.setReplyTo(InternetAddress.parse(reply_to));

            transport.connect();
            Transport.send(message);
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } 

    }
    
    
    class ReaderXML {
        public String read(String path) {
            String str = null;
            str = reader(path);
            return str;
        }

        public String reader(String path) {
            SAXReader reader = new SAXReader();
            String str = null;
            try {
                Document doc = reader.read(new File(path));
                Element ele = doc.getRootElement();
                Element htmlEle = ele.element("html");
                str = htmlEle.asXML();
            } catch (DocumentException e) {
                logger.error(e.getMessage(), e);
            }
            return str;
        }
    }

}
