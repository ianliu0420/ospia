package org.poscomp.eqclinic.util;

import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityTool {

    
    public static Object getActiveUser(){
        
        MyUserPrincipal activeUser = (MyUserPrincipal)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        if(obj instanceof String){
            return null;
        }
        
        if(activeUser.getPatient()!=null){
            return activeUser.getPatient();
        } else if (activeUser.getDoctor()!=null){
            return activeUser.getDoctor();
        }else if(activeUser.getTeacher()!=null){
            return activeUser.getTeacher();
        }else{
            return null;
        }
    }
    
    
}
