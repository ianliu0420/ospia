package org.poscomp.eqclinic.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class TimeTool {

    private static final Logger logger = Logger.getLogger(TimeTool.class);
    
    public static long convertStringToTime(String time) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parsedDate;
        try {
            parsedDate = dateFormat.parse(time);
            Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
            return timestamp.getTime();
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
        return 0;
    }

    public static Date convertStringToDate(String time) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {

            Date date = formatter.parse(time);
            return date;
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }

        return null;
    }

    public static String convertLongToString(long time) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String reportDate = df.format(time);
        return reportDate;
    }
    
    public static String convertLongToString2(long time) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String reportDate = df.format(time);
        return reportDate;
    }
    
    public static String convertToAussieStyle(String interStyle){
        
        SimpleDateFormat formatterInter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatterAussie = new SimpleDateFormat("dd/MM/yyyy");
        String result ="";
        try {

            Date date = formatterInter.parse(interStyle);
            result = formatterAussie.format(date);
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }
 
    public static void main(String[] args){
        
        System.out.println(TimeTool.convertToAussieStyle("2016-05-10"));
    
    
    }
    
}

