package org.poscomp.eqclinic.weka;

import java.io.File;
import java.util.ArrayList;

import weka.classifiers.Classifier;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class WekaTool {

    public static ArrayList<ArrayList<Float>> J48NonNeutralClassfication(String trainingFile, String testingFile,
                    ArrayList<Integer> nonneuNo, float frameRate) throws Exception {

        // the return value store all emotions (6 categories)
        // 1-anger 2-contempt 3-disgust 4-fear 5-happy 6-sadness 7-surprise
        ArrayList<ArrayList<Float>> returnValue = new ArrayList<ArrayList<Float>>();
        for (int j = 0; j < 7; j++) {
            ArrayList<Float> emotions = new ArrayList<Float>();
            returnValue.add(emotions);
        }

        Classifier classifier = new J48();
        // load training data
        File inputFile = new File(trainingFile);
        ArffLoader atf = new ArffLoader();
        atf.setFile(inputFile);
        Instances instancesTrain = atf.getDataSet();

        // load the testing data
        inputFile = new File(testingFile);
        atf.setFile(inputFile);
        Instances instancesTest = atf.getDataSet();

        // set the last column as the classification column, the first column is
        // column 0
        instancesTest.setClassIndex(instancesTest.numAttributes() - 1);

        double sum = instancesTest.numInstances();
        instancesTrain.setClassIndex(instancesTrain.numAttributes() - 1);
        classifier.buildClassifier(instancesTrain);

        for (int i = 0; i < nonneuNo.size(); i++) {

            int lineNo = nonneuNo.get(i);

            // here the classValue is represent the index of class
            double predictValue = classifier.classifyInstance(instancesTest.instance(lineNo));
            double realValue = instancesTest.instance(lineNo).classValue();

            // String temp = lineNo/frameRate + ":" + predictValue;

            int predictValueInt = (int) predictValue;
            returnValue.get(predictValueInt).add(lineNo / frameRate);

        }
        return returnValue;

    }

    public static ArrayList<Integer> J48NeutralClassfication(String trainingFile, String testingFile) throws Exception {
        // the return value store all the non-neutral frame number
        ArrayList<Integer> returnValue = new ArrayList<Integer>();

        Classifier classifier = new J48();
        // load training data
        File inputFile = new File(trainingFile);
        ArffLoader atf = new ArffLoader();
        atf.setFile(inputFile);
        Instances instancesTrain = atf.getDataSet();

        // load the testing data
        inputFile = new File(testingFile);
        atf.setFile(inputFile);
        Instances instancesTest = atf.getDataSet();

        // set the last column as the classification column, the first column is
        // column 0
        instancesTest.setClassIndex(instancesTest.numAttributes() - 1);

        double sum = instancesTest.numInstances();
        instancesTrain.setClassIndex(instancesTrain.numAttributes() - 1);
        classifier.buildClassifier(instancesTrain);

        for (int i = 0; i < sum; i++) {
            // here the classValue is represent the index of class
            double predictValue = classifier.classifyInstance(instancesTest.instance(i));
            double realValue = instancesTest.instance(i).classValue();

            if (predictValue != realValue) {
                returnValue.add(i);
            }
        }

        return returnValue;
    }

}
