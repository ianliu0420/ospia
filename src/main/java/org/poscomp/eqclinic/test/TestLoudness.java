package org.poscomp.eqclinic.test;

import loudness.Loudness;

import com.mathworks.toolbox.javabuilder.MWArray;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class TestLoudness {

    public static void main(String[] args) {

        Object[] result = null;
        Loudness loud = null;

        try {
            loud = new Loudness();
            result = loud.loudness(2, "C:/videos/video_1436761313842_doctor1.wav");
            MWNumericArray temp = (MWNumericArray) result[0];
            float[][] weights = (float[][]) temp.toFloatArray();
            System.out.println(weights[0].length);
            loud.dispose();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MWArray.disposeArray(result);
        }

    }

}
