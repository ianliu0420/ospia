<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
</head>

<body>

	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div class="jumbotron">
							<video id="myvideo" width="480" height="360" controls> <source
								src="<%=request.getContextPath()%>/video/introduction.mp4"
								type="video/mp4"></video>
						</div>

						<div class="jumbotron" style="padding-top:2px;padding-bottom:2px;">
							<h2>OSPIA For Medical Students</h2>

							<br>
							<p class="lead" style=" text-align:justify">Finding patients
								to practice medical communication skills can be tough. The OSPIA
								has a clever booking system to connect students with simulated
								patients. Use the calendar system to book and confirm a
								consultation and practice your communication skills online using
								video conferencing. What's more, get real time feedback from
								your simulated patient, use the video capture to review your
								consultation, and reflect on your communication skills with the
								addition of non-verbal behavior analysis.</p>
								
							<hr/>
                            <div>
                                 <div style="text-align:left; margin-top:30px;">
                                 Dr Renee Lim was the Educational Designer on the OSPIA Project. I am very grateful for Renee's hard work and skills in bringing this project to fruition, not least in brokering the collaboration with University of Sydney's Prof Rafael Calvo and his excellent PhD student Mr Chunfeng Liu, without whom major elements of this website would not have been possible.
                                 </div>
                                 <div style="text-align:right; font-style:italic;">
                                 Dr Silas Taylor, Convenor of Clinical Skills, UNSW Medicine
                                 </div>
                                 <br/>
                                 <div>
                                 <img style="width:350px;height:100px;" src="<%=request.getContextPath()%>/img/gvn_logo.png"><br>
                                 This project received funding from the Australian Government. 
                                 </div>
                            </div>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
