<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Consultations</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/stu/30_Stu_Consultations.js"></script>

</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						
						<input type="hidden" id="agreement" value="${agreement}" />
						
						<h3>Previous Consultations</h3>

						<c:if test="${not empty videoList}">
							<table class="table">
								<thead>
									<tr>
										<th>Date</th>
										<th>Time</th>
										<!-- <th>Scenario</th> -->
										<th>SP Name</th>
										<th>Type</th>
										<th>Review required</th>
										<th>Review complete</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="i" begin="0" end="${videoList.size()-1}">
										<c:set var="video" scope="request"
											value="${videoList.get(i)[0]}" />
										<c:set var="appointmentProgress" scope="request"
											value="${videoList.get(i)[1]}" />
										<c:set var="appointment" scope="request"
											value="${videoList.get(i)[2]}" />
										<tr>
											<td><c:set var="values"
													value="${fn:split(video.getAptDate(), '-')}" />
												${values[2] }/${values[1] }/${values[0] }</td>
											<td>${video.getAptTime()}</td>
											<!-- <td>${video.getSceCode()}</td> -->
											<td>${video.getPatientName()}</td>
											<c:if test="${appointment.getSessionType()==1}">
												<td>Practice</td>
											</c:if>
											<c:if test="${appointment.getSessionType()==2}">
												<td>Assessment</td>
											</c:if>
											<c:if test="${appointment.getSessionType()==0}">
												<td>&nbsp;</td>
											</c:if>
											<c:if test="${appointmentProgress.getHasFinish()==0 }">
												<td><a
													href="<%=request.getContextPath()%>/stu/resumedoctor/${appointmentProgress.getId()}">Continue
														to finish surveys</a></td>
												<td>&nbsp;</td>
											</c:if>


											<c:if test="${appointmentProgress.getHasFinish()==1 }">

												<c:if test="${video.allowFeedback!=1}">
													<td>Generating feedback, please wait...</td>
													<td>&nbsp;</td>
												</c:if>

												<c:if test="${video.allowFeedback==1}">
													<td><c:if
															test="${video.reviewComments==null || video.reviewComments==0}">
															<a target="_blank"
																onclick="reviewBtnClick(1,${video.id}, '<%=request.getContextPath()%>/stu/loadsessionnote/${video.getArchiveId()}')"><button
																	class="btn loginBtn">SP Comments</button></a>
														</c:if> <c:if
															test="${video.reviewSoca==null || video.reviewSoca==0}">
															<a target="_blank"
																onclick="reviewBtnClick(2,${video.id}, '<%=request.getContextPath()%>/stu/refsoca_stu/${video.getSessionId()}')"><button
																	class="btn loginBtn">SOCA Result</button></a>
														</c:if> 
														
														<c:if
															test="${(video.reviewBehav==null || video.reviewBehav==0) && video.allowFeedback==1}">
															<a target="_blank"
																onclick="reviewBtnClick(3,${video.id},'<%=request.getContextPath()%>/stu/vdetail/${video.getName() }&${video.getArchiveId()}')">
																<button name="unreviewedBehavBtn" class="btn loginBtn">Behaviours</button></a>
														</c:if> <c:if
															test="${(video.reviewSurvey==null || video.reviewSurvey==0) && video.allowFeedback==1}">
															<a target="_blank"
																onclick="reviewBtnClick(4,${video.id},'<%=request.getContextPath()%>/stu/nonverbalfeedback_stu/${video.getSessionId() }')"><button
																	class="btn loginBtn">Reflect Survey</button></a>
														</c:if> <c:if
															test="${(video.reviewTutorSoca==null || video.reviewTutorSoca==0) && video.allowTutorNotes==1}">
															<a target="_blank"
																onclick="reviewBtnClick(5,${video.id},'<%=request.getContextPath()%>/stu/review_tutor_soca/${video.getSessionId() }')"><button
																	class="btn loginBtn">Tutor SOCA</button></a>
														</c:if></td>

													<td><c:if test="${video.reviewComments==1}">
															<a target="_blank"
																href="<%=request.getContextPath()%>/stu/loadsessionnote/${video.getArchiveId()}"><button
																	class="btn loginBtn">SP Comments</button></a>
														</c:if> <c:if test="${video.reviewSoca==1}">
															<a target="_blank"
																href="<%=request.getContextPath()%>/stu/refsoca_stu/${video.getSessionId()}"><button
																	class="btn loginBtn">SOCA Result</button></a>
														</c:if> <c:if
															test="${video.reviewBehav==1 && video.allowFeedback==1}">
															<a target="_blank"
																href="<%=request.getContextPath()%>/stu/vdetail/${video.getName() }&${video.getArchiveId()}"><button
																	name="reviewedBehavBtn" class="btn loginBtn">Behaviours</button></a>

														</c:if> <c:if
															test="${video.reviewSurvey==1 && video.allowFeedback==1}">
															<a target="_blank"
																href="<%=request.getContextPath()%>/stu/nonverbalfeedback_stu/${video.getSessionId() }"><button
																	class="btn loginBtn">Reflect Survey</button></a>
														</c:if> <c:if
															test="${video.reviewTutorSoca==1 && video.allowTutorNotes==1}">
															<a target="_blank"
																href="<%=request.getContextPath()%>/stu/review_tutor_soca/${video.getSessionId() }"><button
																	class="btn loginBtn">Tutor SOCA</button></a>
														</c:if></td>
												</c:if>

											</c:if>
											<!-- video_1436761313842 -->
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>

						<c:if test="${empty videoList}">
							<p>Currently, you have no consultation.</p>
						</c:if>

					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
