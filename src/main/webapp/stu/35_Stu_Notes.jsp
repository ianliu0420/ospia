<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Comment Review</title>
<c:import url="/common.jsp"></c:import>
<script src='<%=request.getContextPath()%>/js/jsapi.js'></script>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/timeline.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/timeline_cs.css">
<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/css/vtimeline.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/timeline.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/stu/35_Stu_Notes.js"></script>

</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<header class="page-header">
							<h5>Comments from SP/tutor</h5>
						</header>

						<div class="container">
							<input type="hidden" value="${archiveId}" id="archiveId"> 
							<input type="hidden" value="${sessionId}" id="sessionId"> 
							<input type="hidden" value="${videoLength}" id="videoLength">
						    <input type="hidden" value="${apt.evaluationImage}" id="evaluationImage">

							<div class="row">

								<div class="col-lg-12 col-xs-12" style="text-align: center">
									<video id="myvideo" controls style="width: 500px;">
										<source src="<%=request.getContextPath()%>/video/${videoName }/${videoName }.mp4" type="video/mp4">
									</video>
								</div>
								<h3>Comments:</h3>
								<div id="mytimeline"></div>
								
								<br/>
								<h5>Notes</h5>
								<div id="notes">
								</div>
							</div>

							<br> <br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
