<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student SOCA Result</title>
<link rel="icon" href="../../favicon.ico">
<title>SP_login</title>
<c:import url="/common.jsp"></c:import>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<link href="<%=request.getContextPath()%>/css/wufoo/form.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/wufoo/theme.css" rel="stylesheet">

<script type="text/javascript" src="<%=request.getContextPath()%>/js/survey/loadanswer.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/stu/26_Stu_StartApt_result_second_SOCA.js"></script>
</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
                        <h4 style="color: red">This is the SP-completed SOCA form for your interaction. Please review, click Submit, then proceed to your Reflection on the next page.</h4>
                        <br/>
                        <form id="Survey_SOCA" name="Survey_SOCA" action="spSOCA_submit">
							<input id="sessionId" value="${doctorSessionIdCombined}" type="hidden"> <input type="hidden" value="${answerId}"
								id="answerId"> <input type="hidden" value="${nextpage}" id="nextpage">
							<header class="info">
								<h1 id="surveyTitle"></h1>
							</header>
							<ul id="questions">
							</ul>
						</form>

					</div>

					<div id="waitForSOCAInfo" style="display:none">
						<table class="table borderless" style="border:0">
							<tr>
								<td colspan="2">
									<h4>The simulated patient hasn't finished the SOCA form, please wait for several minutes and refresh this page.</h4>
								</td>
							</tr>
							<tr>
								<td colspan="2"><h4>Thank you for your cooperation.</h4></td>
							</tr>
						</table>
					</div>
					
					<!-- 
					<div id="SOCANotify" style="display:none">
                        <table class="table borderless" style="border:0">
                            <tr>
                                <td colspan="2">
                                    <h4>This is the completed assessment form on your interaction. Please review, click Submit, then proceed to your Reflection on the next page.</h4>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" id="SOCANotifyOKBtn"><h4>OK</h4></td>
                            </tr>
                        </table>
                    </div>
					 -->
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
