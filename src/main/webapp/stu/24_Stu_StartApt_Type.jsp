<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Recording Type</title>
<c:import url="/common.jsp"></c:import>
<link href="<%=request.getContextPath()%>/css/wufoo/form.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/wufoo/theme.css" rel="stylesheet">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/stu/24_Stu_StartApt_Type.js"></script>
</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<br /> <br />

						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-md-4 control-label" style="width:400px" for="radios"> Choose the purpose of this consultation: &nbsp;&nbsp;&nbsp;</label>
								<div class="col-md-7">
									<label class="radio-inline" for="radios-0" style="text-align:left;"> <input type="radio" name="sessionType"
										value="1" checked="checked"> As Practice
									</label> <label class="radio-inline" for="radios-1" style="text-align:left;"> <input type="radio" name="sessionType"
										value="2"> For Assessment
									</label>

								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label" style="width:380px" for="singlebutton"></label>
								<div class="col-md-2" style="text-align:left;">
									<input id="saveForm" name="singlebutton" type="button" class="usyd-ui-button-primary" value="Next">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
