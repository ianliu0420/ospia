<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Baseline</title>
<c:import url="/common.jsp"></c:import>
<!-- <script src='<%=request.getContextPath()%>/js/opentok.min.js'></script> -->
<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
<script src="<%=request.getContextPath()%>/js/stu/21_Stu_StartApt_Init.js"></script>
<script type="text/javascript">
	var doctorSessionIdBaseline = "${doctorSessionIdBaseline}";
	var doctorApiKeyBaseline = "${doctorApiKeyBaseline}";
	var doctorTokenBaseline = "${doctorTokenBaseline}";
</script>
</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<input id="doctorArchiveIdBaseline" value="" type="hidden">
						<div style="height:750px;">
							<div class="col-lg-12">
                                <h2>This is a sound check page.</h2>
                                <p>The system will make a recording which is not a part of your evaluation and will not be seen by others. Please always accept requests to share your camera and microphone.</p>
                                <p>Until the following task is completed, you will not see the simulated patient.</p> 
                                <p><b>Task: Please use several sentences to describe the weather today.</b></p>
                                <p>When you are ready, please click the "Start" button below, speak, and when finished please click the "Stop" button.</p>
                                <div id="baselineContainer"></div>
								<br> <input type="button" id="startBaseline" class="usyd-ui-button-primary" value="Start" disabled='disabled'/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
