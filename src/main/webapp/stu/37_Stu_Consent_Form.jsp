<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Thanks</title>
<link rel="icon" href="../../favicon.ico">
<title>SP_login</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/stu/37_Stu_Consent_Form.js"></script>
</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<div class="jumbotron" style="padding-left:20%;padding-right:20%;">
							<h2>Research Consent Form</h2>
							<input type="hidden" id="doctorId" value='<sec:authentication property="principal.doctor.doctorId"/>' /> <input
								type="hidden" id="agreement" value='<sec:authentication property="principal.doctor.agreement"/>' /> <input type="hidden"
								id="homeloaded" value="${homeloaded}" />

							<table class="table borderless " style="border:0;">
								<tr>
									<td colspan="2" style="text-align: left;">1. Your video conversation will be recorded.</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: left;">2. Some of your non-verbal behaviours will be analyzed by the machine.</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: left;">3. Your data will be used for research purpose only, and will be only
										accessed by the researchers.</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: left;">4. The system will generate a non-verbal feedback report for you, and only
										you can access that.</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
