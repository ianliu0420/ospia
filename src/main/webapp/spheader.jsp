<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
</head>

<body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-302000095 node-type-page">
	<div id="site-header-bg">
		<div id="wrapper-t">
			<div id="header">
				<div class="logo">
					<a href="http://www.unsw.edu.au/" title="The University of New South Wales"><img
						src="<%=request.getContextPath()%>/img/unsw_logo_old.png" alt="The University of New South Wales"
						title="The University of New South Wales"></a>
				</div>
				<div id="top-link-wrapper">
					<div class="top-link-container">
						<div class="region region-menu-top-right">
							<div id="block-system-user-menu" class="block block-system block-menu clearfix">


								<!--
								<div class="content">
									<ul class="menu">
										<li class="first leaf"><a href="https://emed.med.unsw.edu.au/" title="">eMed</a></li>
										<li class="leaf"><a href="http://my.unsw.edu.au/" title="">myUNSW</a></li>
										<li class="last leaf"><a href="https://med.unsw.edu.au/find-a-person" title="Find a person">Find a person</a></li>
										 <li class="last leaf"><a href="https://med.unsw.edu.au/contact-us" title="">Contact us</a></li>
									 
									</ul>
								</div>
								-->
							</div>
						</div>
					</div>
				</div>

				<div class="logo-top-left">
					<a href="https://med.unsw.edu.au/">Medicine</a>
				</div>

				<div id="main_search">
					<div class="region region-main-search">
						<div id="block-search-form" class="block block-search clearfix">
							<div class="content">
							<span id="currentTime" style="margin-right:5px;"></span>
							
							<sec:authorize access="!hasRole('ROLE_PATIENT')">
	                            <button class="btn loginBtn" onclick="location.href='<%=request.getContextPath()%>/sp/ulogin_sp'">Log in</button>
	                        </sec:authorize>
	
	                        <sec:authorize access="hasRole('ROLE_PATIENT')">
	                            <button class="btn loginBtn" onclick="location.href='<%=request.getContextPath()%>/sp/logoff'">Log out</button>
	                        </sec:authorize>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div id="bodyWrapper">
		<div id="wrapper">
			<div id="main-container">
				<div class="main-menu sticky-enabled">
					<div class="region region-top-nav">
						<div id="block-megamenu-0" class="block block-megamenu clearfix">
							<div class="content">
								<ul id="megamenu-main-menu" class="megamenu-menu columnar megamenu-skin-mytheme">

									<c:set var="redpage" scope="page" value="${redpage}" />
									<sec:authorize access="!hasRole('ROLE_PATIENT')">

										<li class='megamenu-parent'><h2 class="megamenu-parent-title">
												<a href="<%=request.getContextPath()%>/sp/uhome_sp" <c:if test="${redpage eq 'uhome_sp'}"> class="active" </c:if>>Information</a>
											</h2></li>

										<li class='megamenu-parent'><h2 class="megamenu-parent-title">
												<a href="<%=request.getContextPath()%>/sp/uregister_sp"
													<c:if test="${redpage eq 'uregister_sp'}"> class="active" </c:if>>Registration</a>
											</h2></li>
											
										<li class='megamenu-parent'><h2 class="megamenu-parent-title">
                                                <a href="<%=request.getContextPath()%>/sp/ulogin_sp"
                                                    <c:if test="${redpage eq 'ulogin_sp'}"> class="active" </c:if>>Log in</a>
                                            </h2></li>
									</sec:authorize>

									<sec:authorize access="hasRole('ROLE_PATIENT')">
										<li class='megamenu-parent'><h2 class="megamenu-parent-title">
												<a href="<%=request.getContextPath()%>/sp/home_sp" <c:if test="${redpage eq 'home_sp'}"> class="active" </c:if>>Home</a>
											</h2></li>

										<li class='megamenu-parent'><h2 class="megamenu-parent-title">
												<a href="<%=request.getContextPath()%>/sp/cal_sp"
													<c:if test="${redpage eq 'cal_sp'}"> class="active" </c:if>>My Calendar</a>
											</h2></li>
										<li class='megamenu-parent'><h2 class="megamenu-parent-title">
												<a href="<%=request.getContextPath()%>/sp/train_sp" <c:if test="${redpage eq 'train_sp'}"> class="active" </c:if>>SP Training</a>
											</h2></li>
										<li class='megamenu-parent'><h2 class="megamenu-parent-title">
                                                <a target="_tab" href="<%=request.getContextPath()%>/files/FAQs_sp.pdf" <c:if test="${redpage eq 'faq_sp'}"> class="active" </c:if>>FAQs</a>
                                            </h2></li>
                                        <!-- 
										<li class='megamenu-parent'><h2 class="megamenu-parent-title">
												<a href="<%=request.getContextPath()%>/sp/info_sp"
													<c:if test="${redpage eq 'info_sp'}"> class="active" </c:if>>Program Info</a>
											</h2></li>
										 -->
									</sec:authorize>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
