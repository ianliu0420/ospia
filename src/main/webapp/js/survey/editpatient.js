$(document).ready(function() {
	validate();
});

function submitForm(){
	var patientId = $("#patientId").val();
	var health = $("#health").val();
	var title = $("#title").val();
	var firstname = $.trim($("#firstname").val());
	var lastname = $.trim($("#lastname").val());
	var gender = parseInt($('input[name=gender]:checked').val());
	var phone = $("#phone").val();
	var email = $("#email").val();
	
	var street = $("#street").val();
	var suburb = $("#suburb").val();
	var postcode = $("#postcode").val();
	var language = $("#language").val();
	var reason = $("#reason").val();
	var ageRange = $("#agerange").val();
	var notes = $("#notes").val();
		
	var formdata = {
		patientid : patientId,
		health : health,
		title : title,
		firstname : firstname,
		lastname : lastname,
		username : email,
		gender : gender,
		phone : phone,
		email : email,
		street:street,
		suburb:suburb,
		postcode:postcode,
		language:language,
		reason:reason,
		age:ageRange,
		notes:notes
	};

	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+SURVEYPATH+'/editpatient',
		contentType : 'application/json',
		dataType : "json",
		data : JSON.stringify(formdata),
		success : function(data) {
			if (data != null) {
				if (data.code == 1) {
					alert("Register success!");
					window.reload();
				} else {
					$("#registerError").text(data.content);
					$("#registerErroDiv").show();
				}
			}
		}
	});
}


function validate(){
	
	$("#signupForm").validate({
		rules: {
			title: {
				required:true,
				notblank:true,
			},
			firstname: {
				required:true,
				notblank:true,
			},
			lastname: {
				required:true,
				notblank:true,
			},
			agerange: {
				required: true,
				valueNotEquals: 0
			},
			email: {
				required: true,
				email: true
			},
			street: {
				required: true,
				notblank:true,
			},
			suburb: {
				required: true,
				notblank:true,
			},
			postcode: {
				required: true,
				postcode:true,
			},
			language: {
				required: true,
				notblank:true,
			},
			reason: {
				required: true,
				notblank:true,
			}
		},
		messages: {
			title: {
				required:"Please enter your title",
				notblank:"Not a valid title",
			},
			firstname: {
				required:"Please enter your firstname",
				notblank:"Not a valid firstname",
			},
			lastname: {
				required:"Please enter your lastname",
				notblank:"Not a valid firstname",
			},
			agerange: {
				required: "Please select a age range",
				valueNotEquals: "Please select a age range",
			},
			email: "Please enter a valid email address",
			street: {
				required: "Please enter your address",
				notblank: "Please enter your address",
			},
			suburb: {
				required: "Please enter your suburb",
				notblank: "Please enter your suburb",
			},
			postcode: {
				required: "Please enter your post code",
				postcode: "Please enter a valid post code",
				
			},
			language: {
				required: "Please enter your fluent spoken language",
				notblank: "Please enter your fluent spoken language",
			},
			reason: {
				required: "Please enter how did you know about this program",
				notblank: "Please enter how did you know about this program",
			}
			
		},
		
		submitHandler: function(form) {  
            if ($(form).valid()){
            	submitForm();
            }
            return false; 
		}
		
	});
	
	
}
