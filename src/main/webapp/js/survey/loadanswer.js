var questionsData;
var unfinished = false;

$(document).ready(function() {

	var redirectPage = $("#nextpage").val();
	var answerId = $("#answerId").val();
	
	if(answerId == ""){
		$.blockUI({
			message : $('#waitForSOCAInfo'),
			css : {
				top : ($(window).height() - 400) / 2 + 'px',
				left : ($(window).width() - 400) / 2 + 'px',
				width : '400px',
				height : '160px',
			}
		});
	}else{
		$.ajax({
			type : 'GET',
			url : '/'+SURVEYPATH+'/loadanswer/' + answerId,
			dataType : "json",
			success : function(data) {

				if (data != null) {
					
					var survey = data.survey;
					var content = data.content;
					var answers = eval(content);
					
					questionsData = survey;
					
					var surveyTitle = survey.title;
					var surveyDescription = survey.description;
					var surveyOrderOfQuestions = survey.orderOfQuestions;
					var questions = survey.questions;
					var questionOrders = surveyOrderOfQuestions.split(",");

					$("#surveyTitle").text(surveyTitle);
					$("#surveyDescription").text(surveyDescription);
	               
					var divId = "questions";
					for (var i = 0; i < questionOrders.length; i++) {
						var answerForQuestion = null;
						var question = findQuestionById(questionOrders[i],questions);
						
						for (var j = 0; j < answers.length; j++) {
							if(answers[j].qid==question.id){
								answerForQuestion = answers[j];
							}
						}
						console.log(answerForQuestion);
						displayQuestion(question, divId, answerForQuestion);
					}
					
					disableAllInput();
					
					var element = "<li><a href='"+redirectPage+"'><input id='saveForm' class='usyd-ui-button-primary' type='button' value='Submit' /></a></li>";
					$("#"+divId).append(element);

				}

			}
		});

	}

	
	
	
	
		

});


function submitAnswer(){
	
	var questions = questionsData.questions;
	var answers = new Array();
	
	for (var i = 0; i < questions.length; i++) {
		var question = questions[i];
		var anwser = new Object();
		if (question.typeS == "TEXTAREA") {
			anwser = submitText(question);
			
		} else if (question.typeS == "TEXTFIELD") {
			anwser = submitText(question);
			
		} else if (question.typeS == "RADIO") {
			anwser = submitSingleChoice(question);
			
		} else if (question.typeS == "CHECKBOX") {
			anwser = submitMultipleChoice(question);
			
		}else if (question.typeS == "LIKERT") {
			anwser = submitSingleChoice(question);
			
		}
		
		answers.push(anwser);
	}
	
	
	var patientId = $("#patientId").val();
	var doctorId = $("#doctorId").val();
	var nextpage = $("#nextpage").val();
	
	var survey = new Object();
	survey.id = $("#surveyId").val();
	var answer = new Object();
	answer.survey = survey;
	answer.content = JSON.stringify(answers);
	
	if(doctorId!=null && doctorId!=""){
		var doctor = new Object();
		doctor.doctorId = doctorId;
		answer.doctor = doctor;
		answer.patient = null;
	}
	
	if(patientId!=null && patientId!=""){
		var patient = new Object();
		patient.patientid = patientId;
		answer.patient = patient;
		answer.doctor = null;
	}
	
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+SURVEYPATH+'/saveanswer',
		contentType : 'application/json',
		dataType : "json",
		data : JSON.stringify(answer),
		success : function(data) {
			if (data.code == 1) {
				// alert("save success");
				window.location.assign(nextpage);
			} else {
				alert("save fail");
			}
		}
	});
	
	
	
}


function submitSingleChoice(question){
	
	var qid = question.id;
	var qvalue = $("input[name='q"+qid+"']:checked").val();	
	
	var result = new Object;
	result.qid = qid;
	result.qvalue = qvalue;
	
	return result;
}

function submitMultipleChoice(question){
	
	var qid = question.id;
	var answers = $("input[name='q"+qid+"']:checked");	
	var qvalue= "";
	for (var i = 0; i < answers.length; i++) {
		
		if(qvalue==""){
			qvalue += answers[i].value;
		}else {
			qvalue += ","+answers[i].value;
		}
	}
	
	var result = new Object;
	result.qid = qid;
	result.qvalue = qvalue;
	
	return result;
}

function submitText(question){
	var qid = question.id;
	var qvalue = $("#q"+qid).val();
	
	var result = new Object;
	result.qid = qid;
	result.qvalue = qvalue;
	
	return result;
}


function findQuestionById(id, questions) {

	for (var i = 0; i < questions.length; i++) {
		if (questions[i].id == id) {
			return questions[i];
		}
	}

	return null;
}

function displayQuestion(question, divId,answerForQuestion) {

	var element = "";
	if (question.typeS == "TEXTAREA") {
		element = displayTextarea(question, answerForQuestion);
	} else if (question.typeS == "TEXTFIELD") {
		element = displayTextfield(question, answerForQuestion);
	} else if (question.typeS == "RADIO") {
		element = displayRadio(question, answerForQuestion);
	} else if (question.typeS == "CHECKBOX") {
		element = displayCheckbox(question, answerForQuestion);
	}else if (question.typeS == "LIKERT") {
		element = displayLikert(question, answerForQuestion);
	}
	
	$("#"+divId).append(element);
	
}

function displayTextfield(question, answerForQuestion) {
	
	var element = "<li id='aaa' class='notranslate'>"
		+"<label class='desc'>"+ question.title + "</label> "
		+"<label>"+ question.description +" </label> "
		+"<div><input  id='q"+question.id+"' name='q"+question.id+"' type='text' class='field text medium' value='"+answerForQuestion.qvalue+"' maxlength='255' tabindex='28' onkeyup='' /></div>"
		+"</li><hr/>";
	
	return element;
}

function displayTextarea(question, answerForQuestion) {
	
	var element = "<li id='aaa' class='notranslate'>"
		+"<label class='desc'>"+ question.title + "</label> "
		+"<label>"+ question.description +" </label> "
		+"<div><textarea  id='q"+question.id+"' name='q"+question.id+"' class='field textarea medium' spellcheck='true' rows='10' cols='50' tabindex='5' onkeyup=''>"+answerForQuestion.qvalue+"</textarea></div>"
		+"</li><hr/>";
	
	return element;
}

function displayRadio(question, answerForQuestion) {
	
	qvalue = answerForQuestion.qvalue;
	var element = "<li id='foli115' class='notranslate threeColumns'>"
		+"<fieldset>"
		+"<legend id='title115' class='desc'> "+question.title+" </legend>"
		+"<label>"+ question.description +" </label> "
		+"<div>";
	
	var options = question.options;
	
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		var optvalue = option.optvalue;
		if(qvalue==optvalue){
			element += "<span> <input  name='q"+question.id+"' type='radio' class='field radio' checked='checked' value="+optvalue+" tabindex='8' />"
	           +"<label class='choice'>"+optkey+"</label>"
	           +"</span>";
		}else{
			element += "<span> <input  name='q"+question.id+"' type='radio' class='field radio' value="+optvalue+" tabindex='8' />"
	           +"<label class='choice'>"+optkey+"</label>"
	           +"</span>";
		}
		
	}
	
	element += "</div></fieldset></li><hr/>";
	
	return element;
}

function displayCheckbox(question, answerForQuestion) {
	
	var options = question.options;
	
	var largestLength=0;
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		if(optkey.length>largestLength){
			largestLength = optkey.length;
		}
	}
	
	var style = "";
	if(largestLength>30){
		style = "oneColumn";
	}else{
		style = "threeColumns";
	}
	
	
	qvalues = answerForQuestion.qvalue.split(",");
	
	var element = "<li id='foli115' class='notranslate "+style+"'>"
		+"<fieldset>"
		+"<legend id='title115' class='desc'> "+question.title+" </legend>"
		+"<label>"+ question.description +" </label> "
		+"<div>";
	
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		var optvalue = option.optvalue;
		var temp="";
		for (var j = 0; j < qvalues.length; j++) {
			qvalue = qvalues[j];
			if(qvalue==optvalue){
				temp += "<span> <input  name='q"+question.id+"' type='checkbox' class='field checkbox' checked='checked' value="+optvalue+" tabindex='12' />"
		           +"<label class='choice'>"+optkey+"</label>"
		           +"</span>";
			}
		}
		
		if(temp==""){
			temp += "<span> <input  name='q"+question.id+"' type='checkbox' class='field checkbox' value="+optvalue+" tabindex='12' />"
	           +"<label class='choice'>"+optkey+"</label>"
	           +"</span>";
		}
		element += temp;
		
	}
	
	element += "</div></fieldset></li><hr/>";

	return element;
}


function displayLikert(question, answerForQuestion){
	
	qvalue = answerForQuestion.qvalue;
	
	var options = question.options;
	
	var optionNumbers = options.length;
	var element = "<li id='foli115' class='likert notranslate col"+optionNumbers+"'>"
		+"<label>"+ question.description +" </label> "
		+"<table cellspacing='0'>"
		+"<thead><tr><th>&nbsp;</th>";
	
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		element += "<td>"+optkey+"</td>";
	}
	element += "</tr></thead><tbody><tr class='statement108 alt'>"
		     + "<th><label for='Field108'>"+question.title+"</label></th>";
		
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optvalue = option.optvalue;
		if(qvalue==optvalue){
			element += "<td title='"+optvalue+"'><input  name='q"+question.id+"' type='radio' tabindex='1' value='"+optvalue+"' checked='checked'/> <label>"+optvalue+"</label></td>";
		}else{
			element += "<td title='"+optvalue+"'><input  name='q"+question.id+"' type='radio' tabindex='1' value='"+optvalue+"' /> <label>"+optvalue+"</label></td>";
		}
	}
	element += "</tr></tbody></table></li><hr/>";
	return element;

}


function disableAllInput(){
	$(":input").attr("disabled","disabled");
}

