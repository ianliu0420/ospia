$(document).ready(function() {
	
	$("#updateSurvey").click(function() {
		var survey = new Object();
		var surveyId = $("#surveyId").val();
		var title = $("#title").val();
		var description = $("#description").val();
		var questions = getAllQuestions();

		
		survey.id = surveyId;
		survey.title = title;
		survey.description = description;
		survey.questions = questions;
		
		var orderOfQuestions = "";
		for(var i = 0 ; i <questions.length; i++){
			if(""==orderOfQuestions){
				orderOfQuestions += questions[i].id;
			}else{
				orderOfQuestions += ","+questions[i].id;
			}
		}
		survey.orderOfQuestions = orderOfQuestions;
		
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+SURVEYPATH+'/savesurvey',
			contentType : 'application/json',
			dataType : "json",
			data : JSON.stringify(survey),
			success : function(data) {
				if (data.code == 1) {
					alert("update success");
					window.location.href="/"+SURVEYPATH+"/listsurvey";
				} else {
					alert("updata fail");
				}
			}
		});
		
	});
	
	
	$("#searchButton").click(function() {
		var surveyId = $("#surveyId").val();
		var searchContent = $("#searchContent").val();
		var url = '/'+SURVEYPATH+'/editsurvey/'+surveyId+"&"+searchContent;
		window.location.href = url;
	});
	

});


function getAllQuestions(){
	
	var questions = $('[id^=tr]');
	var questionsArray = new Array();
	
	$.each(questions, function(i, val) {
		var questionId = val.id;
		var idNumber = parseInt(questionId.substr(2));
		var question = new Object();
		question.id = idNumber;
		questionsArray.push(question);	
		
	});
	return questionsArray;
}


function addQuestionToSurvey(questionId){
	var surveyId = $("#surveyId").val();
	var formdata = {input:surveyId+";"+questionId};
	
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+SURVEYPATH+'/addquestiontosurvey',
		dataType : "json",
		data : formdata,
		success : function(data) {
			if (data.code == 1) {
				alert("save success");
				location.reload();
			} else {
				alert("save fail");
			}
		}
	});
}


function removeQuestionToSurvey(questionId){
	
	if(confirm("Delete this question?")){
		var surveyId = $("#surveyId").val();
		var formdata = {input:surveyId+";"+questionId};
		
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+SURVEYPATH+'/removequestiontosurvey',
			dataType : "json",
			data : formdata,
			success : function(data) {
				if (data.code == 1) {
					alert("delete success");
					location.reload();
				} else {
					alert("delete fail");
				}
			}
		});
	}
	
}






