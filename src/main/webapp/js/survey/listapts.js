$(document).ready(function() {
	
	$("#searchType1").change(function(){
		var searchType1 = $("input[name='searchType1']:checked").val();	
		if(searchType1==1){
			searchType1 = 0;
			$("#searchContentType1").attr('disabled', true);
			$("#searchContentType2").attr('disabled', true);
		}else{
			$("#searchContentType1").attr('disabled', false);
			$("#searchContentType2").attr('disabled', false);
		}
	});
	
	
	// action="<%=request.getContextPath()%>/survey/patients"
	$("#aptsSearchBtn").click(function(){
		
		var searchContent = $("#searchContent").val();
		var searchContentType = parseInt($('input[name=searchContentType]:checked').val());
		var searchType1 = $("input[name='searchType1']:checked").val();	
		var searchType2 = $("input[name='searchType2']:checked").val();	
		var searchType3 = $("input[name='searchType3']:checked").val();	
		var searchType4 = $("input[name='searchType4']:checked").val();	
		
		if(searchContent!="" && (isNaN(searchContentType)==true||searchContentType==0)){
			alert("Please select the 'search by' criteria");
			return;
		}
		
		
		if(searchType1!=1){
			searchType1 = 0;
		}
		if(searchType2!=1){
			searchType2 = 0;
		}
		if(searchType3!=1){
			searchType3 = 0;
		}
		if(searchType4!=1){
			searchType4 = 0;
		}
		var searchType= searchType1+","+searchType2+","+searchType3+","+searchType4;
		var aptSearch = new Object();
		aptSearch.searchType = searchType;
		aptSearch.searchContent = searchContent;
		aptSearch.searchContentType = searchContentType;
		
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+SURVEYPATH+'/apts',
			contentType : 'application/json',
			dataType : "json",
			data : JSON.stringify(aptSearch),
			success : function(data) {
				
				$("#tbodyList").empty();
				curentDayEvent = data;
				if (data.length > 0) {
					
					$("#total").html("Total number is:" + data.length);
					$("#total").show();
					// $("#studentHead").show();
					
					for (var i = 0; i < data.length; i++) {
						var obj = data[i];
						var apt = obj[0];
						var patient = obj[1];
						var doctor = obj[2];
						var trEle = "";
						
						if(doctor==null){
							// $("#studentHead").hide();
							
							trEle = "<tr><td>"
								+ convertFullDateToAPTime(convertToDate(apt.starttime)) +", "+convertNumnberDateToString(apt.starttime)
								+ "</td><td>"
								+ patient.firstname+" "+patient.lastname
								+ "</td><td>"
								+ "&nbsp"
								+ "</td><td>"
								+ "&nbsp"
								+ "</td><td>"
								+ convertStatus(apt.status)
								+ "</td></tr>";
						}else{
							trEle = "<tr><td>"
								+ convertFullDateToAPTime(convertToDate(apt.starttime)) +", "+convertNumnberDateToString(apt.starttime)
								+ "</td><td>"
								+ patient.firstname+" "+patient.lastname
								+ "</td><td>"
								+ doctor.firstname+" "+doctor.lastname
								+ "</td><td>"
								+ doctor.userId
								+ "</td><td>"
								+ convertStatus(apt.status)
								+ "</td></tr>";
						}
						
						$("#tbodyList").append(trEle);
					}
				} else{
					$("#total").hide();
					$("#tbodyList").append("<span style='font-size:14px; padding-top:10px; font-weight:bold'>No appointment searched.<span>");
				}
				$("#detailTable").show();
			}
		});
		
	});
	

});



