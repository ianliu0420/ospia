
$(document).ready(function(){
	if(browserDetection()==false){
		//	if(1==0){
		showFirefoxWarning();
		return;
	}else{
		
		var connectionCount=0;
		
		if (OT.checkSystemRequirements() == 0) {
			OT.log("The client does not support WebRTC.");
		}
		 
		// --------------------- this is the session of original session: start-------------------------------
		session = OT.initSession(testerApiKey, testerSessionId);

		session.on({
			connectionCreated: function (event) {
				connectionCount++;
				OT.log(connectionCount + " connections.");
				
				console.log(connectionCount + " connections.");
			},
			
			connectionDestroyed: function (event) {
				connectionCount--;
				OT.log(connectionCount + " connections.");
				
				console.log(connectionCount + " connections.");
			},
			
			sessionDisconnected: function sessionDisconnectHandler(event) {
		    // The event is defined by the SessionDisconnectEvent class
				if (event.reason == "networkDisconnected") {
					alert("Your network connection terminated.");
				}
			},

			streamCreated: function(event) {
				OT.log("New stream in the session: " + event.stream.streamId);
				var subscriberProperties = {resolution: "1280x720", insertMode: "append", width:640, height:360};
				session.subscribe(event.stream, 'baselineContainer', subscriberProperties, function (error) {
					if (error) {
						alert(error);
					} else {
						$('#oppositeContainerText').remove();
						OT.log("Subscriber added for stream: " + event.stream.streamId);
					}
				});
			},
		});

		session.connect(testerToken, function(error) {
			if (error) {
				alert("Unable to connect: "+error.code+", "+error.message);
				return;
			}
			
			OT.log("Connected to the session.");
			

			if (session.capabilities.publish != 1) {
				alert("You cannot publish an audio-video stream.");
				return;
			}
				
		    // Replace with the replacement element ID:
			 var publisherProperties = {resolution: "1280x720", width:640, height:360};
//			var publisherProperties = {};
			publisher = OT.initPublisher('baselineContainer', publisherProperties, function(error){
				if (error) {
					OT.log("Unable to publish stream: ", error.message);
					return;
				}
				
				publisher.on({
				    streamCreated: function (event) {
				    		OT.log("Publisher started streaming.");
				    },
				    streamDestroyed: function (event) {
				    		OT.log("Publisher stopped streaming. Reason: "
				               + event.reason);
				    	}
			    });
				
		        if (publisher) {
		        		session.publish(publisher);
		        }
			});
		});

		$("#testerVideoReviewBtn").click(function(){
				var testerVideoUrl = $("#testerVideoUrl").val();
				// window.open(testerVideoUrl);	
				window.location.assign(testerVideoUrl);
		});

		$("#startBaseline").click(function(){
		
			if($("#startBaseline").val()=="Start"){
				$("#startBaseline").val("Stop");
				$.ajax({
					type : 'POST',
				    headers: getTokenHeader(),
					url : '/ospiatesterstart/'+testerSessionId,
					dataType : "json",
					success : function(data) {
						if(data.code == 1){
							console.log(data);
							$("#doctorArchiveIdBaseline").val(data.content);		
					     }else{
					    	 // $("#errorInfo").show();
					     }
						
					}
				});
			}else{
				var archiveId = $("#doctorArchiveIdBaseline").val();
				
				$.ajax({
					type : 'POST',
					headers: getTokenHeader(),
					url : '/ospiatesterstop/'+archiveId,
					dataType : "json",
					beforeSend : function() {
						showWaitingPopUp();
					},
					success : function(data) {
						if(data.code == 1){
							$("#testerVideoUrl").val(data.content);
							$("#testerVideoReviewBtn").show();
							$("#startBaseline").hide();
							cancelPopup();
					     }else{
					    	 $("#errorInfo").show();
					     }
					}
				});
			}
		});
		
	}
	
});
	

function showWaitingPopUp() {
	$.blockUI({
		message : $('#waitingPopup'),
		css : {
			top : ($(window).height() - 50) / 2 + 'px',
			left : ($(window).width() - 200) / 2 + 'px',
			width : '300px',
			height : '70px',
		}
	});
}

function cancelPopup() {
	$.unblockUI();
}

function browserDetection(){
	
	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
	var isFirefox = typeof InstallTrigger !== 'undefined';
	var isIE = false || !!document.documentMode;
	
	if((isChrome===false &&isFirefox===false) || isIE===true){
		return false;
	}
}

function showFirefoxWarning() {
	$.blockUI({
		message : $('#firefoxWarning'),
		css : {
			top : ($(window).height() - 100) / 3 + 'px',
			left : ($(window).width() - 500) / 2 + 'px',
			width : '500px',
			height : '100px'
		}
	});
}


	