var DOCTORPATH = "stu";
var PATIENTPATH = "sp";
var TUTORPATH = "tut";
var SURVEYPATH = "survey";
var projectName = "";

//var DOCTORPATH = "/stu";
//var PATIENTPATH = "/sp";
//var TUTORPATH = "/tut";
//var SURVEYPATH = "/survey";
//var projectName = "ospia";


function convertToDate(timestamp) {

	var date = new Date(timestamp);
	var datevalues = [ date.getFullYear(), date.getMonth() + 1, date.getDate(),
			date.getHours(), date.getMinutes(), date.getSeconds(), ];

	return datevalues[0] + "-" + convertSingleToDouble(datevalues[1]) + "-"
			+ convertSingleToDouble(datevalues[2]) + "T"
			+ convertSingleToDouble(datevalues[3]) + ":"
			+ convertSingleToDouble(datevalues[4]) + ":"
			+ convertSingleToDouble(datevalues[5]);
}

function convertToDateShort(timestamp) {
	var date = new Date(timestamp), datevalues = [ date.getFullYear(),
			date.getMonth() + 1, date.getDate(), date.getHours(),
			date.getMinutes(), date.getSeconds(), ];
	return datevalues[0] + "-" + convertSingleToDouble(datevalues[1]) + "-"
			+ convertSingleToDouble(datevalues[2]);
}

function convertSingleToDouble(number) {

	if (number < 10) {
		return "0" + number;
	}
	return number;
}

function convertDayNumberToStr(input) {
	if (input == 0) {
		return "SUN";
	} else if (input == 1) {
		return "MON";
	} else if (input == 2) {
		return "TUE";
	} else if (input == 3) {
		return "WED";
	} else if (input == 4) {
		return "THU";
	} else if (input == 5) {
		return "FRI";
	} else if (input == 6) {
		return "SAT";
	}
}

function convertNumnberDateToString(dateNumber){
	return moment(dateNumber).format("Do MMMM, YYYY");
}

function convertFullDateToAPTime(fulldate){
	return moment(fulldate).format("h:mm a");
}

function convertToAusStyle(date){
	var values = date.split("-");
	return values[2]+"/"+values[1]+"/"+values[0];
}


function convertNumbersToTimeStamp(year, month, day) {
	var date = year + "/" + convertSingleToDouble(month) + "/"
			+ convertSingleToDouble(day) + " 00:00:00";
	var timeStamp = new Date(date).getTime();
	return timeStamp;
}

function convertSecondToMinute(secondLong) {
	var second = Math.floor(secondLong);
	var minute = Math.floor(second / 60);
	second = second % 60;
	minute = convertSingleToDouble(minute);
	second = convertSingleToDouble(second);
	return minute + ":" + second;
}

function moveUp(id, itemIdStart) {
	var length = itemIdStart.length;

	var preDiv = null;
	var currentDiv = null;
	var options = $('[id^=' + itemIdStart + ']');

	$.each(options, function(i, val) {
		preDiv = currentDiv;
		currentDiv = val;

		var divId = currentDiv.id;
		var idNumber = parseInt(divId.substr(length));
		if (idNumber == id) {
			return false;
		}
	});

	if (preDiv != null && currentDiv != null) {
		preDiv = $("#" + preDiv.id);
		currentDiv = $("#" + currentDiv.id);
		currentDiv.after(preDiv);
	}
}

function moveDown(id, itemIdStart) {
	var length = itemIdStart.length;
	var currentDiv = null;
	var nextDiv = null;
	var options = $('[id^=' + itemIdStart + ']');

	var findTheObj = false;
	$.each(options, function(i, val) {
		currentDiv = nextDiv;
		nextDiv = val;

		if (currentDiv != null) {
			var divId = currentDiv.id;
			var idNumber = parseInt(divId.substr(length));
			if (idNumber == id) {
				findTheObj = true;
				return false;
			}
		}
	});

	if (nextDiv != null && currentDiv != null && findTheObj == true) {
		nextDiv = $("#" + nextDiv.id);
		currentDiv = $("#" + currentDiv.id);
		nextDiv.after(currentDiv);
	}
}

function detectBrowser(){
	var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
	    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
	var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
	    // At least Safari 3+: "[object HTMLElementConstructor]"
	var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
	var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6
	
	
	if(isFirefox==true){
		return 1;
	}else{
		return 2;
	}
	
}

function clickVideo(id) {
//	var vid = document.getElementById(id);
//	vid.paused ? vid.play() : vid.pause();
}

function getTokenHeader(){
	var headers = {};
	headers['__RequestVerificationToken'] = $("#csrftoken").val();
	return headers;
}


function convertStatus(status){
	
	if(status==1){
		return "Available";
	}else if(status==2){
		return "Pending";
	}else if(status==3){
		return "Confirmed";
	}else if(status==8){
		return "Completed";
	}else{
		return "Canceled";
	}
	
}



// validation
function isEmail(str){ 
	var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/; 
	return reg.test(str); 
} 

function isEmpty(str){
	if(str==""){
		return true;
	}else{
		return false;
	}
}

function isPhoneNo(str){
	var reg = /^0[1-9]{9}$/;
	return reg.test(str);
}

function isValidPassword(str){
	var reg = /^([a-zA-Z0-9_-]){6,20}/; 
	return reg.test(str); 
}

// jquery validation
$.validator.addMethod("valueNotEquals", function(value, element, arg){
	  return arg != value;
}, "Value must not equal arg.");

$.validator.addMethod('validDate', function(value, element, params) {
  var day = $('#'+params[0]).val(), month = $('#'+params[1]).val(), year = $('#'+params[2]).val();
  var result = day!="0"&&month!="0"&&year!="0"; 
  return result;
}, "Not valid date.");

jQuery.validator.addMethod("digitletteronly", function(value, element) {
	  return this.optional(element) || /^([a-zA-Z0-9_-]){6,20}$/i.test(value);
	}, "Letter or digits only");

jQuery.validator.addMethod("postcode", function(value, element) {
	  return this.optional(element) || /^([0-9]){4}$/i.test(value);
	}, "Not valid post code");

jQuery.validator.addMethod("notblank", function(value, element) {
	  return $.trim(value).length!=0;
	}, "Not valid character");

jQuery.validator.addMethod("dateformat", function(value, element) {
	  return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/i.test(value);
	}, "Not valid date format");

setInterval(updateCurrentTime, 1000);
function updateCurrentTime(){
	var datetime = moment().format("DD/MM/YYYY, h:mm:ss a");
	$("#currentTime").html(datetime);
}

