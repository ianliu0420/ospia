google.load("visualization", "1");

// Set callback to run when API is loaded
google.setOnLoadCallback(drawVisualization);

//Called when the Visualization API is loaded.
function drawVisualization() {
    var formdata = {
    	"sessionId": sessionId
    };

    videoLength = $("#videoLength").val();
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+TUTORPATH+'/loadtutornote/',
		contentType : 'application/json',
		dataType : "json",
		data : JSON.stringify(formdata),
		success : function(data) {
			
			var initTimeStr ='2015-01-01 00:00:00';
		    initTimeStr = initTimeStr.replace(/-/g,"/");
		    var initTime = new Date(initTimeStr);
		    var initLength = new Date(initTime.getTime() + videoLength * 1000);
		    $("#commentBody").empty();
		    
		    // specify options
		    var options = {
				width:  "100%",
		        // height: heightInPixel,
		        height: "auto",
		        layout: "box",
		        axisOnTop: true,
		        eventMargin: 10,  // minimal margin between events
		        eventMarginAxis: 0, // minimal margin between events and the axis
		        // editable: true,
//		        showNavigation: true,
		        max: initLength,
		        min: initTime,
		        showCustomTime: true
		    };
		    
		    /* time line of head movement */
		    timeline_comment = new links.Timeline(document.getElementById('mytimeline'), options);

		    // Create and populate a data table.
		    data_hm = new google.visualization.DataTable();
		    data_hm.addColumn('datetime', 'start');
		    data_hm.addColumn('datetime', 'end');
		    data_hm.addColumn('string', 'content');
		    data_hm.addColumn('string', 'group');
		    data_hm.addColumn('string', 'className');
		    data_hm.addColumn('string', 'id');
		    
			if(!(data.length==1 && data[0]=="")){
				for(var i = 0; i<data.length;i++){
					var comment = data[i];
					var startTime = new Date(initTime.getTime() + comment.time* 1000);
					var noterType = '';
					if(comment.noterType == 1){
						noterType = "tutor";
					}else{
						noterType = "sp";
					}
					var commentId = data[i].id+"";
					
					if(comment.noteContent == '1'){
						data_hm.addRow([startTime, , '<img src="/img/thumbup.png" style="width:34px; height:30px;">',noterType,"",commentId]);
					}else if(comment.noteContent == '2'){
						data_hm.addRow([startTime, , '<img src="/img/thumbdown.png" style="width:34px; height:30px;">',noterType,"",commentId]);
					}else{
						data_hm.addRow([startTime, , data[i].noteContent,noterType,noterType,commentId]);
						
						var tempNode = "<tr>"+
						"<td><a onclick='jumpTo("+data[i].time+")'>"+data[i].time+"</a></td>"+
						"<td>"+data[i].noteContent+"</td>"+
						"<td><button class='usyd-ui-button-primary' onclick='deleteComment("+data[i].id+")'>Delete</button>"
						+"</tr>";
						
						$("#commentBody").append(tempNode);
					}
				}
			}else{
			}
		    
		    // Draw our timeline with the created data and options
			timeline_comment.draw(data_hm);
			
			google.visualization.events.addListener(timeline_comment, 'select',
		            function (event) {
				    	var row = getSelectedRow(timeline_comment);
				    	var startTime = data_hm.getValue(row, 0);
				
				    	var hours = startTime.getHours();   
				    	var minutes = startTime.getMinutes();   
				    	var seconds = startTime.getSeconds(); 
				    	var ms = startTime.getMilliseconds();  
				    	var totalSec = 60*60*hours + 60*minutes+seconds+ms/1000;
				    	jumpTo(totalSec);
				    	timeline_comment.setSelection();
		            }
		    );
			
		}
	});
}

function deleteComment(id){
	
	if(confirm("Delete this comment?")){
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+TUTORPATH+'/deletecomment/'+id,
			dataType : "json",
			success : function(data) {
				if(data.code == 1){
			    	 alert(data.content);
			    	 drawVisualization();
			     }else{
			    	 alert(data.content);
			     }
			}
		});
	}
	
}


function getSelectedRow(timeline) {
    var row = undefined;
    var sel = timeline.getSelection();
    if (sel.length) {
        if (sel[0].row != undefined) {
            row = sel[0].row;
        }
    }
    return row;
}



function jumpTo(time){
	var myvideo = document.getElementById("myvideo");
	myvideo.currentTime=time;
	myvideo.pause();
}




$(document).ready(function(){
	
    var myvideo = document.getElementById("myvideo");
	
	myvideo.ontimeupdate = function(){
		var startTime = myvideo.currentTime;
		var startTimeMunite = Math.floor(startTime/60);
		var startTimeSecond = Math.floor(startTime)%60;
		var windowStartTime = new Date(2015,0,1,0,startTimeMunite,startTimeSecond,0);
		
		timeline_comment.setCustomTime(windowStartTime);
	};
	
	
	if(enablePostcheck==0){
//		$('#postcheck').attr('checked',true);
		$('.Switch').toggleClass('On').toggleClass('Off');
	}
	
	// Switch toggle
    $('.Switch').click(function() {
        $(this).toggleClass('On').toggleClass('Off');
        var switchName = $(this).attr('class');
        if(switchName=='Switch Off'){
        	postcheckChange(1);
        }else if(switchName=='Switch On'){
        	postcheckChange(0);
        }
        
    });
	
	$("#commentBtn").click(function(){
		
		var myvideo = document.getElementById("myvideo");
		var time = myvideo.currentTime;
		var noteContent = $("#comment").val();
		var sessionId = $("#sessionId").val();
		
		
		if(noteContent=="" ){
			 $("#commentSuc").text("Comment cannot be empty.");
	    	 submitSuc();
	    	 return;
		}
		
		if(time==0){
			 $("#commentSuc").text("Curent time is zero, please play the video.");
	    	 submitSuc();
	    	 return;
		}
		
		var formdata = {"time":time,"noteContent":noteContent, "sessionId":sessionId};
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+TUTORPATH+'/addnotetutor',
			contentType: 'application/json',
			dataType : "json",
			data :JSON.stringify(formdata),
			success : function(data) {
			     if(data.code == 1){
			    	 $("#commentSuc").text("Submit Success");
			    	 submitSuc();
			    	 $("#comment").val("");
			    	 drawVisualization();
			     }else{
			    	 $("#commentSuc").text(data[0].content);
			    	 submitSuc();
			     }
			}
		});
	});
	
});


function submitSuc(){
	$("#commentSuc").show();
	setTimeout("hideCommentSuc()", 2000);
}

function hideCommentSuc(){
	$("#commentSuc").hide();
}

function postcheckChange(answer){
	
//	var answer = $("#postcheck").is(':checked');
//	if(answer == true){
//		answer = 1;
//	}else{
//		answer = 0;
//	}
	
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+TUTORPATH+'/enablepostcheck/'+sessionId+"/"+answer,
		contentType: 'application/json',
		dataType : "json",
		success : function(data) {
		     if(data.code == 1){
		    	 alert("Operation Success");
		     }
		},
		error: function() { 
			if(answer==true){
				$('#postcheck').attr('checked',false);
			}else{
				$('#postcheck').attr('checked',true);
			}
			alert("Operation Fail");
	    }  
	});
}


