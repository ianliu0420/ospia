
google.load("visualization", "1");

// Set callback to run when API is loaded
google.setOnLoadCallback(drawVisualization);

var timeline_comment = null;

var pageLoadTime;

//Called when the Visualization API is loaded.
function drawVisualization() {
	
	var archiveId = $("#archiveId").val();
	var videoLength = parseInt($("#videoLength").val())*60;
	var evaluationImage = $("#evaluationImage").val();
	var notesContent = "";
	if(evaluationImage==1){
		notesContent += '<img src="/img/thumbup.png" style="width:34px; height:30px;">' + ' represents a positive moment for the SP, and ' +
		'<img src="/img/thumbdown.png" style="width:34px; height:30px;">' + ' represents a difficult moment for the SP.';
	}else{
		notesContent += '<img src="/img/face_smile.jpg" style="width:34px; height:30px;">' + ' represents a positive moment for the SP, and ' +
		'<img src="/img/face_sad.jpg" style="width:34px; height:30px;">' + ' represents a difficult moment for the SP.';
	}
	$("#notes").html(notesContent);
	
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/loadsessionnote2/'+archiveId,
		dataType : "json",
		success : function(data) {
			
			var initTimeStr ='2015-01-01 00:00:00';
		    initTimeStr = initTimeStr.replace(/-/g,"/");
		    var initTime = new Date(initTimeStr);
		    var initLength = new Date(initTime.getTime() + videoLength * 1000);
			
		    // specify options
		    var options = {
				width:  "100%",
		        // height: heightInPixel,
		        height: "auto",
		        layout: "box",
		        axisOnTop: true,
		        eventMargin: 10,  // minimal margin between events
		        eventMarginAxis: 0, // minimal margin between events and the axis
		        // editable: true,
		        showNavigation: true,
		        max: initLength,
		        min: initTime,
		        showCustomTime: true
		    };

		    
		    /* time line of head movement */
		    timeline_comment = new links.Timeline(document.getElementById('mytimeline'), options);

		    // Create and populate a data table.
		    data_hm = new google.visualization.DataTable();
		    data_hm.addColumn('datetime', 'start');
		    data_hm.addColumn('datetime', 'end');
		    data_hm.addColumn('string', 'content');
		    data_hm.addColumn('string', 'group');
		    data_hm.addColumn('string', 'className');
		    data_hm.addColumn('string', 'id');
		    
			if(!(data.length==1 && data[0]=="")){
				for(var i = 0; i<data.length;i++){
					var comment = data[i];
					var startTime = new Date(initTime.getTime() + comment.time* 1000);
					var noterType = '';
					if(comment.noterType == 1){
						noterType = "tutor";
					}else{
						noterType = "sp";
					}
					var commentId = data[i].id+"";
					
					if(comment.noteContent == '1'){
						if(evaluationImage == 1){
							data_hm.addRow([startTime, , '<img src="/img/thumbup.png" style="width:34px; height:30px;">',noterType,"",commentId]);
						}else{
							data_hm.addRow([startTime, , '<img src="/img/face_smile.png" style="width:34px; height:30px;">',noterType,"",commentId]);
						}
						
					}else if(comment.noteContent == '2'){
						if(evaluationImage == 1){
							data_hm.addRow([startTime, , '<img src="/img/thumbdown.png" style="width:34px; height:30px;">',noterType,"",commentId]);
						}else{
							data_hm.addRow([startTime, , '<img src="/img/face_sad.png" style="width:34px; height:30px;">',noterType,"",commentId]);
						}
						
						
					}else{
						if(noterType == "sp"){
							startTime = new Date(initTime.getTime() + (comment.time-5)* 1000);
							data_hm.addRow([startTime, , data[i].noteContent,noterType,noterType,commentId]);
						}else{
							data_hm.addRow([startTime, , data[i].noteContent,noterType,noterType,commentId]);
						}
						
					}
				}
			}else{
			}
		    
		    // Draw our timeline with the created data and options
			timeline_comment.draw(data_hm);
			
			
			
			google.visualization.events.addListener(timeline_comment, 'select',
		            function (event) {
				    	var row = getSelectedRow(timeline_comment);
				    	var startTime = data_hm.getValue(row, 0);
				
				    	var hours = startTime.getHours();   
				    	var minutes = startTime.getMinutes();   
				    	var seconds = startTime.getSeconds(); 
				    	var ms = startTime.getMilliseconds();  
				    	var totalSec = 60*60*hours + 60*minutes+seconds+ms/1000;
				    	jumpTo(totalSec);
				    	timeline_comment.setSelection();
		            }
		    );
			
		}
	});

}




$(document).ready(function(){
	
	var myvideo = document.getElementById("myvideo");
	
	myvideo.ontimeupdate = function(){
		var startTime = myvideo.currentTime;
		var startTimeMunite = Math.floor(startTime/60);
		var startTimeSecond = Math.floor(startTime)%60;
		var windowStartTime = new Date(2015,0,1,0,startTimeMunite,startTimeSecond,0);
		
		timeline_comment.setCustomTime(windowStartTime);
	};
	
	pageLoadTime = new Date().getTime();
	
});



function getSelectedRow(timeline) {
    var row = undefined;
    var sel = timeline.getSelection();
    if (sel.length) {
        if (sel[0].row != undefined) {
            row = sel[0].row;
        }
    }
    return row;
}



function jumpTo(time){
	var myvideo = document.getElementById("myvideo");
	myvideo.currentTime=time;
	myvideo.pause();
}

// nonverbalType = 21
function addReflectionLog(){
	
	currentReflectionSection = 21;
	sessionId = $("#sessionId").val();
	currentReflectionStartTime = new Date().getTime();
	
	var reflectionLog= new Object();
	
	reflectionLog.sessionId = sessionId;
	reflectionLog.nonverbalType = currentReflectionSection;
	reflectionLog.startTime = pageLoadTime;
	
	var currentTime = new Date().getTime();
	reflectionLog.endTime = currentTime;
	
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/addreflectionlog',
		contentType : 'application/json',
		dataType : "json",
		data : JSON.stringify(reflectionLog),
		success : function(data) {
			if (data.code == 1) {
			} else {
			}
		}
	});
}


$(window).bind("beforeunload",addReflectionLog);

