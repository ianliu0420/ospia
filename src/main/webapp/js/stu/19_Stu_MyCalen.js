var curentDayEvent = null;

$(document).ready(function() {

	accessGCal();

});

function drawFullCal(myevents) {
	$('#calendar').fullCalendar({
		header : {
			// left: 'prev,next today',
			left : 'prev,next',
			center : 'title',
			// right: 'month,agendaWeek,agendaDay'
			right : ''
		},
		timezone: 'local',
		defaultDate : getCurrentDateStr(),
		selectable : true,
		selectHelper : true,
		select : function(start, end) {
			loadEvent(start);
		},
		editable : false,
		eventLimit : true, // allow "more" link when too many events
		events : myevents,
		eventClick: function(calEvent, jsEvent, view){
			loadEvent(calEvent.start);
		}
	});
}

function accessGCal() {
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/doctorcalendar',
		dataType : "json",
		beforeSend : function() {
			showWaitingPopUp();
		},
		success : function(data) {

			var events = new Array();
			for (var i = 0; i < data.length; i++) {
				var apt = data[i];
				var startValue = convertToDate(apt.starttime);
				var endValue = convertToDate(apt.endtime);
				var summary = apt.patientname;
				if (apt.status == 1) {
					color = "green";
					//summary += " ("+apt.applicationNo+")";
				} else if (apt.status == 2) {
					color = "red";
				} else if (apt.status == 3) {
					color = "blue";
				} else {
					color = "orange";
				}
				var tempEvent = {
					title : summary,
					start : startValue,
					end : endValue,
					color : color
				};
				events.push(tempEvent);
			}
			drawFullCal(events);
			cancelPopup();
		}
	});
}

function getCurrentDateStr() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; // January is 0!
	var yyyy = today.getFullYear();

	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}

	today = yyyy + "-" + mm + "-" + dd;
	return today;
}

function showAddEventPopUp(start, end) {
	var dateInfo = new Date();
	var date = dateInfo.getDate();
	var month = dateInfo.getMonth() + 1;
	var year = dateInfo.getFullYear();

	$("#insertEventDate").val(date);
	$("#insertEventMonth").val(month);
	$("#insertEventYear").val(year);

	$.blockUI({
		message : $('#addEventPopup'),
		css : {
			top : ($(window).height() - 400) / 2 + 'px',
			left : ($(window).width() - 400) / 2 + 'px',
			width : '420px',
			height : '320px',
		}
	});
	$('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);
}

function loadEvent(start) {

	var dateInfo = start._d;
	var date = dateInfo.getDate();
	var month = dateInfo.getMonth() + 1;
	var year = dateInfo.getFullYear();
	var getEventWDDate = year + "-" + convertSingleToDouble(month) + "-"
			+ convertSingleToDouble(date);
	$("#dateSpan").text("Appointments on " + convertNumnberDateToString(getEventWDDate));
	$("#dateSpan").parent().show();
	
	var formdata = {
		"getEventWDDate" : getEventWDDate
	};
	$
			.ajax({
				type : 'POST',
				headers: getTokenHeader(),
				url : '/'+DOCTORPATH+'/getdoctoraptswholedate',
				dataType : "json",
				data : formdata,
				beforeSend : function() {
					showWaitingPopUp();
				},
				success : function(data) {
					$("#tbodyList").empty();
					curentDayEvent = data;
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							var apt = data[i];
							var reqInfo = convertToDate(apt.starttime).split(
									"T");
							var trEle = "";

							var aptSce = apt.scenarioCode;
							var spname = apt.patientname;

							if (apt.status == 1) {
								trEle = "<tr><td>"
										+ convertNumnberDateToString(reqInfo[0])
										+ "</td><td>"
										+ convertFullDateToAPTime(convertToDate(apt.starttime))
										+ "</td><td>"
										+ spname
										+ "</td>"
										+ "<td><input type='button' value='Request' onclick='requestApt("
										+ apt.aptid
										+ ")' class='usyd-ui-button-primary'></td>"
										+ "</tr>";
							} else if (apt.status == 2) {
								trEle = "<tr><td>" + convertNumnberDateToString(reqInfo[0]) + "</td><td>"
										+ convertFullDateToAPTime(convertToDate(apt.starttime)) + "</td><td>" + spname
										+ "</td><td>Request submitted</td>" + "</tr>";
							} else if (apt.status == 3) {
								trEle = "<tr><td>"
										+ convertNumnberDateToString(reqInfo[0])
										+ "</td><td>"
										+ convertFullDateToAPTime(convertToDate(apt.starttime))
										+ "</td><td>"
										+ spname
										+ "</td>"
										+ "<td><input type='button' value='Cancel' onclick='cancelApt("
										+ apt.aptid
										+ ")' class='usyd-ui-button-primary'></td>"
										+ "</tr>";
							} else {
								trEle = "<tr><td>" + convertNumnberDateToString(reqInfo[0]) + "</td><td>"
										+ convertFullDateToAPTime(convertToDate(apt.starttime)) + "</td><td>" + spname
										+ "</td><td>&nbsp;</td>" + "</tr>";
							}

							$("#tbodyList").append(trEle);
						}
						$("#emptyNote").hide();
						$("#scrollTable").show();
					} else {
						$("#scrollTable").hide();
						$("#emptyNote").show();
					}
					cancelPopup();
				}
			});
}

function showRequestAptPopup(aptId) {

	$("#makeRequestId").val(aptId);
	var targetEvent = null;
	if (curentDayEvent.length > 0) {
		for (var i = 0; i < curentDayEvent.length; i++) {
			var apt = curentDayEvent[i];
			if (apt.aptid == aptId) {
				targetEvent = apt;
			}
		}
	}
	var startDate = new Date(targetEvent.starttime);
	var endDate = new Date(targetEvent.endtime);
	$("#requireAptDay").text(
			convertDayNumberToStr(startDate.getDay()) + " "
					+ convertToAusStyle(convertToDateShort(startDate)));
	$("#requireAptStartTime").text(
			convertSingleToDouble(startDate.getHours()) + ":"
					+ convertSingleToDouble(startDate.getMinutes()));
	$("#requireAptEndTime").text(
			convertSingleToDouble(endDate.getHours()) + ":"
					+ convertSingleToDouble(endDate.getMinutes()));
	$("#requireSPName").text(targetEvent.patientname);
	$("#requireScenario").text(targetEvent.scenarioCode);

	$.blockUI({
		message : $('#requestAptPopup'),
		css : {
			top : ($(window).height() - 400) / 2 + 'px',
			left : ($(window).width() - 400) / 2 + 'px',
			width : '300px',
			height : '200px',
		}
	});
	$('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);
}

function makeRequest() {
	$("#requestBtn").attr('disabled', 'disabled');
	var aptId = $("#makeRequestId").val();
	
	// requestType: 1: FCFS, 2: wait to be selected
	var requestType = 1;
	
	if(requestType==1){
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+DOCTORPATH+'/applyappoitment2/'+aptId,
			dataType : "json",
			beforeSend : function() {
				$("#reqAptErrorInfo").text(
						"We are processing, please wait a moment.....");
				$("#reqAptErrorInfo").show();
			},
			success : function(data) {
				if (data != null) {
					if (data.code == 1) {
						cancelPopup();
						location.reload();
					} else {
						cancelPopup();
						alert(data.content);
					}
					$("#requestBtn").removeAttr('disabled');
				}
			}
		});
		
	}else{
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+DOCTORPATH+'/applyappoitment/'+aptId,
			dataType : "json",
			beforeSend : function() {
				$("#reqAptErrorInfo").text(
						"We are processing, please wait a moment.....");
				$("#reqAptErrorInfo").show();
			},
			success : function(data) {
				if (data != null) {
					if (data.code == 1) {
						alert("Thank you for requesting an appointment. This request will be collated with all other requests, " +
								"and the allocation of the appointment with the SP will be decided based on each student's previous OSPIA interactions. " +
								"You will be notified by email within 24 hours to confirm if you have been successful in confirming this appointment.");
						cancelPopup();
						location.reload();
					} else {
						cancelPopup();
						alert(data.content);
					}
					$("#requestBtn").removeAttr('disabled');
				}
			}
		});
	}
		
	
}
function showCancelAptPopup() {
	$.blockUI({
		message : $('#cancelAptPopup'),
		css : {
			top : ($(window).height() - 400) / 2 + 'px',
			left : ($(window).width() - 400) / 2 + 'px',
			width : '400px',
			height : '220px',
		}
	});
	$('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);
}

function cancelPopup() {
	$.unblockUI();
}

function editApt() {
	var start = null;
	var end = null;
	showAddEventPopUp(start, end);
}

function requestApt(aptId) {
	showRequestAptPopup(aptId);
}

function cancelApt(aptId) {
	$("#cancelAptId").val(aptId);
	showCancelAptPopup();
}

function confirmCancelApt() {
	var aptId = $("#cancelAptId").val();
	var cancelReason = $("#cancelReason").val();
	var formdata = {
		"aptId" : aptId,
		"cancelReason" : cancelReason,
		"cancelPerson" : 1
	};
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/cancelapt',
		dataType : "json",
		data : formdata,
		beforeSend : function() {
			$("#cancelAptErrorInfo").text(
					"We are processing, please wait a moment.....");
			$("#cancelAptErrorInfo").show();
		},
		success : function(data) {
			if (data != null) {
				if (data.code == 1) {
					alert("Operation success");
					cancelPopup();
					location.reload();
				} else {
					alert("Confirm failed, please try again");
				}
			}
			$("#cancelAptErrorInfo").hide();
		}
	});
}


function showWaitingPopUp() {
	$.blockUI({
		message : $('#waitingPopup'),
		css : {
			top : ($(window).height() - 50) / 2 + 'px',
			left : ($(window).width() - 200) / 2 + 'px',
			width : '200px',
			height : '50px',
		}
	});
}

