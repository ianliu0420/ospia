var ispatient = 0;
var startTime = 0;
var fullScreenCounter = 0;



$(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange', fullScreenChange);

function fullScreenChange(){
	fullScreenCounter++;
	
	if(fullScreenCounter%2==0){
		console.log("full screen change.....");
		$("#oppositeContainer").css("width","640px");
		$("#oppositeContainer").css("height","480px");
		$("#stop_fullscreen").hide();
	}
}

var accessingPatientTimeInterval;

$(document).ready(function(){
	
	connectionCount = 0;
	
	if (OT.checkSystemRequirements() == 0) {
		OT.log("The client does not support WebRTC.");
	}
	 
	// --------------------- this is the session of original session: start-------------------------------
	session = OT.initSession(apiKey, sessionId);

	session.on({
		connectionCreated: function (event) {
			OT.log(connectionCount + " connections.");
		},
		
		connectionDestroyed: function (event) {
			OT.log(connectionCount + " connections.");
			
		},
		
		sessionDisconnected: function sessionDisconnectHandler(event) {
	    // The event is defined by the SessionDisconnectEvent class
			if (event.reason == "networkDisconnected") {
				alert("Your network connection terminated.");
			}
		},

		streamCreated: function(event) {
			OT.log("New stream in the session: " + event.stream.streamId);
			var subscriberProperties = { resolution : "640x480",insertMode: "append", width:"100%", height:"100%"};
//			var subscriberProperties = {insertMode: "append", width:"100%", height:"100%"};
			session.subscribe(event.stream, 'oppositeContainer', subscriberProperties, function (error) {
				if (error) {
					alert(error);
				} else {
					$('#oppositeContainerText').remove();
					$('#noShownNote').hide();
					start();
				}
			});
		},
	});

	session.connect(token, function(error) {
		if (error) {
			alert("Unable to connect: "+error.code+", "+error.message);
			return;
		}
		
		OT.log("Connected to the session.");
		

		if (session.capabilities.publish != 1) {
			alert("You cannot publish an audio-video stream.");
			return;
		}
			
	    // Replace with the replacement element ID:
		 var publisherProperties = {resolution : "640x480", width:280, height:210};
		publisher = OT.initPublisher('selfContainer', publisherProperties, function(error){
			if (error) {
				OT.log("Unable to publish stream: ", error.message);
				return;
			}
			
			publisher.on({
			    streamCreated: function (event) {
			    		OT.log("Publisher started streaming.");
			    },
			    streamDestroyed: function (event) {
			    		OT.log("Publisher stopped streaming. Reason: "
			               + event.reason);
			    	}
		    });
			
	        if (publisher) {
	        		session.publish(publisher);
	        }
		});
	});

	$("#stopsession").click(function(){
		 window.location.assign("/"+DOCTORPATH+"getAnnoQAs/"+sessionId);
	});
	
	$("#fullScreen").click(function(){
		makeFullScreen("oppositeContainer");
	});
	
	$("#fullScreenAnyTime").click(function(){
		
		if($("#hiddenArchiveId").val()!=null && $("#hiddenArchiveId").val()!=""){
			makeFull();
		}else{
			alert("You cannot start the conversation unless two participants are both shown on this page.");
		}
//		$.ajax({
//			type : 'POST',
//			headers: getTokenHeader(),
//			url : '/'+DOCTORPATH+'/doctorstartformalconversation/'+sessionId,
//			dataType : "json",
//			success : function(data) {
//				if(data.code == 1){
//					
//				}else{
//					alert("You cannot start the conversation unless two participants are both shown on this page.");
//				}
//			}
//		});
		
		
	});
	
	
//	$("#stop_fullscreen").click(function(){
//		$("#fullScreen").click();
//	});
	
	$("#stop_fullscreen").one("click",function(){
		$("#fullScreen").click();
	});

	
	accessingPatientTimeInterval = setInterval(accessPatientLoginTime, 10000);
	displayRecordingTime = setInterval(displayRecordingTime, 10000);
	displayArchiveIdTime = setInterval(accessArchiveId, 3000);
	
});


// start the video archiving
function start(){
	var sessionIdStr = sessionId+'__2';
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/start/'+sessionIdStr,
		dataType : "json",
		success : function(data) {
			$("#hiddenArchiveId").val(data[0]);
		}
	});
}

// stop the video archiving
function stop(){

	var url = sessionId;
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/stop/'+url,
		dataType : "json",
		success : function(data) {
//			if(data.code == 1){
//				 window.open("/"+DOCTORPATH+'/perref1_stu','_self');
//		     }else{
//		    	 $("#errorInfo").show();
//		     }
			
			if(data.code == 1){
				 window.open("/"+DOCTORPATH+'/appropriate','_self');
		     }else{
		    	 $("#errorInfo").show();
		     }
		}
	});
}
	
	
function startRecording(){
	start();
	$("#fullScreen").val("Finish Interview");
}
	
var enableFull = false;

function makeFullScreen(divId) {
	
	var button = $("#fullScreen");
	
	if(button.val()=="Finish Interview"){
		stop();
		clearTimerout(timer);
		$("#timer").hide();
	}else{
		startTime = new Date().getTime();
		if($("#hiddenArchiveId").val()!=null && $("#hiddenArchiveId").val()!=""){
			makeFull();
		}
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+DOCTORPATH+'/doctorstartformalconversation/'+sessionId,
			dataType : "json",
			success : function(data) {
				if(data.code == 1){
					
				}else{
					alert("You cannot start the conversation unless two participants are both shown on this page.");
				}
			}
		});
	}
  }


function makeFull(){

	
//	var el = document.getElementById("oppositeContainer");
//	var rfs = el.requestFullScreen
//      || el.webkitRequestFullScreen
//      || el.mozRequestFullScreen;
  
//	$("#oppositeContainer").css("width","100%");
//	$("#oppositeContainer").css("height","100%");
//	rfs.call(el);
//	
	$("#fullScreen").val("Finish Interview");
	$("#stop_fullscreen").show();
	
	var element = document.getElementById("oppositeContainer");
	
    // here is what it means though
    if (element.requestFullScreen) {
      element.requestFullScreen();
    } else if (element.webkitRequestFullScreen) {
      element.webkitRequestFullScreen();
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if (element.msRequestFullScreen) {
      element.msRequestFullScreen();
    } else if (element.oRequestFullScreen) {
      element.oRequestFullScreen();
    } else {
      alert("don't know the prefix for this browser");
    }
    $("#oppositeContainer").css("width","100%");
	$("#oppositeContainer").css("height","100%");
    
}


var timer ;

$(window).bind("beforeunload",unloadChatRoom);

function unloadChatRoom(){
	var sessionIdStr = sessionId + '__2';
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/unloadchatroom/' + sessionIdStr,
		dataType : "json",
		success : function(data) {
			console.log("doctor unload chat room");
		}
	});
}

function accessPatientLoginTime(){
	var sessionIdStr = sessionId;
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/getapt/' + sessionIdStr,
		dataType : "json",
		success : function(data) {
			if(data != null){
				var apt = data;
				if(apt.patientLoginAt == null || apt.patientLoginAt == 0){
					var doctorLoginAt = apt.doctorLoginAt;
					var curTime = new Date().getTime();
					console.log(curTime-doctorLoginAt);
					// if((curTime-doctorLoginAt)>900000){
					if((curTime - apt.starttime)>900000){
						alert("Thank you for waiting. Unfortunately this appointment has been cancelled, you can now close this window");
						clearInterval(accessingPatientTimeInterval);
						
						$.ajax({
							type : 'POST',
							headers: getTokenHeader(),
							url : '/'+DOCTORPATH+'/patientnoshow/' + sessionId,
							dataType : "json",
							success : function(data) {
								if (data.code == 1) {
									
								}
							}
						});
						
					}
				}else{
					clearInterval(accessingPatientTimeInterval);
				}
			}
		}
	});
}


function displayRecordingTime(){
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/requestrecodingtime/' + sessionId,
		dataType : "json",
		success : function(data) {
			if (data.code == 1) {
				
				// split the content
				var content = data.content;
				var contents = content.split(" ");
				if(contents.length>=2){
					var timeInMinute = parseInt(contents[0]);
					if(timeInMinute>=13){
						// double the size and make it in red
						// style="font-size:20px;font-weight:bold;color:red;text-align:center; background-color:black;padding:0px;margin:0px;"
						$("#timer").css("font-size","40px");
						$("#timer").css("color","red");
					}
					$("#timer").text(data.content+"\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0");
					// $("#timer").text("15 minutes");
				}else{
					$("#timer").text(data.content+"\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0");
					/// $("#timer").text("15 minutes");
				}
				
				$("#fullScreenDiv").show();
				console.log(data.content);
			} else {
				$("#fullScreenDiv").hide();
				console.log("not start");
			}
		}
	});
}

function accessArchiveId(){
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/getArchiveId/'+sessionId,
		dataType : "json",
		success : function(data) {
			if(data.content!=null && data.content!=""){
				$("#hiddenArchiveId").val(data.content);
				$("#fullScreenAnyTime").show();
				clearInterval(displayArchiveIdTime);
			}
			console.log("access archiveId...");
		}
	});
}


