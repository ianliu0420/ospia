$(document).ready(function(){
	
	$("#saveForm").click(function(){
		var sessionType = $("input[name='sessionType']:checked").val();	
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+DOCTORPATH+'/submitdoctorrecordingtype/'+sessionType,
			dataType : "json",
			success : function(data) {
				if(data!=null){
					if(data.code == 1){
				    	 window.location.assign("/"+DOCTORPATH+"/evaluatesp_stu");
				     }else{
				    	 $("#errorInfo").show();
				     }	
				}
			}
		});
	});
});