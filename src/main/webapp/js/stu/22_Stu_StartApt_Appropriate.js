$(document).ready(function(){
	
	$("#saveForm").click(function(){
		var appropriateValue = $("input[name='appropriate']:checked").val();
		if(appropriateValue==0){
			showPopUp();
		}else{
			app_yes();
		}
	});
});


function app_yes(){
	var appropriateValue = $("input[name='appropriate']:checked").val();
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/submitappropriatevalue/'+appropriateValue,
		dataType : "json",
		success : function(data) {
			if(data!=null){
				if(data.code == 1){
					 window.open("/"+DOCTORPATH+'/perref1_stu','_self');
			    }else if(data.code==0){
			         window.open("/"+DOCTORPATH+'/inappropriate','_self');
			    }
				
			}
		}
	});
}


function app_no(){
	$.unblockUI();
}


function showPopUp() {
	$.blockUI({
		message : $('#confirmAppPopup'),
		css : {
			top : ($(window).height() - 120) / 3 + 'px',
			left : ($(window).width() - 400) / 2 + 'px',
			width : '400px',
			height : '120px',
		}
	});
}

