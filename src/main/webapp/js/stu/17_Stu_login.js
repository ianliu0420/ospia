$(document).ready(function() {
	
	if(browserDetection()==false){
//	if(1==0){
		showFirefoxWarning();
		return;
	}else{
		/*
		var formdata = {
				"firstname" : "Year",
				"lastname" : "Student",
				"username" : "z1111112@unsw.edu.au",
				"userId" : "z1111112",
				"year" : "1",
				"phase" : "1"
			};
		
		$.ajax({
			type : 'POST',
			headers : getTokenHeader(),
			// url : '/'  + '/verifyUserDetails',
			url : '/verifyUserDetails',
			contentType : 'application/json',
			dataType : "json",
			data : JSON.stringify(formdata),
			beforeSend : function() {
				showWaitingPopUp();
			},
			error: function(xhr, error){
				alert("Currently, you are prevented to access OSPIA system.");
				return;
			},
			success : function(doctor) {
				if (doctor != null) {
					$("#j_username").val(doctor.username);
					$("#j_password").val(doctor.password);
					$("#submit_login").click();
					cancelPopup();
				}else{
					alert("Currently, you are prevented to access OSPIA system.");
				}
			}
		});
		*/
		
		$.ajax({
			type : 'GET',
			url : 'https://emed.med.unsw.edu.au/servlet/GetUserDetails',
			dataType : 'JSONP',
			jsonp : "jsoncallback",
			jsonpCallback : 'success_jsonCallback',
			beforeSend : function() {
				showWaitingPopUp();
			},
			error: function(xhr, error){
				alert("Log in fail, please try again from eMed system. ");
				return;
			},
			success : function(data) {
				
				if(data==null || data['username']=="INVALID"){
					alert("Log in fail, please try again from eMed system. ");
					return;
				}
				
				
				var formdata = {
				"firstname" : data['firstname'],
				"lastname" : data['lastname'],
				"username" : data['email'],
			    "userId" : data['username'],
				"year" : data['year'],
				"phase" : data['phase']
			    };
				

				$.ajax({
					type : 'POST',
					headers : getTokenHeader(),
					url : '/verifyUserDetails',
					contentType : 'application/json',
					dataType : "json",
					data : JSON.stringify(formdata),
					beforeSend : function() {
						showWaitingPopUp();
					},
					error: function(xhr, error){
						alert("Currently, you are prevented to access OSPIA system.");
						return;
					},
					success : function(doctor) {
						if (doctor != null) {
							$("#j_username").val(doctor.username);
							$("#j_password").val(doctor.password);
							$("#submit_login").click();
							// cancelPopup();
						}else{
							alert("Currently, you are prevented to access OSPIA system.");
						}
					}
				});

			}
		});
		validate();
	}

});

function getIframeContent(frameId) {
	var frameObj = document.getElementById(frameId);
	var frameContent = frameObj.contentWindow.document.body.innerHTML;
	alert("frame content : " + frameContent);
}

function accessIFrame() {
	console.log($("#iframeHide").html());

	var iframe = $("#iframeHide");

}

function validate() {
	$("#loginForm").validate({
		rules : {
			j_username : {
				required : true,
				notblank : true
			},
			j_password : {
				required : true,
				notblank : true
			}
		},
		messages : {
			j_username : {
				required : "Please enter your username",
				notblank : "Not a valid firstname"
			},
			j_password : {
				required : "Please provide a password",
				minlength : "Your password must be 6-20 characters long"
			}
		}
	});
}

function cancelPopup() {
	$.unblockUI();
}

function showWaitingPopUp() {
	$.blockUI({
		message : $('#waitingPopup'),
		css : {
			top : ($(window).height() - 50) / 3 + 'px',
			left : ($(window).width() - 200) / 2 + 'px',
			width : '200px',
			height : '50px'
		}
	});
}


function browserDetection(){
	
	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
	var isFirefox = typeof InstallTrigger !== 'undefined';
	var isIE = false || !!document.documentMode;
	
	if((isChrome===false &&isFirefox===false) || isIE===true){
		return false;
	}
}

function showFirefoxWarning() {
	$.blockUI({
		message : $('#firefoxWarning'),
		css : {
			top : ($(window).height() - 100) / 3 + 'px',
			left : ($(window).width() - 500) / 2 + 'px',
			width : '500px',
			height : '100px'
		}
	});
}



