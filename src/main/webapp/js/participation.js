
$(document).ready(function(){
	
	$("#agreeBtn").click(function(){
		$("#spNameMissing").hide();
		
		var spName = $("#spName").val();
		if(spName==null || spName==""){
			$("#spNameMissing").show();
			return;
		}
		
		var formdata = {
			spName : spName,
			agree: 1
		};

		var r = confirm("Do you want to accept above items?");
		if (r == true) {
			$.ajax({
				type : 'POST',
				headers: getTokenHeader(),
				url : '/saveparticipation',
				contentType : 'application/json',
				dataType : "json",
				data : JSON.stringify(formdata),
				beforeSend : function() {
					$("#agreeBtn").attr("disabled", true);
					$("#agreeBtn").val("submitting");
				},
				error: function(data){
					$("#agreeBtn").val("AGREE");
					$("#agreeBtn").removeAttr("disabled");
					alert("Register fail, please refresh the page and submit again");
				},
				success : function(data) {
					$("#agreeBtn").val("AGREE");
					$("#agreeBtn").removeAttr("disabled");
					
					if (data !== null) {
						if (data.code == 1) {
							alert("Submission success!");
							 window.location.href="https://poscomp.wufoo.eu/forms/r1epo5vx1kcinfx/";
						} else {
							alert("Submission fail, please submit again");
						}
					}
				}
			});
		}
		
	});
	
	$("#declineBtn").click(function(){
		$("#spNameMissing").hide();
		
		var spName = $("#spName").val();
		if(spName==null || spName==""){
			$("#spNameMissing").show();
			return;
		}
		
		var formdata = {
			spName : spName,
			agree: 2
		};

		var r = confirm("Do you want to decline above items?");
		if (r == true) {
			$.ajax({
				type : 'POST',
				headers: getTokenHeader(),
				url : '/saveparticipation',
				contentType : 'application/json',
				dataType : "json",
				data : JSON.stringify(formdata),
				beforeSend : function() {
					$("#declineBtn").attr("disabled", true);
					$("#declineBtn").val("submitting");
				},
				error: function(data){
					$("#declineBtn").val("DECLINE");
					$("#declineBtn").removeAttr("disabled");
					alert("Register fail, please refresh the page and submit again");
				},
				success : function(data) {
					$("#declineBtn").val("DECLINE");
					$("#declineBtn").removeAttr("disabled");
					
					if (data !== null) {
						if (data.code == 1) {
							alert("Submission success!");
							 window.location.href="participation_decline.jsp";
						} else {
							alert("Submission fail, please submit again");
						}
					}
				}
			});
		}
	
	});
	
});
	
