$(document).ready(function() {
	
	$("#submit_reset").click(function(){
		
		var emailValue = $("#email").val();
		var formdata = {"username" : emailValue};
		
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+PATIENTPATH+'/resetPwd',
			contentType : 'application/json',
			dataType : "json",
			data : JSON.stringify(formdata),
			beforeSend : function() {
				showWaitingPopUp();
			},
			success : function(data) {
				if(data!==null){
					cancelPopup();
					alert(data.content);
				}
			}
		});
	});
});

function cancelPopup() {
	$.unblockUI();
}

function showWaitingPopUp() {
	$.blockUI({
		message : $('#waitingPopup'),
		css : {
			top : ($(window).height() - 50) / 2 + 'px',
			left : ($(window).width() - 200) / 2 + 'px',
			width : '200px',
			height : '50px',
		}
	});
}
