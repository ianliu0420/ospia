var curentDayEvent = null;

$(document).ready(function() {
	accessGCal();
	accessAllAvailableSce();
});

function drawFullCal(myevents) {
	$('#calendar').fullCalendar({
		header : {
			// left: 'prev,next today',
			left : 'prev,next',
			center : 'title',
			// right: 'month,agendaWeek,agendaDay'
			right : ''
		},
		timezone: 'local',
		defaultDate : getCurrentDateStr(),
		selectable : true,
		selectHelper : true,
		select : function(start, end) {
			loadEvent(start);
		},
		editable : false,
		eventLimit : true, // allow "more" link when too many events
		events : myevents,
		eventClick: function(calEvent, jsEvent, view){
			loadEvent(calEvent.start);
		}
	});
}

function accessGCal() {
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/patientcalendar',
		dataType : "json",
		beforeSend : function() {
			showWaitingPopUp();
		},
		success : function(data) {

			var events = [];
			for (var i = 0; i < data.length; i++) {
				var apt = data[i];
				var startValue = convertToDate(apt.starttime);
				var endValue = convertToDate(apt.endtime);
				var summary = apt.doctorname;
				if (apt.status == 1) {
					color = "green";
					// summary = apt.applicationNo +" applications ";
				} else if (apt.status == 2) {
					color = "red";
				} else if (apt.status == 3) {
					color = "blue";
				} else {
					color = "orange";
				}
				var tempEvent = {
					title : summary,
					start : startValue,
					end : endValue,
					color : color
				};
				events.push(tempEvent);
			}
			drawFullCal(events);
			cancelPopup();
		}
	});
}

function accessAllAvailableSce() {
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/getallscenarios',
		dataType : "json",
		success : function(data) {
			var scenarioSelect = $("#scenario");
			// scenariosSelect.empty();
			// var defaultSelect = "<option value='0' selected='selected'>Scenario</option>";
			// scenarioSelect.append(defaultSelect);
			
			var trainedInTotal = 0;
			
			for (var i = 0; i < data.length; i++) {
				if (data[i].trained == 1) {
					trainedInTotal++;
					var temp = "<option selected='selected' value='" + data[i].id + "'>"+
							data[i].code + "</option>";
					scenarioSelect.append(temp);
				}
			}
			if(trainedInTotal===0){
				var temp = "<option selected='selected' value='-1'>-1</option>";
				scenarioSelect.append(temp);
			}
		}
	});
}

function insertEvent() {

	
	var d = new Date();
	var timeZoneOffset = d.getTimezoneOffset();
	var difference = (timeZoneOffset+660)/60;
	
	
	var insertEventId = $("#insertEventId").val();
	var length = parseInt($("#insertEventLength").val());
	length = 30;
	var scenario = parseInt($("#scenario").val());

	if(scenario==-1){
		alert("Please finish your scenario training first.");
		return;
	}
	
	if ($("#insertEventYear").val() == "0"||
			$("#insertEventMonth").val() == "0"||
			$("#insertEventDate").val() == "0"||
			$("#insertEventHour").val() == "-1"||
			$("#insertEventMinute").val() == "-1") {
		$("#newAptErrorInfo").text("Please select a correct time");
		$("#newAptErrorInfo").show();
		return;
	} else {
		$("#newAptErrorInfo").text("");
		$("#newAptErrorInfo").show();
	}

	if (scenario === 0) {
		$("#newAptErrorInfo").text("Please select a secenario");
		$("#newAptErrorInfo").show();
		return;
	} else {
		$("#newAptErrorInfo").text("");
		$("#newAptErrorInfo").show();
	}

	$("#insertEventBtn").attr('disabled', 'disabled');

	var insertDateDate = convertSingleToDouble($("#insertEventYear").val())+
			"-" + convertSingleToDouble($("#insertEventMonth").val()) + "-"+
			convertSingleToDouble($("#insertEventDate").val());
	
	
	var insertEventAP = parseInt($("#insertEventAP").val());
	var insertedHour = parseInt($("#insertEventHour").val());
	if(insertEventAP === 0){
		// insertedHour = insertedHour;
	}else if(insertEventAP == 1){
		insertedHour = insertedHour + 12;
	}
	var insertDateTimeStart = convertSingleToDouble(insertedHour)+ ":"+ convertSingleToDouble($("#insertEventMinute").val())+ ":00";

	var insertDateTimeEnd = "";
	if (parseInt($("#insertEventMinute").val()) + length >= 60) {
		insertDateTimeEnd = convertSingleToDouble(insertedHour + 1)	+ ":"+ convertSingleToDouble(parseInt($("#insertEventMinute").val())+ length - 60) + ":00";
	} else {
		insertDateTimeEnd = convertSingleToDouble(insertedHour)+ ":"+ convertSingleToDouble(parseInt($("#insertEventMinute").val())+ length) + ":00";
	}
	var formdata;

	if (insertEventId === "") {
		formdata = {
			"insertDateDate" : insertDateDate+","+difference,
			"insertDateTimeStart" : insertDateTimeStart,
			"insertDateTimeEnd" : insertDateTimeEnd,
			"scenario" : scenario
		};
	} else {	
		formdata = {
			"aptid" : insertEventId,
			"insertDateDate" : insertDateDate+","+difference,
			"insertDateTimeStart" : insertDateTimeStart,
			"insertDateTimeEnd" : insertDateTimeEnd,
			"scenario" : scenario
		};

	}
	
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/patientcreateapt',
		dataType : "json",
		data : formdata,
		beforeSend : function() {
			$("#newAptErrorInfo").text(
					"We are processing, please wait a moment.....");
			$("#newAptErrorInfo").show();
		},
		success : function(data) {
			if (data !== null) {
				if (data.code == 1) {
					cancelPopup();
					location.reload(true);
				} else if (data.code === 0 || data.code == -2) {
					$("#newAptErrorInfo").text(data.content);
					$("#newAptErrorInfo").show();
				}else {
					$("#newAptErrorInfo").text(
							"save failed, please try again");
					$("#newAptErrorInfo").show();
				}
				$("#insertEventBtn").removeAttr('disabled');
			}

		}
	});
}

function getCurrentDateStr() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; // January is 0!
	var yyyy = today.getFullYear();

	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}

	today = yyyy + "-" + mm + "-" + dd;
	return today;
}

function showAddEventPopUp(start, end) {
	
	if($("#scenario").val()==-1){
		alert("You have to finish the training before you can have a consultaion.");
		return;
	}
	
	
	var dateInfo = new Date();
	var date = dateInfo.getDate();
	var month = dateInfo.getMonth() + 1;
	var year = dateInfo.getFullYear();

	$("#insertEventDate").val(date);
	$("#insertEventMonth").val(month);
	$("#insertEventYear").val(year);

	$.blockUI({
		message : $('#addEventPopup'),
		css : {
			top : ($(window).height() - 400) / 2 + 'px',
			left : ($(window).width() - 400) / 2 + 'px',
			width : '500px',
			height : '260px',
		}
	});
	$('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);
}

function showEditEventPopUp(editEvent) {

	var dateInfo = new Date(editEvent.starttime);
	var date = dateInfo.getDate();
	var month = dateInfo.getMonth() + 1;
	var year = dateInfo.getFullYear();
	var hour = dateInfo.getHours();
	
	if(hour<12){
		$("#insertEventAP").val(0);
	}else{
		$("#insertEventAP").val(1);
		hour = hour-12;
	}
	
	var minute = dateInfo.getMinutes();

	$("#insertEventDate").val(date);
	$("#insertEventMonth").val(month);
	$("#insertEventYear").val(year);
	$("#insertEventHour").val(hour);
	$("#insertEventMinute").val(minute);
	$("#scenario").val(editEvent.scenario);
	$("#insertEventId").val(editEvent.aptid);

	$.blockUI({
		message : $('#addEventPopup'),
		css : {
			top : ($(window).height() - 400) / 2 + 'px',
			left : ($(window).width() - 400) / 2 + 'px',
			width : '500px',
			height : '260px',
		}
	});
	$('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);
}

function loadEvent(start) {

	var dateInfo = start._d;
	var date = dateInfo.getDate();
	var month = dateInfo.getMonth() + 1;
	var year = dateInfo.getFullYear();
	var getEventWDDate = year + "-" + convertSingleToDouble(month) + "-"+ convertSingleToDouble(date);
	$("#dateSpan").text("Appointments on " + convertNumnberDateToString(getEventWDDate));
	$("#dateSpan").parent().show();
	
	var formdata = {
		"getEventWDDate" : getEventWDDate
	};
	$.ajax({
				type : 'POST',
				headers: getTokenHeader(),
				url : '/'+PATIENTPATH+'/getpatientaptswholedate',
				dataType : "json",
				data : formdata,
				beforeSend : function() {
					showWaitingPopUp();
				},
				success : function(data) {
					$("#tbodyList").empty();
					curentDayEvent = data;
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							var apt = data[i];
							
							var reqInfo = convertToDate(apt.starttime).split(
									"T");
							var trEle = "";

							if (apt.status == 1) {
								trEle = "<tr><td>"
										+ convertNumnberDateToString(reqInfo[0])
										+ "</td><td>"
										+ convertFullDateToAPTime(convertToDate(apt.starttime))
										+ "</td><td>"
										+ ""
										+ "</td>"
										+ "<td><input type='button' value='Edit' onclick='editApt("
										+ apt.aptid+","+apt.applicationNo
										+ ")' class='usyd-ui-button-primary'>&nbsp;&nbsp;"
										+ "<input type='button' value='Delete' onclick='deleteApt("
										+ apt.aptid
										+ ")' class='usyd-ui-button-primary'></td>"
										+ "</tr>";
							} else if (apt.status == 2) {
								trEle = "<tr><td>"
										+ convertNumnberDateToString(reqInfo[0])
										+ "</td><td>"
										+ convertFullDateToAPTime(convertToDate(apt.starttime))
										+ "</td><td>"
										+ apt.doctorname
										+ "</td>"
										+ "<td><input type='button' value='Confirm' onclick='confirmApt("
										+ apt.aptid
										+ ")' class='usyd-ui-button-primary'>&nbsp;&nbsp;"
										+ "<input type='button' value='Decline' onclick='declineAptPopup("
										+ apt.aptid
										+ ")' class='usyd-ui-button-primary'></td>"
										+ "</tr>";
							} else if (apt.status == 3) {
								trEle = "<tr><td>"
										+ convertNumnberDateToString(reqInfo[0])
										+ "</td><td>"
										+ convertFullDateToAPTime(convertToDate(apt.starttime))
										+ "</td><td>"
										+ apt.doctorname
										+ "</td>"
										+ "<td><input type='button' value='Cancel' onclick='cancelApt("
										+ apt.aptid
										+ ")' class='usyd-ui-button-primary'></td>"
										+ "</tr>";
							} else {
								var docName;
								if(apt.doctorname==null){
									docName="";
								}else{
									docName = apt.doctorname;
								}
								
								trEle = "<tr><td>" + convertNumnberDateToString(reqInfo[0]) + "</td><td>"
										+ convertFullDateToAPTime(convertToDate(apt.starttime)) + "</td><td>"
										+ docName + "</td>"
										+ "<td>&nbsp;Unavailable</td></tr>";
							}

							$("#tbodyList").append(trEle);
						}
						
						$("#emptyNote").hide();
						$("#scrollTable").show();
					} else {
						
						$("#scrollTable").hide();
						$("#emptyNote").show();
					}
					cancelPopup();
				}
			});

}

function showDeleteAptPopup() {

	$.blockUI({
		message : $('#deleteAptPopup'),
		css : {
			top : ($(window).height() - 400) / 2 + 'px',
			left : ($(window).width() - 400) / 2 + 'px',
			width : '300px',
			height : '100px',
		}
	});
	$('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);

}

function showCancelAptPopup() {
	$.blockUI({
		message : $('#cancelAptPopup'),
		css : {
			top : ($(window).height() - 400) / 2 + 'px',
			left : ($(window).width() - 400) / 2 + 'px',
			width : '400px',
			height : '220px',
		}
	});
	$('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);
}

function showComfirmAptPopup() {
	$.blockUI({
		message : $('#confirmAptPopup'),
		css : {
			top : ($(window).height() - 400) / 2 + 'px',
			left : ($(window).width() - 400) / 2 + 'px',
			width : '300px',
			height : '120px',
		}
	});
	$('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);
}

function showDeclineAptPopup() {
	$.blockUI({
		message : $('#declineAptPopup'),
		css : {
			top : ($(window).height() - 400) / 2 + 'px',
			left : ($(window).width() - 400) / 2 + 'px',
			width : '300px',
			height : '120px',
		}
	});
	$('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);
}

function cancelPopup() {
	$.unblockUI();
}

function editApt(aptId, applicationNo) {
	
	if(applicationNo>0){
		alert("There are "+applicationNo+" applications on this appointment, you cannot edit it.");
		return;
	}
	
	var editEvent=0;
	for (var i = 0; i < curentDayEvent.length; i++) {
		if (curentDayEvent[i].aptid == aptId) {
			editEvent = curentDayEvent[i];
		}
	}
	showEditEventPopUp(editEvent);
}

function deleteApt(aptId) {
	$("#deleteAptId").val(aptId);
	showDeleteAptPopup();
}

function deleteAptConfirm() {

	var aptId = $("#deleteAptId").val();
	var formdata = {
		"aptId" : aptId
	};

	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/patientdeleteapt',
		dataType : "json",
		data : formdata,
		success : function(data) {
			if(data!==null){
				if (data.code == 1) {
					cancelPopup();
					location.reload();
				} else {
					alert(data.content);
				}	
			}
		}
	});
}

function confirmApt(aptId) {
	$("#confirmAptId").val(aptId);
	showComfirmAptPopup();
}

function declineAptPopup(aptId) {
	$("#confirmAptId").val(aptId);
	showDeclineAptPopup();
}

function acceptApt(type) {
	// type: 1:accept 0:decline
	var aptId = $("#confirmAptId").val();
	var formdata = {
		"aptId" : aptId,
		"type" : type
	};

	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/patientconfirmapt',
		dataType : "json",
		data : formdata,
		beforeSend : function() {
			$("#declineWait"+type).show();
		},
		success : function(data) {
			if(data!==null){
				if (data.code == 1) {
					cancelPopup();
					location.reload();
				} else {
					alert("Confirm failed, please try again");
				}	
			}
		}
	});
//	cancelPopup();
}

function cancelApt(aptId) {
	$("#cancelAptId").val(aptId);
	showCancelAptPopup();
}

function confirmCancelApt() {
	var aptId = $("#cancelAptId").val();
	var cancelReason = $("#cancelReason").val();
	var formdata = {
		"aptId" : aptId,
		"cancelReason" : cancelReason,
		"cancelPerson" : 2
	};
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/cancelapt',
		dataType : "json",
		data : formdata,
		beforeSend : function() {
			$("#cancelAptErrorInfo").text(
					"We are processing, please wait a moment.....");
			$("#cancelAptErrorInfo").show();
		},
		success : function(data) {
			if (data !== null) {
				if (data.code == 1) {
					alert("Operation success");
					cancelPopup();
					location.reload();
				} else {
					alert("Confirm failed, please try again");
				}
			}
			$("#cancelAptErrorInfo").hide();
		}
	});
}


function showWaitingPopUp() {
	$.blockUI({
		message : $('#waitingPopup'),
		css : {
			top : ($(window).height() - 50) / 2 + 'px',
			left : ($(window).width() - 200) / 2 + 'px',
			width : '200px',
			height : '50px',
		}
	});
}
