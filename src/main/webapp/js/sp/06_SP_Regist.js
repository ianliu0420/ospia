$(document).ready(function() {

	validate();
	
});

function submitForm(){
	
	var health = $("#health").val();
	var title = $("#title").val();
	var firstname = $.trim($("#firstname").val());
	var lastname = $.trim($("#lastname").val());
	var gender = parseInt($('input[name=gender]:checked').val());
	var phone = $("#phone").val();
	var email = $("#email").val();
	
	var street = $("#street").val();
	var suburb = $("#suburb").val();
	var state = $("#state").val();
	var postcode = $("#postcode").val();
	var country = $("#country").val();
	var language = $("#language").val();
	var reason = $("#reason").val();
	var ageRange = $("#agerange").val();
	var password = $("#password").val();
		
	var formdata = {
		health : health,
		title : title,
		firstname : firstname,
		lastname : lastname,
		username : email,
		gender : gender,
		phone : phone,
		email : email,
		street:street,
		suburb:suburb,
		state:state,
		postcode:postcode,
		country:country,
		language:language,
		reason:reason,
		age:ageRange,
		password : password,
		status : 1
	};

	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/savepatient',
		contentType : 'application/json',
		dataType : "json",
		data : JSON.stringify(formdata),
		beforeSend : function() {
			$("#registerBtn").attr("disabled", true);
			$("#registerBtn").val("submitting");
		},
		error: function(data){
			$("#registerBtn").val("register");
			$("#registerBtn").removeAttr("disabled");
			alert("Register fail, please refresh the page and register again");
		},
		success : function(data) {
			$("#registerBtn").val("register");
			$("#registerBtn").removeAttr("disabled");
			
			if (data !== null) {
				if (data.code == 1) {
					alert("Register success!");
					window.location.assign("/"+PATIENTPATH+"/ulogin_sp");
				} else {
					$("#registerError").text(data.content);
					$("#registerErroDiv").show();
				}
			}
		}
	});
}


function validate(){
	
	$("#signupForm").validate({
		rules: {
//			health: {
//				required:true,
//				notblank:true,
//			},
			title: {
				required:true,
				notblank:true
			},
			firstname: {
				required:true,
				notblank:true
			},
			lastname: {
				required:true,
				notblank:true
			},
			agerange: {
				required: true,
				valueNotEquals: 0
			},
			phone: {
				required: false,
			    digits: true,
			    minlength: 10,
			    maxlength: 10
			},
			email: {
				required: true,
				email: true
			},
			street: {
				//required: true,
				//notblank:true
			},
			suburb: {
				//required: true,
				//notblank:true
			},
			postcode: {
				required: false,
				postcode:true
			},
			country: {
				required: true,
				notblank:true
			},
			language: {
				required: true,
				notblank:true
			},
			reason: {
				required: true,
				notblank:true
			},
			password: {
				required: true,
				minlength: 6,
				digitletteronly: true
			},
			passwordcfm: {
				required: true,
				minlength: 6,
				equalTo: "#password"
			}
			
//			year:{
//				validDate: ['day','month','year']
//			}
		},
		messages: {
//			health: {
//				required:"Please enter your health description",
//				notblank:"Not a valid description",
//			},
			title: {
				required:"Please enter your title",
				notblank:"Not a valid title"
			},
			firstname: {
				required:"Please enter your firstname",
				notblank:"Not a valid firstname"
			},
			lastname: {
				required:"Please enter your lastname",
				notblank:"Not a valid firstname"
			},
			agerange: {
				required: "Please select a age range",
				valueNotEquals: "Please select a age range"
			},
			phone: {
				required: "Please enter a phone number",
			    digits: "Invalid phone number",
			    minlength: "Invalid phone number",
			    maxlength: "Invalid phone number"
			},
			email: "Please enter a valid email address",
			street: {
				//required: "Please enter your address",
				//notblank: "Please enter your address"
			},
			suburb: {
				//required: "Please enter your suburb",
				//notblank: "Please enter your suburb"
			},
			postcode: {
				//required: "Please enter your post code",
				//postcode: "Please enter a valid post code"
				
			},
			country: {
				required: "Please enter your country",
				notblank: "Please enter your country"
			},
			language: {
				required: "Please enter your fluent spoken language",
				notblank: "Please enter your fluent spoken language"
			},
			reason: {
				required: "Please enter how did you know about this program",
				notblank: "Please enter how did you know about this program"
			},
			
			password: {
				required: "Please provide a password",
				digitletteronly: "Your password can only contains letters and digits",
				minlength: "Your password must be at least 6 characters or numbers"
			},
			passwordcfm: {
				required: "Please provide a password",
				minlength: "Your password must be at least 6 characters or numbers",
				equalTo: "Please enter the same password as above"
			},
			
			year: {
				validDate: "Please select a valid date"
			}
		},
		
		submitHandler: function(form) {  
            if ($(form).valid()){
            	submitForm();
            }
            return false; 
		}
		
	});
	
	
}
