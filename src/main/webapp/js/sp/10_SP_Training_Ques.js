$(document)
		.ready(
				function() {

					// submit the answer for the training
					$("#submitAnswer")
							.click(
									function() {
										
										var sceId = parseInt($("#sceId").val());
										var formdata = {
											"sceId" : sceId
										};
										$
												.ajax({
													type : 'POST',
													headers: getTokenHeader(),
													url : '/'+PATIENTPATH+'/sceanswer',
													dataType : "json",
													data : formdata,
													success : function(data) {
														if (data !== null) {
															if (data.code == 1) {
																alert("You have fulfilled the requirements of this scenario. Now you can provide consultations.");
																window.location
																		.assign("/"+PATIENTPATH+"/train_sp");
															} else {
																$("#errorInfo")
																		.show();
															}
														}
													}
												});
									});

				});