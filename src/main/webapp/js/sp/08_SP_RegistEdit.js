$(document).ready(function() {
	loadPatient(patientid);
});

function loadPatient(patientid) {
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/getpatient/' + patientid,
		dataType : "json",
		success : function(data) {
			if (data !== null) {
				var patient = data;
				$("#name").val(patient.firstname + " " + patient.lastname);
				$("#username").val(patient.username);
			}
		}
	});
}
