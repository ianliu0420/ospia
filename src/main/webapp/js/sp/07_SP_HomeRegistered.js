$(document).ready(function(){
	// browserDetection();
	loadRequestFromStu();
	loadNextAptsSP();
	loadUnfinishedConversation();
	
	
	var agreement = $("#agreement").val();
	var homeloaded = $("#homeloaded").val();
	
	// hide the agreement due to the ethics
	
	 if(agreement == 0 && homeloaded==0){
		$.blockUI({
			message : $('#agreementPopUp'),
			css : {
				top : ($(window).height() - 560) / 2 + 'px',
				left : ($(window).width() - 500) / 2 + 'px',
				width : '500px',
				height : '560px',
			}
		});
		
		setTimeout(function(){
			$('#agreementPopUp').scrollTop(-200);
		},100);
	 }
	
	$("#submitAgreement").click(function(){
		
		var patientid = $("#patientid").val();
		var agreement = $("input[name='agreement']:checked").val();	
		if(agreement == 1){
			var formdata = {"patientid" : patientid,	"agreement" : agreement };
			$.ajax({
				type : 'POST',
				headers: getTokenHeader(),
				url : '/'+PATIENTPATH+'/updateagreement',
				contentType : 'application/json',
				dataType : "json",
				data : JSON.stringify(formdata),
				success : function(data) {
					if(data!==null){
						if (data.code == 1) {
							$.unblockUI();
						} else {
							alert("Submit failed, please try again");
						}	
					}
				}
			});
		}else{
			$.unblockUI();
		}
	});
	
	
});


function loadRequestFromStu(){
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/getpatientpendingrequest',
		dataType : "json",
		beforeSend : function() {
			$("#requestDivLoading").show();
		},
		success : function(data) {
			$("#requestDivLoading").hide();
			$("#requestList").empty();
			
			if(data.length>0){
				for(var i = 0; i<data.length ; i++){
					var apt = data[i];
					var reqInfo = convertToDate(apt.starttime).split("T");
					var trEle="";
					
					trEle = "<tr><td>"+convertNumnberDateToString(reqInfo[0])+"</td><td>"+
					   convertFullDateToAPTime(convertToDate(apt.starttime))+"</td><td>"+apt.doctorname+"</td>" +
			           "<td><input type='button' value='Confirm' onclick='confirmApt("+apt.aptid+")' class='usyd-ui-button-primary'>&nbsp;&nbsp;"+
			           "<input type='button' value='Decline' onclick='declineApt("+apt.aptid+")' class='usyd-ui-button-primary'></td>"+
			           "</tr>";

					$("#requestList").append(trEle);
				}
				$("#requestTable").show();
			}else{
				var obj = $("#requestTable").parent();
				obj.hide();
				$("#requestTable").hide();
				$("#requestNote").html("<br>You have no requests from students.");
			}
		}
	});
}

function loadNextAptsSP(){
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/getpatientnextapts',
		dataType : "json",
		beforeSend : function() {
			$("#nextaptDivLoading").show();
		},
		success : function(data) {
			$("#nextaptDivLoading").hide();
			
			$("#aptList").empty();
			
			if(data.length>0){
				for(var i = 0; i<data.length ; i++){
					var apt = data[i];
					var reqInfo = convertToDate(apt.starttime).split("T");
					var trEle="";
					trEle = "<tr><td>"+convertNumnberDateToString(reqInfo[0])+"</td><td>"+convertFullDateToAPTime(convertToDate(apt.starttime))+
					"</td><td>"+apt.doctorname+"</td>" +
			        "<td><input type='button' value='Start Appointment' onclick='startApt("+apt.aptid+")' class='usyd-ui-button-primary'>"+
			        "</tr>";

					$("#aptList").append(trEle);
				}
				$("#aptTable").show();
			}else{
				var obj = $("#aptTable").parent();
				obj.hide();
				$("#aptTable").hide();
				$("#aptNote").html("<br>You have no appointments in the next two weeks.");
			}
		}
	});
}


function confirmApt(aptId){
	if (confirm("Accept this request?")) {  
		var formdata = {"aptId":aptId,"type":1};
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+PATIENTPATH+'/patientconfirmapt',
			dataType : "json",
			data : formdata,
			beforeSend : function() {
				showWaitingPopUp();
			},
			success : function(data) {
				if(data.code == 1){
					location.reload();
			     }else{
			    	 alert("Confirm failed, please try again");
			     }
				cancelPopup();
			}
		});
    } 
}


function declineApt(aptId){
	if (confirm("Decline this request?")) {  
		var formdata = {"aptId":aptId,"type":0};
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+PATIENTPATH+'/patientconfirmapt',
			dataType : "json",
			data : formdata,
			beforeSend : function() {
				showWaitingPopUp();
			},
			success : function(data) {
				if(data.code == 1){
					location.reload();
			     }else{
			    	 alert("Confirm failed, please try again");
			     }
				 cancelPopup();
			}
		});
    } 
}


function loadUnfinishedConversation(){
	
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/cons_sp',
		dataType : "json",
		beforeSend : function() {
			$("#unfinishedDivLoading").show();
		},
		success : function(data) {
			$("#unfinishedDivLoading").hide();
			$("#unfinishedAptList").empty();
			if(data.length>0){
				for(var i = 0; i<data.length ; i++){
					var video = data[i][0];
					var appointmentProgress = data[i][1];
					var trEle = "";
					if(appointmentProgress.hasFinish===0){
						trEle = "<tr><td>"+convertNumnberDateToString(video.aptDate)+"</td><td>"+convertFullDateToAPTime(video.aptDate+" "+video.aptTime)+"</td><td>"+video.doctorName+"</td>" +
				           "<td><a href='/"+PATIENTPATH+"/resumepatient/"+appointmentProgress.id+"'>Continue to finish surveys</a></td>"+
				           "</tr>";
					}
					$("#unfinishedAptList").append(trEle);
				}
				$("#unfinishedAptTable").show();
			}else{
				$("#unfinishedAptTable").hide();
				$("#unfinishedAptNote").html("<br>You have no unfinished consultations.");
			}
		}
	});
	
}


function startApt(aptId){
	window.location.href="/"+PATIENTPATH+"/spbaseline/"+aptId;
}

function showWaitingPopUp() {
	$.blockUI({
		message : $('#waitingPopup'),
		css : {
			top : ($(window).height() - 50) / 2 + 'px',
			left : ($(window).width() - 200) / 2 + 'px',
			width : '200px',
			height : '50px',
		}
	});
}

function browserDetection(){
	
	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
	var isFirefox = typeof InstallTrigger !== 'undefined';
	var isIE = false || !!document.documentMode;
	
	if((isChrome===false &&isFirefox===false) || isIE===true){
		alert("OSPIA only supports Firefox web browser,and please make sure you are using Firefox. Sorry for any inconvenience.");
	}
}