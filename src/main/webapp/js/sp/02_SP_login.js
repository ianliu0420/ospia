$(document).ready(function() {
	browserDetection();
	validate();
});

function validate(){
	
	$("#loginForm").validate({
		rules: {
			j_username: {
				required:true,
				notblank:true
			},
			j_password: {
				required: true,
				notblank:true
			}
		},
		messages: {
			j_username: {
				required:"Please enter your username",
				notblank:"Not a valid firstname"
			},
			j_password: {
				required: "Please provide a password",
				minlength: "Your password must be 6-20 characters long"
			}
		}
		
	});
	
	
}

function browserDetection(){
	
	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
	var isFirefox = typeof InstallTrigger !== 'undefined';
	var isIE = false || !!document.documentMode;
	
	if((isChrome===false &&isFirefox===false) || isIE===true){
		// showWaitingPopUp();
		alert("Kind reminder: You can only use Chrome or Firefox browsers for OSPIA consultations");
	}
}

function showWaitingPopUp() {
	$.blockUI({
		message : $('#waitingPopup'),
		css : {
			top : ($(window).height() - 100) / 3 + 'px',
			left : ($(window).width() - 500) / 2 + 'px',
			width : '500px',
			height : '100px'
		}
	});
}

