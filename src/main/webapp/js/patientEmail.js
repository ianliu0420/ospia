var emailId;
$(document).ready(function(){
	
	$("#submitFormBtn").click(function(){
		
		var multiAnswer="";
		for(var i=1;i<=$("#totalLength").val();i++){
			var aptId = $("#feedback"+i+"_aptId").val();
			var answerValue = $("#feedback"+i+"_answer").val();
			
			if(answerValue==""){
				alert("Please fill out all the required fields!");
				return;
			}
			
			var group=aptId+","+answerValue+";";
			multiAnswer+=group;
		}
		
		var q1 = $("#q1").val();
		var q2 = $("#q2").val();
		var q3 = $("#q3").val();
		var q4 = $("#q4").val();
		emailId = $("#emailId").val(); 
		
		if(q1=="" ||q2==""||q3==""){
			alert("Please fill out all the required fields!");
			return;
		}
		
		
		var formdata = {
			emailId : emailId,
			q1 : q1,
			q2 : q2,
			q3 : q3,
			q4 : q4,
			q5 : multiAnswer
		};

		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/savepatientq',
			contentType : 'application/json',
			dataType : "json",
			data : JSON.stringify(formdata),
			beforeSend : function() {
				$("#registerBtn").attr("disabled", true);
				$("#registerBtn").val("submitting");
			},
			error: function(data){
				$("#registerBtn").val("register");
				$("#registerBtn").removeAttr("disabled");
				alert("Register fail, please refresh the page and register again");
			},
			success : function(data) {
				// patientPutMoreAppt();
				if (data !== null) {
					if (data.code == 1) {
						// patientPutMoreAppt();
					    if (confirm('Thanks for completing the form. Would you like to book more appointments now?')) {
					    	window.location.assign("/putMoreApptYes/"+emailId);
					    } else {
					    	window.location.assign("/putMoreApptNo/"+emailId);
					    	//window.close();
					    }
					}
				}
				
			}
		});
	});
	
});
	
function patientPutMoreAppt(){
	
//	$.blockUI({
//		message : $('#putMoreAppt'),
//		css : {
//			top : ($(window).height() - 400) / 2 + 'px',
//			left : ($(window).width() - 400) / 2 + 'px',
//			width : '500px',
//			height : '200px',
//		}
//	});
//	$('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);
	
// 	$( "#dialog" ).dialog();
	
}

function cancelPopup() {
	// $.unblockUI();
	$( "#dialog" ).dialog( "close" );
}

function putMore(){
	window.location.assign("/putMoreApptYes/"+emailId);
}

function notPutMore(){
	cancelPopup();
}


function feedbackClick(loopNumber, answer){
	for(var i=1;i<8;i++){
		$("#feedback"+loopNumber+"s"+i).removeClass('btn-danger').addClass('btn-secondary');
	}
	$("#feedback"+loopNumber+"s"+answer).removeClass('btn-secondary').addClass('btn-danger');
	$("#feedback"+loopNumber+"_answer").val(answer);
}

function q1Click(answer){
	
	for(var i=1;i<8;i++){
		$("#q1s"+i).removeClass('btn-danger').addClass('btn-secondary');
	}
	$("#q1s"+answer).removeClass('btn-secondary').addClass('btn-danger');
	$("#q1").val(answer);
	
}

function q2Click(answer){
	
	for(var i=1;i<8;i++){
		$("#q2s"+i).removeClass('btn-danger').addClass('btn-secondary');
	}
	$("#q2s"+answer).removeClass('btn-secondary').addClass('btn-danger');
	$("#q2").val(answer);
	
}

function q3Click(answer){
	
	for(var i=1;i<8;i++){
		$("#q3s"+i).removeClass('btn-danger').addClass('btn-secondary');
	}
	$("#q3s"+answer).removeClass('btn-secondary').addClass('btn-danger');
	$("#q3").val(answer);
	
}
