var agreeValue = 0;

$(document).ready(function() {
	
	// due to ethics, hide the agreement
//	$.blockUI({
//		message : $('#agreementPopUp'),
//		css : {
//			top : ($(window).height() - 560) / 2 + 'px',
//			left : ($(window).width() - 500) / 2 + 'px',
//			width : '500px',
//			height : '560px',
//		}
//	});
	
	setTimeout(function(){
		$('#agreementPopUp').scrollTop(-200);
	},100);
	
	
	$("#submitAgreement").click(function(){
		var agreement = $("input[name='agreement']:checked").val();	
		agreeValue = agreement;
		$.unblockUI();
		$("#agreementValue").val(agreeValue);
	});
	
	$("#notFacultySubmitBtn").click(function(){

		var zpass = $("#zpass").val();
		var formdata = {
				"zpass" : zpass,
				"userType" : 1,
				"agreement" : $("#agreementValue").val()
		};
		
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/cali/savecalibrationuser',
			dataType : "json",
			contentType : 'application/json',
			data : JSON.stringify(formdata),
			beforeSend : function() {
				// showWaitingPopUp();
			},
			success : function(data) {
				if(data.code == 1){
					window.location = "/cali/calibrate";
			     }else{
			    	 alert("Confirm failed, please try again");
			     }
				cancelPopup();
			}
		});
		
	});
	
	
$("#facultySubmitBtn").click(function(){
		
		var clinicalSite = $("#clinicalSite").val();
		var other = $("#other").val();
		var position = $("#position").val();
		var name = $("#name").val();
		var email = $("#email").val();
		
		if(clinicalSite==5){
			clinicalSite = other;
		}
		
		var formdata = {
				"clinicalSite" : clinicalSite,
				"position": position,
				"name": name,
				"email": email,
				"userType" : 1,
				"agreement" : $("#agreementValue").val()
		};
		
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/cali/savecalibrationuser',
			dataType : "json",
			contentType : 'application/json',
			data : JSON.stringify(formdata),
			success : function(data) {
				if(data.code == 1){
					window.location = "/cali/calibrate";
			     }else{
			    	 alert("Confirm failed, please try again");
			     }
				cancelPopup();
			}
		});
		
	});
	
	
	
	
//	var health = $("#health").val();
//	var title = $("#title").val();
//	var year = $("#year").val();
//	var month = $("#month").val();
//	var day = $("#day").val();
//	var firstname = $.trim($("#firstname").val());
//	var lastname = $.trim($("#lastname").val());
//	var gender = parseInt($('input[name=gender]:checked').val());
//	var phone = $("#phone").val();
//	var email = $("#email").val();
//	
//	var street = $("#street").val();
//	var suburb = $("#suburb").val();
//	var postcode = $("#postcode").val();
//	// var ethnicity = $("#ethnicity").val();
//	var language = $("#language").val();
//	var reason = $("#reason").val();
//	var ageRange = $("#agerange").val();
//	// var birthday = convertNumbersToTimeStamp(year, month, day);
//	var password = $("#password").val();
//		
//	var formdata = {
//		health : health,
//		title : title,
//		firstname : firstname,
//		lastname : lastname,
//		username : email,
//		gender : gender,
//		phone : phone,
//		email : email,
//		street:street,
//		suburb:suburb,
//		postcode:postcode,
//		// ethnicity:ethnicity,
//		language:language,
//		reason:reason,
//		age:ageRange,
//		// birthday : birthday,
//		password : password,
//		status : 1
//	};
//
//	$.ajax({
//		type : 'POST',
//		headers: getTokenHeader(),
//		url : PATIENTPATH+'/savepatient',
//		contentType : 'application/json',
//		dataType : "json",
//		data : JSON.stringify(formdata),
//		success : function(data) {
//			if (data != null) {
//				if (data.code == 1) {
//					alert("Register success!");
//					window.location.assign(PATIENTPATH+"/ulogin_sp");
//				} else {
//					$("#registerError").text(data.content);
//					$("#registerErroDiv").show();
//				}
//			}
//		}
//	});
	
	

	$("#facultyBtn").click(function(){
		
		
		hideAll();
		
		var faculty = parseInt($('input[name=faculty]:checked').val());
		
		if(faculty==1){
			$("#facultyDiv").show();
		}else{
			$("#notFacultyDiv").show();
		}
		
	});
	
	
	$('input[type=radio][name=faculty]').change(function(){
		
		hideAll();
		
		var faculty = parseInt($('input[name=faculty]:checked').val());
		
		if(faculty==1){
			$("#facultyDiv").show();
		}else{
			$("#notFacultyDiv").show();
		}
		
	});
	
	
	
	$("#clinicalSite").change(function(){
		
		var clinicalSite = $("#clinicalSite").val();
		if(clinicalSite==5){
			
			$("#otherDiv").show();
			
		}else{
			
			$("#otherDiv").hide();
		}
		
		
	});
	
});


function hideAll(){
	
	$("#facultyDiv").hide();
	$("#notFacultyDiv").hide();
	$("#facultyBtnDiv").hide();
	
	
}