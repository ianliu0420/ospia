<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Tutor Review</title>
<c:import url="/common.jsp"></c:import>
<script src='<%=request.getContextPath()%>/js/tut/32_Tut_search.js'></script>
</head>

<body>
	<c:import url="/tutheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<div align="center">
							<form class="form-inline" action="tut/searchvideo" method="GET"
								id="searchForm">
								<div class="form-group">
									<input type="text" class="form-control" id="searchContent"
										name="searchContent"
										placeholder="Search interations by student user name"
										style="width:400px" value="${searchContent}">
								</div>
								<input type="submit" class="usyd-ui-button-primary"
									value="Search" id="searchButton" />
							</form>
						</div>

						<div id="searchResultDiv">
							<h3>Search Result</h3>

							<c:if test="${not empty videoList}">
								<table class="table">
									<thead>
										<tr>
											<th>Date</th>
											<th>Time</th>
											<th>Student Name</th>
											<th>SP Name</th>
											<th>Operations</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="i" begin="0" end="${videoList.size()-1}">
											<tr>
												<td>${videoList.get(i).getAptDate()}</td>
												<td>${videoList.get(i).getAptTime()}</td>
												<td>${videoList.get(i).getDoctorName()}</td>
												<td>${videoList.get(i).getPatientName()}</td>
												<td>
													<!-- <a target="_blank"
									href="vdetail/video_1436758597137&3ec80d4c-5029-425b-8ad1-f37f4fde8514"><button
											class="usyd-ui-button-primary">Reflection</button></a>  --> <a
													target="_blank"
													href="tut/reviewscreen_tut/${videoList.get(i).getArchiveId()}"><button
															class="usyd-ui-button-primary">Review</button></a>

												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:if>

							<c:if test="${empty videoList}">
								<p>No consultation.</p>
							</c:if>

						</div>
						
						
						<div class="jumbotron" style="padding-top:200px;padding-bottom:2px;">
                        <hr/>
                            <div>
                                 <div style="text-align:left; margin-top:30px;">
                                 <h5>OSPIA Project Acknowledgements</h5>
                                 <br>
                                 Through the hard work, collaboration and skills of the following people, the OSPIA Project has been made possible. I thank them sincerely for their contributions.
                                 <br/>
                                 <ul style="margin-left:20px;">
                                     <li>Dr Renee Lim, Educational Designer and Director of Program Development, Pam McLean Centre and Positive Computing Lab, Sydney University</li>
                                    <li>Prof Rafael Calvo, Positive Computing Lab at the University of Sydney </li>
                                    <li>Mr Chunfeng Liu, PhD student, Positive Computing Lab at the University of Sydney</li>
                                    <li>Ms Kiran Thwaites, Clinical Skills Administrator, UNSW</li>
                                 </ul>
                                 
                                 </div>
                                 <div style="text-align:right; font-style:italic;">
                                 Dr Silas Taylor, Convenor of Clinical Skills, UNSW Medicine
                                 </div>
                                 <br/>
                                 <div>
                                 <img style="width:350px;height:100px;" src="<%=request.getContextPath()%>/img/gvn_logo.png"><br>
                                 This project received funding from the Australian Government.
                                 </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
