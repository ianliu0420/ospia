<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Tutor Review</title>
<c:import url="/common.jsp"></c:import>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/timeline.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/timeline_cs.css">
<link rel="stylesheet" type="text/css" media="all"
	href="<%=request.getContextPath()%>/css/vtimeline.css">
	 
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/timeline.js"></script>

<script type="text/javascript">
	var sessionId = "${video.getSessionId()}";
	var enablePostcheck = "${video.getAllowTutorNotes()}";
</script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/tut/33_Tut_SP_screen.js"></script>
</head>

<body>
	<c:import url="/tutheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div class="row">
							<div class="col-lg-7">
								<h4>Recording of the conversation</h4>
								<video id="myvideo" width="500" height="400" controls>
									<source
										src="<%=request.getContextPath()%>/video/${video.getName()}/${video.getName()}.mp4"
										type="video/mp4">
									<!-- <source src="<%=request.getContextPath()%>/video/video_1436761313842.mp4"	type="video/mp4">-->
								</video>
								<input type="hidden" value="${video.getSessionId()}"
									id="sessionId"> <input type="hidden"
									value="${video.getLength()+100}" id="videoLength">
							</div>

							<div class="col-lg-5">
							    <b style="color:red">Let this student view my comments:</b> &nbsp;&nbsp;
							    <div class="Switch Off">
							        <div class="Toggle"></div>
								    <span class="On">ON</span>
								    <span class="Off">OFF</span>
							    </div>
							
								<h3>Notes/Comments</h3>
								<p style="color:red;display:none" id="commentSuc">Submit
									Success</p>
								<textarea rows="8" cols="5" id="comment"></textarea>
								<br />
								<br /> <input type="button" id="commentBtn"
									class="usyd-ui-button-primary" value="Comment"> <br>
								<br>
								<!-- 
								<div>
									<input type="checkbox" id="postcheck">&nbsp;&nbsp;Let this
									student view my comments
								</div>
                                 -->
                                 
								<hr>
								<a
									href="<%=request.getContextPath()%>/tut/soca_tut/${video.getSessionId()}"
									target="_blank"> <input type="button" id="commentBtn"
									class="usyd-ui-button-primary" value="Finish SOCA">
								</a>
							</div>
						</div>
                        <br>
						<!-- <div><h4>Comments:</h4></div> -->
						<div id="mytimeline"></div>

						<br>
						<br>
						<h4>Comment List</h4>
						
						<div class="sidebar-module" style="height:300px;overflow: auto">
							<table class="table">
								<thead>
									<tr>
										<th>Time</th>
										<th>Comment</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="commentBody">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
