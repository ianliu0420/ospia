<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Program Information</title>
<c:import url="/common.jsp"></c:import>
</head>

<body>
    <c:import url="/tutheader.jsp"></c:import>

    <c:import url="/programinfo_tut.jsp"></c:import>

    <c:import url="/footer.jsp"></c:import>
</body>
</html>
