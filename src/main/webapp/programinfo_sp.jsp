<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
    String path = request.getContextPath();
    String basePath =
                    request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                                    + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
</head>

<body>

	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<div class="jumbotron">
							<video id="myvideo" width="480" height="360" controls> <source
								src="<%=request.getContextPath()%>/video/introduction.mp4" type="video/mp4"></video>
						</div>

						<div class="jumbotron" style="padding-top:2px;padding-bottom:2px;">
							<h2>OSPIA For Simulated Patients</h2>
							<br/>
							<p class="lead" style=" text-align:justify">The OSPIA means you can help medical students develop effective communication
								skills when and where you want. Use the booking system to connect with students and receive text or email confirmation for
								your simulated consultation. With the OSPIA platform you can comment on student's skills during the simulated consultation
								with a simple 'thumbs up' or 'thumbs down' or you can add a comment to let students know when they're doing well and where
								they can improve.</p>
								
							<hr/>
                            <div>
                                 <div style="text-align:left; margin-top:30px;">
                                 Dr Renee Lim was the Educational Designer on the OSPIA Project. I am very grateful for Renee's hard work and skills in bringing this project to fruition, not least in brokering the collaboration with University of Sydney's Prof Rafael Calvo and his excellent PhD student Mr Chunfeng Liu, without whom major elements of this website would not have been possible.
                                 </div>
                                 <div style="text-align:right; font-style:italic;">
                                 Dr Silas Taylor, Convenor of Clinical Skills, UNSW Medicine
                                 </div>
                                 <br/>
                                 <div>
                                 <img style="width:350px;height:100px;" src="<%=request.getContextPath()%>/img/gvn_logo.png"><br>
                                 This project received funding from the Australian Government. 
                                 </div>
                            </div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>


</body>
</html>
