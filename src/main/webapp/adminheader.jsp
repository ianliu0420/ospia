<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
</head>

<body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-302000095 node-type-page">
    <div id="site-header-bg">
        <div id="wrapper-t">
            <div id="header">
                <div class="logo">
                    <a href="http://www.unsw.edu.au/" title="The University of New South Wales"><img
                        src="<%=request.getContextPath()%>/img/unsw_logo_old.png" alt="The University of New South Wales"
                        title="The University of New South Wales"></a>
                </div>
                <div id="top-link-wrapper">
                    <div class="top-link-container">
                        <div class="region region-menu-top-right">
                            <div id="block-system-user-menu" class="block block-system block-menu clearfix">


                                <div class="content">
                                    <ul class="menu">
                                        <li class="first leaf"><a href="https://emed.med.unsw.edu.au/" title="">eMed</a></li>
                                        <li class="leaf"><a href="http://my.unsw.edu.au/" title="">myUNSW</a></li>
                                        <li class="leaf"><a href="https://med.unsw.edu.au/find-a-person" title="Find a person">Find a person</a></li>
                                        <li class="last leaf"><a href="https://med.unsw.edu.au/contact-us" title="">Contact us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="logo-top-left">
                    <a href="https://med.unsw.edu.au/">Medicine</a>
                </div>

                <div id="main_search">
                    <!-- Start Main Search-->
                    <div class="region region-main-search">
                        <div id="block-search-form" class="block block-search clearfix">
                        
                            <div class="content">
                            <span id="currentTime" style="margin-right:5px;"></span>
                            
                            <sec:authorize access="isAnonymous()">
                                <a href="<%=request.getContextPath()%>/survey/ulogin_admin" class="usyd-wasm-unauthed-view usyd-wasm-login"><button
                                        class="btn loginBtn">Log in</button></a>
                            </sec:authorize>
    
                            <sec:authorize access="isAuthenticated()">
                                <a href="<%=request.getContextPath()%>/survey/logoff" class="usyd-wasm-unauthed-view usyd-wasm-login"><button
                                        class="btn loginBtn">Log out</button></a>
                            </sec:authorize>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="bodyWrapper">
        <div id="wrapper">
            <div id="main-container">
                <div class="main-menu sticky-enabled">
                    <div class="region region-top-nav">
                        <div id="block-megamenu-0" class="block block-megamenu clearfix">
                            <div class="content">
                                <ul id="megamenu-main-menu" class="megamenu-menu columnar megamenu-skin-mytheme">

                                    <c:set var="redpage" scope="page" value="${redpage}" />
                                   <sec:authorize access="isAnonymous()">
                                        <li class='megamenu-parent'><h2 class="megamenu-parent-title">
                                                <a href="<%=request.getContextPath()%>/survey/uhome_admin" <c:if test="${redpage eq 'uhome_admin'}"> class="active" </c:if>>Home</a>
                                            </h2></li>
                                    </sec:authorize>

                                    <sec:authorize access="isAuthenticated()">
                                        <li class='megamenu-parent'><h2 class="megamenu-parent-title">
                                                <a href="<%=request.getContextPath()%>/survey/uhome_admin" <c:if test="${redpage eq 'uhome_admin'}"> class="active" </c:if>>Home</a>
                                            </h2></li>

                                        <li class='megamenu-parent'><h2 class="megamenu-parent-title">
                                                <a href="<%=request.getContextPath()%>/survey/listsurvey"
                                                   <c:if test="${redpage eq 'uhome_survey'}"> class="active" </c:if>> Surveys</a>
                                            </h2></li>
                                            
                                        <li class='megamenu-parent'><h2 class="megamenu-parent-title">
                                                <a href="<%=request.getContextPath()%>/survey/patients"
                                                   <c:if test="${redpage eq 'sp_admin'}"> class="active" </c:if>> SP mgmt</a>
                                            </h2></li>
                                            
                                        <li class='megamenu-parent'><h2 class="megamenu-parent-title">
                                                <a href="<%=request.getContextPath()%>/survey/periods"
                                                   <c:if test="${redpage eq 'period_admin'}"> class="active" </c:if>> Time mgmt</a>
                                            </h2></li>
                                        <li class='megamenu-parent'><h2 class="megamenu-parent-title">
                                                <a href="<%=request.getContextPath()%>/survey/apts"
                                                   <c:if test="${redpage eq 'apts_admin'}"> class="active" </c:if>> Appointment mgmt</a>
                                            </h2></li>
                                         <li class='megamenu-parent'><h2 class="megamenu-parent-title">
                                                <a href="<%=request.getContextPath()%>/survey/assessors"
                                                   <c:if test="${redpage eq 'assessors_admin'}"> class="active" </c:if>> Assessor mgmt</a>
                                         </h2></li>
                                         <li class='megamenu-parent'><h2 class="megamenu-parent-title">
                                                <a href="<%=request.getContextPath()%>/survey/students"
                                                   <c:if test="${redpage eq 'students_admin'}"> class="active" </c:if>> Student mgmt</a>
                                         </h2></li>
                                    </sec:authorize>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
