<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>SP Home</title>

<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

</head>

<body class="usyd-js-load">
	<div class="usyd-page usyd-no-feedback usyd-no-yammer ">
		<c:import url="/spheader.jsp"></c:import>
		
		<div class="jumbotron">
		<h2>Thanks for participating</h2>
			<a href="sp/cal_sp"></a>
		</div>
		
	</div>
	
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
