<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>OSPIA video test</title>
<c:import url="/common.jsp"></c:import>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/js/participation.js"></script>
<style type="text/css">
p,b {
    font-size: 16px;
}

h1 {
    font-size: 30px;
}

</style>
</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div style="margin-left:8%;margin-right:8%; font-size:15px; ">
						<a href="<%=request.getContextPath()%>/files/PISCFV3.pdf">Download This Document</a>
						
						<div class="jumbotron" style="padding-top:2px;padding-bottom:2px;">
							<h1>
								<b>PARTICIPANT INFORMATION STATEMENT AND CONSENT FORM</b>
							</h1>
						</div>
						
						<p><b>The study is being carried out by the following researchers</b></p>
						 <b>Chief Investigator:</b> <br/>
                         &nbsp;&nbsp;&nbsp;&nbsp; Dr Silas Taylor (UNSW) <br/>
                         <b>Co-Investigator/s:</b> <br/>
                         &nbsp;&nbsp;&nbsp;&nbsp; Prof Rafael Calvo (University of Sydney)<br/>
                         &nbsp;&nbsp;&nbsp;&nbsp; Dr Renee Lim (University of Sydney)<br/>
                         &nbsp;&nbsp;&nbsp;&nbsp; A/Prof Boaz Shulruf (UNSW)<br/>
                         <b>Student Investigator/s:</b> <br>
						 &nbsp;&nbsp;&nbsp;&nbsp; Ms Khushnood Z. Naqshbandi (University of Sydney), who is conducting this study as the basis for the degree of M.Phil at The University of Sydney. This will take place under the supervision of Prof Rafael Calvo.<br>
						 &nbsp;&nbsp;&nbsp;&nbsp; Mr Chunfeng Liu (University of Sydney), who is conducting this study as the basis for the degree of PhD at The University of Sydney. This will take place under the supervision of Prof Rafael Calvo.
						<br><br>
						
						
						<b>What is the research study about?</b>
						<p>You are invited to take part in this research study. You have been invited because you are a volunteer Simulated Patient (SP) for UNSW Medicine students in the Clinical Skills program.</p>
						<p>To participate in this project you need to meet the following inclusion criteria:</p>
						&nbsp;&nbsp;&nbsp;&bull; You have completed an OSPIA, that is an online interaction OR a campus interaction with a medical student.<br>
                        &nbsp;&nbsp;&nbsp;&bull; You have completed the relevant training modules for interacting in the above mentioned sessions.<br><br>
						
						<p>The purpose of this research is to assess the needs of the volunteers who use the OSPIA system and consequently, implement and evaluate the changes based on those findings. The specific aims are:</p>
						<p>1. To assess the motivations, psychological needs and usability expectations and experiences of the volunteers by conducting a preliminary survey followed by focus groups and online interviews. Respondents who agree to participate in the focus groups or online interviews in the survey will be invited at a convenient time and place.</p>
						<p>2. To improve the online OSPIA experience of the SPs based on the requirements generated from the previously mentioned activities and evaluate the effect of those implementations.</p>
						
						<p><b>Importance of the study </b>- In order to maintain the sustainability of OSPIA, considering the enjoyment and long-term engagement of the SPs is important. The availability of the SPs ensures the ongoing OSPIA sessions, which makes it important to assess and evaluate their needs and modify them accordingly. This study will take into account the SP needs and experiences to define changes in the overall OSPIA interaction with the SP.</p>
						
						<b>Do I have to take part in this research study?</b>
						<p>Participation in this research study is voluntary. If you don’t wish to take part, you don’t have to. Your decision will not affect your relationship with The University of New South Wales or the University of Sydney. However, if you agree, you will help the researchers to fulfil some important objectives, the improvement of the experience of the simulated patients being the primary amongst them. </p>
						<p>This Participant Information Statement and Consent Form tells you about the research study. It explains the research tasks involved. Knowing what is involved will help you decide if you want to take part in the research.</p>
						<p>Please read this information carefully. Ask questions about anything that you don’t understand or want to know more about.  Before deciding whether or not to take part, you might want to talk about it with a relative or friend.</p>
						<p>If you decide you want to take part in the research study, you will be asked to:</p>
						&nbsp;&nbsp;&nbsp;&bull; Click 'Agree' and provide your name on the online consent form;<br>
                        &nbsp;&nbsp;&nbsp;&bull; Keep a copy of this Participant Information Statement (available on the OSPIA website)<br><br>
						
						<b>What does participation in this research require, and are there any risks involved?</b>
						<p>If you agree to take part in the research study, you will be directed to an online questionnaire, which takes an average of about 5 minutes to complete. Within the survey, you will be asked about your interest in further participation in a focus group or online interview. Based on your answers, you will be contacted about further participation. The respondents will be given a time and venue for the focus group (held within the UNSW premises), or a mutually convenient time for the online interview.</p>
						
						<b>Study tasks & risks:</b>
						<p>Before partaking in the survey and subsequent focus group or interview, you are required to give your informed consent. Upon agreeing to participate, you will be directed to an online questionnaire. The questionnaire will ask about some demographic details including your name, age, employment status, cultural identity, mode of attendance (online, in campus or both), while as the rest of the questionnaire will gauge your reasons for volunteering as well as your basic psychological needs experience with OSPIA. If you choose to participate in the focus group or an online interview, you will be invited accordingly. Ideally, the focus group/ interviews will be held a few weeks after the email with the survey is sent out. It is anticipated that we will hold two focus group sessions; with interview sessions being held only if we do not manage to recruit enough participants for the focus groups. Each focus group session will last for about 1-1.5 hours, with several discussions interspersed by short activities. The individual interview discussions will follow the same structure as the focus group albeit without any activities.</p>
						
						<b>Will I be paid to participate in this project?</b>
						<p>You will receive a $20 gift card if you participate in the focus group.</p>
						
						<b>What are the possible benefits to participation?</b>
						<p>This study will also allow you to provide researchers with your opinions about the OSPIA system. We hope that the findings of this study help us to improve OSPIA for you and other volunteer simulated patients.</p>
						
						<b>What will happen to information about me?</b>
						<p>By signing the online consent form, you consent to the research team collecting and using information about you audio recordings of the focus group and interviews for the research study. We will keep your data for up to 7 years. We will store information from the study on the UNSW server and Sydney University password protected computers and on UNSW password protected computers thereafter.</p>
						<p>It is anticipated that the results of this research study will be published and/or presented in a variety of forums. In any publication and/or presentation, information will be published in a way such that you will not be individually identifiable.</p>
						<p>We store these files on the UNSW server. Your confidentiality will be ensured by this being a password protected, enterprise quality system. The server can only be accessed by the researchers in the team.</p>
						
						<b>How and when will I find out what the results of the research study are?</b>
						<p>You have a right to receive feedback about the overall results of this study. You can tell us that you wish to receive feedback by emailing <a>csadmin@unsw.edu.au</a>. This feedback will be in the form of a link to a webpage that will have a summary of results, or a published conference or journal article. You can receive this feedback after the study is finished.</p>
						
						<b>What if I want to withdraw from the research study?</b>
						<p>If you do not want to participate in the study, please select the appropriate button on the webpage. If you do consent to participate, you may withdraw at any time during the study. Please make sure to contact us through email to let us know if you withdraw during the study. If you withdraw during the focus group or online interview, you will be asked to complete and sign the ‘Withdrawal of Consent Form’, which is provided at the end of this document.</p>
						<p>Any information that we have already collected, however, will be kept in our study records and may be included in the study results.</p>
						
						<b>What should I do if I have further questions about my involvement in the research study?</b>
						<p>The person you may need to contact will depend on the nature of your query. If you want any further information concerning this project or if you have any problems that may be related to your involvement in the project, you can contact the following member/s of the research team:</p>
						
						<b>Research Team Contact</b><br>
						&nbsp;&nbsp;&nbsp;&bull; <b>Name: </b> Dr Silas Taylor <br>
                        &nbsp;&nbsp;&nbsp;&bull; <b>Position: </b> Chief Investigator<br>
                        &nbsp;&nbsp;&nbsp;&bull; <b>Telephone: </b> 9385 2607<br>
                        &nbsp;&nbsp;&nbsp;&bull; <b>Email: </b> <a>silas.taylor@unsw.edu.au</a><br><br>
						
						<b>What if I have a complaint or any concerns about the research study?</b>
						<p>If you have any complaints about any aspect of the project, the way it is being conducted, then you may contact:</p>
						
						<b>Complaints Contact</b><br>
                        &nbsp;&nbsp;&nbsp;&bull; <b>Position: </b> Human Research Ethics Coordinator<br>
                        &nbsp;&nbsp;&nbsp;&bull; <b>Telephone: </b> + 61 2 9385 6222<br>
                        &nbsp;&nbsp;&nbsp;&bull; <b>Email: </b> <a>humanethics@unsw.edu.au</a><br>
                        &nbsp;&nbsp;&nbsp;&bull; <b>HC Reference Number: </b> HC16048 <br><br>
						
						<b>Declaration by Researcher*</b>
						<p>An explanation of the research study, its study activities and risks is laid out in this document, presented online. We believe that a participant who agrees to participate, and fills in the above details online, has understood that explanation. </p>
						<p>*An appropriately qualified member of the research team is available by email to expand on and provide further explanation of, and information concerning the research study (silas.taylor@unsw.edu.au).</p>
						
						<b style="color:red; font-size:18px;">I agree to participate and I have</b> <br/>
                        <p>&nbsp;&nbsp;&nbsp;&bull; Read the Participant Information Sheet or someone has read it to me in a language that I understand;</p>
                        <p>&nbsp;&nbsp;&nbsp;&bull; Understood the purposes, study tasks and risks of the research described in the project;</p>
                        <p>&nbsp;&nbsp;&nbsp;&bull; Had an opportunity to email questions to the Chief Investigator and I am satisfied with the answers I have received;</p>
                        <p>&nbsp;&nbsp;&nbsp;&bull; Freely agree to participate in this research study as described and understand that I am free to withdraw at any time during the project and withdrawal will not affect my relationship with any of the named organisations and/or research team members;</p>
                        <p>&nbsp;&nbsp;&nbsp;&bull; Understand that I am able to down load a copy of this document to keep from the OSPIA website (student login);</p><br>
                          
                        <b>Please type your name to provide consent:</b> <input type="text" id="spName" name="spName" style="width:200px;"> <br><br> 
                        <div id="spNameMissing" style="display:none; color:red">Please provide your name before we can process.</div>
                        
                        <div align="center">  
						 <input type="button" id="agreeBtn" value="Agree" class="usyd-ui-button-primary">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 <input type="button" id="declineBtn" value="DEcline" class="usyd-ui-button-primary">
						</div>
						
						</div>
							
					<div id="waitingPopup" style="display:none">
                            <h4>We are generating your video. Please wait a moment.</h4>
                    </div>
                    
                    <div id="firefoxWarning" style="display:none">
                    <h4>OSPIA cannot run on iPads and only supports Firefox and Chrome web browser sorry for any inconvenience. If you do not have Firefox or Chrome installed, please download them on these pages:</h4>
                        <a href="https://www.mozilla.org/en-US/firefox/new/"  style="font-size:20px;">Download Firefox</a>
                        &nbsp;&nbsp;&nbsp;<a href="https://www.google.com.au/intl/en/chrome/browser/desktop/index.html"  style="font-size:20px;">Download Chrome</a>
                    </div>
                    
				</div>
			</div>
		</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
