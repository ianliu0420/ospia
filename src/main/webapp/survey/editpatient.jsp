<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Registration</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/survey/editpatient.js"></script>
<style type="text/css">
#signupForm label.error {
	color: red;
	width: auto;
	margin-top: 2px;
}
</style>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<br /> <span style="font-size:14px;"> <b>Register details of the simulated patient </b> (<span
						class="redlabel">*</span> indicates compulsory field)
					</span> <br />
					<hr class="autohr">
					<br />
                    <input type="hidden" value="${patient.patientid}" name="patientId" id="patientId">
                    
					<form class="form-horizontal" id="signupForm" method="post">
						<div class="usyd-form-component">
							<div class="control-group" id="registerErroDiv" style="display:none">
								<label class="control-label" for="textinput"></label>
								<div class="controls">
									<p style="color:red" id="registerError"></p>
								</div>
							</div>
						</div>

						

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Title <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="title" name=title type="text" placeholder="" value="${patient.title}">
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">First Name <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="firstname" name=firstname type="text" placeholder="First Name" value="${patient.firstname}">
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Last Name <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="lastname" name="lastname" type="text" placeholder="Last Name" value="${patient.lastname}">
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="radios">Gender <span class="redlabel">*</span></label>
								<div class="controls">
									<label class="radio-inline" for="radios-0"> <input type="radio" name="gender" id="radios-0" value="1"
										<c:if test="${patient.gender==1}">checked="checked" </c:if> > Female
									</label> 
								    <label class="radio-inline" for="radios-1"> <input type="radio" name="gender" id="radios-1" value="2"
								    <c:if test="${patient.gender==2}">checked="checked" </c:if> >
										Male
									</label>
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="selectbasic">Age Range <span class="redlabel">*</span></label>
								<div class="controls">
									<select id="agerange" name="agerange" class="form-control" style="width:100px;">
										<option value="0">Range</option>
										<option value="20" <c:if test="${patient.age==20}"> selected="selected" </c:if> >Under 50</option>
										<option value="50" <c:if test="${patient.age==50}"> selected="selected" </c:if>>Above 50</option>
									</select>
								</div>
							</div>
						</div>


						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Mobile phone</label>
								<div class="controls">
									<input id="phone" name="phone" type="text" placeholder="Phone" value="${patient.phone}">
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Email <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="email" name="email" type="text" placeholder="Email" value="${patient.email}">
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Address - No, & Street Name <span class="redlabel">*</span></label>
								<div class="controls">
									<textarea rows="" cols="" id="street" name="street" >${patient.street}</textarea>
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Suburb <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="suburb" name="suburb" type="text" placeholder="Suburb" value="${patient.suburb}">
								</div>
							</div>
						</div>
						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Post Code <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="postcode" name="postcode" type="text" placeholder="Post Code" value="${patient.postcode}">
								</div>
							</div>
						</div>
						<hr>
						
                        <div class="usyd-form-component">
                            <div class="control-group">
                                <label class="control-label" for="textinput">General health description</label>
                                <div class="controls">
                                    <textarea rows="" cols="" id="health" name="health">${patient.health}</textarea>
                                </div>
                            </div>
                        </div>
                         
						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Fluent spoken languages <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="language" name="language" type="text" placeholder="" value="${patient.language}">
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput"> How did you hear about this program? <span class="redlabel">*</span></label>
								<div class="controls">
									<textarea rows="" cols="" id="reason" name="reason">${patient.reason}</textarea>
								</div>
							</div>
						</div>
						
						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput"> Notes for admin </label>
								<div class="controls">
									<textarea rows="" cols="" id="notes" name="notes">${patient.notes}</textarea>
								</div>
							</div>
						</div>

						<hr>
						<!-- Button -->
						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="singlebutton"></label>
								<div class="controls">
									<input type="submit" id="registerBtn" name="registerBtn" class="usyd-ui-button-primary" value="update">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<c:import url="/footer.jsp"></c:import>
</body>
</html>
