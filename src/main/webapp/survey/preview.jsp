<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Survey Preview</title>
<c:import url="/common.jsp"></c:import>
<link href="<%=request.getContextPath()%>/css/wufoo/form.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/wufoo/theme.css"
	rel="stylesheet">
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/survey/preview.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<form id="Survey_SOCA" name="Survey_SOCA" action="spSOCA_submit">
							<input type="hidden" value="${surveyId}" id="surveyId"> <input
								type="hidden" value="${ispreview}" id="ispreview">
							<header class="info">
								<h1 id="surveyTitle"></h1>
								<div>
									<p id="surveyDescription"></p>
								</div>
							</header>

							<ul id="questions">
							</ul>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
