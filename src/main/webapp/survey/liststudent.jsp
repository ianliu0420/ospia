<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Survey List</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/survey/liststudent.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
					
						<div><a onclick='expandUploadStudentMajor()'>Upload Students (major, for new study) >> </a></div><br>
						<div id="uploadStudentDivMajor" style="text-align:left; display:none">
							<b>Instruction for uploading student list:</b><br>
							0.  This function will be used when a new study/semester begins.  <br>
							1.	Download the template. An excel spreadsheet will open or download.<br>
							2.	Add new students from row 2. (Please ensure you delete any spaces before or after the names and zIds) <br>
							3.  Save the file.<br>
							4.	Click "Browse" or "Choose file" button, and select the file you just created. <br>
							5.  Click "Upload file" button to finish uploading. <br><br>
						
							<div><a target="_tab" href="<%=request.getContextPath()%>/files/StudentUploadTemplate.xlsx"><button class="usyd-ui-button-primary">download template</button></a></div><br>
							<form method="post" enctype="multipart/form-data" action="<%=request.getContextPath()%>/survey/uploadExcelFileStudentMajor">
							    <input id="excel_file" type="file" name="filenameMajor" accept=".xls,.xlsx"/>
							    <input type="submit" value="Upload file" />
							</form>
							<br><br><br>
						</div>
						
						<div><a onclick='expandUploadStudentMinor()'>Upload Students (minor, for additional students) >> </a></div><br>
						<div id="uploadStudentDivMinor" style="text-align:left; display:none">
							<b>Instruction for uploading student list:</b><br>
							0.  This function will be used when additional students need to be added to an existing study.  <br>
							1.	Download the template. An excel spreadsheet will open or download.<br>
							2.	Add new students from row 2. (Please ensure you delete any spaces before or after the names and zIds) <br>
							3.  Save the file.<br>
							4.	Click "Browse" or "Choose file" button, and select the file you just created. <br>
							5.  Click "Upload file" button to finish uploading. <br><br>
							<div><a target="_tab" href="<%=request.getContextPath()%>/files/StudentUploadTemplate.xlsx"><button class="usyd-ui-button-primary">download template</button></a></div><br>
							<form method="post" enctype="multipart/form-data" action="<%=request.getContextPath()%>/survey/uploadExcelFileStudentMinor">
							    <input id="excel_file" type="file" name="filenameMinor" accept=".xls,.xlsx"/>
							    <input type="submit" value="Upload file" />
							</form>
							<br><br><br>
						</div>
					
						<div style="text-align:center">
							<form action="<%=request.getContextPath()%>/survey/students" method="GET">
								<input type="text" id="searchContent" name="searchContent" value="${searchContent}" style="width:500px;"> <input
									type="submit" id="searchBtn" value="search" class="usyd-ui-button-primary">
							</form>
						</div>
						<br/><br/>
						
						<div id="searchResultDiv" style="height:600px;overflow: auto;">
							<br>
							<h3>All students (${students.size()})</h3>
							<table class="table">
								<thead>
									<tr>
										<th style="width:15%">First name</th>
										<th style="width:15%">Last name</th>
										<th style="width:35%">zid</th>
										<th style="width:10%">Status</th>
										<th style="width:25%">Operation</th>
									</tr>
								</thead>

								<tbody>
									<c:forEach var="student" items="${students}">
										<tr>
											<td>${student.firstName}</td>
											<td>${student.lastName}</td>
											<td>${student.studentId}</td>
											<c:if test="${student.status==0}">
												<td>Disabled</td>
											</c:if>
											<c:if test="${student.status==1}">
												<td>Active</td>
											</c:if>
											<td>
												<c:if
													test="${student.status==1}">
													<button class="usyd-ui-button-primary" onclick="disableStudent(${student.id})">Disable</button>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
