<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Edit Question</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/survey/editquestion.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<form class="form-horizontal">
							<input value="${questionId}" type="hidden" id="questionId">

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput"></label>
									<div class="controls">
										<h3>Edit a question</h3>
									</div>
								</div>
							</div>

							<div class="form-group" id="registerErroDiv" style="display:none">
								<label class="col-md-4 control-label" for="textinput"></label>
								<div class="col-md-4">
									<p style="color:red" id="registerError"></p>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput">Question
										Title</label>
									<div class="controls">
										<!-- <input id="title" name=title type="text" placeholder="Question Title"> -->
										<textarea id="title" name="title" rows="" cols=""></textarea>
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput">Question
										Description</label>
									<div class="controls">
										<textarea id="description" name="description" rows="" cols=""></textarea>
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="selectbasic">Question
										Type</label>
									<div class="controls">
										<select id="type" name="type">
											<c:forEach var="questionType" items="${questionTypes}">
												<option value="${questionType.id}">${questionType.type}</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="selectbasic">&nbsp;</label>
									<div class="controls">
										<input class="checkbox" type="checkbox" name="isCompulsory"
											id="isCompulsory" value="1"><span
											style="font-size:14px;"> make is compulsory</span>
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput"></label>
									<div class="controls">
										<input class="usyd-ui-button-primary" type="button"
											value="save the edit" id="saveQuestion" />
									</div>
								</div>
							</div>

							<div id="allOptionsDiv" class="form-group" style="display:none">
								<hr />
								<div class="col-md-12">
									<label class="col-md-4 control-label" for="selectbasic"></label>
									<input type="button" id="addoption" value="add option"
										class="usyd-ui-button-primary"
										style="margin-left:0px;margin-bottom:10px" /> <br>
									<div id="allOptions"></div>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
