
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Question List</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/survey/listquestion.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<div id="searchResultDiv" style="">

							<a href="<%=request.getContextPath()%>/survey/addquestion"><button
									class="usyd-ui-button-primary">Create a question</button></a>

							<h3>Question list</h3>
							<table class="table">
								<thead>
									<tr>
										<th style="width:15%">Title</th>
										<th style="width:30%">Description</th>
										<th style="width:5%">Type</th>
										<th style="width:25%">Options</th>
										<th style="width:7%">Operations</th>
										<th style="width:8%">Compulsory</th>
									</tr>
								</thead>

								<tbody>
									<c:forEach var="question" items="${questions}">
										<tr>
											<td>${question.title}</td>
											<td>${question.description}</td>
											<td>${question.typeS}</td>
											<td><c:forEach var="option" items="${question.options}">
							         ${option.optkey}:${option.optvalue} &nbsp;
							     </c:forEach></td>
											<td><a
												href="<%=request.getContextPath()%>/survey/editque/${question.id}"><img
													src="<%=request.getContextPath()%>/img/edit.png"
													class="operationicon"></a> <a
												onclick="deleteQuestion(${question.id})"><img
													src="<%=request.getContextPath()%>/img/delete.png"
													class="operationicon"></a></td>
											<td><c:if
													test="${question.isCompulsory==null||question.isCompulsory==0}">
                                     false
                                 </c:if> <c:if
													test="${question.isCompulsory==1}">
                                     true
                                 </c:if></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
