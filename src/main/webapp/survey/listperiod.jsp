<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Survey List</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/survey/listperiod.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
                            <form class="form-horizontal" method="post" id="periodForm">
                            <div class="usyd-form-component"
                                <c:if test="${param.error != 'true'}"> style="display:none" </c:if>>
                                <div class="control-group" id="errorInfo" style="color:red">
                                    <label class="control-label" for="textinput"></label>
                                    <div class="controls">
                                        <p>invalid username or password</p>
                                    </div>
                                </div>
                            </div>
                            
                            <input id="id" name="id" type="hidden" />

                            <div class="usyd-form-component">
                                <div class="control-group">
                                    <label class="control-label" for="textinput">Start date</label>
                                    <div class="controls">
                                        <input id="startdate" name="startdate" type="text" placeholder="dd/mm/yyyy"/>
                                    </div>
                                </div>
                            </div>

                            <div class="usyd-form-component">
                                <div class="control-group">
                                    <label class="control-label" for="passwordinput">End date</label>
                                    <div class="controls">
                                        <input id="enddate" name="enddate" type="text" placeholder="dd/mm/yyyy"/>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="usyd-form-component">
	                            <div class="control-group">
	                                <label class="control-label" for="textinput">Notes</label>
	                                <div class="controls">
	                                    <textarea rows="" cols="" id="note" name="note"></textarea>
	                                </div>
	                            </div>
	                        </div>

                            <div class="usyd-form-component">
                                <div class="control-group">
                                    <label class="control-label" for="singlebutton"></label>
                                    <div class="controls">
                                        <input id="submit" name="submit"
                                            class="usyd-ui-button-primary" type="submit" value="Save">
                                            
                                        <input id="clear" name="clear"
                                            class="usyd-ui-button-primary" onclick="clearForm()" type="button" value="clear the form">
                                    </div>
                                </div>
                            </div>
                        </form>

						<div id="searchResultDiv" style="">
							<br>
							<h3>All times</h3>
							<table class="table">
								<thead>
									<tr>
										<th style="width:15%">Start date</th>
										<th style="width:15%">End data</th>
										<th style="width:50%">Note</th>
										<th>Operations</th>
									</tr>
								</thead>

								<tbody>
									<c:forEach var="period" items="${periods}">
										<tr>
											<td>${period.startDateStr}</td>
											<td>${period.endDateStr}</td>
											<td>${period.note}</td>
											<td>
												<button class="usyd-ui-button-primary" onclick="editPeriod(${period.id})">Edit</button> 
												<button class="usyd-ui-button-primary" onclick="disablePeriod(${period.id})">Delete</button>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
