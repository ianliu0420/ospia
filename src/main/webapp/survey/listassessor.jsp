<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Survey List</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/survey/listassessor.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
					
						<div><a onclick='expandUploadAssessors()'>Upload Assessors >> </a></div><br>
						
						<div id="uploadAssessorsDiv" style="text-align:left; display:none">
							<b>Instruction for uploading assessor list:</b><br>
							1.	Download the template. An excel spreadsheet will open or download.<br>
							2.	Add new assessors from row 2. (Please ensure you delete any spaces before or after the names and email addresses) <br>
							3.  Save the file.<br>
							4.	Click "Browse" or "Choose file" button, and select the file you just created. <br>
							5.  Click "Upload file" button to finish uploading. <br><br>
						
							<div><a target="_tab" href="<%=request.getContextPath()%>/files/AssessorUploadTemplate.xlsx">download template</a></div><br>
							<form method="post" enctype="multipart/form-data" action="<%=request.getContextPath()%>/survey/uploadExcelFileAssessor">
							    <input id="excel_file" type="file" name="filename" accept=".xls,.xlsx"/>
							    <input type="submit" value="Upload file" />
							</form>
							<!-- <input id="videofile" name="myfile" type="file" placeholder=""> -->
							<br><br><br>
						</div>
					
						<div style="text-align:center">
							<form action="<%=request.getContextPath()%>/survey/assessors" method="GET">
								<input type="text" id="searchContent" name="searchContent" value="${searchContent}" style="width:500px;"> <input
									type="submit" id="patientSearchBtn" value="search" class="usyd-ui-button-primary">
							</form>
						</div>
						<br/><br/>
						
						<div id="searchResultDiv" style="height:600px;overflow: auto;">
							<br>
							<h3>All assessors</h3>
							<table class="table">
								<thead>
									<tr>
										<th style="width:15%">First name</th>
										<th style="width:15%">Last name</th>
										<th style="width:35%">Email</th>
										<th style="width:10%">Status</th>
										<th style="width:25%">Operation</th>
									</tr>
								</thead>

								<tbody>
									<c:forEach var="assessor" items="${assessors}">
										<tr>
											<td>${assessor.firstname}</td>
											<td>${assessor.lastname}</td>
											<td>${assessor.username}</td>
											<c:if test="${assessor.status==0}">
												<td>Disabled</td>
											</c:if>
											<c:if test="${assessor.status==1}">
												<td>Active</td>
											</c:if>
											<td>
												<c:if
													test="${assessor.status==1}">
													<button class="usyd-ui-button-primary" onclick="disableAssessor(${assessor.teacherId})">Disable</button>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
