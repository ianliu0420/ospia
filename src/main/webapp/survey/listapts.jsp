<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Survey List</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/survey/listapts.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div style="text-align:center">
							<form>
								<div style="padding-bottom:15px;">
									<label class="control-label"> 
										<input class="checkbox" type="checkbox" name="searchType1" id="searchType1" value="1"> Available appts, 
										<input class="checkbox" type="checkbox" name="searchType2" id="searchType2" value="1"> Pending appts, 
										<input class="checkbox" type="checkbox" name="searchType3" id="searchType3" value="1"> Confirmed appts
										<input class="checkbox" type="checkbox" name="searchType4" id="searchType4" value="1"> Completed appts
									</label>
								</div>	
								
								<div style="padding-bottom:15px;">
									<label class="control-label"> 
										Search by: 
										<input class="radio" type="radio" id="searchContentType0" name="searchContentType" value="0" checked> none, 
										<input class="radio" type="radio" id="searchContentType1" name="searchContentType" value="1"> zId, 
										<input class="radio" type="radio" id="searchContentType2" name="searchContentType" value="2"> Student name,
										<input class="radio" type="radio" id="searchContentType3" name="searchContentType" value="3"> Patient name
									</label>
								</div>	
									
								<input type="text" id="searchContent" name="searchContent" value="${searchContent}" style="width:500px;"> <input
									type="button" id="aptsSearchBtn" value="search" class="usyd-ui-button-primary">
									<br><br>You can search by SP's first name, last name, full name or email, and student's first name, last name, full name or zid. (case insensitive) 
							</form>
						</div>
						<div id="searchResultDiv" style="height:500px; overflow: auto;">
							<h3>Search Results</h3>
							<br>
							<span id="total" style="display:none; font-size:14px; padding-top:10px; font-weight:bold"></span>
							<table id="detailTable" class="table" style="display:none;">
								<thead>
										<tr>
											<th>Time</th>
											<th>SP Name</th>
											<th id="studentHead">Student Name</th>
											<th>Student zid</th>
											<th>Status</th>
										</tr>
								</thead>
								<tbody id="tbodyList">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
