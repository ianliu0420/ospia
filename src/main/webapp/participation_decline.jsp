<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>OSPIA video test</title>
<c:import url="/common.jsp"></c:import>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>

<style type="text/css">
p,b {
    font-size: 16px;
}

h2 {
    font-size: 24px;
}

</style>
</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div style="margin-left:8%;margin-right:8%; font-size:15px; ">
						<div class="jumbotron" style="padding-top:2px;padding-bottom:2px;">
							<h2><b>Thank you for your patience! Unfortunately, you won't be able to proceed unless you agree to participate in this study. Please click on this link to agree and proceed with the study: <a href="https://ospia.med.unsw.edu.au/participation.jsp">https://ospia.med.unsw.edu.au/participation.jsp</a></b></h2>
						</div>
                    </div>
				</div>
			</div>
		</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
