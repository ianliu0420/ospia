<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>SP Home</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/patientEmail.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>

<body class="usyd-js-load">
	<div class="usyd-page usyd-no-feedback usyd-no-yammer ">
		<c:import url="/spheader.jsp"></c:import>
		
		<div id="dialog" title="Basic dialog" style="display:none">
		  <p>Thanks for completing the form. Would you like to book more appointment now?</p>
		  <button onclick="putMore()">Yes, please</button> 
		  <button onclick="notPutMore()">No, thanks</button>
		</div>


		<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
					
						<div class="emailQuestionnaire" >
							
							<input type="hidden" value="${aptList.size()}" id="totalLength">
							
							<c:forEach items="${aptList}" var="apt" varStatus="loop"> 
								
								<jsp:useBean id="myDate" class="java.util.Date"/> 
								<c:set target="${myDate}" property="time" value="${apt.starttime}"/> 
								
				              <p>${loop.count}. ${docList[loop.count-1].firstname}, who had an OSPIA session with you on ${apt.aptdate}, wrote: </p>
				               
				               <p><b>"${answerList[loop.count-1].content}"</b></p>
				               <p>Please rate the following statement as sincerely as you can: </p>
						       <p><b>The student message expresses gratitude:</b></p>
				               
				               <input type="hidden" value="${apt.aptid}" name="feedback${loop.count}_aptId" id="feedback${loop.count}_aptId">
				               <input type="hidden" value="" name="feedback${loop.count}_answer" id="feedback${loop.count}_answer">
								<button name="feedback${loop.count}s1" id="feedback${loop.count}s1" type="button" class="btn btn-secondary emailLikertButton" onclick="feedbackClick(${loop.count},1)">Strongly Disagree 1</button><br/>
								<button name="feedback${loop.count}s2" id="feedback${loop.count}s2" type="button" class="btn btn-secondary emailLikertButton" onclick="feedbackClick(${loop.count},2)">Moderately Disagree 2</button><br/>
								<button name="feedback${loop.count}s3" id="feedback${loop.count}s3" type="button" class="btn btn-secondary emailLikertButton" onclick="feedbackClick(${loop.count},3)">Slightly Disagree 3</button><br/>
								<button name="feedback${loop.count}s4" id="feedback${loop.count}s4" type="button" class="btn btn-secondary emailLikertButton" onclick="feedbackClick(${loop.count},4)">Neutral 4</button><br/>
								<button name="feedback${loop.count}s5" id="feedback${loop.count}s5" type="button" class="btn btn-secondary emailLikertButton" onclick="feedbackClick(${loop.count},5)">Slightly Agree 5</button><br/>
								<button name="feedback${loop.count}s6" id="feedback${loop.count}s6" type="button" class="btn btn-secondary emailLikertButton" onclick="feedbackClick(${loop.count},6)">Moderately Agree 6</button><br/>
								<button name="feedback${loop.count}s7" id="feedback${loop.count}s7" type="button" class="btn btn-secondary emailLikertButton" onclick="feedbackClick(${loop.count},7)">Strongly Agree 7</button><br/>
				               	<br>
							</c:forEach>
							
							<p>Please rate the following statements as sincerely as you can:</p>
							<input type="hidden" value="${emailId}" name="emailId" id="emailId">

							<b>This week, I experienced a warm feeling for the student/s with whom I did the OSPIA session/s.</b><br>
							
							<input type="hidden" value="" name="q1" id="q1">
							<button name="q1s1" id="q1s1" type="button" class="btn btn-secondary emailLikertButton" onclick="q1Click(1)">Strongly Disagree 1</button><br/>
							<button name="q1s2" id="q1s2" type="button" class="btn btn-secondary emailLikertButton" onclick="q1Click(2)">Moderately Disagree 2</button><br/>
							<button name="q1s3" id="q1s3" type="button" class="btn btn-secondary emailLikertButton" onclick="q1Click(3)">Slightly Disagree 3</button><br/>
							<button name="q1s4" id="q1s4" type="button" class="btn btn-secondary emailLikertButton" onclick="q1Click(4)">Neutral 4</button><br/>
							<button name="q1s5" id="q1s5" type="button" class="btn btn-secondary emailLikertButton" onclick="q1Click(5)">Slightly Agree 5</button><br/>
							<button name="q1s6" id="q1s6" type="button" class="btn btn-secondary emailLikertButton" onclick="q1Click(6)">Moderately Agree 6</button><br/>
							<button name="q1s7" id="q1s7" type="button" class="btn btn-secondary emailLikertButton" onclick="q1Click(7)">Strongly Agree 7</button><br/>
							
							<br/><br/>
							<b>This week, I felt a sense of connection with the OSPIA community.</b><br>
					
							<input type="hidden" value="" name="q2" id="q2">
							<button name="q2s1" id="q2s1" type="button" class="btn btn-secondary emailLikertButton" onclick="q2Click(1)">Strongly Disagree 1</button><br/>
							<button name="q2s2" id="q2s2" type="button" class="btn btn-secondary emailLikertButton" onclick="q2Click(2)">Moderately Disagree 2</button><br/>
							<button name="q2s3" id="q2s3" type="button" class="btn btn-secondary emailLikertButton" onclick="q2Click(3)">Slightly Disagree 3</button><br/>
							<button name="q2s4" id="q2s4" type="button" class="btn btn-secondary emailLikertButton" onclick="q2Click(4)">Neutral 4</button><br/>
							<button name="q2s5" id="q2s5" type="button" class="btn btn-secondary emailLikertButton" onclick="q2Click(5)">Slightly Agree 5</button><br/>
							<button name="q2s6" id="q2s6" type="button" class="btn btn-secondary emailLikertButton" onclick="q2Click(6)">Moderately Agree 6</button><br/>
							<button name="q2s7" id="q2s7" type="button" class="btn btn-secondary emailLikertButton" onclick="q2Click(7)">Strongly Agree 7</button><br/>
							
							<br/><br/>
							<b>I intend to do more OSPIA sessions during the semester.</b><br>
					
							<input type="hidden" value="" name="q3" id="q3">
							<button name="q3s1" id="q3s1" type="button" class="btn btn-secondary emailLikertButton" onclick="q3Click(1)">Strongly Disagree 1</button><br/>
							<button name="q3s2" id="q3s2" type="button" class="btn btn-secondary emailLikertButton" onclick="q3Click(2)">Moderately Disagree 2</button><br/>
							<button name="q3s3" id="q3s3" type="button" class="btn btn-secondary emailLikertButton" onclick="q3Click(3)">Slightly Disagree 3</button><br/>
							<button name="q3s4" id="q3s4" type="button" class="btn btn-secondary emailLikertButton" onclick="q3Click(4)">Neutral 4</button><br/>
							<button name="q3s5" id="q3s5" type="button" class="btn btn-secondary emailLikertButton" onclick="q3Click(5)">Slightly Agree 5</button><br/>
							<button name="q3s6" id="q3s6" type="button" class="btn btn-secondary emailLikertButton" onclick="q3Click(6)">Moderately Agree 6</button><br/>
							<button name="q3s7" id="q3s7" type="button" class="btn btn-secondary emailLikertButton" onclick="q3Click(7)">Strongly Agree 7</button><br/>
							
							<br/><br/>
							<b>Comments (optional)</b><br>
					
							<textarea style="width: 100%;" rows="8" name="q4" id="q4"></textarea> <br/><br/>
							
							<button id="submitFormBtn" class="btn btn-success">SUBMIT</button>
							
						
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="putMoreAppt" style="display:none">
		<input type="hidden" id="deleteAptId" value="${emailId}">
		<table class="table borderless" style="border:0px">
			<tr>
				<td colspan="2"><h4>Thanks for completing the form. Would you like to book more appointment now?</h4></td>
			</tr>
			<tr>
				<td><button style="width:150px" class="usyd-ui-button-primary" onclick="putMore()">Yes, please</button></td>
				<td><button style="width:150px" class="usyd-ui-button-primary" onclick="notPutMore()">No, thanks</button></td>
			</tr>
		</table>
	</div>
	
	<c:import url="/footer.jsp"></c:import>
	</div>
</body>
</html>
