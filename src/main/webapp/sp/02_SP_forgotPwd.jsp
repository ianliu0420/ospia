<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Login</title>
<c:import url="/common.jsp"></c:import>
<style type="text/css">
#loginForm label.error {
	color: red;
	margin-left: 10px;
	width: auto;
	display: inline;
}
</style>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/sp/02_SP_forgotPwd.js"></script>
</head>
<body>
    <c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<div class="usyd-inline-wrap"> 
							<h1>Password resetting</h1>
						</div>
						<br>
						<form class="form-horizontal" id="loginForm">
							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput">Your registered email:</label>
									<div class="controls">
										<input id="email" name="email" type="text" />
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="singlebutton"></label>
									<div class="controls">
										<input id="submit_reset" name="submit_reset" class="usyd-ui-button-primary" type="button" value="reset">
									</div>
								</div>
							</div>
						</form>
					</div>
					
					<div id="waitingPopup" style="display:none">	
                        <h4>Please wait a moment.</h4>
                    </div>
					
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>

</body>

</html>
