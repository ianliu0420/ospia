<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Login</title>
<c:import url="/common.jsp"></c:import>
<style type="text/css">
#loginForm label.error {
	color: red;
	margin-left: 10px;
	width: auto;
	display: inline;
}
</style>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/sp/02_SP_login.js"></script>
</head>
<body>
    <c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<div class="usyd-inline-wrap"> 
							<h1>OSPIA log in</h1>
						</div>
						<form class="form-horizontal" action="<%=request.getContextPath()%>/sp/j_spring_security_check" method="post" id="loginForm">
							<div class="usyd-form-component" <c:if test="${param.error != 'true'}"> style="display:none" </c:if>>
								<div class="control-group" id="errorInfo" style="color:red">
									<label class="control-label" for="textinput"></label>
									<div class="controls">
										<p>invalid email or password</p>
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput">User Email</label>
									<div class="controls">
										<input id="j_username" name="j_username" type="text" />
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="passwordinput">Password</label>
									<div class="controls">
										<input id="j_password" name="j_password" type="password" />
									</div>
								</div>
							</div>
							<!-- -->
							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="passwordinput"></label>
									<div class="controls">
										<a href="sp/forgotPwd">Forgotten password?</a>
									</div>
								</div>
							</div>
							 
							 
							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="singlebutton"></label>
									<div class="controls">
										<input id="submit_login" name="submit_login" class="usyd-ui-button-primary" type="submit" value="Log In">
									</div>
								</div>
							</div>
						</form>
					</div>
					
					<div id="waitingPopup" style="display:none">
                        <h4>OSPIA cannot run on iPads and only supports Firefox and Chrome web browser sorry for any inconvenience. If you do not have Firefox or Chrome installed, please download them on these pages:</h4>
                        <a href="https://www.mozilla.org/en-US/firefox/new/"  style="font-size:20px;">Download Firefox</a>
                        &nbsp;&nbsp;&nbsp;<a href="https://www.google.com.au/intl/en/chrome/browser/desktop/index.html"  style="font-size:20px;">Download Chrome</a>
                    </div>
					
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>

</body>

</html>
