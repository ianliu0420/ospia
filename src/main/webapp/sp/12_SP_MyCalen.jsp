<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Calendar</title>
<c:import url="/common.jsp"></c:import>

<link href='<%=request.getContextPath()%>/css/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<%=request.getContextPath()%>/css/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<%=request.getContextPath()%>/js/fullcalendar/moment.min.js'></script>
<script src='<%=request.getContextPath()%>/js/fullcalendar/fullcalendar.min.js'></script>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<script src='<%=request.getContextPath()%>/js/sp/12_SP_MyCalen.js'></script>
</head>

<body>
	<c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<h4 style="padding-bottom:15px;">Please do not offer appointments 9am to 12 noon (Australian Eastern Standard Time), Monday to Friday as students are in class at these times.</h4>
					
						<h3>
							<span id="dateSpan"> </span>
						</h3>
						<div id="emptyNote" style="display:none">
							<p>No appointment for this day.</p>
						</div>
						<div class="scrollTable" id="scrollTable">
							<table id="detailTable" class="table">
								<thead>
									<tr>
										<th>Request Date</th>
										<th>Request Time</th>
										<th>Student Name</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="tbodyList">
								</tbody>
							</table>
						</div>

						<button class="usyd-ui-button-primary" onclick="showAddEventPopUp()" style="margin-bottom:5px;">New Appointment</button>
						<br /> <b> <span style="color:green;font-size:14px">Available &nbsp;&nbsp;&nbsp;</span> <span
							style="color:red;font-size:14px">Student Request</span>&nbsp;&nbsp;&nbsp; <span style="color:blue;font-size:14px">Confirmed
								Request</span>&nbsp;&nbsp;&nbsp; <span style="color:orange;font-size:14px">Unavailable</span>
						</b>


						<div id='calendar'></div>

						<div id="addEventPopup" style="display:none">
						    <br/>
							<form class="form-horizontal">
								<input type="hidden" id="insertEventId" value="" />
								<div class="form-group">
									<div class="col-md-12">
										<h3>Appointment Details</h3>
										<p id="newAptErrorInfo" style="color:red;display:none; margin-top:10px;margin-bottom:10px;"></p>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label" for="selectbasic">Date:&nbsp;&nbsp;</label>

									<div class="col-md-8" style="text-align:left">
										<select id="insertEventDate" name="insertEventDate" class="form-control" style="width:100px;">
											<option value="0" selected="selected">Day</option>
											<c:forEach var="i" begin="1" end="31">
												<option value="${i}">${i}</option>
											</c:forEach>
										</select> <select id="insertEventMonth" name="insertEventMonth" class="form-control" style="width:100px;">
											<option value="0" selected="selected">Month</option>
											<c:forEach var="i" begin="1" end="12">
												<option value="${i}">${i}</option>
											</c:forEach>
										</select> <select id="insertEventYear" name="insertEventYear" class="form-control" style="width:100px;">
											<option value="0" selected="selected">Year</option>
											<option value="2016">2016</option>
											<option value="2017">2017</option>
											<option value="2018">2018</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label" for="selectbasic">Time:&nbsp;&nbsp;</label>
									<div class="col-md-8" style="text-align:left">
										<select id="insertEventAP" name="selectbasic" class="form-control" style="width:100px;">
											<option value="0" selected="selected">AM</option>
											<option value="1">PM</option>
										</select> <select id="insertEventHour" name="selectbasic" class="form-control" style="width:100px;">
											<option value="-1" selected="selected">Hour</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8</option>
											<option value="9">9</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="0">12</option>
										</select> <select id="insertEventMinute" name="selectbasic" class="form-control" style="width:100px;">
											<option value="-1" selected="selected">Minute</option>
											<option value="0">00</option>
											<option value="15">15</option>
											<option value="30">30</option>
											<option value="45">45</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label" for="selectbasic">Length:&nbsp;&nbsp;</label>
									<div class="col-md-8" style="text-align:left">
										<select id="insertEventLength" name="selectbasic" class="form-control" style="width:100px;">
											<!-- <option value="15" selected="selected">15 mins</option> -->
											<option value="30" selected="selected">30 mins</option>
											<!-- <option value="45">45 mins</option> -->
										</select>
									</div>
								</div>

								<div class="form-group" style="display:none">
									<div class="col-md-11" style="padding-right:5px;">
										<label class="col-md-3 control-label" for="selectbasic">Scenario</label> <select id="scenario" name="scenario"
											class="form-control" style="width:100px;">
										</select>
									</div>
								</div>
								<!-- Button -->

								<div class="form-group">
									<label class="col-md-3 control-label" for="selectbasic">&nbsp;</label>
									<div class="col-md-8" style="text-align:left">
										<input type="button" id="insertEventBtn" name="insertEventBtn" class="usyd-ui-button-primary form-control"
											onclick="insertEvent()" value="Save" style="width:70px;float:left"> <input type="button" id="cancelbutton"
											name="cancelbutton" class="usyd-ui-button-primary form-control" onclick="cancelPopup()" value="Cancel"
											style="width:100px;float:left; margin-left:20px;">
									</div>

								</div>
							</form>
						</div>



						<div id="confirmAptPopup" style="display:none">
							<input type="hidden" id="confirmAptId" value="">
							<div style="color:red; font-size:14px; display:none" id="declineWait1">We are processing, please wait...</div>
							<table class="table borderless" style="border:0">
								<tr>
									<td colspan="2"><h4>Accept this request?</h4></td>
								</tr>
								<tr>
									<td><button style="width:80px" class="usyd-ui-button-primary" onclick="acceptApt(1)">Yes</button></td>
									<td><button style="width:100px" class="usyd-ui-button-primary" onclick="cancelPopup()">Cancel</button></td>
								</tr>
							</table>
						</div>

						<div id="declineAptPopup" style="display:none">
							<div style="color:red; font-size:14px; display:none" id="declineWait0">We are processing, please wait...</div>
							<table class="table borderless" style="border:0">
								<tr>
									<td colspan="2"><h4>Decline this request?</h4></td>
								</tr>
								<tr>
									<td><button style="width:80px" class="usyd-ui-button-primary" onclick="acceptApt(0)">Yes</button></td>
									<td><button style="width:80px" class="usyd-ui-button-primary" onclick="cancelPopup()">Cancel</button></td>
								</tr>
							</table>
						</div>

						<div id="deleteAptPopup" style="display:none">
							<input type="hidden" id="deleteAptId" value="">
							<table class="table borderless" style="border:0px">
								<tr>
									<td colspan="2"><h4>Delete this appointment?</h4></td>
								</tr>
								<tr>
									<td><button style="width:80px" class="usyd-ui-button-primary" onclick="deleteAptConfirm()">Yes</button></td>
									<td><button style="width:80px" class="usyd-ui-button-primary" onclick="cancelPopup()">Cancel</button></td>
								</tr>
							</table>
						</div>

						<div id="cancelAptPopup" style="display:none">
							<input type="hidden" id="cancelAptId" value="">
							<p id="cancelAptErrorInfo" style="color: red; display: none"></p>
							<table class="table borderless" style="border:0">
								<tr>
									<td colspan="2"><h4>What is your reason for canceling?</h4></td>
								</tr>
								<tr>
									<td colspan="2"><textarea id="cancelReason" name="cancelReason" cols="50" rows="8"></textarea></td>
								</tr>
								<tr>
									<td><button style="width:80px" class="usyd-ui-button-primary" onclick="confirmCancelApt()">Submit</button></td>
									<td><button style="" class="usyd-ui-button-primary" onclick="cancelPopup()">I don't want to Cancel</button></td>
								</tr>
							</table>
						</div>

						<div id="waitingPopup" style="display:none">
							<h4>Please wait a moment.</h4>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>

</body>
</html>
