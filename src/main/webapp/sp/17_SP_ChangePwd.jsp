<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Change Password</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/sp/17_SP_ChangePwd.js"></script>
</head>

<body>
	<c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div class="row">
							<div class="usyd-inline-wrap">
								<h1>Change Password</h1>
							</div>
						</div>
						<form class="form-horizontal" action="<%=request.getContextPath()%>/sp/j_spring_security_check" method="post" id="loginForm">
							<input type="hidden" id="patientId" name="patientId" value="<sec:authentication property="principal.patient.patientid"/>">

							<div class="usyd-form-component" <c:if test="${param.error != 'true'}"> style="display:none" </c:if>>
								<div class="control-group" id="errorInfo" style="color:red">
									<label class="control-label" for="textinput"></label>
									<div class="controls">
										<p>invalid username or password</p>
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput">Old Password</label>
									<div class="controls">
										<input id="opwd" name="opwd" type="password" placeholder="Old Password">
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput">New Password</label>
									<div class="controls">
										<input id="npwd" name="npwd" type="password" placeholder="New Password">
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput">Confirm New Password</label>
									<div class="controls">
										<input id="cnpwd" name="cnpwd" type="password" placeholder="Confirm New Password">
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="singlebutton"></label>
									<div class="controls">
										<input id="submit_changpwd" name="submit_changpwd" class="usyd-ui-button-primary" type="button" value="submit">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<c:import url="/footer.jsp"></c:import>

</body>
</html>
