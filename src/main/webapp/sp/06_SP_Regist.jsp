<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1" />
<link rel="icon" href="../../favicon.ico">
<title>SP Registration</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/sp/06_SP_Regist.js"></script>
<style type="text/css">
#signupForm label.error {
	color: red;
	width: auto;
	margin-top: 2px;
}
</style>
</head>

<body>
	<c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<div class="usyd-inline-wrap">
							<h1>Online Simulated Patient Interaction and Assessment</h1>
						</div>
						<span style="font-size:14px;"><b> This is not for student access.</b></span><br>
						<span style="font-size:14px;"> When you choose to be involved in the program, you will work in our online OSPIA platform with junior medical students. The focus of the interactions is to develop students' communication skills. This includes their ability to initiate and maintain respectful interactions with patients and to develop rapport so that patients feel able to divulge all relevant information during a consultation. </span>
					</div>
					<br /> <span style="font-size:14px;">Thank you for your interest! <b>To participate, please register your details below </b> (<span
						class="redlabel">*</span> indicates compulsory field)
					</span> <br />
					<hr class="autohr">
					<br />

					<form class="form-horizontal" id="signupForm" method="post" autocomplete="off">
						<div class="usyd-form-component">
							<div class="control-group" id="registerErroDiv" style="display:none">
								<label class="control-label" for="textinput"></label>
								<div class="controls">
									<p style="color:red" id="registerError"></p>
								</div>
							</div>
						</div>
						
						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Title <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="title" name=title type="text" placeholder="">
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">First Name <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="firstname" name=firstname type="text" placeholder="First Name">
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Last Name <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="lastname" name="lastname" type="text" placeholder="Last Name">
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="radios">Gender <span class="redlabel">*</span></label>
								<div class="controls">
									<label class="radio-inline" for="radios-0"> <input type="radio" name="gender" id="radios-0" value="1"
										checked="checked"> Female
									</label> <label class="radio-inline" for="radios-1"> <input type="radio" name="gender" id="radios-1" value="2">
										Male
									</label>
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="selectbasic">Age Range <span class="redlabel">*</span></label>
								<div class="controls">
									<select id="agerange" name="agerange" class="form-control" style="width:100px;">
										<option value="0" selected="selected">Range</option>
										<option value="20">Under 50</option>
										<option value="50">Above 50</option>
									</select>
								</div>
							</div>
						</div>


						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Mobile phone</label>
								<div class="controls">
									<input id="phone" name="phone" type="text" placeholder="Phone"> 
									You will only receive text message reminders if you enter your number.
									<div>Phone number format: 0402123123</div>
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Email <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="email" name="email" type="text" placeholder="Email">
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Address - No, & Street Name </label>
								<div class="controls">
									<textarea rows="" cols="" id="street" name="street"></textarea>
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Suburb</label>
								<div class="controls">
									<input id="suburb" name="suburb" type="text" placeholder="Suburb">
								</div>
							</div>
						</div>
						
						<div class="usyd-form-component">
                            <div class="control-group">
                                <label class="control-label" for="textinput">State</label>
                                <div class="controls">
                                    <input id="state" name="state" type="text" placeholder="state">
                                </div>
                            </div>
                        </div>
						
						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Post/Zip code</label>
								<div class="controls">
									<input id="postcode" name="postcode" type="text" placeholder="Post/Zip Code"> 
								</div>
							</div>
						</div>
						
						<div class="usyd-form-component">
                            <div class="control-group">
                                <label class="control-label" for="textinput">Country <span class="redlabel">*</span></label>
                                <div class="controls">
                                    <input id="country" name="country" type="text" placeholder="Country"> 
                                </div>
                            </div>
                        </div>
						<hr>
                        <div class="usyd-form-component">
                            <div class="control-group">
                                <label class="control-label" for="textinput">General health description</label>
                                <div class="controls">
                                    <textarea rows="" cols="" id="health" name="health"></textarea>
                                </div>
                            </div>
                        </div>
                         
						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Fluent spoken languages <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="language" name="language" type="text" placeholder="">
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput"> How did you hear about this program? <span class="redlabel">*</span></label>
								<div class="controls">
									<textarea rows="" cols="" id="reason" name="reason"></textarea>
								</div>
							</div>
						</div>

						<hr>
                        
						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Create password <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="password" name="password" type="password" placeholder="Password" autocomplete="off"> (at least 6 characters or numbers - case sensitive)
								</div>
							</div>
						</div>

						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="textinput">Confirm password <span class="redlabel">*</span></label>
								<div class="controls">
									<input id="passwordcfm" name="passwordcfm" type="password" placeholder="Password Confirm" autocomplete="off">
								</div>
							</div>
						</div>

						<!-- Button -->
						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="singlebutton"></label>
								<div class="controls">
									<input type="submit" id="registerBtn" name="registerBtn" class="usyd-ui-button-primary" value="register">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<c:import url="/footer.jsp"></c:import>
</body>
</html>
