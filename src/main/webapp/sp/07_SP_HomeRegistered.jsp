<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Home</title>
<c:import url="/common.jsp"></c:import>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/js/sp/07_SP_HomeRegistered.js"></script>
</head>

<body>
	<c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b" style="margin-bottom:200px;">

						<span class="pagefont_b">Hi <sec:authentication property="principal.patient.firstname" /><br /></span>
						<a href="<%=request.getContextPath()%>/sp/changepwd"><span class="pagefont_b">change password</span></a>
                        <br>
                        <br>

						<h3>Requests from students</h3>
						<div id="requestDivLoading" class="scrollTable" style="display:none">loading... please wait</div>
						<p id="requestNote"></p>
						<div style="height: 200px; overflow-y:auto;">
						<table class="table" id="requestTable" style="display:none">
							<thead>
								<tr>
									<th>Appointment Date</th>
									<th>Appointment Time</th>
									<th>Student Name</th>
									<th></th>
								</tr>
							</thead>
							<tbody id="requestList">
							</tbody>
						</table>
						</div>
                        
                        <br>
						<h3>Your appointments in the next two weeks</h3>
						<div id="nextaptDivLoading"  class="scrollTable" style="display:none">loading... please wait</div>
						<p id="aptNote"></p>
						<div style="height: 200px; overflow-y:auto;">
						<table class="table" id="aptTable" style="display:none">
							<thead>
								<tr>
									<th>Appointment Date</th>
									<th>Appointment Time</th>
									<th>Student Name</th>
									<th></th>
								</tr>
							</thead>
							<tbody id="aptList">
							</tbody>
						</table>
						</div>

                        <br>
						<h3>Your unfinished consultations</h3>
						<div id="unfinishedDivLoading" class="scrollTable" style="display:none">loading... please wait</div>
						<p id="unfinishedAptNote"></p>
						<div style="height: 200px; overflow-y:auto;">
						<table class="table" id="unfinishedAptTable" style="display:none">
							<thead>
								<tr>
									<th>Appointment Date</th>
									<th>Appointment Time</th>
									<th>Student Name</th>
									<th>Operations</th>
								</tr>
							</thead>
							<tbody id="unfinishedAptList">
							</tbody>
						</table>
						</div>
						
						
						<div class="jumbotron" style="padding-top:100px;padding-bottom:2px;">
						<hr/>
                            <div>
                                 <div style="text-align:left; margin-top:30px;">
                                 <h5>OSPIA Project Acknowledgements</h5>
                                 <br>
                                 Through the hard work, collaboration and skills of the following people, the OSPIA Project has been made possible. I thank them sincerely for their contributions.
                                 <br/>
                                 <ul style="margin-left:20px;">
                                     <li>Dr Renee Lim, Educational Designer and Director of Program Development, Pam McLean Centre and Positive Computing Lab, Sydney University</li>
                                    <li>Prof Rafael Calvo, Positive Computing Lab at the University of Sydney </li>
                                    <li>Mr Chunfeng Liu, PhD student, Positive Computing Lab at the University of Sydney</li>
                                    <li>Ms Kiran Thwaites, Clinical Skills Administrator, UNSW</li>
                                 </ul>
                                 
                                 </div>
                                 <div style="text-align:right; font-style:italic;">
                                 Dr Silas Taylor, Convenor of Clinical Skills, UNSW Medicine
                                 </div>
                                 <br/>
                                 <div>
                                 <img style="width:350px;height:100px;" src="<%=request.getContextPath()%>/img/gvn_logo.png"><br>
                                 This project received funding from the Australian Government.
                                 </div>
                            </div>
                        </div>
						

						<div id="waitingPopup" style="display:none">
							<h4>Please wait a moment.</h4>
						</div>
						
						
						<div id="codeConductPopUp" style="display:none;height: 560px; overflow-y : scroll;">
                            <table class="table borderless " style="border:0;">
                                <tr>
                                    <td colspan="2"><b>Code of conduct</b>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;"><b>1. Behaviour and Appearance</b></td>
                                </tr>

                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    We expect interactions between students and simulated patients during OSPIA sessions to be polite, respectful, appropriate and professional in nature. Your use of language should align with these principles. For example, swearing is considered inappropriate. Both parties should be clearly identified at the start of the interaction. The appearance and dress of participants in online interactions should also be appropriate.
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" style="text-align: left;"><b>2. Confidentiality</b></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">Students are bound to keep all information gathered in interactions with simulated patients as confidential. However, you are not expected to reveal any personal information about yourself, unless you choose to do so. Usually you will only divulge information from the patient scenario.</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;"><b>3. Failure to abide by the code of conduct</b></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">If behavior is found to be inappropriate during an interaction, the interview can be terminated by either party. As all video interactions are recorded, the video can be reviewed and appropriate action taken if required.</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="checkbox" value="1" id="codeCondect">&nbsp; I agree</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input disabled type="button" id="submitCodeConduct" class="usyd-ui-button-primary" value="submit"></td>
                                </tr>
                            </table>
                        </div>

                        <div id="agreementPopUp" style="display:none;height: 560px; overflow-y: scroll;">
                            <input type="hidden" id="patientid" value="<sec:authentication property="principal.patient.patientid"/>" /> 
                            <input type="hidden" id="agreement" value="<sec:authentication property="principal.patient.agreement"/>" /> 
                            <input type="hidden" id="homeloaded" value="${homeloaded}" />

                            <table class="table borderless " style="border:0;">
                                <tr>
                                    <td id="top" colspan="2" style="font-size:18px;"><b>PARTICIPANT INFORMATION STATEMENT AND CONSENT FORM</b>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;"><b>The study is being carried out by the following researchers</b> <br>    
                                    <b>Chief Investigator:</b> <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Dr Silas Taylor (UNSW) <br/>
                                    <b>Co-Investigator/s:</b> <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Prof Rafael Calvo (University of Sydney)<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Dr Renee Lim (University of Sydney)<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp; A/Prof Boaz Shulruf (UNSW)<br/>
                                    <b>Student Investigator/s:</b> <br>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Chunfeng Liu (PhD student, University of Sydney)<br>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Khushnood Z. Naqshbandi (MPhil student, University of Sydney)
                                    <br/>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;"><b>What is the research study about?</b> <br> 
                                    You are invited to take part in this research study. You have been invited because you are a volunteer Simulated Patient (SP) for UNSW Medicine students in the Clinical Skills program. <br><br>
                                    To participate in this project you need to meet the following inclusion criteria:<br>
                                      &nbsp;&nbsp;&nbsp;&bull; You are completing an OSPIA, that is an online interaction with a medical student.<br>
                                      &nbsp;&nbsp;&nbsp;&bull; You have completed the online training modules for interacting in the OSPIA session.<br><br>
                                    The purpose of this research is to provide evidence that medical students will improve their communication skills by using the OSPIA system. The research will also assess the needs and motivations of the volunteers who use OSPIA and consequently, implement and evaluate the changes based on those findings. The specific aims are listed below:
									<br>&nbsp;&nbsp;1. Help medical students to improve their overall communication skill learning by providing focused and specific feedback on nonverbal skills in online sessions.
									<br>&nbsp;&nbsp;2. Improve the algorithms that generate the automated feedback. This study will provide further data that will continue to improve the accuracy of computing algorithms that analyse the video and detect nonverbal features of the interactions.
									<br>&nbsp;&nbsp;3. Explore students' engagement with feedback on non-verbal communication behaviours.
									<br>&nbsp;&nbsp;4. Investigate the importance of students' autonomy support to simulated patients in the interactions. Autonomy support describes a person in an authority role (e.g., a health care provider) taking into consideration the other's (e.g., the patient's) perspective, acknowledging the other's feelings and perceptions, providing the other with information and choice, and minimizing the use of pressure and control. Recent evidence shows that providers' behaving in this manner have positive effects on patients' health-promoting behaviors and health status e.g. greater attendance, compliance, adherence and successful behaviour change. In addition, we will measure the correlation between the level of autonomy support and nonverbal behaviour.
									<br>&nbsp;&nbsp;5. Assess the motivations, psychological needs and usability expectations and experiences of the volunteers by conducting a preliminary survey followed by focus groups and online interviews. Respondents who agree to participate in the focus groups or online interviews in the survey will be invited at a convenient time and place.
									<br>&nbsp;&nbsp;6. Improve the online OSPIA experience of the SPs based on the requirements generated from the co-research activities with SPs and evaluate the effect of those improvements on the contribution and satisfaction of the SPs. The current improvement will focus on the quality of acknowledgment emails received by SPs.
                                    <br><br>
                                    <b>Importance of the study </b>-Clinical interviews play an important role in the therapeutic process. Medical education thus includes the teaching of communication skills. However, much communication between individuals occurs nonverbally, and this can be difficult to teach students about during live interactions. Providing a sustainable system that aims to improve medical students' nonverbal communication skills, including provision of good quality automated feedback, is a significant intervention.
                                    <br><br>In order to maintain the sustainability of OSPIA, looking at the enjoyment and long-term engagement of the SP's is important. The availability of the SP's ensures the ongoing OSPIA sessions, which makes it important to assess and evaluate their needs and modify them accordingly. This study will take into account the SP needs and experiences to define changes in the overall OSPIA interaction with the SP. The current study will look at the effect of different kinds of acknowledgement emails on SP contribution and satisfaction.
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>Do I have to take part in this research study?</b> <br/>
                                    Participation in this research study is voluntary. If you don't wish to take part, you don't have to. Your decision will not affect your relationship with The University of New South Wales or the University of Sydney. However, if you do not agree for the first part of the study (you can find that in part 1 of the "Study tasks and risks" section), you will not be able to take part in the trial of the online system. For the second part, (you can find that in part 2 of the "Study tasks and risks" section) if you disagree, you will still be able to participate in the online system.
                                    <br><br>
                                    This Participant Information Statement and Consent Form tells you about the research study. It explains the research tasks involved. Knowing what is involved will help you decide if you want to take part in the research.
                                    <br><br>
                                    Please read this information carefully. Ask questions about anything that you don't understand or want to know more about.  Before deciding whether or not to take part, you might want to talk about it with a relative or friend.
                                    <br><br>
                                    If you decide you want to take part in the research study, you will be asked to:
                                    <br>
                                    &nbsp;&nbsp;&nbsp;&bull; Tick 'Agree' on the online consent form;
                                    <br>
                                    &nbsp;&nbsp;&nbsp;&bull; Keep a copy of this Participant Information Statement (available on the OSPIA website)
                                    <br>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What does participation in this research require, and are there any risks involved?</b> <br/>
                                    If you agree to take part in the first part of the research study, you will participate in an OSPIA session, part of the normal requirements for the UNSW Medicine Clinical Skills curriculum.
                                    <br><br>
                                    If you agree to take part in the second part of the study, you will be directed to an online questionnaire, which takes an average of about 5 minutes to complete. Within the survey, you will be asked about your interest in further participation in a focus group or online interview. Based on your answers, you will be contacted about further participation. The respondents will be given a time and venue for the focus group (held within the UNSW premises), or a mutually convenient time for the online interview.
                                    <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>Study tasks & risks:</b> <br/>
                                    You will perform an OSPIA interaction, as part of the UNSW Medicine Clinical Skills curriculum. As part of the study, with your permission, we would like to gain access to the automated computer-generated analysis of non-verbal communication in the recordings of the OSPIA interactions. This is to collect information that tells us about the importance of non-verbal communication skills in student-SP interactions.
                                     <br><br>
									 In addition, the research team will access all user created content, namely: SOCA assessments of students; comments relating to student performance entered during an interaction; all questionnaire data; identifiable video file recordings of interactions. This is in order to assess the students' nonverbal behaviour, compare it to the algorithmic analysis and assessment of the behaviour, and assess the impact of the OSPIA on non-verbal communication behaviour in students in subsequent interactions. The collected information may be reviewed and annotated by authorised experts to assess overall communication skills, and nonverbal communication skills as a subset, in student-SP interactions.
									 <br><br>
									 You also have the option to provide consent to your video recordings being shared with other researchers outside of this project for academic purposes only. Researchers use videos to train machine learning algorithms. We would like the videos in which you appear to be a part of a collection they can use for this purpose and this purpose only. Researchers using the videos in which you appear will sign a formal agreement form preventing them from distributing the videos. The researchers will not have access to any data that identifies you (except the images of your face that cannot be anonymised).
									 <br><br>
									An OSPIA interaction will take approximately 30-45 minutes. You will need to complete two parts in each interaction: a tele-consultation with a student and some additional questionnaires (include a communication skills assessment questionnaire and an autonomy support measurement questionnaire).  
									<br><br>
									You will receive an acknowledgement email and filling the acknowledgement email survey will take 2-5 minutes at most.
									<br><br>
									Aside from giving up your time, we do not expect that there will be any risks or costs associated with taking part in this study.
                                    <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>Will I be paid to participate in this project?</b> <br/>
                                    There are no costs associated with participating in the first part of this research study, nor will you be paid.
                                     <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What are the possible benefits to participation?</b> <br/>
                                    We hope to use information we get from this research study to benefit others who will participate in communication skills teaching in future. This study will also allow you to provide researchers with your opinions about the OSPIA system. We hope that the findings of this study help us to improve OSPIA for you and other volunteer simulated patients.
                                     <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What will happen to information about me?</b> <br/>
                                    By signing the online consent form, you consent to the research team collecting and using information about from your survey responses and (if applicable) the audio recordings of the focus group and online interviews for the research study. We will keep your data for up to 7 years. We will store information from the study on the UNSW server and Sydney University password protected computers and on UNSW password protected computers thereafter.
                                    <br><br>
									It is anticipated that the results of this research study will be published and/or presented in a variety of forums. In any publication and/or presentation, information will be published in a way such that you will not be individually identifiable.
									<br><br>
									After each OSPIA, we will store digital video recordings for up to one year. We also store digital video recordings on the UNSW server. Your confidentiality will be ensured by this being a password protected, enterprise quality system. The server can only be accessed by the researchers in the team.
									 <br>
                                    </td>
                                </tr>
                                
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>How and when will I find out what the results of the research study are?</b> <br/>
                                    You have a right to receive feedback about the overall results of this study. You can tell us that you wish to receive feedback by emailing <a>csadmin@unsw.edu.au</a>. This feedback will be in the form of a link to a webpage that will have a summary of results, or a published conference or journal article. You can receive this feedback after the study is finished.
                                    <br>
                                    </td>
                                </tr>
                                
                                 <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What if I want to withdraw from the research study?</b> <br/>
									If you do consent to participate, you may withdraw at any time. If you withdraw, you will be asked to complete and sign the 'Withdrawal of Consent Form' which is provided at the end of this document, and/or as another tick box option on the OSPIA website. Alternatively, you can ring the research team and tell them you no longer want to participate.
									
									<br><br>
									You are free to stop the OSPIA interaction at any time if it does not comply with the code of conduct - please use the on-screen STOP button. For teaching purposes, these recordings will be reviewed by Dr Silas Taylor. If deemed to not be an appropriate student-SP interview, the research team will delete all files and analysis of that recording, unless follow up disciplinary action should be deemed required. 
									<br>
									Submitting your completed questionnaires and survey is an indication of your consent to participate in the study. You can withdraw your responses if you change your mind about having them included in the study, up to the point that we have analysed the results.
									<br>
									If you decide to withdraw from the study, we will not collect any more information from you. Any information that we have already collected, however, will be kept in our study records and may be included in the study results.
									<br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What should I do if I have further questions about my involvement in the research study?</b> <br/>
                                    The person you may need to contact will depend on the nature of your query. If you want any further information concerning this project or if you have any problems that may be related to your involvement in the project, you can contact the following member/s of the research team:
                                    <br><br>
                                    <b>Research Team Contact</b><br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Name: </b> Dr Silas Taylor <br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Position: </b> Chief Investigator<br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Telephone: </b> 9385 2607<br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Email: </b> <a>silas.taylor@unsw.edu.au</a><br>
                                    
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What if I have a complaint or any concerns about the research study?</b> <br/>
                                    If you have any complaints about any aspect of the project, the way it is being conducted, then you may contact:
                                    <br><br>
                                    <b>Complaints Contact</b><br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Position: </b> Human Research Ethics Coordinator<br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Telephone: </b> + 61 2 9385 6222<br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Email: </b> <a>humanethics@unsw.edu.au</a><br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>HC Reference Number: </b> HC16048 <br>
                                    </td>
                                </tr>
                                
                                 <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b style="color:red; font-size:18px;">I would like to participate and I have</b> <br/><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Read the Participant Information Sheet or someone has read it to me in a language that I understand;<br><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Understood the purposes, study tasks and risks of the research described in the project;<br><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Had an opportunity to email questions to the Chief Investigator and I am satisfied with the answers I have received;<br><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Freely agree to participate in this research study as described and understand that I am free to withdraw at any time during the project and withdrawal will not affect my relationship with any of the named organisations and/or research team members;<br><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Understand that I am able to down load a copy of this document to keep from the OSPIA website (student login)<br><br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2">Do you agree above items?</td>
                                </tr>
                                <tr>
                                    <td style="width:50%"><input type="radio" value="1" name="agreement" checked="checked">&nbsp;I agree</td>
                                    <td style="width:50%"><input type="radio" value="0" name="agreement">&nbsp;I don't agree</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="button" id="submitAgreement" class="usyd-ui-button-primary" value="submit"></td>
                                </tr>
                            </table>
                        </div>
						

					</div>
				</div>
			</div>
		</div>
	</div>

	<c:import url="/footer.jsp"></c:import>


</body>
</html>
