<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Training Detail</title>
<c:import url="/common.jsp"></c:import>
</head>

<body class="usyd-js-load">
	<div class="usyd-page usyd-no-feedback usyd-no-yammer ">
		<c:import url="/spheader.jsp"></c:import>
		<a href="<%=request.getContextPath()%>/sp/train_sp"
			style="font-size:14px">More trainings</a>

		<div class="row">
			<div class="col-lg-7 col-xs-7" style="text-align:center">
				<video id="myvideo" width="500" height="400" controls
					onclick="clickVideo('myvideo')">
					<!-- <source src="video/${scenario.getVideoLoc()}" type="video/mp4">  -->
					<source src="video/video_1436761313842/video_1436761313842.mp4"
						type="video/mp4">
				</video>
			</div>

			<div class="col-lg-5 col-xs-5">
				<h3>Scenario ${scenario.getCode()}</h3>

				<table class="table" style="margin-top:15px;">
					<tr>
						<td width="40%">Gender: ${scenario.getGender()}</td>
						<td width="30%">Age: ${scenario.getAge()}</td>
						<td width="30%">Physicality: ${scenario.getPysicality()}</td>
					</tr>
				</table>

				<p>${scenario.getDetail() }</p>
				<c:if test="${training.getTrained() == 1}">
				</c:if>
				<c:if test="${scenario.getTrained() == 0}">
					<p style="color:red;font-weight:bold">Notes: To finish this
						training, you have to watch the video and answer a questionnaire.</p>
					<p>
						<a class="usyd-ui-button-primary" style="curcor:pointer"
							href="<%=request.getContextPath()%>/sp/scequestions/${scenario.id }"
							role="button">Answer questions &raquo;</a>
					</p>
				</c:if>
			</div>
		</div>



		<c:import url="/footer.jsp"></c:import>

	</div>

</body>
</html>
