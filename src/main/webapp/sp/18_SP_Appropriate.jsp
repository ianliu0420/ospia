<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Appropriate</title>
<c:import url="/common.jsp"></c:import>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/js/sp/18_SP_Appropriate.js"></script>
</head>

<body>
	<c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
					
					   <input type="hidden" value="${sessionId}" id="sessionId">
					
                        <br/><br/>
						<form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="width:400px" for="radios"> Was the interview conducted appropriately <br/> and according to the Code of Conduct? &nbsp;&nbsp;&nbsp;</label>
                                <div class="col-md-7" style="margin-top:10px;margin-left:10px;">
                                    <label class="radio-inline" for="radios-0" style="text-align:left;"> <input type="radio" name="appropriate"
                                        value="1" checked="checked"> Yes
                                    </label> <label class="radio-inline" for="radios-1" style="text-align:left;"> <input type="radio" name="appropriate"
                                        value="0"> No
                                    </label>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" style="width:380px" for="singlebutton"></label>
                                <div class="col-md-2" style="text-align:left;">
                                    <input id="saveForm" name="singlebutton" type="button" class="usyd-ui-button-primary" value="Next">
                                </div>
                            </div>
                        </form>
					</div>
					
					<div id="confirmAppPopup" style="display:none">
                            <table class="table borderless" style="border:0">
                                <tr>
                                    <td colspan="2"><h4>Are you sure this was an inappropriate interaction?</h4></td>
                                </tr>
                                <tr>
                                    <td><button style="width:150px" class="usyd-ui-button-primary" onclick="app_yes()">Yes, I am sure.</button></td>
                                    <td><button style="width:80px" class="usyd-ui-button-primary" onclick="app_no()">No</button></td>
                                </tr>
                            </table>
                        </div>
					
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
