<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
</head>

<body>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div class="row">
							<div class="jumbotron">
								<video id="myvideo" width="600" height="400" controls> <source
									src="<%=request.getContextPath()%>/video/introduction.mp4"
									type="video/mp4"></video>
							</div>

							<div class="jumbotron"
								style="padding-top:2px;padding-bottom:2px;">
								<h2>OSPIA For Simulated Patients</h2>

								<p class="lead" style=" text-align:justify">The OSPIA means
									you can help medical students develop effective communication
									skills when and where you want. Use the booking system to
									connect with students and receive text or email confirmation
									for your simulated consultation. With the OSPIA platform you
									can comment on student's skills during the simulated
									consultation with a simple 'thumbs up' or 'thumbs down' or you
									can add a comment to let students know when they're doing well
									and where they can improve.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
